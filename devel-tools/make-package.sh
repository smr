#!/bin/bash
#
#	Create a SMR packages from git Repository.
#
#	The basic procedure is:
#		- work on a specific branch of tag in the git repo
#		- search for debug code and warn
#		- generate GNU ChangeLog files
#		- make documentation
#		- create tarball(s)

#	Note: this script is 'raw' code and should only be used by developers.
#	

# load configuration
. config

# check and assign params
if [ "$1" = '--help' ] || [ "$1" = '-h' ]; then
	echo -e "usage: $0 [ TAG ] [ PREV_TAG  ]\n\n"
	echo -e "   TAG        - package code from this tag\n"
	echo -e "   PREV_TAG   - use this tag as starting point for ChangeLog files\n"
	exit 1
fi

if [ -n "$1" ]; then
	tag=$1
fi

if [ -n "$2" ]; then
	prev_tag=$2
fi

###############################################################################
#
#	Functions
#
abort() {
  echo -e "\n--> removing temp directory"
  rm -rf $TMP
  echo "--> exit"
  exit
}

# usage: $1 = tag or branch name (or whatever else got show understands)
get_git_commit() {
  REPLY=`$git show $1 | head -1 |cut -d" " -f 2`
  if [ -n "$REPLY" ]; then
    return 0
  else
	return 1
  fi
}

# usage: $1 = Ruby filename
#        $2 = constant name
get_constant() {
  REPLY=`grep -e ".*${2}.*=.*" ${1} |cut -d "'" -f 2`
  
  if [ -n "$REPLY" ]; then
        return 0
  else
        return 1
  fi
}


###############################################################################
#
#	Script Code
#

# initialize some paths
oldpwd=`pwd`
git=`which git`
git2cl=git2cl
rake=`which rake`

cd ${SMR_REPO}

# checkout tag (if given) and determine current commit
if [ -n "${tag}" ]; then
	$git checkout ${tag}
else
  tag=''
  echo "--> no tag given - using whatever commit we are in"
fi 

# make sure our store is there
package_store=${oldpwd}/${SMR_PACKAGES}
if [ ! -d $package_store ]; then
  echo "--> create package store ${package_store}"
  mkdir --parents $package_store
fi

TMP=`mktemp -d /tmp/devel-tools.XXXXXX`
echo "--> temp dir is ${TMP}"

# determine the commit we are in
get_git_commit 
commit=$REPLY
echo "--> discovered commit $commit"

echo "--> exporting code to temp dir"
$git archive $commit | tar --totals -x -C ${TMP}

###############################################################################
#
# inspect the exported code tree
# 
echo -e "\n==> INSPECTING SOURCE TREE in ${TMP}/"
cd ${TMP}

if [ ! -d "${SMR_GUI}" ] ; then
  echo "--> exported dir structure is not as expected. Did the repository structure change?"
  abort
fi


if [ -d ${SMR_DAEMON} ]; then
  echo "--> found Daemon directory. Will be packaged separately."
  _build_daemon=1
fi

if [ -d ${SMR_MANUAL} ]; then
  echo "--> found Manual directory. Will be packaged separately."
  _build_manual=1
else
  echo " -> Manual directory not present, create it and attempt to build anyway"
  mkdir manual/
  _build_manual=1
fi

if [ -d ${SMR_PLATFORM} ]; then
  echo "--> found platform directory. Will be added to the raw source package."
  _build_platform=1
fi


# get version from sourcecode
if [ -e ${SMR_GUI}/config/application.rb ]; then
  smr_config=${SMR_GUI}/config/application.rb
else
  echo "!!> could not find config file with VERSION constant"
  echo "!!> Note: versions until 0.5 did not have a version tag."
  echo "!!>       Therefore this script does not work for them."
  abort
fi

get_constant ${smr_config} VERSION
version="${REPLY}"

if [ -n "${version}" ]; then
  echo "--> discovered version=${version} (from ${smr_config})"
else
  echo "!!> could not extract release version from ${smr_config}"
  abort
fi

# rename config file templates
#if [ $_build_daemon ]; then
#	if [ -e ${SMR_DAEMON}/smrd.conf.template ]; then
#		echo "--> renaming Daemon config file template..."
#		mv ${SMR_DAEMON}/smrd.conf.template ${SMR_DAEMON}/smrd.conf
#
#		echo "--> updating release version in daemon code..."
#		sed -i "s/__VERSION__/${version}/" ${SMR_DAEMON}/lib/SMR/Config.pm
#	fi
#fi

# walk back into the repository
cd $GITREPO

# count FIXMEs
echo "--> count things to be fixed: "
for i in ${TMP}/${SMR_GUI} ${TMP}/${SMR_DAEMON}; do
  echo " -> looking through $i"
  grep --count --recursive -e 'FIXME' $i/*         \
    | awk -F':' '{s=s+$2; if($2>0){l++}} END{print s, "FIXMEs in", l, "files"}'
done

###############################################################################
#
# make ChangeLog file(s)
#
echo -e "\n==> GENERATE CHANGELOGS"
if [ -n "$tag" ] && [ -n "${commit}" ]; then
  echo "--> this release is:     ${tag} -> commit ${commit}"
else
  echo "--> this release is:     *** untagged *** -> commit ${commit}"
fi

if [ -z "${prev_tag}" ]; then
  # figure what the previous tag *could* be
  # - pretty trivial, but works most of the time.
  # - if the result is not sufficient, specify the previous tag as second
  #   parameter
  base=`echo $tag | sed 's/_[0-9]*$//'`
  patchlevel=`echo $tag | sed 's/RELEASE_[0-9]*_[0-9]*_//'`

  patchlevel=$((patchlevel-1))

  if [ $patchlevel -gt -1 ]; then
  	prev_tag="${base}_${patchlevel}"
  fi
fi

if [ -n "${prev_tag}" ]; then
  get_git_commit ${prev_tag}
  prev_commit=$REPLY
  echo "--> previous release is: ${prev_tag} -> commit ${prev_commit}"
else
  # fall back to when we went public
  prev_commit=43496b57d517aca217d452317a34ab2401d9c330
  echo "--> previous release is: *** unknown ***  -> commit ${prev_commit} (assumed)"
fi

echo "--> writing $SMR_GUI/CHANGELOG"
cd $SMR_REPO
$oldpwd/$git2cl $prev_commit $commit $SMR_GUI > $TMP/$SMR_GUI/CHANGELOG

if [ -n "$_build_daemon" ]; then
	echo "--> writing $SMR_DAEMON/CHANGELOG"
	$oldpwd/$git2cl $prev_commit $commit $SMR_DAEMON > $TMP/$SMR_DAEMON/CHANGELOG
fi

###############################################################################
#
# compile user & developer manual
#
if [ -n "$_build_manual" ] && [ -z "$rake" ]; then
  echo "!!> rake command not found. Check that it is installed."
  abort
fi

if [ -n "$_build_manual" ]; then
    echo -e "\n==> COMPILE MANUAL"
    cd "$TMP/$SMR_GUI"
    $rake doc:app &
    wait
    mv ./doc/* "$TMP/$SMR_MANUAL/"
    rmdir ./doc
fi

###############################################################################
#
# build packages
# - one for the gui (rails app)
# - one for the daemon 
# - one for the manual
echo -e "\n==> BUILD PACKAGES"

cd $TMP
if [ -n "${tag}" ]; then
  package_src="${SMR_PACKAGE_SOURCE}-$version.tar.gz"
  package_gui="${SMR_PACKAGE_GUI}-$version.tar.gz"
  
  if [ -n "$_build_daemon" ]; then
	package_daemon="${SMR_PACKAGE_DAEMON}-$version.tar.gz"
  fi
  if [ -n "$_build_manual" ]; then
	package_manual="${SMR_PACKAGE_MANUAL}-$version.tar.gz"
  fi
else
  package_src="${SMR_PACKAGE_SOURCE}-${commit}.tar.gz"
  package_gui="${SMR_PACKAGE_GUI}-${commit}.tar.gz"
  
  if [ -n "$_build_daemon" ]; then
	package_daemon="${SMR_PACKAGE_DAEMON}-${commit}.tar.gz"
  fi
  if [ -n "$_build_manual" ]; then
	package_manual="${SMR_PACKAGE_MANUAL}-${commit}.tar.gz"
  fi
fi

echo "--> GUI package is:     ${package_gui}"
tar --totals -zcf $package_store/$package_gui ${SMR_GUI}

if [ -n "$_build_daemon" ]; then
  echo "--> Daemon package is:  ${package_daemon}"
  tar --totals -zcf $package_store/$package_daemon ${SMR_DAEMON}
fi

if [ -n "$_build_manual" ]; then
  echo "--> Manual package is:  ${package_manual}"
  tar --totals -zcf $package_store/$package_manual ${SMR_MANUAL}
fi

###############################################################################
#
# finally build the raw source package
# - this one is for further packaging (RPM, Deb, ...)
# - add platform specific files here
# - tradition says that raw sources should be stamped with versions to avoid
#   conflicts on development systems and during further packaging...  we do so
echo "--> SOURCE package is:     ${package_src}"
echo "--> stamping versions..."
mv ${SMR_GUI} ${SMR_GUI}-${version}

if [ $_build_daemon ]; then
    mv ${SMR_DAEMON} ${SMR_DAEMON}-${version}
fi

if [ $_build_manual ]; then
    mv ${SMR_MANUAL} ${SMR_MANUAL}-${version}
fi

if [ $_build_platform ]; then
    mv ${SMR_PLATFORM} ${SMR_PLATFORM}-${version}
fi

# a little reminder...
{
    echo
    echo "                          SMR Raw Source Package"
    echo
    echo "This tarball contains raw sourcecode prepared for further packaging. It should"
    echo "ONLY be used as basis for creating RPMs, Debs, ... or similar packages."
    echo
    echo "Please do NOT use it for PRODUCTION installations. Take one (or all) of the files"
    echo "below for manual installation:"
    echo
    echo "        - ${package_gui}"
    echo "        - ${package_daemon}"
    echo "        - ${package_manual}"
    echo
    echo "or just use the final RPM, Deb, ... packages."
    echo
} > IMPORTANT

tar --totals --exclude=devel-tools -zcf $package_store/$package_src *

# we are done
abort
