#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# provides fundamental means to use SMR
class ApplicationController < ActionController::Base
    before_filter :force_login
    helper_method :current_user
    helper_method :smr_menu_addsubitem

    ##
    # extend parent constructor with @smr_menu_items, see
    # smr_menu_addsubitem()
    #
    # = NOTE
    # I don not understand where these symbols come from? However, 
    # when smr_menu() builds the menu it happens that +:root+
    # refers to +root_path+ and +:cashflow+ refers to +cashflow_path+
    # etc...
    # If one specifies a symbol for the actual method name, like
    # +:root_path+ that will become translated into +:root_path_path+
    # and trigger an error 'unknown method'.
    def initialize
        super
        @smr_menu_items = {
            'assets'        => :root,
            'cashflow'      => :cashflow_index,
            'workdesk'      => :workdesk_index,
            'watchlist'     => :watchlist_index,
            'report'        => :report_index,
            'quote records' => :quoterecords,
            'figures'       => :figures,
            'blog'          => :blog_index,
            'documents'     => :documents,
            'objects'       => :objects,
        }
    end

 private

    ##
    # redirect to login unless we have a authenticated current_user
    def force_login 
      unless current_user or params[:controller].eql?'sessions'
        redirect_to new_session_path
      end
    end

    ##
    # find current_user by :auth_token cookie
    def current_user
      @current_user ||= User.find_by_password_auth_token!(cookies[:auth_token]) if cookies[:auth_token]
    end

 public 

    ##
    # set session parameters that are of global use
    # - these can be modified through POST from anywhere
    # 
    # you may POST these params:
    #
    # - smr_browse_date = "YYYY-MM-DD"
    # - smr_step_date   = "day_back" or "day_forward"
    #
    def set_session_params

        if params[:smr_browse_date] then
            session[:smr_browse_date] = Time.parse(params[:smr_browse_date]).end_of_day.to_i
        end

        if params[:smr_step_date] then
            case params[:smr_step_date]
                when 'day_back'    then session[:smr_browse_date] -= 1.days
                when 'day_forward' then session[:smr_browse_date] += 1.days
            end
        end

        redirect_to :back
    end

    ##
    # return date the user is currently browsing, as Time
    def smr_browse_date
        session[:smr_browse_date] = Time.now.end_of_day.to_i if session[:smr_browse_date].nil?
        Time.at(session[:smr_browse_date])
    end

    ##
    # extends @smr_menu_items by adding sub elements to a top level
    # entry. Note that you can only extend exising entries but not
    # add your own top level entry.
    def smr_menu_addsubitem(name, subitems)
        raise 'subitems must be a Hash' if not subitems.is_a?(Hash)
        raise 'name must exist in @smr_menu_items' if not @smr_menu_items[name]
      
        toplink=@smr_menu_items[name] 
        @smr_menu_items[name] = subitems
        @smr_menu_items[name]['_toplink'] = toplink
    end

    ##
    # Collection of Securitiy names and ids for display or select forms.
    # Note: the Smr::ID_CASH_SECURITY is dropped from here.
    def smr_securities_list
        securities = Security.is_actively_traded(smr_browse_date).to_a
        securities.sort!
    end

    ##
    # Collection of Organization names and ids for display or select forms.
    #
    # Note: the Smr::ID_UNIVERSE_ORGANIZATION is dropped by default as its as
    # it counts as 'not yet organized'. Use the :drop_universe option to alter
    # that.
    def smr_organizations_list(options={:drop_universe=>true})
        collection = if options[:drop_universe] then
            Organization.where.not(:id=>Smr::ID_UNIVERSE_ORGANIZATION)
        else Organization end
        collection.order(:name=>:desc).to_a
    end

    ##
    # Collection of Portfolio objects of current user to display or select
    # from.
    #
    # By default, only those having open positions in it will be shown. Use the
    # options to change that behaviour.
    def smr_portfolios_list(options={:show_all=>false})
        if options[:show_all] then
            Portfolio.where(:id_user=>current_user.id).order(:order).to_a
        else
            Portfolio.with_open_positions(current_user.id).order(:order).to_a
        end
    end

    ##
    # Collection of Security.id numbers held in open positions by the current
    # user.
    #
    # This is cached in the session. Either some code fills the cache by giving
    # :fill_initially or this method takes care of filling it (expensive).
    #
    # :fill_initially should be a collection of Smr::AssetsPosition.
    def smr_securities_in_open_positions_list(fill_initially=false)
        if fill_initially then
           session[:smr_securities_in_open_positions_cache] = fill_initially.collect{ |p| p.security.id }.uniq
        end        

        if not session[:smr_securities_in_open_positions_cache].is_a?(Array) then
            assets = Smr::Asset.new(current_user.id, smr_browse_date)
            session[:smr_securities_in_open_positions_cache] = assets.open_positions.collect{ |p| p.security.id }.uniq
        end
        session[:smr_securities_in_open_positions_cache]
    end

    ##
    # Returns sanitized page number as set by +params[:page]+.
    def smr_page
        JustPaginate.page_value(params[:page])
    end

    ##
    # Paginate a collection if things.
    def smr_paginate(current_page, collection, items_per_page=20)
        JustPaginate.paginate(current_page, items_per_page, collection.count) do |range|
            collection.slice(range)
        end
    end
end

