#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'asset'

##
# Shows total Assets.
class AssetController < ApplicationController

    ##
    # Generates lists of currently open and closed positions where "currently"
    # is defined by #smr_browse_date, also see Smr::Asset.
    #
    #  * smr_securities_list cache is populated by this
    def index
        smr_menu_addsubitem('assets', {
            '+ position'=>:new_position,
            '+ quote'=>:new_asset,
        })
        session[:asset_portfolio_shown] = params[:id_portfolio] if params[:id_portfolio]

        @shown_portfolio = session[:asset_portfolio_shown]
        @shown_portfolio = false if @shown_portfolio == ''

        @assets = Smr::Asset.new(
            current_user.id,
            smr_browse_date,
            :id_portfolio=>@shown_portfolio
        )
        smr_securities_in_open_positions_list(@open_positions = @assets.open_positions)
        @securities = smr_securities_list
        @portfolios = smr_portfolios_list
    end

    ##
    # handle new/updated Quote
    def new
        self.index
        if params[:id] then
            @quote = Quote.where(:id=>params[:id]).first
        else @quote = Quote.new(:date_last=>Time.now.to_i) end

        render :index
    end

    ##
    # handles creates and updates for Quote
    def create
        if params[:quote][:id].to_i > 0 then
            q = Quote.where(:id=>params[:quote][:id]).first
        else q = Quote.new(quote_params) end

        q.date_last = Time.parse(params[:timestring]).to_i if params[:timestring]
        q.exchange = 'quotation by %s' % current_user.fullname if q.exchange.empty?

        if params[:quote][:id].to_i > 0 then
            q.update(quote_params)
        else q.save end

        # FIXME: bug in rails?
        # - this :notice flash message it lost when using redirect_to
        # - it is not lost if the default template is rendered: app/views/asset/create.html.erb
        #redirect_to assets_path, :notice=>q.errors.full_messages.uniq.join(', ')
        flash.notice = 'Adding Quote failed: ' + q.errors.full_messages.uniq.join(', ') if not q.errors.empty?

        if URI(request.referer).path == new_asset_path then
            index
            render :index
        else redirect_to :back end
    end


 protected

    ##
    # internal helper defining parameters acceptable for Quote create/update
    def quote_params
        params.require(:quote).permit(
            :id, :id_security, :date_last, :last, :low,
            :high, :volume, :exchange
        )
    end

end
