#
#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'blog'

##
# The blog is a roll of comments a User made on various items.
#
# That is on Order, Quoterecord, uploaded Document and other records. In
# addition to that a User may write own articles as Comment.
#
# All of that is shown as a list of Smr::BlogItem objects collected by Smr::Blog.
class BlogController < ApplicationController

    ##
    # show the blogroll
    def index
        smr_menu_addsubitem('blog', {'+ article'=>:new_blog})
        session[:blog_pattern] = params[:pattern] if params[:pattern]
        @blogroll = Smr::Blog.new(current_user, smr_browse_date, :pattern=>session[:blog_pattern])
    end

    ##
    # handle article writing/editing (which is a Comment actually)
    def new
        self.index
        if params[:id] then
            @article = Comment.where(:id=>params[:id], :id_user=>current_user.id).first
        else @article = Comment.new end

        render :index
    end

    ##
    # handles creates and updates
    def create
        if params[:comment][:id].to_i > 0 then
            c = Comment.where(:id=>params[:comment][:id], :id_user=>current_user.id).first
        else c = Comment.new(comment_params) end

        c.date_recorded = Time.now.to_i
        c.id_user = current_user.id

        if params[:comment][:id].to_i > 0 then
            c.update(comment_params)
        else c.save end

        redirect_to blog_index_path, :notice=>c.errors.full_messages.uniq.join(', ')
    end


 protected

    ##
    # internal helper defining parameters acceptable for Comment create/update
    def comment_params
      params.require(:comment).permit(:title, :comment)
    end
end
