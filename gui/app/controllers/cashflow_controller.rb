#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'cashflowlog'
require 'cashflowforecast'

##
# see Smr::CashflowLog
class CashflowController < ApplicationController

    ##
    # shows cashflows in adjustable manner
    def index
        smr_menu_addsubitem('cashflow', {'+ dividend'=>:new_cashflow})

        @filters = Smr::CashflowLog.filters.merge(Smr::CashflowForecast.filters)
        @log = Array.new
        @forecast = Array.new

        # timeframes this report can work with
        @timeframes = {
            'This Month'   => :this_month,
            'This Quarter' => :this_quarter,
            'This Year'    => :this_year,
            '3 Years'      => :three_years,
            '5 Years'      => :five_years,
            'All Time'     => :all_time,
        }

        # handle params
        case params[:timeframe].to_s.to_sym
            when :this_month
                report_start = smr_browse_date.beginning_of_month
                report_end = smr_browse_date.end_of_month
            when :this_quarter
                report_start = smr_browse_date.beginning_of_quarter
                report_end = smr_browse_date.end_of_quarter
            when :this_year
                report_start = smr_browse_date.beginning_of_year
                report_end = smr_browse_date.end_of_year
            when :three_years
                report_start = (smr_browse_date - 3.years).beginning_of_year
                report_end = (smr_browse_date + 3.years).end_of_year
            when :five_years
                report_start = (smr_browse_date - 5.years).beginning_of_year
                report_end = (smr_browse_date + 5.years).end_of_year
            when :all_time
                report_start = Time.new(2000,1,1)
                report_end = (smr_browse_date + 100.years).end_of_year
            else
                report_start = smr_browse_date.beginning_of_quarter
                report_end = smr_browse_date.end_of_quarter
        end
        @timeframe = params[:timeframe] || :this_quarter
        @filter = if params[:filter] then filter_params else :none end
        @portfolio = params[:portfolio] || 0
        @portfolios = smr_portfolios_list

        # make the log
        if Smr::CashflowLog.filters.has_value? @filter
            @log = Smr::CashflowLog.new(report_start, smr_browse_date, current_user.id)
            @log.set_filter(@filter) if @log.filters.has_value? @filter
            @log.set_portfolio(@portfolio) if @portfolio
        end

        # make the forecast
        # - looking forward what @timeframe looks backwards
        if Smr::CashflowForecast.filters.has_value? @filter
            @forecast = Smr::CashflowForecast.new(smr_browse_date, report_end, current_user.id)
            @forecast.set_filter(@filter)
            @forecast.set_portfolio(@portfolio) if @portfolio
        end
    end

    ##
    # add new cashflows (right now Dividend only)
    def new
        self.index
        if params[:id] then
            @dividend = Dividend.where(:id=>params[:id]).first
        else @dividend = Dividend.new(:time_exdate=>smr_browse_date) end
        @positions = Smr::Asset.new(current_user.id, smr_browse_date, :skip_cash_positions=>true).open_positions

        render :index
    end

    ##
    # handles data creates and updates
    #
    # This method redirects differently depending on :params received. Ie its
    # guessing whats the right thing of the moment. If guessing fails it
    # redirects to #cashflow_index_path.
    def create
        exdate = params[:dividend][:time_exdate].to_time
        valutadate = params[:valutadate].to_time
        n = Array.new

        # book received amount against cash (if we have shares)
        ap = Smr::AssetPosition.new(params[:dividend][:id_position].to_i, current_user.id, exdate)

        if  ap.shares == 0
            n << 'can not book dividend because position holds no shares at ex-date'
        else
            apt = Smr::Transaction.new(ap, current_user)
            cp = Smr::Transaction.new(apt.cash_position, current_user, :type=>:dividend_booking)
            cp.book(params[:received].to_f * ap.shares, :time=>valutadate, :comment=>'from %s' % ap)

            # create the dividend payment
            d = Smr::DividendIncome.new(ap)
            d.add_payment(
                params[:dividend][:paid].to_f,
                cp.order.id,
                exdate
            )
            n << d.error if d.error

            # handle document
            if params[:dividend][:document] then
                doc = Smr::UploadedFile.store(current_user.id, params[:dividend][:document])
                doc.assign(:order=>cp.order.id)
            end
        end

        # handle redirection
        return_path = case params[:redirect]
            when 'position' then return_path = position_path(:id=>ap.id)
        else cashflow_index_path end
        redirect_to return_path, :flash=>{:notice=>n.join(', ')}
    end


 protected

    ##
    # internal helper defining parameters acceptable for Dividend create/update
    def dividend_params
        params.require(:dividend).permit(:id_position, :exdate, :paid)
    end

    ##
    # internal helper defining parameters acceptable for filter specification
    def filter_params
      if @filters.has_value?(params[:filter].parameterize.to_sym) then
            params[:filter].parameterize.to_sym
      else
            raise 'symbol "%s" is not accepted as filter' % params[:filter]
      end
    end
end
