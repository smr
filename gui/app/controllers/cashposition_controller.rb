
#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'uploaded_file'

##
# Handles a Cash Position
#
# From the model perspective its the same as a Position holding a security.
# From the usage point of view its completly different: a cash position looks
# and behaves like a bank deposit.
class CashpositionController < ApplicationController

    ##
    # Show Cash Deposit at current browse date.
    def show
        @position = Smr::AssetPosition.new(params[:id].to_i, current_user.id, smr_browse_date)

        smr_menu_addsubitem('assets',
            {
                '+ booking'=>'%s?id=%i' % [new_cashposition_path, params[:id]],
                '- settle'=>settle_cashposition_path(params[:id])
            }
        ) unless @position.is_closed?

        redirect_to root_path if not @position.is_cash_position?

        @page = smr_page
        @documents, @total_pages = smr_paginate(@page, Smr::UploadedFiles.new(current_user.id, :id_position=>@position.id))
    end

    ##
    # Prepare a cash booking Transaction.
    def new
        @booking = Smr::Transaction.new(Position.find(params[:id]), current_user)
        show
        render :show
    end

    ##
    # Creates a Transaction to book cash on the Position.
    def create
        bp = booking_params
        valuta = Time.parse(bp[:date])
        cp = Smr::Transaction.find_cashposition(  # find cashposition in portfolio at date of valuta
           Position.find(bp[:id_cashposition]).id_portfolio,
           current_user.id,
           valuta
        )

        booking = Smr::Transaction.new(cp, current_user, :issue_time=>valuta, :type=>bp[:type].to_sym)
        booking.set_comment(bp[:comment])
        booking.book(bp[:amount].to_f, :time=>valuta)

        # handle document
        if params[:document] then
            doc = Smr::UploadedFile.store(current_user.id, params[:document])
            doc.assign(:order=>booking.order.id)
        end

        redirect_to cashposition_path(:id=>bp[:id_cashposition]), :notice=>booking.error
    end

    ##
    # Settle cash position
    # - the cashposition is closed and its balance is booked into
    #   a cashposition for the next year
    # - once settled its not possible to book new transactions into
    #   the portfolio that year. Read: "accounting has closed its books".
    def settle
        p = Smr::AssetPosition.new(params[:id].to_i, current_user.id, smr_browse_date)
        p.settle
        redirect_to :back
    end

 protected

    ##
    # Internal helper defining which parameters to accept for a booking.
    def booking_params
      params.require(:booking).permit(
        :amount, :date, :comment, :id_cashposition, :type
      )
    end
end
