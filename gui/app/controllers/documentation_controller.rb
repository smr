
#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# Serves files from doc/ directory to authenticated users.
class DocumentationController < ApplicationController

    ##
    # Serve a file
    def get_file
        path = Dir[Rails.root.join("doc", params[:file_or_folder])]
        if path.length > 0
            send_file path[0], :disposition=>'inline'
        else
            render :text=>'File Not Found'
        end

    end
end
