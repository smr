#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'uploaded_file'

##
# Controller for all sorts of Smr::UploadedFile things.
#
# This shows all documents available for the user, no matter where they've been
# attached to, when or if.
class DocumentsController < ApplicationController

    def index
        session[:documents_query] = params[:q] if params[:q]

        @portfolios = Portfolio.where(:id_user=>current_user.id).order(order: :asc)
        @document = Document.new

        files = Smr::UploadedFiles.new(
            current_user.id,
            :date=>smr_browse_date,
            :ransack_query=>session[:documents_query]
        )
        @query = files.query
        @documents, @total_pages = smr_paginate(@page = smr_page, files)
    end

    ##
    # save broser-supplied file content as new Document
    def create
        uploaded_file = params[:document][:content]
                
        if uploaded_file then
            d = Smr::UploadedFile.store(current_user.id, uploaded_file, params[:document][:comment])
            n = 'saved file %s' % uploaded_file.original_filename
        else
            n = 'please select a file first'
        end

        unless params[:assign].blank? and d.is_a?(Smr::UploadedFile) then
            da = DocumentAssign.where(:id_document=>d.id).first || DocumentAssign.new(:id_document=>d.id)

            if not params[:id_portfolio].blank?
                da.id_portfolio = params[:id_portfolio].to_i
                n += ', assigned to Portfolio #%i' % params[:id_portfolio]
            elsif not params[:id_position].blank?
                if 1 > Position.joins(:Portfolio).where(:id=>params[:id_position].to_i, :portfolio=>{:id_user=>current_user.id}).count
                    # FIXME: shouldnt the model check things like this?
                    raise 'DocumentAssign: position does not belong to current_user'
                end
                da.id_position = params[:id_position].to_i
                n += ', assigned to Position #%i' % params[:id_position]
            elsif not params[:id_order].blank?
                if 1 > Order.joins(:Portfolio).where(:id=>params[:id_order].to_i, :portfolio=>{:id_user=>current_user.id}).count
                    # FIXME: shouldnt the model check things like this?
                    raise 'DocumentAssign: order does not belong to position of current_user'
                end
                da.id_order = params[:id_order].to_i
                n += ', assigned to Order #%i' % params[:id_order]
            end

            da.is_assigned = true
            da.save!
        end

        redirect_to :back, :notice=>n
    end

    ##
    # serve DocumentData file content with correct mimetype to the browser
    def download
        if params[:id] then 
            d = Smr::UploadedFile.new(params[:id], current_user.id)
            if not d.empty? then
                send_data(d.data, :type=>d.mimetype, :filename=>d.filename, :disposition=>'attachment', :length=>d.size)
                return
            else
                n = 'requested document #%i does not exist' % params[:id]
            end
        else
            n = 'please specify a document id to download'
        end
        redirect_to :back, :notice=>n
    end

    ##
    # delete Document with DocumentData and DocumentAssign associations
    #
    # WORKAROUND:
    # - the standard way in Rails to delete a record requires JavaScript, but
    #   SMr is ment to work without it
    # - therefore deletion is implemented as  non-standard action with seperate
    #   route
    def delete_document
        d = Document.where(:id_user=>current_user.id, :id=>params[:id]).first
        d.destroy!
        redirect_to :back, :notice=>'deleted document and its associations'
    end
end
