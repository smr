#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'figures'

##
# Smr::Figures are to record and follow fundamental information across fiscal
# quarters and years.
class FiguresController < ApplicationController
    def index
        smr_menu_addsubitem('figures', {'+ data'=>:new_figure})
        errors = Array.new

        @datatable = Smr::FiguresDataTable.new
        @have_data = false
        @keep_form_open = session[:figures_keep_form_open]
        @scale_value = session[:scale_value]

        @securities = smr_securities_list
        if params[:id_security] and params[:id_security].to_i>1 then
            session[:figures_security_index] = @securities.index{|s|s.id==params[:id_security].to_i}
        end

        case params[:show]
            when 'previous' then session[:figures_security_index] -= 1
            when 'next' then session[:figures_security_index] += 1
        end
        session[:figures_security_index] = 0 if not (0..@securities.count-1) === session[:figures_security_index]
        @selected_security = @securities[ session[:figures_security_index] ]



        # obtain years we have FigureData for (not concerned by smr_browse_date)
        @datayears = Array.new
        FigureData.select(:date).where(:id_security=>@selected_security.id).each do |fd|
            y = fd.time.year
            @datayears << y if not @datayears.include?(y)
        end
        @datayears.sort!.reverse!

        # select the year shown
        session[:figures_since] = smr_browse_date if not session[:figures_since]
        if params[:year]
            session[:figures_since] = Time.parse('%s-1-1'%params[:year]).beginning_of_year
        elsif not @datayears.empty? and not @datayears.include?(session[:figures_since]) then
            session[:figures_since] = Time.new(@datayears.first,1,1)
        end

        # obtain autofigures from database (those FigureVar with expression)
        autofigures = Array.new
        FigureVar.where(:id_user=>[0,current_user.id]).where.not(:expression=>'').all.each do |fv|
            autofigures << Smr::Autofigure.new(fv)
        end

        # obtain figure data, add them to datatable and feed into autofigures
        FigureData.where(:id_security=>@selected_security.id)
            .where('date >= %i' % session[:figures_since].beginning_of_year )
            .where('date <= %i' % smr_browse_date.end_of_year )
            .joins(:FigureVar)
            .where('figure_var.id_user IN (%i,%i)' % [0,current_user.id])
            .order(:date=>:desc)
            .each do |fd|
            @datatable.add(fd)
            @have_data = true

            autofigures.each { |af| af.add(fd) }
        end

        # collect autofigure data objects (whatever could be solved) and feed into datatable
        autofigures.each do |af|
            af.get.each{ |ad| @datatable.add(ad) }
            errors += af.errors unless af.errors.empty?
        end

        flash.alert = errors.join(', ') unless errors.empty?
        @datatable.render if @have_data
    end

    ##
    # makes new FigureData object
    def new
        self.index

        if session[:figures_keep_form_open]
            # pre-populate new object with some fields from the one last added/edited
            last=FigureData.where(:id_security=>@selected_security.id).last
            @figuredata = FigureData.new(
                :id_security=>@selected_security.id,
                :date=>last.time,
                :id_figure_var=>last.id_figure_var,
                :period=>last.period,
                :analyst=>last.analyst,
                :is_expected=>last.is_expected,
                :is_audited=>last.is_audited
            )
        else
            @figuredata = FigureData.new(:id_security=>@selected_security.id, :date=>smr_browse_date)
        end

        @figurevariables = FigureVar.where(:id_user=>[0,current_user.id])

        render :index
    end

    ##
    # edits FigureData object
    def edit
        self.index

        @figuredata = FigureData.where(:id=>params[:id].to_i).first
        @figurevariables = FigureVar.where(:id_user=>[0,current_user.id])

        render :index
    end

    ##
    # handles creates and updates
    def create
        session[:figures_keep_form_open] = if params[:keep_form_open] then true else false end
        session[:scale_value] = if params[:scale_value].to_i > 1 then params[:scale_value].to_i end

        if params[:figure_data][:id].to_i > 0 then
            fd = FigureData.where(:id=>params[:figure_data][:id])
                .joins(:FigureVar).where('figure_var.id_user IN (%i,%i)' % [0,current_user.id])
                .first
        else
            fd = FigureData.new(figuredata_params)
            fd.value *= session[:scale_value] if session[:scale_value]
        end

        fd.date = Time.parse(params[:figure_data][:time]).to_i

        if params[:figure_data][:id].to_i > 0 then
            fd.update(figuredata_params)
        else fd.save end

        if session[:figures_keep_form_open] then
            redirect_to new_figure_path, :notice=>fd.errors.full_messages.uniq.join(', ')
        else
            redirect_to figures_path, :notice=>fd.errors.full_messages.uniq.join(', ')
        end
    end

 protected

    ##
    # internal helper defining parameters acceptable for FigureData create/update
    def figuredata_params
      params.require(:figure_data).permit(
        :id_security, :id_figure_var, :period, :date, :analyst, :value, :is_expected, :is_audited, :comment
      )
    end
end
