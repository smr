#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# handles CRUD on Bookmark records
class Objects::BookmarkController < ObjectsController

    ##
    # List all Bookmark records for editing.
    def index
        super
        session[:objects_bookmark_query] = params[:q] if params[:q]

        @query = Bookmark.ransack(session[:objects_bookmark_query])
        @bookmarks, @total_pages = smr_paginate(
            @page=smr_page,
            @query.result.to_a
        )
    end

    ##
    # defines empty Bookmark to #create a new one
    def new
        @securities = smr_securities_list
        @organizations = smr_organizations_list(:drop_universe=>false)
        @portfolios = smr_portfolios_list(:show_all=>true)
        @figure_vars = FigureVar.for_user(current_user.id)

        @bookmark = Bookmark.new(:id_user=>current_user.id)
    end

    ##
    # retrieve Bookmark for editing
    def edit
        self.new
        @bookmark = Bookmark.for_user(current_user).where(:id=>params[:id]).first
        render :new
    end

    ##
    # handles creates and updates
    def create
        n = Array.new

        if params[:bookmark][:id].to_i > 0 then
            bookmark = Bookmark.for_user(current_user).where(:id=>params[:bookmark][:id]).first
            bookmark.update(bookmark_params)
        else bookmark = Bookmark.create(bookmark_params.merge({:id_user=>current_user.id})) end

        bookmark.id_user = params[:mark_bookmark_private] == '1' ? current_user.id : 0
        bookmark.save
        n << bookmark.errors.full_messages.uniq

        redirect_to objects_bookmark_index_path, :notice=>n.compact.flatten.join(', ')
    end

    ##
    # delete Bookmark
    def destroy
        b = Bookmark.find(params[:id])
        begin b.destroy
        rescue ActiveRecord::DeleteRestrictionError => errors
            flash[:alert] = errors.to_s
        end

        redirect_to :action=>:index
    end

 protected

    ##
    # internal helper defining parameters acceptable for Bookmark create/update
    def bookmark_params
        params.require(:bookmark).permit(
          :name, :url, :rank,
          :id_security, :id_organization, :id_portfolio, :id_figure_var
        )
    end

end
