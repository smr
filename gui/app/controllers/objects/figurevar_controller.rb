#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# handles CRUD on FigureVar records
class Objects::FigurevarController < ObjectsController
    def index
        super
        @figurevars, @total_pages = smr_paginate(
            @page = smr_page,
            FigureVar.where(:id_user=>[0,current_user.id]).order(:name).to_a
        )
    end

    ##
    # defines empty FigureVar to create() a new one
    def new
        @figurevar = FigureVar.new
    end

    ##
    # edit existing FigureVar
    def edit
        @figurevar = FigureVar.where(:id=>params[:id]).for_user(current_user.id).first
        render :new
    end

    ##
    # handles creates and updates
    def create
        if params[:figure_var][:id].to_i > 0 then
            fv = FigureVar.where(:id=>params[:figure_var][:id]).for_user(current_user.id).first
        else fv = FigureVar.new(figurevar_params) end

        # hard-wired so noone spoofes it in here
        # FIXME: how to handle properly? given 0 _or_ current user id
#        fv.id_user = current_user.id

        if params[:figure_var][:id].to_i > 0 then
            fv.update(figurevar_params)
        else fv.save end

        redirect_to objects_figurevar_index_path, :notice=>fv.errors.full_messages.uniq.join(', ')
    end

 protected

    ##
    # internal helper defining parameters acceptable for FigureVar create/update
    def figurevar_params
        params.require(:figure_var).permit(
          :name, :unit, :expression, :comment,
          :Bookmark_attributes=> [ :id, :name, :url, :rank ]
        )
    end
end
