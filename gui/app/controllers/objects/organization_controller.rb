#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# handles CRUD on Organization records
class Objects::OrganizationController < ObjectsController
    def index
        super
        session[:objects_organization_query] = params[:q] if params[:q]

        @query = Organization.ransack(session[:objects_organization_query])
        @organizations, @total_pages = smr_paginate(
            @page = smr_page,
            @query.result.to_a
        )
    end

    ##
    # defines empty Organization to create() a new one
    def new
        @organization = Organization.new
        render :edit
    end

    ##
    # edit existing Organization
    def edit
        @organization = Organization.where(:id=>params[:id]).first
    end

    ##
    # handles creates and updates
    def create
        if params[:organization] and params[:organization][:id].to_i > 0 then
            o = Organization.where(:id=>params[:organization][:id]).first
        else o = Organization.new(organization_params) end

        # hard-wired so noone spoofes it in here
        # FIXME: how to handle properly? given 0 _or_ current user id
#        fv.id_user = current_user.id

        if params[:organization][:id].to_i > 0 then
            o.update(organization_params)
        else o.save end

        redirect_to objects_organization_index_path, :notice=>o.errors.full_messages.uniq.join(', ')
    end

 protected

    ##
    # internal helper defining parameters acceptable for Organization create/update
    def organization_params
        params.require(:organization).permit(
          :id, :name, :contact_email, :description,
          :Bookmark_attributes=> [ :id, :name, :url, :rank ]
        )
    end
end
