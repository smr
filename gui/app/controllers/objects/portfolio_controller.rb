#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# handles CRUD on Portfolio records
class Objects::PortfolioController < ObjectsController
    def index
        super
        @portfolios, @total_pages = smr_paginate(@page = smr_page, smr_portfolios_list(:show_all=>true))
    end

    ##
    # defines empty Portfolio to create() a new one
    def new
        @portfolio = Portfolio.new(:date_created=>Time.now.to_i)
    end

    ##
    # edit Portfolio
    def edit
        @portfolio = Portfolio.find(params[:id])

        @portfolios = smr_portfolios_list(:show_all=>true)
        @assets = Smr::Asset.new(current_user.id, smr_browse_date,
            :id_portfolio=>@portfolio.id, :skip_cash_positions=>true)
        @open_positions = @assets.open_positions
        render :new
    end

    ##
    # handles creates and updates
    def create
        if params[:portfolio][:id].to_i > 0 then
            p = Portfolio.where(:id=>params[:portfolio][:id], :id_user=>current_user.id).first
        else p = Portfolio.new(portfolio_params) end

        # hard-wired so noone spoofes it in here
        p.id_user = current_user.id
        p.date_created = Time.parse(params[:time_created]).to_i if params[:time_created]

        # FIXME: not used any longer but required by schema, drop it?
        p.type = ''
        if params[:portfolio][:id].to_i > 0 then
            p.update(portfolio_params)
        else p.save end

        redirect_to objects_portfolio_index_path, :notice=>p.errors.full_messages.uniq.join(', ')
    end

    ##
    # handles Position transfers to another Portfolio
    def patch
        n = Array.new
        a = Array.new

        if params[:id_portfolio_transfer] and not params[:positions].empty? then
            Position.where(:id=>params[:positions])
                .joins(:Portfolio).where(:portfolio=>{:id_user=>current_user.id}).each do |p|
                p.id_portfolio = params[:id_portfolio_transfer]
                p.save || a << 'transfering position #%i failed' % p.id
            end
            n << 'Positions have been transfered sucessfully.'
        else
            a << 'Please specify a portfolio as transfer destination and select at lease one position.'
        end

        redirect_to objects_portfolio_index_path, :notice=>n.join(','), :alert=>a.join(',')
    end

 protected

    ##
    # internal helper defining parameters acceptable for Portfolio create/update
    def portfolio_params
        params.require(:portfolio).permit(
          :id, :name, :date_created, :taxallowance, :order, :comment,
          :Bookmark_attributes=> [ :id, :name, :url, :rank ]
        )
    end
end
