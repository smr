#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'csv'

##
# handles CRUD on Security records
class Objects::SecurityController < ObjectsController

    ##
    # List all Security records for editing.
    def index
        super
        session[:objects_security_query] = params[:q] if params[:q]

        @query = Security.ransack(session[:objects_security_query])
        @securities, @total_pages = smr_paginate(
            @page=smr_page,
            @query.result.includes(:Organization).to_a
        )
        @select_securities = smr_securities_list
    end

    ##
    # defines empty Security to #create a new one
    def new
        @organizations = Organization.all.order(:name).to_a

        if params[:symbol]
            @security = Security.new(:symbol=>params[:symbol])
            @security.id_organization = session[:objects_security_organization] if session[:objects_security_organization]

            begin @security.fetch_metadata
            rescue ActiveRecord::RecordInvalid => errors
                flash[:alert] = errors.to_s
                @security = false
            end
        else @security = Security.new end
    end

    ##
    # retrieve Security for editing
    # - NOTE: disabled @quote_retrieval_status because it hinders fluent
    # security editing
    def edit
        self.new

        @security = Security.find(params[:id])
        render :new
    end

    ##
    # handles creates and updates
    def create
        n = Array.new

        if params[:security][:id].to_i > 0 then
            security = Security.find(params[:security][:id])
            security.update(security_params)
        else security = Security.create(security_params) end

        if params[:security_use_type] then
            security.create_type(params[:security_use_type])
        end

        if params[:security_stock]
            if params[:security_stock][:id].to_i > 0 then
                st = SecurityStock.find(params[:security_stock][:id])
                st.update(security_stock_params)
            else st = SecurityStock.create(security_stock_params) end
            security.id_security_stock = st.id
            n << st.errors.full_messages.uniq
        end

        if params[:security_bond]
            if params[:security_bond][:id].to_i > 0 then
                st = SecurityBond.find(params[:security_bond][:id])
                st.update(security_bond_params)
            else st = SecurityBond.create(security_bond_params) end
            security.id_security_bond = st.id
            n << st.errors.full_messages.uniq
        end

        if params[:security_fund]
            if params[:security_fund][:id].to_i > 0 then
                st = SecurityFund.find(params[:security_fund][:id])
                st.update(security_fund_params)
            else st = SecurityFund.create(security_fund_params) end
            security.id_security_fund = st.id
            n << st.errors.full_messages.uniq
        end

        if params[:security_metal]
            if params[:security_metal][:id].to_i > 0 then
                st = SecurityMetal.find(params[:security_metal][:id])
                st.update(security_metal_params)
            else st = SecurityMetal.create(security_metal_params) end
            security.id_security_metal = st.id
            n << st.errors.full_messages.uniq
        end

        if params[:security_derivative]
            if params[:security_derivative][:id].to_i > 0 then
                st = SecurityDerivative.find(params[:security_derivative][:id])
                st.update(security_derivative_params)
            else st = SecurityDerivative.create(security_derivative_params) end
            security.id_security_derivative = st.id
            n << st.errors.full_messages.uniq
        end

        if params[:security_index]
            if params[:security_index][:id].to_i > 0 then
                st = SecurityIndex.find(params[:security_index][:id])
                st.update(security_index_params)
            else st = SecurityIndex.create(security_index_params) end
            security.id_security_index = st.id
            n << st.errors.full_messages.uniq
        end

        security.save
        n << security.errors.full_messages.uniq

        redirect_to objects_security_index_path, :notice=>n.flatten.join(', ')
    end

    ##
    # delete Security unless (!) important data is related to it
    # - basically delete is not possible if a Position exist (of any user)
    # - also "observations" block it, ie. a assigned Document or FigureData
    # - see the model to know what else is important
    # - unimportant records such as Quote will be removed along
    def destroy
        s = Security.find(params[:id])
        begin s.destroy
        rescue ActiveRecord::DeleteRestrictionError => errors
            flash[:alert] = errors.to_s
        end

        redirect_to :action=>:index
    end

    ##
    # export list of Securities as CSV data
    def export
        self.index
        respond_to do |format|
          format.html { redirect_to :action=>:index }

          format.csv do
            typemodel_join_str = Security.types.map{ |t|
                unless [:unknown, :cash].include?(t.to_sym) then
                    'LEFT OUTER JOIN security_%s ON security_%s.id=security.id_security_%s' % [t, t, t]
                end
            }.compact.join(' ')

            lines = ActiveRecord::Base.connection.exec_query(
                'SELECT * FROM security %s' % typemodel_join_str
            ).to_hash

            csvdata = CSV.generate(:return_headers=>true, :force_quotes=>true){ |csv|
                csv.add_row(['name', 'type'] + lines.first.keys) # header with column names
                lines.each do |l|
                    s = Security.find_by_symbol l['symbol']
                    csv.add_row( [s.to_s, s.type] + l.values )
                end
            }

            send_data(
                csvdata,
                :type => 'text/csv; charset=utf-8; header=present',
                :disposition => 'attachment; filename=payments.csv'
            )
          end
        end
    end

 protected

    ##
    # internal helper defining parameters acceptable for Security create/update
    def security_params
        params.require(:security).permit(
          :symbol,
          :preferred_reaper, :preferred_reaper_locked,
          :id_organization, :brief, :description, :fetch_quote,
          :collateral_coverage_ratio,
          :time_trading_ceased, :trading_ceased_reason,
          :Bookmark_attributes=> [ :id, :name, :url, :rank ]
        )
    end

    ##
    # internal helper defining parameters acceptable for Security Type model create/update
    def security_stock_params
        params.require(:security_stock).permit(
          :dividend, :time_dividend_exdate, :dividend_interval, :type, :comment
        )
    end

    def security_bond_params
        params.require(:security_bond).permit(
          :type, :currency, :denomination, :issue_size,
          :is_subordinated, :is_callable, :is_stepup, :is_stepdown,
          :coupon, :coupon_interval, :interest_method, :time_first_coupon, :time_last_coupon,
          :time_maturity, :redemption_price, :redemption_installments, :redemption_interval,
          :time_issue, :time_first_call, :call_price, :call_interval, :call_notify_deadline,
          :comment
        )
    end

    def security_fund_params
        params.require(:security_fund).permit(
          :type, :currency, :tranche, :time_inception, :time_fiscalyearend, :investment_minimum, :is_synthetic,
          :distribution, :time_distribution, :distribution_interval, :is_retaining_profits,
          :issue_surcharge, :management_fee, :redemption_fee, :redemption_period,
          :comment
        )
    end

    def security_metal_params
        params.require(:security_metal).permit(
          :type, :unit, :comment
        )
    end

    def security_derivative_params
        params.require(:security_derivative).permit(
          :type, :direction, :id_security_underlying, :time_inception, :time_end,
          :strike_price, :subscription_ratio, :is_leveraged, :is_dead, :comment
        )
    end

    def security_index_params
        params.require(:security_index).permit(
          :comment
        )
    end
end
