#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# handles CRUD on User records
class Objects::UserController < ObjectsController
    def index
        super
        @users, @total_pages = smr_paginate(
            @page = smr_page,
            User.order(:login).to_a
        )
    end

    ##
    # defines new User to #create
    def new
        @user = User.new
    end

    ##
    # edit User
    # - until proper permissions are implemented a User can only edit itself,
    #   but not others
    # - each User can, however, create new users.
    def edit
        @user = current_user
        render :new
    end

    ##
    # handles creates and updates
    # - until proper permissions are implemented a User can only edit itself,
    #   but not others
    def create
        if params[:user][:id].to_i > 0 then
            u = User.where(:id=>current_user.id).first
            u.update(user_params)

            if not params[:user][:password].empty? then
                if params[:user][:password].eql?(params[:user][:password_confirm]) then
                    u.password = params[:user][:password].to_s
                    u.save
                    flash[:notice] = 'Password changed.'
                else
                    redirect_to :back, :alert=>'Confirming password failed. Typo?'
                    return
                end
            end
        else
            u = User.new(user_params)
            flash[:notice] = 'new users initial password is: "%s"' % u.generate_password
            u.save
        end

        flash[:alert] = u.errors.full_messages.uniq.join(', ') unless u.errors.empty?
        redirect_to objects_user_index_path
    end

 protected

    ##
    # internal helper defining parameters acceptable for User create/update
    def user_params
        params.require(:user).permit(
            :id, :login, :is_admin, :fullname, :email, :comment,
            :Bookmark_attributes=> [ :id, :name, :url, :rank ]
        )
    end

end
