#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# CRUD objects such as Portfolio, Security, FigureVar, SecurityQuotesource,
# SecuritySymbolextension, ...
class ObjectsController < ApplicationController
    ##
    # builds the menu for all objects namespaced below
    def index
        @headline='Objects'
        smr_menu_addsubitem('objects', {
            'securities'=>:objects_security_index,
            '+ security'=>:new_objects_security,
            'organizations'=>:objects_organization_index,
            '+ organization'=>:new_objects_organization,
            'bookmarks'=>:objects_bookmark_index,
            '+ bookmark'=>:new_objects_bookmark,
            'portfolios'=>:objects_portfolio_index,
            '+ portfolio'=>:new_objects_portfolio,
            'figures'=>:objects_figurevar_index,
            '+ figure'=>:new_objects_figurevar,
            'users'=>:objects_user_index,
            '+ user'=>:new_objects_user,
        })
    end
end
