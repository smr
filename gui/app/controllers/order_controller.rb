#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'uploaded_file'

##
# Handle Order records.
#
# TODO: move Order create, execute, cancel here
#
class OrderController < ApplicationController

    ##
    # show a single order
    def show
        @order = Order.where(:id=>params[:id])
                .joins(:Portfolio).where('portfolio.id_user = %i' % current_user.id)
                .first
        if @order
            @page = smr_page
            @documents, @total_pages = smr_paginate(@page, Smr::UploadedFiles.new(current_user.id, :id_order=>@order.id))
        else redirect_to root_path, :alert=>'No Order found for :id=>%s' % params[:id] end
    end

end
