#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'cashflowforecast'
require 'figures'

##
# Shows portrait of Security or Organization
class PortraitController < ApplicationController

    ##
    # Gathers lots of information on given Security or Organization.
    def index
        @securities = smr_securities_list
        @organizations = smr_organizations_list
        @cashflow = Smr::CashflowForecast.new(
            smr_browse_date, smr_browse_date.end_of_year + 2.years, current_user.id
        )
        @positions = Array.new
        @positions_total_volume = 0
        @open_positions_gain = 0
        @all_positions_gain = 0
        @all_positions_dividend = 0
        @errors = Array.new
    end

    def show_security
        index
        @security = Security.find params[:id].to_i

        @intraday_quotes = @security.Quote
            .where(:date_last=>(smr_browse_date.beginning_of_day.to_i .. smr_browse_date.end_of_day.to_i))
            .order(:date_last=>:desc)
            .all

        @cashflow.set_security(@security)

        @figures_datatable = Smr::FiguresDataTable.new
        @figures_have_data = false
        build_figures

        @quoterecords = Smr::Quoterecords.new(current_user.id, @security, smr_browse_date)

        Position.where.not(:id_security=>Smr::ID_CASH_SECURITY)
            .joins(:Portfolio).where('portfolio.id_user=%i' % current_user.id)
            .joins(:Security).where('security.id=%i' % @security.id)
            .order(:date_closed=>:asc).each do |p|
            p = Smr::AssetPosition.new(p.id, current_user.id, smr_browse_date)
            @positions << p
            if p.profit_loss
               @all_positions_gain += p.profit_loss
               @open_positions_gain += p.profit_loss unless p.is_closed?
            end
            @all_positions_dividend += p.dividend.received
        end

        flash.alert = @errors.join(', ') unless @errors.empty?
        render :index
    end

    def show_organization
        index
        @organization = Organization.find params[:id].to_i

        @cashflow.set_organization(@organization)

        Position.where.not(:id_security=>Smr::ID_CASH_SECURITY)
            .joins(:Portfolio).where('portfolio.id_user=%i' % current_user.id)
            .joins(:Security).where('security.id_organization=%i' % @organization.id)
            .order(:date_closed=>:asc).each do |p|
            p = Smr::AssetPosition.new(p.id, current_user.id, smr_browse_date)
            @positions << p
            if p.profit_loss
               @all_positions_gain += p.profit_loss
               @open_positions_gain += p.profit_loss unless p.is_closed?
            end
            @all_positions_dividend += p.dividend.received
        end

        flash.alert = @errors.join(', ') unless @errors.empty?
        render :index
    end

  protected

    # find and build figures and autofigures for @security
    # FIXME: copy'n'pasted from figures_controller, not how it should be
    def build_figures
        autofigures = Array.new
        FigureVar.where(:id_user=>[0,current_user.id]).where.not(:expression=>'').all.each do |fv|
            autofigures << Smr::Autofigure.new(fv)
        end

        # obtain figure data, add them to datatable and feed into autofigures
        FigureData.where(:id_security=>@security.id)
            .where(:date=>smr_browse_date.beginning_of_year.to_i..smr_browse_date.end_of_year.to_i)
            .joins(:FigureVar)
            .where('figure_var.id_user IN (%i,%i)' % [0,current_user.id])
            .order(:date=>:desc)
            .each do |fd|
            @figures_datatable.add(fd)
            @figures_have_data = true

            autofigures.each { |af| af.add(fd) }
        end

        # collect autofigure data objects (whatever could be solved) and feed into datatable
        autofigures.each do |af|
            af.get.each{ |ad| @figures_datatable.add(ad) }
            @errors += af.errors unless af.errors.empty?
        end

        @figures_datatable.render if @figures_have_data
    end
end
