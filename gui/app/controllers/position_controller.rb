#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'uploaded_file'

##
# Controller for AssetPosition
#
# This also cares about orders and other things since an AssetPosition
# is more than just the Position model.
class PositionController < ApplicationController

    ##
    # Show details about a AssetPosition.
    def show
        @position = Smr::AssetPosition.new(params[:id].to_i, current_user.id, smr_browse_date)
        redirect_to root_path if @position.is_cash_position?

        if @position.is_new? then
            smr_menu_addsubitem('assets', {
                '+ order'=>new_position_path + '?id_position=%i'%params[:id],
                '- sell off'=>'/close?id=%i' % params[:id]
            })
        elsif not @position.is_closed?
            smr_menu_addsubitem('assets', {
                '+ order'=>new_position_path + '?id_position=%i' % params[:id],
                '+ dividend'=>position_path + '?add_dividend=true',
                '- sell off'=>'/close?id=%i' % params[:id],
                '+ document'=>position_path + '?add_document=true',
            })
        elsif @position.is_closed? and @position.time_closed > 1.month.ago
            smr_menu_addsubitem('assets', {
                '+ dividend'=>position_path + '?add_dividend=true'
            })
        end

        @page = smr_page
        @documents, @total_pages = smr_paginate(
            @page,
            Smr::UploadedFiles.new(current_user.id, :id_position=>@position.id, :date=>smr_browse_date)
        )

        if params[:add_dividend] then
            @dividend = Dividend.new(:date_exdate=>smr_browse_date - 2.days, :id_position=>@position.id)
        end
    end

    ##
    # conditionally make a new position
    #
    # Most of the time this will not create a new position but rather add an
    # order to an existing position. It can perform one of these things
    # actually:
    #
    # 1) make a new order on existing position
    # 2) make new position in given portfolio with new order
    # 3) make new position in selectable portfolio holding selectable security
    #    order
    #    - OR -
    #    make new order on selected existing position
    #
    # In any case the +session+ is checked for :position_prefilled_order which
    # will be used as basis for the new order, if present.
    def new
        if session[:position_prefilled_order] and session[:position_prefilled_order].is_a?(Order) then
            @order = session[:position_prefilled_order]
        else
            @order = Order.new(:time_issued=>smr_browse_date, :time_expire=>(smr_browse_date+1.month).end_of_day)
        end

        if params[:id_position] then
            # 1)
            @position = Smr::AssetPosition.new(params[:id_position], current_user.id, smr_browse_date)
        elsif params[:id_portfolio] then
            # 2)
            @portfolio = Portfolio.find_by_id(params[:id_portfolio])
            @securities = Security.where.not(:id=>Smr::ID_CASH_SECURITY).all.to_a
        else
            # 3)
            @portfolios = Portfolio.select(:id, :name).where(:id_user=>current_user.id).order(order: :desc)
            @open_positions = Smr::Asset.new(current_user.id, smr_browse_date, :skip_cash_positions=>true).open_positions
            @securities = Security.where.not(:id=>Smr::ID_CASH_SECURITY).all.to_a
        end
    end

    ##
    # creates an position/order by taking POST
    #
    # FIXME: Gummicode! Test carefully, ie issue on closed position?
    def create
        redirect = :back

        order = Order.new(order_params)
        order.time_issued = Time.parse params[:time_issued]
        order.time_expire = Time.parse params[:time_expire]

        if not params[:id_position].empty? then
            # 1)
            order.id_position = params[:id_position].to_i
            redirect = position_path(order.id_position)
        elsif params[:id_portfolio].empty? or params[:id_security].empty?  then
            redirect_to :back, :notice=>'Please select some Security and Portfolio or use an existing Position to order.'
            return
        else
            # 2) and 3)
            position = Position.new(
                :id_portfolio=>params[:id_portfolio].to_i,
                :id_security=>params[:id_security].to_i,
                :comment=>''
            )
            position.save!

            order.id_position=position.id

            redirect = position_path(position.id)
        end

        order.save!

        # handle execute
        if params[:execute_now].to_i == 1 and params[:execute_quote].to_f > 0.0 then
            execute_time = Time.parse(params[:execute_time])
            t = Smr::Transaction.new(
                    Position.find(order.id_position),
                    current_user,
                    :issue_time=>order.time_issued,
                    :order=>order, :order_is_redemption=>(params[:is_redemption] || false)
            )
            t.execute(params[:execute_quote].to_f, execute_time)
            n = t.to_s

            if params[:close_position].to_i == 1 then
                stat = Smr::AssetPosition.new(order.id_position, current_user.id, smr_browse_date)
                        .close(execute_time)
                n += ' Closed position because all shares were sold.' if stat.is_a?(TrueClass)
            end
        else
            n = 'Issued new Order to %s %f shares.' % [order.type, order.shares]
        end

        # handle document
        if params[:order][:document] then
            doc = Smr::UploadedFile.store(current_user.id, params[:order][:document])
            doc.assign(:order=>order.id)
        end

        redirect_to redirect, :notice=>n
    end

    ##
    # update Position details
    # - that is things that wont change the positions` status
    def patch
        if params[:id] and params[:comment]
            p = Position.find(params[:id])
            p.comment = params[:comment]
            p.save
        end
        redirect_to :back
    end

    ##
    # close this position at smr_browse_date
    def close
        p = Smr::AssetPosition.new(params[:id].to_i, current_user.id, smr_browse_date)
        stat = p.close(smr_browse_date)
        if stat.is_a?(TrueClass) then
            n='Closed position #%i as of %s.' % [p.id, p.time_closed]
            r=:back
        elsif stat.is_a?(Order) then
            session[:position_prefilled_order] = stat
            n='Position holds shares. Can not close it right away. A new '\
             +'order to sell them off has been pre-filled, please edit and '\
             +'execute.'
            r=new_position_path + '?id_position=%i' % p.id
        else
            n='Can not close position #%i because it is closed already.' % p.id
            r=:back
        end
        redirect_to r, :notice=>n
    end

    def edit
    end

    def update
    end

    ##
    # apply Order to Position at #smr_browse_date
    def execute_order
        execute_time = Time.parse(params[:execute_time])
        p = Smr::AssetPosition.new(params[:id], current_user.id, smr_browse_date)
        t = Smr::Transaction.new(p, current_user, :issue_time=>execute_time, :order=>Order.find(params[:id_order]))
        t.execute(params[:execute_quote].to_f, execute_time)

        # try to close the position, it may remain open unless empty
        if t.order.is_sale?
            Smr::AssetPosition.new(params[:id], current_user.id, execute_time+1.second).close
        end

        redirect_to :back, :notice=>t.to_s
    end

    ##
    # cancel pending order by setting the canceled flag
    def cancel_order
        o = Order.find_by_id(params[:id_order])
        o.is_canceled = 1
        o.save!
        redirect_to :back, :notice=>'Order #%i has been canceled' % o.id
    end

    protected

    ##
    # internal helper defining which parameters to accept for an order
    def order_params
      params.require(:order).permit(
        :exchange, :shares, :limit, :addon, :provision, :courtage, :type,
        :comment, :expense, :accrued_interest, :time_issued, :time_expire
      )
    end
end

