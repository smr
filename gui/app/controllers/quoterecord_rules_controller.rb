#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# CRUD QuoterecordRules
class QuoterecordRulesController < ApplicationController
    def index
        smr_menu_addsubitem('quote records', {
            'rules' => :quoterecord_rules,
            'add rule' => :new_quoterecord_rule,
        })

        @rules = QuoterecordRule.where(:id_user=>current_user.id).order(:order)
        @possible_columns = Hash.new
        Quoterecord.get_columns.collect do |c|
            @possible_columns[Quoterecord.translate_column(c)] = c
        end
    end

    ##
    # renders :index with existing QuoterecordRule loaded into the form
    def show
        self.index
        @new_rule = QuoterecordRule.find(params[:id])
        smr_menu_addsubitem('quote records', {'revert to add new rule'=>:quoterecord_rules})
        render :index
    end

    ##
    # handles new and edit QuoterecordRule
    def new
        self.index

        if params[:id]
            @rule = QuoterecordRule.where(:id=>params[:id], :id_user=>current_user.id).first
        else
            @rule = QuoterecordRule.new
            @rule.order = if @rules.last then @rules.last.order+1 else 1 end
        end

        render :index
    end

    ##
    # handles creates and updates
    def create
        related_columns = params[:related_columns] || Array.new

        if params[:quoterecord_rule][:id].to_i > 0 then
            r = QuoterecordRule.where(:id=>params[:quoterecord_rule][:id], :id_user=>current_user.id).first
        else r = QuoterecordRule.new(quoterecord_rule_params) end

        r.id_user = current_user.id
        r.date_last_modified = Time.now.to_i
        r.set_related_columns(related_columns)

        if params[:quoterecord_rule][:id].to_i > 0 then
            r.update(quoterecord_rule_params)
        else r.save end
        redirect_to quoterecord_rules_path, :notice=>r.errors.full_messages.uniq.join(', ')
    end

 protected

    ##
    # internal helper defining parameters acceptable for quoterecord_rule
    def quoterecord_rule_params
      params.require(:quoterecord_rule).permit(
        :order, :name, :rule
      )
    end
end
