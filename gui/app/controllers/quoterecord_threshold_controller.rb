#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# dumb controller to handle QuoterecordThreshold records
# - no views intented, so dont look for them
class QuoterecordThresholdController < ApplicationController

    ##
    # handles create and update for QuoterecordThreshold
    def create
        if params[:id].to_i > 0 then
            qt = QuoterecordThreshold.where(:id=>params[:id], :id_user=>current_user.id).first
            qt.update(quoterecord_threshold_params)
        else qt = QuoterecordThreshold.new(quoterecord_threshold_params) end
        qt.id_user = current_user.id
        qt.save
        redirect_to :back, :notice=>qt.errors.full_messages.join(', ')
    end


    protected

    ##
    # internal helper defining parameters acceptable for quoterecord_threshold
    def quoterecord_threshold_params
      params.require(:quoterecord_threshold).permit(
        :id_security, :quote_sensitivity, :hit_sensitivity
      )
    end
end
