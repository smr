#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'quoterecords'

##
# Quote Records are a way of ordering market quotes into trends and putting
# personal notes on them. Interpretation of events that is.
class QuoterecordsController < ApplicationController

    ##
    # Obtains SmrQuoterecords collection.
    def index
        smr_menu_addsubitem('quote records', {
            'rules'=>:quoterecord_rules,
            '+ quote'=>:new_quoterecord,
        })

        @securities = smr_securities_list
        if params[:id_security] and params[:id_security].to_i>1 then
            session[:quoterecords_security_index] = @securities.index{|s|s.id==params[:id_security].to_i}
        else i = 0 end

        case params[:show]
            when 'previous' then session[:quoterecords_security_index] -= 1
            when 'next' then session[:quoterecords_security_index] += 1
        end
        session[:quoterecords_security_index]  = 0 if not (0..@securities.count-1) === session[:quoterecords_security_index]
        @selected_security = @securities[ session[:quoterecords_security_index]  ]

        @intraday_quotes = Quote.where(:id_security=>@selected_security.id, :date_last=>smr_browse_date.beginning_of_day.to_i..smr_browse_date.end_of_day.to_i).order(:date_last=>:desc)
        @quoterecords, @total_pages = smr_paginate(
            @page = smr_page,
            Smr::Quoterecords.new(current_user.id, @selected_security, smr_browse_date)
        )
        @sensitivity = QuoterecordThreshold.where(:id_user=>current_user.id, :id_security=>@selected_security.id).first || (QuoterecordThreshold.new(:id_security=>@selected_security.id))

        @possible_columns = Hash.new
        Quoterecord.get_columns.collect do |c|
            @possible_columns[Quoterecord.translate_column(c)] = c
        end
    end

    ##
    # handle new Quote data
    def new
        self.index
        @quote = Quote.new(:date_last=>Time.now.to_i)
        render :index
    end

    ##
    # handles creates and updates
    def create
        if params[:quoterecord][:id].to_i > 0 then
            qr = Quoterecord.where(:id=>params[:quoterecord][:id], :id_user=>current_user.id).first
            qr.update(quoterecord_params)
        else
            qr = Quoterecord.new(quoterecord_params)
            qr.id_user = current_user.id
            qr.date_created = Time.now.to_i
            qr.save!
        end

        index
        render :index
    end

    ##
    # present exiting Quoterecord for editing.
    def show
        index
        @quoterecord = Quoterecord.where(:id=>params[:id], :id_user=>current_user.id).first
        render :index
    end

    ##
    # View QuoterecordRule.
    def view_rules
        redirect_to :back
    end


    protected

    ##
    # internal helper defining parameters acceptable for quoterecord
    def quoterecord_params
      params.require(:quoterecord).permit(
        :id_security, :id_quote, :column, :comment, :is_pivotal_point, :is_uphit,
        :is_downhit, :is_signal
      )
    end
end
