#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'asset_position'

##
# Shows various statistical numbers and closed positions.
class ReportController < ApplicationController

    ##
    # Generates list of closed positions and figures various statistics during
    # the selected reporting period.
    def index

        # timeframes this report can work with
        @timeframes = {
            'This Month'   => :this_month,
            'This Quarter' => :this_quarter,
            'This Year'    => :this_year,
            '3 Years'      => :three_years,
            '5 Years'      => :five_years,
            'All Time'     => :all_time,
        }

        # improve precision when looping time
        snap_end_of_month = true

        # handle params
        case params[:timeframe].to_s.to_sym
            when :this_month
                report_start = smr_browse_date.beginning_of_month
                asset_interval = 1.week
                snap_end_of_month = false
            when :this_quarter
                report_start = smr_browse_date.beginning_of_quarter
                asset_interval = 4.weeks
            when :this_year
                report_start = smr_browse_date.beginning_of_year - 1.day  # the end of previous year!
                asset_interval = 1.months
            when :three_years
                report_start = (smr_browse_date - 3.years).beginning_of_year - 1.day
                asset_interval = 3.months
            when :five_years
                report_start = (smr_browse_date - 5.years).beginning_of_year - 1.day
                asset_interval = 6.months
            when :all_time
                report_start = Time.new(1999,12,31)
                asset_interval = 1.year
            else
                report_start = smr_browse_date.beginning_of_quarter - 1.day
                asset_interval = 4.weeks
                snap_end_of_month = false
        end
        @timeframe = params[:timeframe] || :this_quarter
        @portfolio = params[:portfolio].to_i || 0
        limit_portfolio = @portfolio > 0 ? 'and portfolio.id=%i' % @portfolio : ''

        # asset totals
        @asset_totals = []
        d = report_start
        while d <= smr_browse_date
            prev_month = d.month
            @asset_totals << Smr::Asset.new(
                current_user.id, d.end_of_day, :id_portfolio=>(@portfolio > 0 ? @portfolio : false)
            )
            d += asset_interval
            d = d.end_of_month if snap_end_of_month
        end
        @asset_totals << Smr::Asset.new(
            current_user.id, smr_browse_date, :id_portfolio=>(@portfolio > 0 ? @portfolio : false)
        ) unless smr_browse_date.to_date === d.to_date


        # closed positions
        @closed_positions = []
        @revenue = 0
        @realized_gain = 0

        # disabled:
        # - confusing since only closed_positions are summed up
        # - its *not* the absolute total paid/received during report period
        #@dividend_received = 0
        #@charges_paid = 0

        Position.closed(report_start, smr_browse_date)
            .where.not(:id_security=>Smr::ID_CASH_SECURITY)
            .joins(:Portfolio).where('portfolio.id_user=%i %s' % [current_user.id, limit_portfolio])
            .order(:date_closed=>:desc).each do |p|
            p = Smr::AssetPosition.new(p.id, current_user.id, smr_browse_date)

            @revenue += p.purchase_volume.abs + (p.is_viewed_before_closure? ? 0 : p.settled_volume.abs)
            @realized_gain += p.profit_loss if p.profit_loss
            #@charges_paid += p.charges
            #@dividend_received += p.dividend.received

            @closed_positions << p
        end

        @portfolios = smr_portfolios_list
    end

end
