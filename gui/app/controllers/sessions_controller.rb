#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'digest/md5'

class SessionsController < ApplicationController

  def create
    user = User.find_by_login(params[:login])

    unless user
        redirect_to login_path, :alert=>'Invalid login or password.'
        return
    end

    if user.password_digest.nil?
        # migrate old MD5 passwords
        if user[:password].eql?(Digest::MD5.hexdigest(params[:password]))
          user.password = params[:password]
          user.generate_token
          user.save!
        end
    end

    if user.authenticate(params[:password])
      cookies[:auth_token] = user.password_auth_token
      session[:date] = Time.new.end_of_day.to_i
      redirect_to root_url, :notice=>'Logged in!'
    else
      redirect_to login_path, :alert=>'Invalid login or password.'
    end
  end
  
  def destroy
    session[:user_id] = nil
    cookies.delete(:auth_token)
    redirect_to root_url, :notice=>'Logged out!'
  end

end
