#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'watchlist'

##
# Shows Securities not held in Assets, permits to filter them.
class WatchlistController < ApplicationController

    ##
    # Generates lists of Securities to watch by utilizing Smr::Watchlist.
    #
    # Naturally each security that is not held in a open position is on the
    # watchlist. There is no reason for the user to add/remove items. There is
    # only one global watchlist in SMR.
    #
    # It provides means to filter, sort and comment the listed items.
    def index
        smr_menu_addsubitem('watchlist', {
            '+ quote'=>:new_watchlist,
        })
        session[:watchlist_query] = params[:q] if params[:q]

        session[:watchlist_exclude_securities] = (params[:exclude_securities] ? params[:exclude_securities] : false)
        @opt_exclude_securities = session[:watchlist_exclude_securities]

        session[:watchlist_provides_collateral] = (params[:provides_collateral] ? params[:provides_collateral] : false)
        @opt_provides_collateral = session[:watchlist_provides_collateral]

        session[:watchlist_provides_cashflow] = (params[:provides_cashflow] ? params[:provides_cashflow] : false)
        @opt_provides_cashflow = session[:watchlist_provides_cashflow]

        session[:watchlist_provides_huge_coupon] = (params[:provides_huge_coupon] ? params[:provides_huge_coupon] : false)
        @opt_provides_huge_coupon = session[:watchlist_provides_huge_coupon]

        session[:watchlist_provides_subannual_payments] = (params[:provides_subannual_payments] ? params[:provides_subannual_payments] : false)
        @opt_provides_subannual_payments = session[:watchlist_provides_subannual_payments]

        session[:watchlist_matures_within_year] = (params[:matures_within_year] ? params[:matures_within_year] : false)
        @opt_matures_within_year = session[:watchlist_matures_within_year]

        @watchlist = Smr::Watchlist.new(
            current_user,
            smr_browse_date,
            :ransack_query=>session[:watchlist_query],
            :exclude_securities=>(session[:watchlist_exclude_securities] ? smr_securities_in_open_positions_list : false),
            :provides_collateral=>@opt_provides_collateral,
            :provides_cashflow=>@opt_provides_cashflow,
            :provides_huge_coupon=>@opt_provides_huge_coupon,
            :provides_subannual_payments=>@opt_provides_subannual_payments,
            :matures_within_year=>@opt_matures_within_year,
        )
        @securities = smr_securities_list

        if params[:intraday_data] then
            @have_intraday_data = params[:intraday_data].to_i
            @intraday_data = Quote.where(:id_security=>params[:intraday_data].to_i)
                             .where(:date=>smr_browse_date.beginning_of_day.to_i..smr_browse_date.to_i)
                             .order(date: :desc)
        end
    end

    ##
    # handle new/updated Quote
    def new
        self.index
        if params[:id] then
            @quote = Quote.where(:id=>params[:id]).first
        else @quote = Quote.new(:date_last=>Time.now.to_i) end

        render :index
    end

    ##
    # handles creates and updates for Quote
    def create
        if params[:quote][:id].to_i > 0 then
            q = Quote.where(:id=>params[:quote][:id]).first
        else q = Quote.new(quote_params) end

        q.date_last = Time.parse(params[:time]).to_i if params[:time]
        q.exchange = 'Quotation by SMR User' if q.exchange.empty?

        if params[:quote][:id].to_i > 0 then
            q.update(quote_params)
        else q.save end

        # FIXME: bug in rails?
        # - this :notice flash message it lost when using redirect_to
        # - it is not lost if the default template is rendered: app/views/asset/create.html.erb
        #redirect_to assets_path, :notice=>q.errors.full_messages.uniq.join(', ')
        flash.notice = 'Adding Quote failed: ' + q.errors.full_messages.uniq.join(', ') if not q.errors.empty?

        if URI(request.referer).path == new_asset_path then
            index
            render :index
        else redirect_to :back end
    end
end
