#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# The Workdesk handles a stack of research work for each User.
#
# It can contain Security and/or Organization objects.
class WorkdeskController < ApplicationController

    ##
    # List all things on the stack of current User.
    def index
        @stack = Workdesk.where(:id_user=>@current_user.id).order(:date_added=>:desc)
    end

    ##
    # Add item to Workdesk.
    def add
        columns = { :id_user=>@current_user.id, :date_added=>Time.now.to_i }
        columns.merge!( params.permit(:id_security, :id_organization) )

        if Workdesk.create columns
            flash.notice = 'Added to Workdesk.'
        else flash.notice = 'Is on Workdesk already.' end
        redirect_to :back
    end

    ##
    # Drop item from Workdesk.
    def drop(id=false)
        id = params[:id] unless id
        if Workdesk.destroy_all({:id_user=>@current_user.id, :id=>id})
            flash.notice = 'Dropped from Workdesk.'
        else flash.alert = 'Failed to drop item form Workdesk' end
        redirect_to :back
    end

    ##
    # Update status of Workdesk item or #drop it.
    def update
        redirect_to :back unless wdi = params[:workdesk]
        if params[:save]
            if i = Workdesk.where(:id=>wdi[:id], :id_user=>@current_user.id).first
                i.comment = wdi[:comment]
                i.save!
            end
            redirect_to :back
        elsif params[:drop]
            drop wdi[:id]
        end
    end

end
