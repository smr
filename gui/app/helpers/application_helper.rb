#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

require 'percentage'

##
# Collection of helper methods for use in all views.
module ApplicationHelper

    ##
    # Date the user is currently browsing, as Time, positioned at
    # end of day (23:59:59)
    #
    # use :keep_current_time=>true to take the time part of Time.now and apply
    # it to the browse date
    def smr_browse_date(options={:keep_current_time=>false})
        bd = if session[:smr_browse_date].nil? then Time.new
             else Time.at(session[:smr_browse_date]) end

        if options[:keep_current_time]
            bd.beginning_of_day + (Time.now - Time.now.beginning_of_day)
        else bd end
    end

    ##
    # Place the users us browsing as +String+.
    def smr_browse_position
        controller.class.to_s.sub('Controller', '')
    end

    ##
    # Returns change from +val1+ to +val2+ in percent, as smr_humanize()d String.
    def percentage_change(val1, val2)
        return nil if val1.zero? or val2.blank?
        smr_humanize(Percentage.change(val1, val2))
    end

    ##
    # Returns percentage of +val+ from +base+, as smr_humanize()d String.
    def percentage_of(base, val)
        return nil if base.zero? or not val
        smr_humanize(BigDecimal(val.to_s).as_percentage_of(base))
    end

    ##
    # Humanize a number in ways useful in SMR.
    #
    # This method is written to return as quick as possible because it is used
    # many times in almost every view.
    #
    # Options:
    # - :scientific notation, ie 1000 => 1.0e3
    # - :shortword as spoken but short form, ie 1000 = 1 Kilo => 1k
    # - :scale to force it to N digits, no matter what
    # - :date_with_time adds time information, default for showing a Time
    #   is YYYY-MM-DD
    # - :date_time_only shows only the time information from given Time
    # - :time_for_humans is fuzzy wording relative to #smr_browse_date
    # - :seconds_for_humans converts a number into a String of hours, minutes,
    #    seconds. For example: 12345 becomes '3 hours 25 minutes 45 seconds'.
    #
    def smr_humanize(n, options={
            :scientific=>false, :shortword=>false, :scale=>false,
            :date_with_time=>false, :date_time_only=>false,
            :time_for_humans=>false, :seconds_for_humans=>false,
        })
        unit = String.new
        format = '%s'
        skip_rounding=false

        # handle 'inappropriate' data quickly
        return '-' if n == 0.0 or n == false
        return '' if n == ''
        return n if n.is_a?(String)

        # decide whether to round
        skip_rounding = true if n.is_a?(Integer) and n.abs < 1000
        skip_rounding = true if (n.nil? or n == false)

        if n.is_a?(Percentage)
            unit = '%'
            if options[:scale]
                format = "%.#{options[:scale]}f"
            else
                format = if n.to_f < 10 then '%.1f' else '%i' end
            end
            skip_rounding = true           
        end

        if n.is_a?(Time)
            return '-' if n.to_i == 0

            dateformat = ''
            if options[:time_for_humans]
                return 'Today' if n.between? smr_browse_date.beginning_of_day, smr_browse_date.end_of_day
                return 'Yesterday' if n.between? (smr_browse_date - 1.day).beginning_of_day, (smr_browse_date - 1.day).end_of_day
                return n.strftime('%Y') if n.between?(n.end_of_year-1.day, n.end_of_year)
                return n.strftime('%%Y Q%i' % ((n.month - 1) / 3 + 1) ) if n.between?(n.end_of_quarter-1.day, n.end_of_quarter)
                return n.strftime('%B') if n.between?(n.end_of_month-1.day, n.end_of_month)
                dateformat = '%%A, %s' % n.day.ordinalize
                dateformat += ' %B' if n.month!=smr_browse_date.month or n.year!=smr_browse_date.year
                dateformat += ' %Y' if n.year != smr_browse_date.year
            else
                dateformat = if options[:date_with_time] then '%Y-%m-%d %H:%M'
                    elsif options[:date_time_only] then '%H:%M:%S'
                    else '%Y-%m-%d'
                end
            end
            return n.strftime(dateformat)
        end

        if options[:shortword]
            skip_rounding = true
            n = number_to_human(
                n,
                :units=>{
                    :unit=>'', :thousand=>'k', :million=>'m', :billion=>'b',
                    :trillion=>'t', :quadrillion=>'q'
                }
            )
        end

        if options[:seconds_for_humans]
            return [[60, :seconds], [60, :minutes], [24, :hours], [1000, :days]].map{ |count, name|
                if n > 0
                  n, v = n.divmod(count)
                  v > 0 ? "#{v.to_i} #{name}" : nil
                end
            }.compact.reverse.join(' ')
        end
 
        # do generic rounding if necessary
        if not skip_rounding and not options[:scale]
            case n.abs
                when 0..1 then format = '%.4f'
                when 1..9 then format = '%.3f'
                when 10..99 then format = '%.2f'
                when 100..1000 then format = '%.1f'
            else
                format = if options[:scientific] then '%.3e' else '%i' end
            end
        elsif options[:scale]
            format = "%.#{options[:scale]}f"
        end

        # return a String
        (format + '%s') % [n, unit]
    end

    ##
    # Returns menu to browse SMR functionality in HTML format, see
    # @smr_menu_items and smr_menu_addsubitem().
    # TODO: supports one sublevel only, do we ever need more?
    def smr_menu
        content_tag(:ul, nil, :id=>'smr_menu') {
          @smr_menu_items.reduce('') { |ctag, top|
            if top.second.is_a?(Hash) then
                # sub level
                ctag << content_tag(:li) do
                    link_to(top.first, top.second.delete('_toplink')) + 
                    content_tag(:ul, nil) do
                        top.second.reduce('') { |c, sub|
                            c << content_tag(:li, link_to(sub.first, sub.second))
                        }.html_safe
                    end
                end
            else
                # top level
                ctag << content_tag(:li, link_to(top.first, top.second))
            end
          }.html_safe
        }
    end

    ##
    # Create HTML container with page links.
    #
    # It will be positioned at +current_page+ with a maximum of +num_pages+
    # and each link will direct to +basepath/?page=N+.
    def smr_paginate(current_page, num_pages, basepath)
	    JustPaginate.page_navigation(current_page, num_pages) do |p|
            '%s/?page=%i' % [basepath, p]
        end.html_safe
    end

    ##
    # Profit/Loss status number :n as text for humans to read or CSS class
    # name.
    def smr_profitloss_status(n, css=false)
        if n.is_a?(FalseClass) or n == 0
            s='Unvalued'
        elsif n.between?(-150, 200)
            s='Even'
        elsif n > 0.0
            s='Profit'
        elsif n < 0.0
            s='Loss'
        end

        if css then s.downcase else s end
    end

    ##
    # Highlight +pattern+ in +string+.
    #
    # All occurences of +pattern+ will be surrounded with by a
    # <tt><em class="pattern_match">...</em></tt> HTML tag. The matching is
    # case insensitive.
    #
    # How the highlighting actually looks if defined in the +application.css+
    # stylesheet.
    #
    # If no +pattern+ is given (or +pattern+ is false), then +string+
    # is returned without modification.
    def smr_highlight(string, pattern=false)
        return string if pattern.blank?
        if matched = /.*(#{pattern}).*/i.match(string)
            string.gsub(matched[1], '<em class="pattern_match">%s</em>' % matched[1]).html_safe
        else string end
    end
end
