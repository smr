#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
module PositionsHelper

    ##
    # calculates profit/loss for a single PositionRevision
    def revision_profitloss(current_quote, shares, purchase_quote)
        return 0 if current_quote.is_a?(FalseClass)
        (current_quote - purchase_quote ) * shares
    end

end
