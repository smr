#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# Collection of helper methods related to Quoterecord objects.
module QuoterecordsHelper

    ##
    # CSS class reflecting the state of :quoterecord.
    #
    # Its +nil+ if there is nothing special about that record or if the
    # record does not belong to :current_column
    #
    # Possible classes are +pivotalpoint+, +signal+, +uphit+ and +downhit+ as
    # well as any combination of them, separated by space.
    def make_css_class(quoterecord, current_column)
        raise 'quoterecord must be a Quoterecord' unless quoterecord.is_a?(Quoterecord)

        return nil unless quoterecord.is_column?(current_column)

        klass = Array.new
        klass << 'pivotalpoint' if quoterecord.is_pivotal_point
        klass << 'signal' if quoterecord.is_signal
        klass << 'uphit' if quoterecord.is_uphit
        klass << 'downhit' if quoterecord.is_downhit

        klass.join(' ') || nil
    end

end
