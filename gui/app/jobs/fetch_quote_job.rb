#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
module Smr #:nodoc:

    ##
    # Runs Security#fetch_quote and returns whatever it returns.
    #
    # Supply a Security as first argument.
    #
    # Hint: Backburner is a good backend choice for production. See
    # https://github.com/nesquena/backburner
    #
    # Its usually issued by the +smr:update_quotes+ Rake test.
    class FetchQuoteJob < ActiveJob::Base
      queue_as :quote_update

      def perform(*args)
        args.first.update_quote
      end
    end

end # module
