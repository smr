#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
require 'smr/link'

##
# Refer to something in the web, owned by a User.
class Bookmark < ActiveRecord::Base
    include Smr::Extensions::DateTimeWrapper

	belongs_to :User,         :foreign_key=>:id_user
    belongs_to :Security,     :foreign_key=>:id_security
    belongs_to :Organization, :foreign_key=>:id_organization
    belongs_to :Portfolio,    :foreign_key=>:id_portfolio
    belongs_to :FigureVar,    :foreign_key=>:id_figure_var

    before_create :set_date_added
    before_validation :convert_blank_ids

    # Models that can have one or more Bookmark
    SUPPORTED_MODELS = [ Organization, Security, Portfolio, FigureVar, User ]

    # data validations
    validates :id_user, :name, presence: true
    validates :id_user, numericality: { greater_than_or_equal_to: 0 }
    validates :name, length: { minimum: 2 }
    validates :url, presence: true
    validates :url, format: { with: URI.regexp }, if: 'url.present?'

    scope :for_user, lambda { |id|
        where(:id_user=>[0, id]).order(:rank=>:asc)
    }
    scope :for_object, lambda { |model_class, id|
        return false unless id  # catch Security.new.bookmarks_for_form
        model_class = 'figure_var' if model_class == FigureVar   # quick hack
        where('id_%s = %i' % [ model_class.to_s.downcase, id])
    }

    ##
    # information quality rank
    # NOTE: do NOT change the relations, just ADD! there is existing data.
    enum rank: {
        :norank=>0,
        :primary=>1, :secondary=>2, :tertiary=>3, :discussion=>4,
        :opinion=>10
    }

    ##
    # Returns human readable String describing :rank. Lookup may be given as
    # symbol, as string or as numerical index.
    def self.describe_rank(lookup)
        descriptions = {
            :norank      => 'Not Ranked',
            :primary     => 'Primary Information Source',
            :secondary   => 'Second Grade Information',
            :tertiary    => 'Third Grade Information',
            :discussion  => 'Discussions',
            :opinion     => 'Opinions',
        }

        if lookup.is_a?(Numeric) then
            lookup = self.ranks.key(lookup) || ''
        end
        descriptions[lookup.to_sym]
    end

    ##
    # Describes #rank of the instance object.
    def describe_rank
        Bookmark.describe_rank(rank)
    end

    ##
    # Returns a collection of all ranks in their described form.
    #
    # Useful for Select boxes in forms, also see Bookmark#describe_rank. The
    # :as_number option toggles whether symbols or integers are retured as
    # index.
    def self.ranks_for_form(options={ :as_number=>false })
        Bookmark.ranks.map { |r|
            [
                Bookmark.describe_rank(r.first),
                options[:as_number] ? Bookmark.ranks[r.first] : r.first
            ]
        }
    end

    ##
    # Returns collection of Bookmark records converted to Smr::Link.
    #
    # :item is an instance of a class listed in SUPPORTED_MODELS.
    #
    # :user is an authenticated(!) User.
    def self.for(item, user)
        unless SUPPORTED_MODELS.include? item.class
            raise ':item should be one of %s' % SUPPORTED_MODELS.inspect
        end

        item.Bookmark.for_user(user).each do |b|
            Smr::Link.new :url, b.url, b.name
        end
    end

    ##
    # Human-readable representation of Bookmark
    def to_s
        name
    end

    ##
    # Returns collection of objects related to this Bookmark.
    #
    # That is one or more instances of classes from SUPPORTED_MODELS.
    def relations
        SUPPORTED_MODELS.map do |m|
            self.send m.to_s.to_sym
        end.compact
    end


  private

    ##
    # A Bookmark can be assigned but does not have to.
    #
    # Hint: make #select_tag set these fields to blank ('') by using :prompt or
    # :include_blank options.
    def convert_blank_ids
        self.id_security=0       if id_security.blank?
        self.id_organization = 0 if id_organization.blank?
        self.id_portfolio = 0    if id_portfolio.blank?
        self.id_figure_var = 0   if id_figure_var.blank?
    end

    ##
    # Initialize :date_added with current Time.
    def set_date_added
        self.date_added = Time.now.to_i
    end
end
