#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
class Dividend < ActiveRecord::Base
    include Smr::Extensions::DateTimeWrapper

	belongs_to :Security, :foreign_key=>:id_security
    has_many :Position, :foreign_key=>:id_security, :primary_key=>:id_security
    has_one :Position, :foreign_key=>:id, :primary_key=>:id_position
    has_one :Order, :foreign_key=>:id, :primary_key=>:id_order

    # data validations
    validates :id_security, :date_exdate, :paid, :id_position, :id_order, presence: true
    validates :id_security, :date_exdate, :paid, numericality: { greater_than_or_equal_to: 0 }
    validates :id_position, :id_order, numericality: { greater_than_or_equal_to: 1 }

end
