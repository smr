#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
class DocumentAssign < ActiveRecord::Base
	belongs_to :Document, :foreign_key=>:id_document
	belongs_to :Portfolio, :foreign_key=>:id_portfolio
	belongs_to :Position, :foreign_key=>:id_position
	belongs_to :Order, :foreign_key=>:id_order
	belongs_to :Security, :foreign_key=>:id_security
end
