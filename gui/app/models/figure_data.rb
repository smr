#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
class FigureData < ActiveRecord::Base
	belongs_to :FigureVar, :foreign_key=>:id_figure_var
	belongs_to :Security, :foreign_key=>:id_security

    ##
    # possible values for 'period' field
    #
    # keys of this hash are the actual column values while the hash values are
    # a human readable representation
    # <b>ATTENTION</b>: the order matters for viewing
    POSSIBLE_PERIODS = {
	    'year'  => 'Fiscal Year',
	    'q1'    => '1st Quarter',
	    'q2'    => '2nd Quarter',
	    'q3'    => '3rd Quarter',
	    'q4'    => '4th Quarter',
	    'other' => 'Other',
    }

    # data validations
    validates :period, inclusion: { in: POSSIBLE_PERIODS.keys, message: "%{value} is not a valid period name" }

    # returns Time object from unix-timestamp in date column
    def time
        Time.at(date)
    end

    ##
    # returns array of possible values for :period field
    def get_periods
        POSSIBLE_PERIODS.keys
    end

    ##
    # returns human readable +String+ for given period name, see get_periods()
    def translate_period(period=self.period)
        POSSIBLE_PERIODS[period]
    end
end
