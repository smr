#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
class FigureVar < ActiveRecord::Base
    include Smr::Extensions::Link
    include Smr::Extensions::BookmarkTools

	belongs_to :User, :foreign_key=>'id_user'
	has_many   :FigureData
    has_many   :Bookmark, :foreign_key=>:id_figure_var

    accepts_nested_attributes_for :Bookmark

    # data validations
    validates :id_user, :name, presence: true
    validates :id_user, numericality: { greater_than_or_equal_to: 0 }
    validates :name, length: { minimum: 1 }

    scope :for_user, lambda { |id|
        where(:id_user=>[0, id]).order(:name)
    }

    ##
    # tell whether this FigureVar is available to all users
    #
    # Note: this also applies to related FigureData! Systemwide means all users
    # can see, add, modify the variable and the data.
    def is_systemwide?
        true if self.id_user == 0
    end

    def to_s
        name
    end
end
