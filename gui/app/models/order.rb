#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'smr/link.rb'

class Order < ActiveRecord::Base
    include Smr::Extensions::Link
    include Smr::Extensions::DateTimeWrapper

	belongs_to :Position, :foreign_key=>'id_position'
	has_many   :PositionRevision, :foreign_key=>'id_order'
	has_many   :DocumentAssign, :foreign_key=>'id_order'
	has_one    :Portfolio, :through => :Position
	has_one    :Security, :through => :Position
	has_one    :Organization, :through => :Security

    # define scopes and ransack aliases for finding things
    scope :executed, -> { where('quote>0') }
    scope :pending,  -> { where(:quote=>0, :is_canceled=>[false, nil]) }
    scope :canceled, -> { where(:is_canceled=>true) }

    ransack_alias :ransackcolumns, :comment_or_Security_brief_or_Security_symbol_or_Organization_name

    # structural validations
    validates_associated :Position

    # data validations
    validates :type, inclusion: { in: %w(buy sale), message: "%{value} is not a valid Order type" }
    validates :type, presence: true
    validates :addon, inclusion: { in: %w(none StopLoss StopBuy), message: "%{value} is not a valid Order AddOn option" }

    validates :shares, numericality: { greater_than: 0 }
    validates :limit, numericality: { greater_than: 0 }
    validates :provision, numericality: true
    validates :courtage, numericality: true
    validates :expense, numericality: true

    self.inheritance_column = :none

    ##
    # Time when it was executed or False in case it is not
    def time_executed
        return false unless is_executed?
        self.PositionRevision.first.time_created
    end

    # returns Order status as human readable string
    def status
        s = Array.new
        _is_expired = false

        if is_canceled
            s << 'canceled'
        elsif expire > 0 and not is_executed?
            if time_expire > Time.now then
                s << 'will expire on %s' % time_expire.to_date
            else
                s << 'expired on %s' % time_expire.to_date
                _is_expired = true
            end
        end

        if not _is_expired and quote == 0
            s << 'active'
        elsif quote > 0
            s << 'executed'
        end

        if accrued_interest > 0
            if is_purchase?
                s << 'paid %.2f interest' % accrued_interest
            else
                s << 'received %.2f interest' % accrued_interest
            end
        end

        return s.join(', ')
    end

    # tells humans what this Order is about
    def to_s
        if triggered_by_order then
            '%s for %s.' % [
                self.PositionRevision.first.describe_type,
                get_trigger_order.Position.Security
            ]
        elsif self.Position.is_cash_position?
            self.PositionRevision.first.describe_type
        else
            addon = if self.addon!='none' then ' ' + self.addon else nil end
            limit = if self.limit > 0 then 'limit %.4f'%self.limit else 'at market' end
            quote = if self.quote > 0 then 'at quote %.4f'%self.quote else false end

            '%s %.2f shares %s%s' % [
                self.type, self.shares, (quote||limit), addon
            ]
        end
    end

    ##
    # tell whether this Order purchases shares when being executed
    def is_purchase?
        type == 'buy'
    end

    ##
    # tell whether this Order sells shares when being executed
    def is_sale?
        type == 'sale'
    end

    ##
    # tell whether this Order was executed
    def is_executed?
        quote > 0
    end

    ##
    # tell whether this Order has been canceled
    def is_canceled?
        is_canceled
    end

    ##
    # tell whether this Order is still pending the decision of being executed,
    # canceled or expire.
    def is_pending?
        quote == 0 and not is_canceled
    end

    ##
    # volume of this order
    def volume
        self.shares * self.quote
    end

    ##
    # total charges recorded on this order
    def charges
        self.provision + self.courtage + self.expense
    end

    ##
    # charges per 1k of order volume
    def charges_per_kilo
        self.charges / (self.volume / 1000)
    end

    ##
    # Tell it there is a Document assigned.
    def has_document?
        if self.DocumentAssign.first.is_a?(DocumentAssign) then true else false end
    end
    
    ##
    # Document.id of assigned Smr::UploadedFile or +nil+. Use #has_document?
    # first.
    def id_document
        self.DocumentAssign.first.id_document
    end

    ##
    # return Order that triggered /this/ Order or False.
    def get_trigger_order
        if triggered_by_order then
            Order.find(triggered_by_order)
        else false end
    end
end
