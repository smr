#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
class Organization < ActiveRecord::Base
    include Smr::Extensions::Link
    include Smr::Extensions::BookmarkTools

	has_many :Security,           :foreign_key=>:id_organization
	has_many :SecurityDerivative, :foreign_key=>:id_issuer
    has_many :Workdesk,           :dependent=>:destroy, :foreign_key=>:id_security
    has_many :Bookmark,           :foreign_key=>:id_organization

    accepts_nested_attributes_for :Bookmark

    ##
    # human readable String identifying this Organization
    def to_s
        name
    end
end
