#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
class Portfolio < ActiveRecord::Base
    include Smr::Extensions::Link
    include Smr::Extensions::DateTimeWrapper
    include Smr::Extensions::BookmarkTools

	belongs_to :User,        :foreign_key=>:id_user
	has_many :Position,      :foreign_key=>:id_portfolio
	has_many :DocumentAssign
    has_many :Bookmark,      :foreign_key=>:id_portfolio

    accepts_nested_attributes_for :Bookmark

    # Scope like class method to select open positions of :id_user.
    def self.with_open_positions(id_user)
        where(:id_user=>id_user)
        .joins(:Position).where(:position=>{:date_closed=>0}).group(:id)
    end

    def to_s
        name
    end
end
