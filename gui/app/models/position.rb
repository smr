#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'smr/link.rb'

##
# A Position record is basis for a Smr::AssetPosition.
#
# Note that the  :date_record_created (:time_record_created) column does NOT
# tell the time of the first transaction NOR the beginning of holding the
# Security. Its just when the record was created, that is when the User decided
# to track the holding with SMR.
#
# Therefore do NOT use this for time-browsing. A Position (and
# a Smr::AssetPosition) may have revisions years before the date saved here.
class Position < ActiveRecord::Base
    include Smr::Extensions::Link
    include Smr::Extensions::DateTimeWrapper

    belongs_to :Portfolio, :foreign_key=>:id_portfolio
    belongs_to :Security, :foreign_key=>:id_security
    has_many :PositionRevision, :foreign_key=>:id_position

    # define scopes for easier finding
    scope :open, -> { where(:date_closed => false) }

    scope :closed, lambda { |time_since, time_until|
        _until = 'and position.date_closed <= %i' % time_until
        where 'position.date_closed>%i %s' % [ time_since, _until ]
    }

    ##
    # Tell if Position is closed, regardless when that happened.
    def is_closed?
        date_closed > 0
    end

    # tell whether this is a cash deposit or a regular security
    def is_cash_position?
        id_security == Smr::ID_CASH_SECURITY
    end
end

