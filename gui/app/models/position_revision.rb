#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
class PositionRevision < ActiveRecord::Base
    include Smr::Extensions::DateTimeWrapper

    self.inheritance_column = :none
	belongs_to :Order, :foreign_key=>'id_order'
	belongs_to :Position, :foreign_key=>'id_position'
    has_one :Stock, :through => :Position
    has_one :Portfolio, :through => :Position

    ##
    # types of revisions supported
    # NOTE: do NOT change the relations! just ADD new numbers!
    enum type: {
        :shares=>0, :order_booking=>1, :cash_booking=>2, :dividend_booking=>3,
        :redemption_booking=>4, :salary_booking=>5
    }

    # data validations
    validates :id_order, :id_position, numericality: { greater_than: 0 }

    ##
    # Returns human readable String describing :type. Lookup may be given as
    # symbol, as string or as numerical index.
    def PositionRevision.describe_type(lookup)
        descriptions = {
            :shares           => 'Share Deposit / Discharge',
            :order_booking    => 'Money Equivalent of Order',
            :cash_booking     => 'Cash Deposit / Withdrawal',
            :dividend_booking => 'Dividend / Interest',
            :redemption_booking => 'Redemption',
            :salary_booking   => 'Salary',
        }

        if lookup.is_a?(Numeric) then
            lookup = self.types.key(lookup) || ''
        end
        descriptions[lookup.to_sym]
    end

    ##
    # Describes #type of the instance object.
    def describe_type
        PositionRevision.describe_type(type)
    end

end
