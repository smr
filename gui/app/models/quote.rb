#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
class Quote < ActiveRecord::Base
    include Smr::Extensions::DateTimeWrapper

	belongs_to :Stock, :foreign_key=>'id_security'
	has_one    :Quoterecord

    # data validations
    validates :id_security, :date_last, :last, presence: true
    validates :id_security, :date_last, :last, numericality: { greater_than: 0 }

    ##
    # CSS class as String, depending on age of Quote.
    def css_class(date=Time.now, options={:intraday=>false})
        return 'quote_empty' if last == 0
        c = false

        if options[:intraday]
            if time_last > date - 20.minutes
                c = 'quote_within20min'
            elsif time_last > date - 1.hour
                c = 'quote_withinhour'
            end
        else
            if time_last > date.beginning_of_day
                c = 'quote_today'
            elsif time_last > date.weeks_ago(1)
                c = 'quote_withinweek'
            end
        end

        if c then c else 'quote_outdated' end
    end
end
