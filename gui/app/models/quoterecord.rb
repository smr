#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
class Quoterecord < ActiveRecord::Base
    include Smr::Extensions::DateTimeWrapper

	belongs_to :User, :foreign_key=>:id_user
	belongs_to :Quote, :foreign_key=>:id_quote
	belongs_to :Security, :foreign_key=>:id_security
	has_one    :Organization, :through => :Security

    ##
    # possible values for 'column' field
    #
    # keys of this hash are the actual column values while the hash values are
    # a human readable representation
    # <b>ATTENTION</b>: the order matters for viewing
    POSSIBLE_COLUMNS = {
        'secondary_rally'    => 'Secondary Rally',
        'natural_rally'      => 'Natural Rally',
        'upward'             => 'Upward Trend',
        'downward'           => 'Downward Trend',
        'natural_reaction'   => 'Natural Reaction',
        'secondary_reaction' => 'Secondary Reaction',
    }

    # data validations
    validates :id_user, :id_security, :id_quote, presence: true
    validates :id_user, :id_security, :id_quote, :date_created, numericality: { greater_than: 0 }
    validates :column, inclusion: { in: POSSIBLE_COLUMNS.keys, message: "%{value} is not a valid column name" }

    # define scopes and ransack aliases for finding things
    ransack_alias :ransackcolumns, :comment_or_Security_brief_or_Security_symbol_or_Organization_name

    ##
    # returns array of possible values for :column field
    def self.get_columns
        POSSIBLE_COLUMNS.keys
    end

    ##
    # returns human readable +String+ for given column name, see get_columns()
    def self.translate_column(column=self.column)
        POSSIBLE_COLUMNS[column]
    end

    ##
    # tell whether this record has been sorted in to column
    def is_column?(column_name)
        self.column == column_name
    end

    ##
    # tell a human what happened
    def to_s
        msg = Array.new
        msg << 'UP hit' if self.is_uphit
        msg << 'DOWN hit' if self.is_downhit 
        msg << 'classified quote' if msg.empty?

        msg << 'on %s' % self.Security.to_s

        msg << 'into %s column' % self.class.translate_column(self.column)

        msg << 'which is a pivotal point' if self.is_pivotal_point
        msg << 'and triggered SIGNAL' if self.is_signal

        msg.join(' ')
    end
end
