#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
class QuoterecordRule < ActiveRecord::Base
    include Smr::Extensions::DateTimeWrapper

	belongs_to :User, :foreign_key=>'id_user'

    # data validations
    validates :id_user, :name, :rule, :related_columns, :date_last_modified, presence: true
    validates :date_last_modified, numericality: { greater_than: 0 }
    validate :related_columns_valid

    # returns :related_columns field as array
    def get_related_columns
       if self.related_columns then self.related_columns.split('|')
        else Array.new end
    end

    # compact given column names into :related_columns field
    def set_related_columns(array_of_column_names)
        self.related_columns = array_of_column_names.uniq.join('|')
    end    

private

    ##
    # custom validator to make sure :related_columns field only talks about
    # columns a Quoterecord object knows of.
    def related_columns_valid
        valid_cols = Quoterecord.get_columns
        self.get_related_columns.each do |c|
            if not valid_cols.include?(c) then errors.add(:related_columns, 'a column named "%s" is not accepted by Quoterecord class' % c) end
        end
    end
end
