#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'smr/link.rb'

class Security < ActiveRecord::Base
    include Smr::Fetch
    include Smr::Extensions::Link
    include Smr::Extensions::DateTimeWrapper
    include Smr::Extensions::BookmarkTools
    include Comparable

    self.inheritance_column = :none

    has_one  :Organization,            :foreign_key=>:id, :primary_key=>:id_organization
	has_one  :SecurityQuotesource,     :foreign_key=>:id, :primary_key=>:id_security_quotesource
	has_one  :SecuritySymbolextension, :foreign_key=>:id, :primary_key=>:id_security_symbolextension

    has_one  :SecurityBond,  :dependent=>:destroy, :autosave=>true, :foreign_key=>:id, :primary_key=>:id_security_bond
    has_one  :SecurityStock, :dependent=>:destroy, :autosave=>true, :foreign_key=>:id, :primary_key=>:id_security_stock
    has_one  :SecurityFund,  :dependent=>:destroy, :autosave=>true, :foreign_key=>:id, :primary_key=>:id_security_fund
    has_one  :SecurityMetal, :dependent=>:destroy, :autosave=>true, :foreign_key=>:id, :primary_key=>:id_security_metal
    has_one  :SecurityDerivative, :dependent=>:destroy, :autosave=>true, :foreign_key=>:id, :primary_key=>:id_security_derivative
    has_one  :SecurityIndex, :dependent=>:destroy, :autosave=>true, :foreign_key=>:id, :primary_key=>:id_security_index

	has_many :Position,       :dependent=>:restrict_with_exception, :foreign_key=>:id_security
	has_many :FigureData,     :dependent=>:restrict_with_exception, :foreign_key=>:id_security
	has_many :Quoterecord,    :dependent=>:restrict_with_exception, :foreign_key=>:id_security
	has_many :Dividend,       :dependent=>:restrict_with_exception, :foreign_key=>:id_security
	has_many :DocumentAssign, :dependent=>:restrict_with_exception, :foreign_key=>:id_security
	has_many :Quote,          :dependent=>:destroy,                 :foreign_key=>:id_security
	has_many :QuoterecordThreshold, :dependent=>:destroy,           :foreign_key=>:id_security
	has_many :Split,          :dependent=>:destroy,                 :foreign_key=>:id_security
	has_many :Workdesk,       :dependent=>:destroy,                 :foreign_key=>:id_security
    has_many :Bookmark,                                             :foreign_key=>:id_security

    accepts_nested_attributes_for :Bookmark

    # data validations
    validates :id_organization, :symbol, presence: true
    validates_uniqueness_of :symbol

    # callbacks
    before_update :sanitize_reaper_config, :on=>[ :update ]

    # Caches to reduce queries to one. See #get_type and #last_quote. This is faster
    # then relying on ActiveRecords cache since that one adds latency.
    attr_reader :typemodel
    attr_reader :lastquote

    # define scopes and ransack aliases for finding things
    # NOTE: also check #is_expired? here, but how?
    scope :is_actively_traded, lambda { |date|
        where.not(:id=>Smr::ID_CASH_SECURITY, :date_trading_ceased=>1..date.to_i)
    }

    ransack_alias :ransackcolumns, :brief_or_symbol_or_Organization_name

    ##
    # Tell whether trading has been stopped.
    #
    # If :date is given the result tells whether trading was stopped before
    # that time.
    #
    # Inspect :trading_ceased_reason for why it was stopped.
    def has_trading_ceased?(date=false)
       cd = read_attribute(:date_trading_ceased)
       if date.is_a?(Time) and cd > 0
          (date.to_i >= cd)
       else (cd > 0) end
    end

    ##
    # Returns typemodel instance available for this Security.
    def get_type
        return @typemodel if @typemodel
        @typemodel = if id_security_stock > 0
                self.SecurityStock
            elsif id_security_bond > 0
                self.SecurityBond
            elsif id_security_fund > 0
                self.SecurityFund
            elsif id_security_metal > 0
                self.SecurityMetal
            elsif id_security_derivative > 0
                self.SecurityDerivative
            elsif id_security_index > 0
                self.SecurityIndex
            else
                false
            end
    end

    ##
    # Tell whether there is a :typemodel instance available.
    #
    # This does not create the instance and is therefore much faster then
    # #get_type. It just says True or False.
    def has_type_model?
        (id_security_stock + id_security_bond + id_security_fund + id_security_metal + id_security_derivative + id_security_index > 0)
    end

    ##
    # Create typemodel related to this Security. Pass :type as symbol or
    # String, use #get_type to obtain it without knowing its type.  False is
    # returned if a typemodel exists already or if :type is not supported.
    def create_type(type)
        type = type.to_sym
        return false if has_type_model?
        return false unless Security.types.include?(type)
        case type
            when :stock then
                t = SecurityStock.create!
                write_attribute(:id_security_stock, t.id)
            when :bond  then
                t = SecurityBond.create!
                write_attribute(:id_security_bond, t.id)
            when :fund  then
                t = SecurityFund.create!
                write_attribute(:id_security_fund, t.id)
            when :metal  then
                t = SecurityMetal.create!
                write_attribute(:id_security_metal, t.id)
            when :derivative  then
                t = SecurityDerivative.create!
                write_attribute(:id_security_derivative, t.id)
            when :index  then
                t = SecurityIndex.create!
                write_attribute(:id_security_index, t.id)
        end
        save!
        t
    end

    ##
    # Supported typemodels, also see #describe_type
    def Security.types
        [ :unknown, :cash, :stock, :bond, :fund, :metal, :derivative, :index ]
    end

    ##
    # Returns type of this Security as symbol
    def type
        return :unknown if not has_type_model?
        if id_security_stock > 0
            :stock
        elsif id_security_bond > 0
            :bond
        elsif id_security_fund > 0
            :fund
        elsif id_security_metal > 0
            :metal
        elsif id_security_derivative > 0
            :derivative
        elsif id_security_index > 0
            :index
        end
    end

    ##
    # Returns human readable String describing :type. It may be given as
    # symbol or as string.
    def Security.describe_type(type=:unknown)
        case type.to_sym
            when :stock then SecurityStock.describe
            when :bond  then SecurityBond.describe
            when :fund  then SecurityFund.describe
            when :metal then SecurityMetal.describe
            when :index then SecurityIndex.describe
            when :derivative then SecurityDerivative.describe
            when :cash  then 'Cash Deposit'         # no existing as type model yet
            else 'Unknown Type of Security'
        end
    end

    ##
    # Describes #type of the instance object.
    def describe_type
        Security.describe_type(type)
    end

    ##
    # Returns a collection of all types in their described form.
    #
    # Useful for Select boxes in forms, also see Security#describe_type.
    def self.types_for_form
        Security.types.map { |t|
            [ Security.describe_type(t), t ]
        }
    end

    ##
    # Constructs human readable name from knowledge about the Organization, the
    # :type and what is known by the Security* model.
    #
    # This is highly dynamic but should create consistent naming of Security
    # records throughout the application. Concept is: Users specify the
    # parameters, SMR constructs the names.
    def to_s(options={ :with_symbol=>false })
        org = if id_organization != Smr::ID_UNIVERSE_ORGANIZATION then
                self.Organization.name
              else brief end

        a = [ org ]
        a << get_type if has_type_model?
        a << '-- [%s]' % symbol if options[:with_symbol]

        a.join(' ').split(' ').compact.uniq.join(' ')
    end

    ##
    # wrapper for #to_s(:with_symbol=>true) for use as :symbol reference
    def to_s_with_symbol
        to_s :with_symbol=>true
    end

    ##
    # compares by Organization#name and #type
    def <=>(other)
       name1 = ( id_organization != Smr::ID_UNIVERSE_ORGANIZATION ? self.Organization.name : brief )
       name2 = ( other.id_organization != Smr::ID_UNIVERSE_ORGANIZATION ? other.Organization.name : other.brief )
       comp = (name1 <=> name2)
       if (name1 <=> name2).zero?
          (type <=> other.type)
       else comp end
    end

    ##
    # provide wrappers calling on the type class model
    #
    # Fails gracefully (with 0) when the Security has no typeclass model.
    def method_missing(typeclassmethod, *args, &block)
        return 0 if not has_type_model?
        get_type.public_send(typeclassmethod.to_sym, *args, &block)
    end

    ##
    # Finds most recent Quote, that is the one closest but before :date.
    #
    # In case no Quote is found, a new object for this security is returned,
    # unless the :false_if_not_found option is True. In that case False will
    # be returned.
    def last_quote(date=Time.now, options={ :no_caching=>false, :false_if_not_found=>false })
        return @lastquote if @lastquote unless options[:no_caching]
        @lastquote = self.Quote.where('date_last <= %i' % date)
                   .order(:date_last=>:desc).limit(1).first
        return @lastquote if @lastquote
        return Quote.new(:id_security=>id, :date_last=>date.to_i) unless options[:false_if_not_found]
    end

    ##
    # Reader and Writer for convert :collateral_coverage_ratio to/from Percentage
    def collateral_coverage_ratio=(ratio)
        write_attribute(:collateral_coverage_ratio, ratio.to_f)
    end
    def collateral_coverage_ratio
        Percentage.new read_attribute(:collateral_coverage_ratio)
    end


  protected

    ##
    # set useful values for all reaper_* columns
    # Optionally preset a :reaper_name. This puts a bias upon the next
    # #update_quote.
    def reset_reaper_config reaper_name=nil
        write_attribute :preferred_reaper, reaper_name
        write_attribute :preferred_reaper_fail_count, 0
        write_attribute :preferred_reaper_delay, 0
    end

    def sanitize_reaper_config
        reset_reaper_config if preferred_reaper.blank?
    end

end
