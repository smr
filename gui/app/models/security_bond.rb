#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'percentage'
require 'finance'
require 'cashflowitem'
require 'cashflowstream'

class SecurityBond < ActiveRecord::Base
    include Smr::Extensions::HelperMethods
    include Smr::Extensions::SecurityTypemodelMandatoryMethods
    include Smr::Extensions::DateTimeWrapper

    after_initialize :init_caches

    self.inheritance_column = :none
	has_one :Security, :foreign_key=>:id_security_bond, :inverse_of=>:SecurityBond

    ##
    # types of bond supported (may alter behaviour of some methods)
    # NOTE: do NOT change the relations! just ADD!
    enum type: {
        :fixed_rate=>0, :floating_rate=>1, :zero_rate=>2, :fixtofloat_rate=>3,
        :convertible=>4
    }

    # data validations
#    validates :date_maturity, :date_first_coupon, :date_last_coupon, numericality: { greater_than_or_equal_to: 0 }


    ##
    # Returns human readable String describing :type. Lookup may be given as
    # symbol, as string or as numerical index.
    def SecurityBond.describe_type(lookup)
        descriptions = {
            :fixed_rate      => 'Fixed Interest Rate',
            :floating_rate   => 'Floating Interest Rate',
            :zero_rate       => 'Zero Interest Rate',
            :fixtofloat_rate => 'Fix-to-Float Interest Rate',
            :convertible     => 'Convertible',
        }

        if lookup.is_a?(Numeric) then
            lookup = self.types.key(lookup) || ''
        end
        descriptions[lookup.to_sym]
    end

    ##
    # Describes #type of the instance object.
    def describe_type
        SecurityBond.describe_type(type)
    end

    ##
    # Returns humanreadable String describing the model itself.
    def SecurityBond.describe
        'Debt Ownership'
    end
    def describe
        SecurityBond.describe
    end

    ##
    # Returns a collection of all types in their described form.
    #
    # Useful for Select boxes in forms, also see SecurityBond#describe_type. The
    # :as_number option toggles whether symbols or integers are retured as
    # index.
    def self.types_for_form(options={ :as_number=>false })
        SecurityBond.types.map { |t|
            [
                SecurityBond.describe_type(t.first),
                options[:as_number] ? SecurityBond.types[t.first] : t.first
            ]
        }
    end

    ##
    # interest calculation methods supported
    enum interest_method: {
        :act_act=>0, :act_366=>1, :act_365=>2, :act_360=>3,
        :_30_360=>4, :_30_365=>5, :_30E_360=>6, :_30E_365=>7
    }

    ##
    # Returns human readable String describing the :interest_method. May be
    # given as symbol, as string or as numerical index.
    def SecurityBond.describe_interest_method(lookup)
        descriptions = {
            :act_act => 'ICMA Rule: actual days per month and per year.',
            :act_366 => 'Actual days per month, 366 days per year.',
            :act_365 => 'English: actual days per month, 365 days per year.',
            :act_360 => 'European / French: actual days per month, 360 days per year.',
            :_30_366 => '30 days per month, 366 days per year.',
            :_30_365 => '30 days per month, 365 days per year.',
            :_30E_360 => 'German: 30 days per month, 360 days per year.',
            :_30E_365 => 'German: 30 days per month, 365 days per year.',
        }

        if lookup.is_a?(Numeric) then
            lookup = self.interest_methods.key(lookup) || ''
        end
        descriptions[lookup.to_sym]
    end

    ##
    # Describes #interest_method of the instance object.
    def describe_interest_method
        SecurityBond.describe_interest_method(interest_method)
    end

    ##
    # Returns a collection of all interest methods in their described form.
    #
    # Useful for Select boxes in forms, also see
    # SecurityBond#describe_interest_method. The :as_number option toggles
    # whether symbols or integers are retured as index.
    def self.interest_methods_for_form(options={ :as_number=>false })
        SecurityBond.interest_methods.map { |m|
            [
                SecurityBond.describe_interest_method(m.first),
                options[:as_number] ? SecurityBond.interest_methods[m.first] : m.first
            ]
        }
    end

    ##
    # overwrite :coupon attribute to provide a Percentage
    def coupon
        Percentage read_attribute(:coupon)
    end
    def coupon=(value)
        write_attribute(:coupon, value.to_f)
    end

    ##
    # Human readable String composed of essential properties known by a
    # SecurityBond on its own. Useful as part to compose the name of a
    # Security.
    def to_s
        s = Array.new
        s << '%s' % coupon if coupon > 0

        _redemption_year = if date_maturity <= 0
            'UNDEF'
        elsif redemption_installments == 1
            time_maturity.strftime('%y')
        else
            '%s-%s' % [
                (time_maturity - ((redemption_installments-1) * redemption_interval).months).year,
                time_maturity.strftime('%y')
            ]
        end
        _issue_year = if date_issue > 0
           time_issue.year
        else time_first_coupon.year end
        s << '%s(%s)' % [_issue_year, _redemption_year]


        s << case read_attribute(:type)
            when 1 then 'FlR'
            when 2 then 'Zero'
            when 3 then 'FtF'
            when 4 then 'CV'
        end
        s << currency unless currency.blank? or currency == Smr::DEFAULT_CURRENCY

        s.join ' '
    end

    ##
    # find the next call date, based on :date_first_call and :call_interval
    def time_next_call(date=Time.now)
        return false if date_first_call == 0

        i = 1
        next_call = time_first_call
        until next_call >= date do
            next_call = time_first_call + ((1.year / 12)* call_interval) * i  # using call_interval.months here introduces many days offset
            i += 1
        end
        next_call
    end

    ##
    # mandatory method: Number of tradable items
    def shares
        return 0 if denomination==0 or issue_size==0
        issue_size / denomination
    end

    ##
    # mandatory method: result based on :coupon and :quote.
    def current_yield(quote=false)
        if not quote
            return false if @cy_asked_for_lastquote
            quote = self.Security.last_quote
            @cy_asked_for_lastquote = true if quote.last.zero?
        end
        raise ':quote must be of Quote' if quote and not quote.is_a?(Quote)
        return false if quote.last.zero?
        return false if quote.last >= date_last_coupon
        BigDecimal(coupon.to_s).as_percentage_of(quote.last)
    end

    ##
    # mandatory method: Yield to Call if :time_first_call is known
    def yield_to_call(quote=false)
        if not quote
            return false if @ytc_asked_for_lastquote
            quote = self.Security.last_quote
            @ytc_asked_for_lastquote = true if quote.last.zero?
        end
        raise ':quote must be of Quote' unless quote and quote.is_a?(Quote)
        return false if quote.last.zero? or date_first_call <= 0 or call_interval == 0 or call_price == 0
        return false if quote.date_last >= date_maturity

        return @cache_ytc.fetch(quote.id) if @cache_ytc.has_key?(quote.id)

        cf_stream_call = Smr::CashflowStream.new(
            coupon,
            time_first_coupon,
            :payment_interval=>coupon_interval,
            :split_amount_by_interval=>true,
            :last_payment=>time_first_call,
            :redemption_interval=>call_interval,
            :redemptions=>1,
            :redemption_price=>call_price,
            :last_redemption=>time_next_call(quote.time_last),
            :item_description=>'',
            :id_for_caching=>('call%s%i' % [self.class, (id || 0)]).to_sym,
        ) if date_first_call!=0 and call_price!=0

        # purchase at quote is a cash-outflow
        transactions = []
        transactions << Finance::Transaction.new(
            denomination * (quote.last/100) * -1 ,
            :date=>quote.time_last
        )

        # coupons + redemptions will flow back in
        cf_stream_call.get(denomination, :start=>quote.time_last).each do |c|
            transactions << Finance::Transaction.new(
                c.total, :date=>c.date
            )
        end

        @cache_ytc[quote.id] = begin
               if time_first_call < quote.time_last + 1.year
                   # absolute, when maturity is near
                   gain = transactions.inject(0){|sum,t| sum += t.amount}
                   BigDecimal(gain.to_s).as_percentage_of(transactions.first.amount.abs)
               else
                   # annualized
                   Percentage.new(transactions.xirr(0.1).effective)
               end
           rescue
               # FIXME: see #yield_to_maturity below
#p '==> %s' %  'strange too'
               false
        end
    end

    ##
    # mandatory method: YTM if all coupons and par is payed as planned until
    # :date_maturity
    # False is returned if there is no Quote or :date_maturity is UNDEF (== 0)
    def yield_to_maturity(quote=false)
        if not quote
            return false if @ytm_asked_for_lastquote
            quote = self.Security.last_quote
            @ytm_asked_for_lastquote = true if quote.last.zero?
        end
        raise ':quote must be of Quote' unless quote.is_a?(Quote)
        return false if quote.last.zero? or date_maturity <= 0 or redemption_installments == 0
        return false if quote.date_last >= date_maturity

        return @cache_ytm.fetch(quote.id) if @cache_ytm.has_key?(quote.id)

        transactions = []

        # purchase at quote is a cash-outflow
        transactions << Finance::Transaction.new(
            denomination * (quote.last/100) * -1 ,
            :date=>quote.time_last
        )

        # coupons + redemptions will flow back in
        @cf_stream.get(denomination, :start=>quote.time_last).each do |c|
            transactions << Finance::Transaction.new(
                c.total, :date=>c.date
            )
        end

        @cache_ytm[quote.id] = begin
               if time_maturity < quote.time_last + 1.year
                   # absolute, when maturity is near
                   gain = transactions.inject(0){|sum,t| sum += t.amount}
                   BigDecimal(gain.to_s).as_percentage_of(transactions.first.amount.abs)
               else
                   # annualized
                   Percentage.new(transactions.xirr(0.1).effective)
               end
           rescue
               # FIXME: some Security records raise
               #     'Singular Jacobian matrix. No change at x[0]'
               # from jacobian.rb. Happens with /strange/ :transactions, ie. by
               # giving very high coupon value, dont understand it really.
               false
        end
    end

    ##
    # mandatory method: True if :date is past :date_maturity
    def is_expired?(date=Time.now)
        true if date > time_maturity and date_maturity > 0
    end


    ##
    # mandatory method: return collection of Smr::CashflowItem with
    # future/past/all cashflows created by this SecurityBond.
    #
    # The :amount option states the amount of Security items for which the
    # cashflow is calculated. If not given, the smallest possible amount is
    # used. This is :denomination in the case of bonds, one share in the case
    # of stocks.
    #
    # The :type option allows to filter what is returned. This model supports
    # :none, :dividend_booking and :redemption_booking. Also see
    # PositionRevision#types to have all types of cashflow known to
    # SMR.
    #
    # In case :date_maturity is UNDEF (== 0) the cashflow is projected for 10
    # years from :start_date.
    def cashflow(options={:start_date=>false, :end_date=>false, :amount=>false, :type=>:none, :item_link=>false})
        start_date = options[:start_date] || time_first_coupon
        end_date = options[:end_date]    if options[:end_date]
        end_date = time_maturity + 1.day if not end_date and date_maturity>0
        end_date = start_date + 10.years if not end_date

        last_coupon_payment = (date_last_coupon > 0 ? time_last_coupon : start_date + 10.years)
        nominal_value = options[:amount] || denomination
        filter = options[:type] || :none

        @cf_stream.get(
            nominal_value,
            :start=>start_date, :end=>end_date,
            :filter=>(options[:type] || :none),
            :item_link=>options[:item_link]
        )
    end

    ##
    # mandatory method: inspect :coupon_interval for happening multiple times a year
    def has_subannual_payments?
        coupon_interval < 12
    end

    ##
    # mandatory method: interest accrued since previous coupon
    def accrued_interest(date=Time.now)
        s = @cf_stream.get(denomination, :start=>(date - 1.year), :end=>date, :filter=>:dividend_booking)
        prev_coupon = s.last

        if prev_coupon
            days_since_coupon = (date - prev_coupon.date).to_i / 1.day
            days_in_year = (Date.ordinal(date.year) - Date.ordinal(date.year-1)).to_i  # implies ACT_ACT interest method
            Percentage.new( read_attribute(:coupon) / 100.0 / days_in_year * days_since_coupon )
        else 0 end
   end

 protected

    ##
    # create Smr::CashflowStream instances from SecurityBond attributes and a
    # number of result caches
    def init_caches
        @cache_ytc = Hash.new
        @cache_ytm = Hash.new

        @ytc_asked_for_lastquote = false
        @ytm_asked_for_lastquote = false
        @cy_asked_for_lastquote  = false

        @cf_stream = Smr::CashflowStream.new(
            coupon,
            time_first_coupon,
            :payment_interval=>coupon_interval,
            :split_amount_by_interval=>true,
            :last_payment=>(date_last_coupon <= 0 ? 0 : time_last_coupon),
            :redemption_interval=>redemption_interval,
            :redemptions=>redemption_installments,
            :redemption_price=>redemption_price,
            :last_redemption=>time_maturity,
            :item_description=>self.Security.to_s,
            :id_for_caching=>('%s%i' % [self.class, (id || 0)]).to_sym,
        )
    end

end
