#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

class SecurityDerivative < ActiveRecord::Base
    include Smr::Extensions::HelperMethods
    include Smr::Extensions::SecurityTypemodelMandatoryMethods
    include Smr::Extensions::DateTimeWrapper

    self.inheritance_column = :none
    has_one :Security, :foreign_key=>:id_security_derivative, :inverse_of=>:SecurityDerivative

    ##
    # types of derivatives supported (may alter behaviour of some methods)
    # NOTE: do NOT change the relations! just ADD!
    enum type: {
        :option=>0, :future=>1, :forward=>2, :swap=>3, :credit=>4,
        :warrant=>5, :structured=>6
    }

    ##
    # direction of the underlying to make a profit using the derivative
    enum direction: { :long=>0, :short=>1, :sideways=>2 }

    ##
    # Returns human readable String describing :type. It may be given as
    # symbol, as string or as numerical index.
    def SecurityDerivative.describe_type(lookup)
        descriptions = {
            :option     => 'Option',
            :future     => 'Future',
            :forward    => 'Forward Contract',
            :swap       => 'Swap',
            :credit     => 'Credit Based',
            :warrant    => 'Warrant',
            :structured => 'Structured Certificate',
        }

        if lookup.is_a?(Numeric) then
            lookup = self.types.key(lookup) || ''
        end
        descriptions[lookup.to_sym]
    end

    ##
    # Describes type of the instance object.
    def describe_type
        SecurityDerivative.describe_type(type)
    end

    ##
    # Returns humanreadable String describing the model itself.
    def SecurityDerivative.describe
        'Derivative Contract'
    end
    def describe
        SecurityDerivative.describe
    end

    ##
    # Returns a collection of all types in their described form.
    #
    # Useful for Select boxes in forms, also see SecurityDerivative#describe_type. The
    # :as_number option toggles whether symbols or integers are retured as
    # index.
    def self.types_for_form(options={ :as_number=>false })
        SecurityDerivative.types.map { |t|
            [
                SecurityDerivative.describe_type(t.first),
                options[:as_number] ? SecurityDerivative.types[t.first] : t.first
            ]
        }
    end

    ##
    # Returns a collection of all directions in their described form.
    #
    # Useful for Select boxes in forms, also see SecurityDerivative#directions.
    def self.directions_for_form
        SecurityDerivative.directions.map { |d| [ d.first.camelize, d.first ] }
    end

    ##
    # Human readable String composed of essential properties known by a
    # SecurityDerivative on its own. Useful as part to compose the name of a
    # Security.
    def to_s
        [
          (self.Security.id_organization != Smr::ID_UNIVERSE_ORGANIZATION ? self.Security.brief : nil),
          SecurityDerivative.describe_type(type),
          direction.camelcase,
          '(on %s)' % underlying.to_s,
        ].compact.join(' ')    end

    ##
    # Returns underlying Security
    #
    # Note: implemented as method since ActiveRecord probably (?) cant handle
    #       two #has_one references to the same model.
    def underlying
        return false if id_security_underlying == 0
        Security.find(id_security_underlying)
    end

    ##
    # mandatory method: a derivative contract may have ended
    def is_expired?(date=Time.now)
        is_dead or (date_end > 0 and date > time_end) ? true : false
    end

end
