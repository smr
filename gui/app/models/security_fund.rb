#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'percentage'
require 'finance'
require 'cashflowitem'
require 'cashflowstream'

class SecurityFund < ActiveRecord::Base
    include Smr::Extensions::HelperMethods
    include Smr::Extensions::SecurityTypemodelMandatoryMethods
    include Smr::Extensions::DateTimeWrapper

    after_initialize :build_cf_stream

    self.inheritance_column = :none
	has_one :Security, :foreign_key=>:id_security_fund, :inverse_of=>:SecurityFund

    ##
    # types of funds supported (may alter behaviour of some methods)
    # NOTE: do NOT change the relations! just ADD!
    enum type: { :equity=>0, :bond=>1, :balanced=>2, :etf=>3, :fund_of_funds=>4,
                 :hedge=>5, :real_estate=>6, :commodity=>7 }

    # data validations
    #validates :id_security, numericality: { greater_than: 0 }

    ##
    # Returns human readable String describing :type. It may be given as
    # symbol, as string or as numerical index.
    def SecurityFund.describe_type(lookup)
        descriptions = {
            :equity        => 'Equity Fund',
            :bond          => 'Fixed Income Fund',
            :balanced      => 'Equity/Fixed Income Fund',
            :etf           => 'Indexing Fund',
            :fund_of_funds => 'Fund of Funds',
            :hedge         => 'Hedge Fund',
            :real_estate   => 'Real Estate Fund',
            :commodity     => 'Commodities Fund',
        }

        if lookup.is_a?(Numeric) then
            lookup = self.types.key(lookup) || ''
        end
        descriptions[lookup.to_sym]
    end

    ##
    # Describes type of the instance object.
    def describe_type
        SecurityFund.describe_type(type)
    end

    ##
    # Returns humanreadable String describing the model itself.
    def SecurityFund.describe
        'Investment Fund'
    end
    def describe
        SecurityFund.describe
    end

    ##
    # Returns a collection of all types in their described form.
    #
    # Useful for Select boxes in forms, also see SecurityFund#describe_type. The
    # :as_number option toggles whether symbols or integers are retured as
    # index.
    def self.types_for_form(options={ :as_number=>false })
        SecurityFund.types.map { |t|
            [
                SecurityFund.describe_type(t.first),
                options[:as_number] ? SecurityFund.types[t.first] : t.first
            ]
        }
    end

    ##
    # Human readable String composed of essential properties known by a
    # SecurityFund on its own. Useful as part to compose the name of a
    # Security.
    def to_s
        [
            self.Security.brief,
            unless tranche.blank? then '(%s)' % tranche end,
            SecurityFund.describe_type(type),
            unless currency.blank? and currency != Smr::DEFAULT_CURRENCY then currency end,
        ].join(' ')
    end

    ##
    # mandatory method: result based on :distribution declared, :distribution_interval
    # and :quote.
    def current_yield(quote=false)
        quote = self.Security.last_quote unless quote
        raise ':quote must be of Quote' if quote and not quote.is_a?(Quote)
        if quote.last.zero? then return false end
        BigDecimal( (
            distribution * 12 / distribution_interval
        ).to_s ).as_percentage_of(quote.last)
    end

    ##
    # mandatory method: funds never expires but we expire it
    # artificially if the last available quotation is older than 3
    # years. New Quote data will automatically unexpire it.
    def is_expired?(date=Time.now)
        quote = self.Security.last_quote(date)
        quote.time_last < date - 3.years
    end

    ##
    # mandatory method: constructs :distribution payments forecast
    #
    # Its based on :time_distribution and :distribution_interval.
    # Since a fund never expires, the steam of cashflow is limited to
    # 10 years from :start_date.
    #
    # The :type option allows to filter what is returned. This model supports
    # :none and :dividend_booking. Also see PositionRevision#types to have all
    # types of cashflow known to SMR.
    #
    # :start_date is Time.now unless given.
    def cashflow(options={:start_date=>false, :end_date=>false, :amount=>false, :type=>:none, :item_link=>false})
        start_date = options[:start_date] || Time.now
        end_date = options[:end_date] || (start_date + 10.years).end_of_year
        shares = options[:amount] || 1

        @cf_stream.get(
            shares,
            :start=>start_date, :end=>end_date,
            :filter=>(options[:type] || :none),
            :item_link=>options[:item_link]
        )
    end

    ##
    # mandatory method: inspects :distribution_interval for happening multiple times a year
    def has_subannual_payments?
        distribution_interval < 12
    end

 protected

    ##
    # create Smr::CashflowStream from SecurityFund attributes
    def build_cf_stream
        @cf_stream = Smr::CashflowStream.new(
            distribution, time_distribution,
            :payment_interval=>distribution_interval,
            :item_description=>self.Security.to_s,
            :id_for_caching=>('%s%i' % [self.class, (id || 0)]).to_sym
        )
    end
end
