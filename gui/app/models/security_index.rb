#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

class SecurityIndex < ActiveRecord::Base
    include Smr::Extensions::HelperMethods
    include Smr::Extensions::SecurityTypemodelMandatoryMethods

    self.inheritance_column = :none
	has_one :Security, :foreign_key=>:id_security_index, :inverse_of=>:SecurityIndex

    # data validations
    #validates :id_security, numericality: { greater_than: 0 }

    ##
    # Returns humanreadable String describing the model itself.
    def SecurityIndex.describe
        'Index'
    end
    def describe
        SecurityIndex.describe
    end

    ##
    # Human readable String composed of essential properties known by a
    # Securitymetal on its own. Useful as part to compose the name of a
    # Security.
    def to_s
        [
          (self.Security.id_organization != Smr::ID_UNIVERSE_ORGANIZATION ? self.Security.brief : nil),
          SecurityIndex.describe,
        ].compact.join(' ')
    end

end
