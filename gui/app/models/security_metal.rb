#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

class SecurityMetal < ActiveRecord::Base
    include Smr::Extensions::HelperMethods
    include Smr::Extensions::SecurityTypemodelMandatoryMethods

    self.inheritance_column = :none
	has_one :Security, :foreign_key=>:id_security_metal, :inverse_of=>:SecurityMetal

    ##
    # types of metals supported (may alter behaviour of some methods)
    # NOTE: do NOT change the relations! just ADD!
    enum type: { :base=>0, :precious=>1, :strategic=>2  }

    # data validations
    #validates :id_security, numericality: { greater_than: 0 }

    ##
    # Returns human readable String describing :type. It may be given as
    # symbol, as string or as numerical index.
    def SecurityMetal.describe_type(lookup)
        descriptions = {
            :base        => 'Base Metal',
            :precious    => 'Precious Metal',
            :strategic   => 'Strategic Metal',
        }

        if lookup.is_a?(Numeric) then
            lookup = self.types.key(lookup) || ''
        end
        descriptions[lookup.to_sym]
    end

    ##
    # Describes type of the instance object.
    def describe_type
        SecurityMetal.describe_type(type)
    end

    ##
    # Returns humanreadable String describing the model itself.
    def SecurityMetal.describe
        'Industrialized Metal'
    end
    def describe
        SecurityMetal.describe
    end

    ##
    # Returns a collection of all types in their described form.
    #
    # Useful for Select boxes in forms, also see SecurityMetal#describe_type. The
    # :as_number option toggles whether symbols or integers are retured as
    # index.
    def self.types_for_form(options={ :as_number=>false })
        SecurityMetal.types.map { |t|
            [
                SecurityMetal.describe_type(t.first),
                options[:as_number] ? SecurityMetal.types[t.first] : t.first
            ]
        }
    end

    ##
    # Human readable String composed of essential properties known by a
    # SecurityMetal on its own. Useful as part to compose the name of a
    # Security.
    def to_s
        [
          (self.Security.id_organization != Smr::ID_UNIVERSE_ORGANIZATION ? self.Security.brief : nil),
          SecurityMetal.describe_type(type),
        ].compact.join(' ')
    end

end
