#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'percentage'
require 'finance'
require 'cashflowitem'
require 'cashflowstream'

class SecurityStock < ActiveRecord::Base
    include Smr::Extensions::HelperMethods
    include Smr::Extensions::SecurityTypemodelMandatoryMethods
    include Smr::Extensions::DateTimeWrapper

    after_initialize :build_cf_stream

    self.inheritance_column = :none
	has_one :Security, :foreign_key=>:id_security_stock, :inverse_of=>:SecurityStock

    ##
    # types of stock supported (may alter behaviour of some methods)
    # see http://api.rubyonrails.org/v4.1.0/classes/ActiveRecord/Enum.html
    # NOTE: do NOT change the relations! just ADD!
    enum type: { :common=>0, :preferred=>1, :bearer=>2, :registered=>3, :vinculum=>4, :ADR=>5, }

    # data validations
    #validates :id_security, numericality: { greater_than: 0 }

    ##
    # Returns human readable String describing :type. It may be given as
    # symbol, as string or as numerical index.
    def SecurityStock.describe_type(lookup)
        descriptions = {
            :common     => 'Common Stock',
            :preferred  => 'Preferred Stock',
            :bearer     => 'Bearer Share',
            :registered => 'Registered Share',
            :vinculum   => 'Registered Share with restricted transferability',
            :ADR        => 'American Depositary Receipt',
        }

        if lookup.is_a?(Numeric) then
            lookup = self.types.key(lookup) || ''
        end
        descriptions[lookup.to_sym]
    end

    ##
    # Describes type of the instance object.
    def describe_type
        SecurityStock.describe_type(type)
    end

    ##
    # Returns humanreadable String describing the model itself.
    def SecurityStock.describe
        'Equity Ownership'
    end
    def describe
        SecurityStock.describe
    end

    ##
    # Returns a collection of all types in their described form.
    #
    # Useful for Select boxes in forms, also see SecurityStock#describe_type. The
    # :as_number option toggles whether symbols or integers are retured as
    # index.
    def self.types_for_form(options={ :as_number=>false })
        SecurityStock.types.map { |t|
            [
                SecurityStock.describe_type(t.first),
                options[:as_number] ? SecurityStock.types[t.first] : t.first
            ]
        }
    end

    ##
    # Human readable String composed of essential properties known by a
    # SecurityStock on its own. Useful as part to compose the name of a
    # Security.
    def to_s
        SecurityStock.describe_type(type)
    end

    ##
    # mandatory method: result based on :dividend declared, :dividend_interval
    # and :quote.
    def current_yield(quote=false)
        quote = self.Security.last_quote unless quote
        raise ':quote must be of Quote' if quote and not quote.is_a?(Quote)
        if quote.last.zero? then return false end
        BigDecimal( (
            dividend * 12 / dividend_interval
        ).to_s ).as_percentage_of(quote.last)
    end

    ##
    # mandatory method: equity never expires but we expire it
    # artificially if the last available quotation is older than 3
    # years. New Quote data will automatically unexpire it.
    def is_expired?(date=Time.now)
        quote = self.Security.last_quote(date)
        quote.time_last < date - 3.years
    end

    ##
    # mandatory method: constructs :dividend payments
    #
    # Its based on :dividend_exdate and :dividend_interval. Since equity never
    # expires, the steam of cashflow is limited to 10 years from :start_date.
    #
    # The :type option allows to filter what is returned. This model supports
    # :none and :dividend_booking. Also see PositionRevision#types to have all
    # types of cashflow known to SMR.
    #
    # :start_date is Time.now unless given.
    def cashflow(options={:start_date=>false, :end_date=>false, :amount=>false, :type=>:none, :item_link=>false})
        start_date = options[:start_date] || Time.now
        end_date = options[:end_date] || (start_date + 10.years).end_of_year
        shares = options[:amount] || 1

        @cf_stream.get(
            shares,
            :start=>start_date, :end=>end_date,
            :filter=>(options[:type] || :none),
            :item_link=>options[:item_link]
        )
    end

    ##
    # mandatory method: inspects :dividend_interval for happening multiple times a year
    def has_subannual_payments?
        dividend_interval < 12
    end

 protected

    ##
    # create Smr::CashflowStream from SecurityStock attributes
    def build_cf_stream
        @cf_stream = Smr::CashflowStream.new(
            dividend, time_dividend_exdate,
            :payment_interval=>dividend_interval,
            :item_description=>self.Security.to_s,
            :id_for_caching=>('%s%i' % [self.class, (id || 0)]).to_sym
        )
    end
end
