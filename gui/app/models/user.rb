#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'digest/sha1'
require 'securerandom'

class User < ActiveRecord::Base
    include Smr::Extensions::Link
    include Smr::Extensions::BookmarkTools

    has_many :Access
    has_many :Comment
    has_many :Document
    has_many :FigureVar
    has_many :News
    has_many :NewsRead, :through => :News
    has_many :NewsFeed, :through => :News
    has_many :Portfolio
    has_many :Position, :through => :Portfolio
    has_many :Quoterecord
    has_many :QuoterecordRule
    has_many :Quoterecordthreshold
    has_many :Setting
    has_many :Timetag
    has_many :Bookmark, :foreign_key=>:id_user

    accepts_nested_attributes_for :Bookmark

    validates_uniqueness_of :login
    validates_presence_of :login,:fullname,:email

    before_create :generate_token

    # authentication 
    has_secure_password

    # generate and mail a password reset token to the user
#    def send_password_reset
#      generate_token(:password_reset_token)
#      self.password_reset_sent = Time.zone.now
#      save!
#      #UserMailer.password_reset(self).deliver
#    end
    
    # generate a random token, by default on :password_auth_token column
    def generate_token(column=:password_auth_token)
      begin
        self[column] = Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64.to_s)
      end while User.exists?(column => self[column])
    end

    # Set :password to a random string that may contain +A-Z+, +a-z+,
    # +0-9+, +++ and +/+ characters. Returns that string once.
    # Note: call #save after this
    def generate_password(length=8)
        self.password = SecureRandom.base64(length-2).delete('=')
    end

    ##
    # Return human readable String.
    def to_s
        fullname
    end
end
