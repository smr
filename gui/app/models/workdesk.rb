#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# The Workdesk handles a stack of research work for each User.
#
class Workdesk < ActiveRecord::Base
    include Smr::Extensions::DateTimeWrapper

	has_one :User,          :foreign_key=>:id, :primary_key=>:id_user
	has_one :Security,      :foreign_key=>:id, :primary_key=>:id_security
	has_one :Organization,  :foreign_key=>:id, :primary_key=>:id_organization

    validates_uniqueness_of :id, scope: [:id_user, :id_security, :id_organization]

    # define scopes and ransack aliases for finding things
    ransack_alias :ransackcolumns, :comment_or_Security_brief_or_Security_symbol_or_Organization_name

    ##
    # tell what this item is refering to
    def is_security?
        id_security > 0
    end
    def is_organization?
        id_organization > 0
    end

    ##
    # human readable summary
    def to_s
        s = String.new
        if id_security > 0
            s += self.Security.to_s
        else s += self.Organization.name end
        s += ' on Workdesk'
    end
end

