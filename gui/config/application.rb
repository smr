#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require File.expand_path('../boot', __FILE__)
require 'rails/all'

# SMR Release Version
VERSION = '1.0.0'

# If you have a Gemfile, require the gems listed there, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env) if defined?(Bundler)

# Setup database backed session store
ActiveRecord::SessionStore::Session.table_name = 'user_session'

##
# Groups all constants and classes that form the SMR application. Application
# wide constants are defined directly on this module.
module Smr

  ##
  # Security.id to be used for cash deposits.
  # - defined here to have the entire app use only one
  # - see db/seeds.rb especially
  ID_CASH_SECURITY = 1

  ##
  # Organization owning a Security by default. The universe is always there.
  ID_UNIVERSE_ORGANIZATION = 1

  ##
  # Default currency code. Taken if nothing else is available.
  # Note that SMR is using currency codes as defined by the ISO 4217 standard.
  DEFAULT_CURRENCY = 'EUR'

  ##
  # Definition of what is considered a blank time value.
  # For most database columny this means whatever they define is not defined.
  # Compare with TIME_ZERO to find out.
  TIME_ZERO = Time.at(0)

  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += %W(#{config.root}/lib)
    config.autoload_paths += Dir["#{config.root}/lib/**/"]

    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    config.active_record.pluralize_table_names = false

    # Use native SQL schema to support database specifics such as triggers and special indices
    config.active_record.schema_format = :sql

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # JavaScript files you want as :defaults (application.js is always included).
    # config.action_view.javascript_expansions[:defaults] = %w(jquery rails)
    #config.action_view.javascript_expansions[:defaults] = %w()

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]
  end
end


