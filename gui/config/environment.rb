#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
# Load the rails application
require File.expand_path('../application', __FILE__)

# fix order when loading fixtures
# - see http://techpolesen.blogspot.de/2007/04/rails-fixture-tips.html
ENV['FIXTURES'] ||= 'news,news_item,news_read,news_assign'

# fix inflections causing trouble with models
ActiveSupport::Inflector.inflections do |inflect|
  inflect.irregular 'FigureData', 'FigureData'
end

# Initialize the rails application
Smr::Application.initialize!
