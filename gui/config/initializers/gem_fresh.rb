# Any gems that you put in the Gemfile should also be listed here.
# The rake metrics:outdated_gems task calculates which gems are
# outdated and then combines that information with the information
# listed here about a particular gem's reach in the application code.
#
GemFresh::Config.configure do |gems|

  # Updating these gems could require you to make large, system-wide changes
  # to the application code.
  gems.with_system_wide_impact %w(
    just_paginate
    percentage
    finance
    ransack
  )


  # Updating these gems could require you to make some changes to small
  # sections of the application.
  gems.with_local_impact %w(
    nokogiri
    math_engine
  )

  # When updating these gems, you barely have to touch any code at all.
  gems.with_minimal_impact %w(
    mysql2
    activerecord-mysql-adapter
    activerecord-session_store
    thin
    sdoc
    bcrypt
    gem_fresh
  )

  # We ignore these since we are in complete control of their update cycles.
  gems.that_are_private %w(
  )

end
