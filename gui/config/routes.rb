#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
Smr::Application.routes.draw do

  # See how all your routes lay out with 'rake routes'
  #
  # See http://guides.rubyonrails.org/routing.html
  #
  # The priority is based upon order of creation:
  # first created -> highest priority.
  root :to => 'asset#index'

  resources :asset
  get  '/asset/show_portfolio/:id_portfolio', :to=>'asset#index', :as=>'show_portfolio'
  post '/asset/show_portfolio/',              :to=>'asset#index', :as=>'show_portfolio_post'

  resources :order
  resources :report
  resources :cashflow
  resources :watchlist
  resources :figures
  resources :quoterecords
  resources :quoterecord_threshold
  resources :quoterecord_rules

  # date
  post "set_session_params" => "application#set_session_params"

  # authentication
  get 'logout' => 'sessions#destroy', :as => 'logout'
  get 'login' => 'sessions#new', :as => 'login'
  resources :sessions
  resources :password_reset

  resources :position
  post 'execute_order', to: 'position#execute_order'
  post 'cancel_order', to: 'position#cancel_order'
  post 'patch_position', to: 'position#patch'
  get 'close', to: 'position#close'

  get '/workdesk/add',      :to=>'workdesk#add',  :as=>'workdesk_add'
  get '/workdesk/:id/drop', :to=>'workdesk#drop', :as=>'workdesk_drop'
  resources :workdesk

  resources :portrait
  get '/portrait/organization/:id', :to=>'portrait#show_organization', :as=>'portrait_organization'
  get '/portrait/security/:id',     :to=>'portrait#show_security',     :as=>'portrait_security'

  resources :cashposition
  get '/cashposition/:id/settle', :to=>'cashposition#settle', :as=>'settle_cashposition'

  resources :documents
  get 'download' => 'documents#download'
  get 'delete_document' => 'documents#delete_document'

  resources :blog
  get 'blog' => 'blog#index'

  # ObjectsController shows things while resource routes to the actual models
  # are used to the CRUDing
  get 'objects' => 'objects#index'
  namespace 'objects' do
      resources :organization, :security, :bookmark, :portfolio, :figurevar, :user
      post 'patch_portfolio', to: 'portfolio#patch'
  end
  get '/objects/security/:id/delete', :to=>'objects/security#destroy', :as=>'delete_security'
  get '/objects/export/securities',   :to=>'objects/security#export',  :as=>'export_securities'
  get '/objects/bookmark/:id/delete', :to=>'objects/bookmark#destroy', :as=>'delete_bookmark'

  # DocumentationController shows API docs from doc/
  resources :doc, :only=>:get_file
  get "/doc/:file_or_folder", :controller=>:documentation, :action=>:get_file, :file_or_folder=>/.*/
end
