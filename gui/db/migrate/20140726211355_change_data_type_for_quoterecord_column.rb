##
# this field was enum previously and was converted to varchar with size 0
class ChangeDataTypeForQuoterecordColumn < ActiveRecord::Migration
  def change
    change_table :quoterecord do |t|
      t.change :column, :string, limit: 25
    end
  end
end
