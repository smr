##
# there is no such thing as a assignment table without a primary key in rails' world
class QuoterecordThresholdNeedsPrimaryKey < ActiveRecord::Migration
    def up
        add_column :quoterecord_threshold, :id, :primary_key
    end

    def down
        remove_column :quoterecord_threshold, :id
    end
end
