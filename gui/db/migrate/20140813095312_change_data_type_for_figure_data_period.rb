# this field was enum previously and was converted to varchar with size 0
class ChangeDataTypeForFigureDataPeriod < ActiveRecord::Migration
  def change
    change_table :figure_data do |t|
      t.change :period, :string, limit: 25
    end
  end
end

