class AddUserSession < ActiveRecord::Migration
  def change
    create_table :user_session do |t|
      t.string :session_id, :null => false
      t.text :data
      t.timestamps
    end

    add_index :user_session, :session_id, :unique => true
    add_index :user_session, :updated_at
  end
end
