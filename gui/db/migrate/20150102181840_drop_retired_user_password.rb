class DropRetiredUserPassword < ActiveRecord::Migration
  def change
     remove_column :user, :password
  end
end
