class DropAnnualMeetingFromStock < ActiveRecord::Migration
  def change
     remove_column :stock, :annual_meeting
  end
end
