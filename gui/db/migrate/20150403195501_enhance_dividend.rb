class EnhanceDividend < ActiveRecord::Migration
  def change
    rename_column :dividend, :date, :exdate

    # this is what the company has paid, what was actually received does into a
    # cash booking
    rename_column :dividend, :received, :paid

    # disallow NULL to make sure new records all relate to a Position
    # and a cash booking
    add_column :dividend, :id_position, :integer, :default=>0, :null=>false
    add_column :dividend, :id_order, :integer, :default=>0, :null=>false
  end
end
