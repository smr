class OrganizationCentricSecurities < ActiveRecord::Migration
  def change

    create_table :organization do |t|
      t.string :name, :null => false
      t.string :url
      t.string :contact_email
      t.text :description
    end

    rename_table :stock, :security
    add_column :security, :type, :integer, :default=>0, :null=>false
    add_column :security, :id_organization, :integer, :default=>1, :null=>false
    add_foreign_key :security, :organization, :column=>:id_organization, :null=>false, :default=>1
    rename_column :security, :id_stock_quotesource, :id_security_quotesource
    rename_column :security, :id_stock_symbolextension, :id_security_symbolextension
    rename_column :security, :name, :brief
    rename_column :security, :comment, :description

    rename_table :stock_quotesource, :security_quotesource
    rename_table :stock_symbolextension, :security_symbolextension

    rename_column :dividend, :id_stock, :id_security
    rename_column :document_assign, :id_stock, :id_security
    rename_column :figure_data, :id_stock, :id_security
    rename_column :position, :id_stock, :id_security
    rename_column :quote, :id_stock, :id_security
    rename_column :quoterecord, :id_stock, :id_security
    rename_column :quoterecord_threshold, :id_stock, :id_security
    rename_column :split, :id_stock, :id_security
  end
end
