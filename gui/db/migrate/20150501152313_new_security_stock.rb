class NewSecurityStock < ActiveRecord::Migration
  def change
    create_table :security_stock do |t|
        t.integer :id_security, :null=>false
        t.integer :type, :null=>false, :default=>0
        t.float :dividend
    end
  end
end
