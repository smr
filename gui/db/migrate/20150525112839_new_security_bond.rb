class NewSecurityBond < ActiveRecord::Migration
  def change
    create_table :security_bond do |t|
        t.integer :id_security, :null=>false
        t.integer :type, :null=>false, :default=>0
        t.integer :interest_method, :null=>false, :default=>0
        t.integer :denomination, :null=>false, :default=>1000
        t.string :currency
        t.float :coupon, :null=>false, :default=>0
        t.integer :coupon_interval, :null=>false, :default=>12
        t.float :redemption_price, :null=>false, :default=>100
        t.integer :redemption_installments, :null=>false, :default=>1
        t.integer :redemption_interval, :null=>false, :default=>12

        t.integer :date_maturity, :limit=>8, :null=>false, :default=>0
        t.integer :date_first_coupon, :limit=>8, :null=>false, :default=>0
        t.integer :date_last_coupon, :limit=>8, :null=>false, :default=>0

        t.boolean :is_subordinated, :limit=>1, :default=>false
        t.boolean :is_callable, :limit=>1, :default=>false
        t.boolean :is_stepup, :limit=>1, :default=>false
        t.boolean :is_stepdown, :limit=>1, :default=>false

        t.text :comment, :limit=>65535
    end
  end
end
