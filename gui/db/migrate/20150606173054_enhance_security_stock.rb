class EnhanceSecurityStock < ActiveRecord::Migration
  def change
      change_column :security_stock, :dividend, :float, :null=>false, :default=>0
      add_column :security_stock, :dividend_interval, :integer, :null=>false, :default=>12
      add_column :security_stock, :dividend_exdate, :integer, :limit=>8, :null=>false, :default=>0
      add_column :security_stock, :comment, :text, :limit=>65535
  end
end
