class EnhanceSecurityAddCollateralCoverageRatio < ActiveRecord::Migration
  def change
    add_column :security, :collateral_coverage_ratio, :decimal, :precision=>2, :default=>0.0, :null=>false
  end
end
