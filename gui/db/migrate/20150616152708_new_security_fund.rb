class NewSecurityFund < ActiveRecord::Migration
  def change
    create_table :security_fund do |t|
        t.integer :id_security, :null=>false

        t.integer :type, :null=>false, :default=>0
        t.string :currency
        t.string :tranche
        t.integer :date_inception, :limit=>8, :null=>false, :default=>0
        t.integer :date_fiscalyearend, :limit=>8, :null=>false, :default=>0
        t.integer :investment_minimum, :null=>false, :default=>0
        t.boolean :is_synthetic, :limit=>1, :default=>false

        t.float :distribution, :null=>false, :default=>0, :precision=>4
        t.integer :distribution_interval, :null=>false, :default=>12
        t.integer :date_distribution, :limit=>8, :null=>false, :default=>0
        t.boolean :is_retaining_profits, :limit=>1, :default=>false

        t.integer :redemption_period, :null=>false, :default=>5
        t.float :redemption_fee, :default=>0, :precision=>2
        t.float :issue_surcharge, :null=>false, :default=>0, :precision=>2
        t.float :management_fee, :null=>false, :default=>0, :precision=>2

        t.text :comment, :limit=>65535
    end
  end
end
