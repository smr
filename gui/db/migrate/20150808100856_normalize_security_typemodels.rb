##
# It was a bad idea to not do this at the very beginning... didn't some SQL
# teacher say that actually?
# This migration is based on the knowledge of this - no retired - column
# definition:
#
#    # possible types for a Security (can be one only)
#    # - exept for :unknown these are tables named 'security_XXX' with
#    #   corresponding models
#    # - these tables/models provide additional data fields and methods that
#    #   are specific to the type
#    # - see #types
#    enum type: { :unknown=>0, :cash=>1, :stock=>2, :bond=>3, :fund=>4 }
#
# Serveral methods in the Security model have been adjusted to the now missing
# column while keeping the fnuctionality.
class NormalizeSecurityTypemodels < ActiveRecord::Migration
  def up
    add_column :security, :id_security_bond, :integer, :null=>false, :default=>0
    add_column :security, :id_security_fund, :integer, :null=>false, :default=>0
    add_column :security, :id_security_stock, :integer, :null=>false, :default=>0

    Security.find_each do |s|
        case s.read_attribute(:type)
            when 2
                t=SecurityStock.where(:id_security=>s.id).first
                tid=(t.blank? ? 0 : t.id)
                s.update_attribute(:id_security_stock, tid)
            when 3
                t=SecurityBond.where(:id_security=>s.id).first
                tid=(t.blank? ? 0 : t.id)
                s.update_attribute(:id_security_bond, tid)
            when 4
                t=SecurityFund.where(:id_security=>s.id).first
                tid=(t.blank? ? 0 : t.id)
                s.update_attribute(:id_security_fund, tid)
        end
    end
    remove_column :security, :type
    remove_column :security_stock, :id_security
    remove_column :security_bond, :id_security
    remove_column :security_fund, :id_security
  end

  def down
    add_column :security, :type, :integer, :default=>0, :null=>false
    add_column :security_stock, :id_security, :integer, :null=>false
    add_column :security_bond, :id_security, :integer, :null=>false
    add_column :security_fund, :id_security, :integer, :null=>false

    Security.find_each do |s|
        if s.id_security_stock > 0
            s.update_attribute(:type, 2)
            s.SecurityStock.update_attribute(:id_security, s.id)
        elsif s.id_security_bond > 0
            s.update_attribute(:type, 3)
            s.SecurityBond.update_attribute(:id_security, s.id)
        elsif s.id_security_fund > 0
            s.update_attribute(:type, 4)
            s.SecurityFund.update_attribute(:id_security, s.id)
        end
    end

    remove_column :security, :id_security_bond
    remove_column :security, :id_security_fund
    remove_column :security, :id_security_stock
  end

end
