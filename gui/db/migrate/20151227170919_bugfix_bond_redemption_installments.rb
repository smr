class BugfixBondRedemptionInstallments < ActiveRecord::Migration
  def change
    change_table :security_bond do |t|
        # bond without maturity date have no [known] redemption
        t.change :redemption_installments, :integer, :default=>0
    end
  end
end
