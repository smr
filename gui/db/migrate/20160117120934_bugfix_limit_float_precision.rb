class BugfixLimitFloatPrecision < ActiveRecord::Migration
  def change
    change_table :portfolio do |t|
      t.change :taxallowance, :float, :precision=>50, :scale=>4, :default=>0.0
    end
    change_table :position_revision do |t|
      t.change :invested, :float, :precision=>50, :scale=>4, :default=>0.0
      t.change :shares,   :float, :precision=>50, :scale=>4, :default=>0.0
    end
    change_table :order do |t|
      t.change :shares,           :float, :precision=>50, :scale=>4, :default=>0.0
      t.change :limit,            :float, :precision=>50, :scale=>4, :default=>0.0
      t.change :quote,            :float, :precision=>50, :scale=>4, :default=>0.0
      t.change :provision,        :float, :precision=>50, :scale=>4, :default=>0.0
      t.change :courtage,         :float, :precision=>50, :scale=>4, :default=>0.0
      t.change :expense,          :float, :precision=>50, :scale=>4, :default=>0.0
      t.change :accrued_interest, :float, :precision=>50, :scale=>4, :default=>0.0
    end
    change_table :dividend do |t|
      t.change :paid, :float, :precision=>50, :scale=>4, :default=>0.0
    end
    change_table :quote do |t|
      t.change :quote, :float, :precision=>50, :scale=>4, :default=>0.0
      t.change :low,   :float, :precision=>50, :scale=>4, :default=>0.0
      t.change :high,  :float, :precision=>50, :scale=>4, :default=>0.0
    end
    change_table :quoterecord_threshold do |t|
      t.change :quote_sensitivity, :float, :precision=>50, :scale=>4, :default=>0.0
      t.change :hit_sensitivity, :float, :precision=>50, :scale=>4, :default=>0.0
    end
  end
end
