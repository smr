class NewSecurityMetal < ActiveRecord::Migration
  def change
    add_column :security, :id_security_metal, :integer, :null=>false, :default=>0

    create_table :security_metal do |t|
        t.integer :type, :null=>false, :default=>0
        t.string :unit
        t.text :comment, :limit=>65535
    end
  end
end
