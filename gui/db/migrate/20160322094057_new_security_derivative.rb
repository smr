class NewSecurityDerivative < ActiveRecord::Migration
  def change
    add_column :security, :id_security_derivative, :integer, :null=>false, :default=>0

    create_table :security_derivative do |t|
        t.integer :type, :null=>false, :default=>0
        t.integer :direction, :null=>false, :default=>0

        t.integer :id_issuer, :null=>false, :default=>Smr::ID_UNIVERSE_ORGANIZATION
        t.integer :date_inception, :limit=>8, :null=>false, :default=>0
        t.integer :date_end, :limit=>8, :null=>false, :default=>0

        t.float :strike_price, :null=>false, :default=>0, :precision=>4
        t.float :subscription_ratio, :null=>false, :default=>1, :precision=>4
        t.boolean :is_leveraged, :limit=>1, :default=>false
        t.boolean :is_knock_out, :limit=>1, :default=>false

        t.text :comment, :limit=>65535
    end
  end
end
