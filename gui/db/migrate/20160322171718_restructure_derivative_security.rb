class RestructureDerivativeSecurity < ActiveRecord::Migration
  def change
    remove_column :security_derivative, :id_issuer, :integer, :null=>false, :default=>0
    add_column :security_derivative, :id_security_underlying, :integer, :null=>false, :default=>0
    add_column :security_derivative, :is_dead, :boolean, :limit=>1, :default=>false
  end
end
