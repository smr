class NewSecurityIndex < ActiveRecord::Migration
  def change
    add_column :security, :id_security_index, :integer, :null=>false, :default=>0

    create_table :security_index do |t|
        t.text :comment, :limit=>65535
    end
  end
end
