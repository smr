class SmartenizedQuoteRetrieval < ActiveRecord::Migration
  def change
     # clean very old columns and tables
     remove_column :security, :id_security_quotesource, :integer, :null=>false, :default=>0
     remove_column :security, :id_security_symbolextension, :integer, :null=>false, :default=>0
     drop_table :security_quotesource
     drop_table :security_symbolextension

     # new columns to account the retrieval status
     add_column :security, :preferred_reaper, :string, :length=>32
     add_column :security, :preferred_reaper_fail_count, :integer, :null=>false, :default=>0
     add_column :security, :preferred_reaper_locked, :boolean, :limit=>1, :default=>false
     add_column :security, :preferred_reaper_delay, :integer, :null=>false, :default=>0
  end
end
