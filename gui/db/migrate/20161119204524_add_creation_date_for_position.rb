class AddCreationDateForPosition < ActiveRecord::Migration
  def up
      add_column :position, :date_record_created, :integer, :null=>false, :default=>0

      Position.find_each do |p|
        if p.is_cash_position?
            # set record create data to Jan 1st of year in :comment
            p.update_attribute(
                :date_record_created,
                Time.new( p.comment.scan(/\d/).join('').to_i, 1, 1).to_i
            )
        end
      end
  end

  def down
      remove_column :position, :date_record_created
  end
end
