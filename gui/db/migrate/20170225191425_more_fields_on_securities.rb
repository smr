class MoreFieldsOnSecurities < ActiveRecord::Migration
  def change
      add_column :security, :trading_ceased_reason, :string, :limit=>32
      add_column :security, :trading_ceased_date, :integer, :limit=>8, :null=>false, :default=>0

      add_column :security_bond, :date_issue, :integer, :limit=>8, :null=>false, :default=>0
      add_column :security_bond, :date_first_call, :integer, :limit=>8, :null=>false, :default=>0
      add_column :security_bond, :call_price, :float, :null=>false, :default=>100
      add_column :security_bond, :call_interval, :integer, :limit=>4, :null=>false, :default=>0
      add_column :security_bond, :call_notify_deadline, :integer, :limit=>4, :null=>false, :default=>0
      add_column :security_bond, :issue_size, :integer, :limit=>8, :null=>false, :default=>0
  end
end
