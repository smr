##
# The Workdesk handles a stack of research work for each User.
class Workdesk < ActiveRecord::Migration
  def change
    create_table :workdesk do |t|
      t.integer :id_user,         :null=>false
      t.integer :id_security,     :null=>false, :default=>0
      t.integer :id_organization, :null=>false, :default=>0
      t.integer :date_added,      :null=>false, :default=>0
      t.text    :comment
    end
  end
end
