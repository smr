class SpeedUsingIndexes < ActiveRecord::Migration
  def change
    add_index :security, :symbol
    add_index :security, :brief
    add_index :organization, :name
    add_index :quote, [:id_security, :date]
  end
end
