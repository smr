class DateColumnConsistency < ActiveRecord::Migration
  def change
    rename_column :comment, :date, :date_recorded

    rename_column :dividend, :exdate, :date_exdate
    change_table :dividend do |t|
        t.change :date_exdate, :integer, :default=>0
    end

    rename_column :order, :issued, :date_issued
    rename_column :order, :expire, :date_expire

    change_table :portfolio do |t|
        t.change :date_created, :integer, :default=>0
    end

    rename_column :position, :closed, :date_closed
    rename_column :position_revision, :date, :date_created

    rename_column :quote, :date,  :date_last
    rename_column :quote, :quote, :last

    rename_column :quoterecord, :created, :date_created
    rename_column :quoterecord_rule, :last_modified, :date_last_modified

    rename_column :security, :trading_ceased_date, :date_trading_ceased

    rename_column :security_stock, :dividend_exdate, :date_dividend_exdate

    rename_column :timetag, :start, :date_start
    rename_column :timetag, :end,   :date_end
    change_table :timetag do |t|
        t.change :date_start, :integer, :default=>0
        t.change :date_end,   :integer, :default=>0
    end
  end
end
