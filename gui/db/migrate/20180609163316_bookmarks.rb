##
# Bookmarks store links owned by User, related to Security, Organization, Portfolio
class Bookmarks < ActiveRecord::Migration
  def up
    create_table :bookmark do |t|
      t.integer :id_user,         :null=>false, :default=>0
      t.integer :id_security,     :null=>false, :default=>0
      t.integer :id_organization, :null=>false, :default=>0
      t.integer :id_portfolio,    :null=>false, :default=>0
      t.integer :id_figure_var,   :null=>false, :default=>0
      t.integer :date_added,      :null=>false, :default=>0
      t.string  :name,            :null=>false
      t.string  :url,             :null=>false
      t.integer :rank,            :null=>false, :default=>0
    end

    Security.find_each do |s|
        next unless s.url and s.url.match(URI.regexp)
        b = Bookmark.create!(
            :url=>s.url,
            :name=>'Terms for ' + s.to_s,
            :id_security=>s.id
        )
    end
    remove_column :security, :url

    Organization.find_each do |o|
        next unless o.url and o.url.match(URI.regexp)
        b = Bookmark.create!(
            :url=>o.url,
            :name=>'Investor Relations of ' + o.to_s,
            :id_organization=>o.id
        )
    end
    remove_column :organization, :url
  end

  def down
    add_column :security, :url, :string
    add_column :organization, :url, :string
    Bookmark.find_each do |b|
        if b.id_security != 0
            Security.find(b.id_security).update_attribute(:url, b.url)
        elsif b.id_organization != 0
            Organization.find(b.id_organization).update_attribute(:url, b.url)
        end
    end

    drop_table :bookmark
  end
end

