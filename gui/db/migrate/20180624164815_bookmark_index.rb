class BookmarkIndex < ActiveRecord::Migration
  def change
    add_index(:bookmark,
        [ :id_user, :id_security, :id_organization, :id_portfolio, :id_figure_var ],
        :name=>'index_bookmarks'
    )
  end
end
