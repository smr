# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170225191425) do

  create_table "access", id: false, force: :cascade do |t|
    t.integer "id_user", limit: 4,  default: 0,  null: false
    t.string  "name",    limit: 32, default: "", null: false
    t.boolean "access"
  end

  create_table "comment", force: :cascade do |t|
    t.integer "id_user", limit: 4,        null: false
    t.integer "date",    limit: 4,        null: false
    t.string  "title",   limit: 255,      null: false
    t.text    "comment", limit: 16777215
  end

  create_table "dividend", force: :cascade do |t|
    t.integer "id_security", limit: 4,                null: false
    t.integer "exdate",      limit: 4,                null: false
    t.float   "paid",        limit: 24, default: 0.0, null: false
    t.integer "id_position", limit: 4,  default: 0,   null: false
    t.integer "id_order",    limit: 4,  default: 0,   null: false
  end

  create_table "document", force: :cascade do |t|
    t.integer "id_user",     limit: 4,     null: false
    t.string  "mimetype",    limit: 50,    null: false
    t.string  "filename",    limit: 255,   null: false
    t.integer "size",        limit: 4,     null: false
    t.integer "date_upload", limit: 4,     null: false
    t.text    "comment",     limit: 65535
  end

  create_table "document_assign", primary_key: "id_document", force: :cascade do |t|
    t.integer "id_portfolio", limit: 4
    t.integer "id_position",  limit: 4
    t.integer "id_order",     limit: 4
    t.integer "id_security",  limit: 4
    t.boolean "is_assigned"
  end

  add_index "document_assign", ["id_document"], name: "id_document", unique: true, using: :btree

  create_table "document_data", primary_key: "id_document", force: :cascade do |t|
    t.binary "content", limit: 16777215, null: false
  end

  add_index "document_data", ["id_document"], name: "id_document", unique: true, using: :btree

  create_table "figure_data", force: :cascade do |t|
    t.integer "id_figure_var", limit: 4,     null: false
    t.integer "id_security",   limit: 4,     null: false
    t.integer "date",          limit: 4,     null: false
    t.string  "period",        limit: 25,    null: false
    t.string  "analyst",       limit: 255
    t.float   "value",         limit: 24,    null: false
    t.boolean "is_expected"
    t.boolean "is_audited"
    t.text    "comment",       limit: 65535
  end

  create_table "figure_var", force: :cascade do |t|
    t.integer "id_user",    limit: 4,        default: 0, null: false
    t.string  "name",       limit: 255,                  null: false
    t.string  "unit",       limit: 3
    t.text    "expression", limit: 16777215
    t.text    "comment",    limit: 65535
  end

  create_table "news", id: false, force: :cascade do |t|
    t.integer "id_user",       limit: 4, default: 0,     null: false
    t.integer "id_news_feed",  limit: 4, default: 0,     null: false
    t.boolean "is_subscribed",           default: false, null: false
  end

  create_table "news_assign", id: false, force: :cascade do |t|
    t.integer "id_news_item", limit: 4, default: 0, null: false
    t.integer "id_position",  limit: 4, default: 0, null: false
    t.boolean "is_assigned"
  end

  add_index "news_assign", ["id_news_item", "id_position"], name: "NID", unique: true, using: :btree

  create_table "news_feed", force: :cascade do |t|
    t.string  "url",          limit: 255
    t.boolean "fetch_feed",                 default: true
    t.integer "last_fetched", limit: 4,     default: 0
    t.string  "title",        limit: 255
    t.string  "link",         limit: 255
    t.text    "description",  limit: 65535
    t.string  "language",     limit: 10
    t.integer "ttl",          limit: 4
    t.string  "copyright",    limit: 255
    t.integer "count_total",  limit: 4,                    null: false
  end

  create_table "news_item", force: :cascade do |t|
    t.integer "id_news_feed",      limit: 4,     default: 0, null: false
    t.string  "identifier",        limit: 255,               null: false
    t.string  "identifier_method", limit: 0,                 null: false
    t.integer "fetched",           limit: 4,                 null: false
    t.string  "title",             limit: 255
    t.string  "link",              limit: 255
    t.text    "description",       limit: 65535
    t.string  "pubDate",           limit: 50
  end

  create_table "news_read", id: false, force: :cascade do |t|
    t.integer "id_news_feed",  limit: 4, null: false
    t.integer "id_user",       limit: 4, null: false
    t.integer "lastread",      limit: 4
    t.integer "count_notread", limit: 4, null: false
  end

  create_table "order", force: :cascade do |t|
    t.integer "id_position",        limit: 4,                         null: false
    t.integer "issued",             limit: 4,        default: 0,      null: false
    t.integer "expire",             limit: 4,        default: 0,      null: false
    t.string  "type",               limit: 10,       default: "buy",  null: false
    t.float   "shares",             limit: 24,       default: 0.0,    null: false
    t.float   "limit",              limit: 24,       default: 0.0,    null: false
    t.string  "addon",              limit: 10,       default: "none", null: false
    t.string  "exchange",           limit: 25
    t.float   "quote",              limit: 24,       default: 0.0,    null: false
    t.float   "provision",          limit: 24,       default: 0.0
    t.float   "courtage",           limit: 24,       default: 0.0
    t.float   "expense",            limit: 24,       default: 0.0
    t.text    "comment",            limit: 16777215
    t.boolean "is_canceled"
    t.integer "triggered_by_order", limit: 4
    t.float   "accrued_interest",   limit: 24,       default: 0.0,    null: false
  end

  create_table "organization", force: :cascade do |t|
    t.string "name",          limit: 255,   null: false
    t.string "url",           limit: 255
    t.string "contact_email", limit: 255
    t.text   "description",   limit: 65535
  end

  create_table "portfolio", force: :cascade do |t|
    t.integer "id_user",      limit: 4,                   null: false
    t.string  "name",         limit: 255,                 null: false
    t.string  "type",         limit: 0,                   null: false
    t.integer "date_created", limit: 4,                   null: false
    t.float   "taxallowance", limit: 24,    default: 0.0
    t.integer "order",        limit: 1,     default: 0
    t.text    "comment",      limit: 65535
  end

  create_table "position", force: :cascade do |t|
    t.integer "id_portfolio",        limit: 4,                 null: false
    t.integer "id_security",         limit: 4,                 null: false
    t.integer "closed",              limit: 4,     default: 0
    t.text    "comment",             limit: 65535,             null: false
    t.integer "date_record_created", limit: 4,     default: 0, null: false
  end

  create_table "position_revision", force: :cascade do |t|
    t.integer "id_order",    limit: 4,                null: false
    t.integer "id_position", limit: 4,                null: false
    t.integer "date",        limit: 4,  default: 0,   null: false
    t.float   "shares",      limit: 24, default: 0.0, null: false
    t.float   "invested",    limit: 24, default: 0.0, null: false
    t.integer "type",        limit: 4,  default: 0,   null: false
  end

  create_table "quote", force: :cascade do |t|
    t.integer "id_security", limit: 4,  default: 0,   null: false
    t.integer "date",        limit: 4,  default: 0,   null: false
    t.float   "quote",       limit: 24, default: 0.0, null: false
    t.float   "low",         limit: 24, default: 0.0
    t.float   "high",        limit: 24, default: 0.0
    t.integer "volume",      limit: 4
    t.string  "exchange",    limit: 50
  end

  create_table "quoterecord", force: :cascade do |t|
    t.integer "id_user",          limit: 4,     null: false
    t.integer "id_security",      limit: 4,     null: false
    t.integer "id_quote",         limit: 4,     null: false
    t.integer "created",          limit: 4,     null: false
    t.string  "column",           limit: 25,    null: false
    t.text    "comment",          limit: 65535
    t.boolean "is_pivotal_point"
    t.boolean "is_signal"
    t.boolean "is_uphit"
    t.boolean "is_downhit"
  end

  create_table "quoterecord_rule", force: :cascade do |t|
    t.integer "id_user",         limit: 4,     null: false
    t.integer "last_modified",   limit: 4
    t.integer "order",           limit: 4
    t.string  "related_columns", limit: 255
    t.string  "name",            limit: 255,   null: false
    t.text    "rule",            limit: 65535
  end

  create_table "quoterecord_threshold", force: :cascade do |t|
    t.integer "id_user",           limit: 4,                null: false
    t.integer "id_security",       limit: 4,                null: false
    t.float   "quote_sensitivity", limit: 24, default: 0.0
    t.float   "hit_sensitivity",   limit: 24, default: 0.0
  end

  create_table "security", force: :cascade do |t|
    t.boolean "fetch_quote",                                             default: true
    t.string  "symbol",                      limit: 32
    t.string  "brief",                       limit: 255,                 default: "--",  null: false
    t.string  "url",                         limit: 255
    t.text    "description",                 limit: 65535
    t.integer "id_organization",             limit: 4,                   default: 1,     null: false
    t.decimal "collateral_coverage_ratio",                 precision: 2, default: 0,     null: false
    t.integer "id_security_bond",            limit: 4,                   default: 0,     null: false
    t.integer "id_security_fund",            limit: 4,                   default: 0,     null: false
    t.integer "id_security_stock",           limit: 4,                   default: 0,     null: false
    t.integer "id_security_metal",           limit: 4,                   default: 0,     null: false
    t.integer "id_security_derivative",      limit: 4,                   default: 0,     null: false
    t.integer "id_security_index",           limit: 4,                   default: 0,     null: false
    t.string  "preferred_reaper",            limit: 255
    t.integer "preferred_reaper_fail_count", limit: 4,                   default: 0,     null: false
    t.boolean "preferred_reaper_locked",                                 default: false
    t.integer "preferred_reaper_delay",      limit: 4,                   default: 0,     null: false
    t.string  "trading_ceased_reason",       limit: 32
    t.integer "trading_ceased_date",         limit: 8,                   default: 0,     null: false
  end

  add_index "security", ["id_organization"], name: "fk_rails_79816ed41e", using: :btree

  create_table "security_bond", force: :cascade do |t|
    t.integer "type",                    limit: 4,     default: 0,     null: false
    t.integer "interest_method",         limit: 4,     default: 0,     null: false
    t.integer "denomination",            limit: 4,     default: 1000,  null: false
    t.string  "currency",                limit: 255
    t.float   "coupon",                  limit: 24,    default: 0.0,   null: false
    t.integer "coupon_interval",         limit: 4,     default: 12,    null: false
    t.float   "redemption_price",        limit: 24,    default: 100.0, null: false
    t.integer "redemption_installments", limit: 4,     default: 0,     null: false
    t.integer "redemption_interval",     limit: 4,     default: 12,    null: false
    t.integer "date_maturity",           limit: 8,     default: 0,     null: false
    t.integer "date_first_coupon",       limit: 8,     default: 0,     null: false
    t.integer "date_last_coupon",        limit: 8,     default: 0,     null: false
    t.boolean "is_subordinated",                       default: false
    t.boolean "is_callable",                           default: false
    t.boolean "is_stepup",                             default: false
    t.boolean "is_stepdown",                           default: false
    t.text    "comment",                 limit: 65535
    t.integer "date_issue",              limit: 8,     default: 0,     null: false
    t.integer "date_first_call",         limit: 8,     default: 0,     null: false
    t.float   "call_price",              limit: 24,    default: 0.0,   null: false
    t.integer "call_interval",           limit: 4,     default: 0,     null: false
    t.integer "call_notify_deadline",    limit: 4,     default: 0,     null: false
    t.integer "issue_size",              limit: 8,     default: 0,     null: false
  end

  create_table "security_derivative", force: :cascade do |t|
    t.integer "type",                   limit: 4,     default: 0,     null: false
    t.integer "direction",              limit: 4,     default: 0,     null: false
    t.integer "date_inception",         limit: 8,     default: 0,     null: false
    t.integer "date_end",               limit: 8,     default: 0,     null: false
    t.float   "strike_price",           limit: 24,    default: 0.0,   null: false
    t.float   "subscription_ratio",     limit: 24,    default: 1.0,   null: false
    t.boolean "is_leveraged",                         default: false
    t.boolean "is_knock_out",                         default: false
    t.text    "comment",                limit: 65535
    t.integer "id_security_underlying", limit: 4,     default: 0,     null: false
    t.boolean "is_dead",                              default: false
  end

  create_table "security_fund", force: :cascade do |t|
    t.integer "type",                  limit: 4,     default: 0,     null: false
    t.string  "currency",              limit: 255
    t.string  "tranche",               limit: 255
    t.integer "date_inception",        limit: 8,     default: 0,     null: false
    t.integer "date_fiscalyearend",    limit: 8,     default: 0,     null: false
    t.integer "investment_minimum",    limit: 4,     default: 0,     null: false
    t.boolean "is_synthetic",                        default: false
    t.float   "distribution",          limit: 24,    default: 0.0,   null: false
    t.integer "distribution_interval", limit: 4,     default: 12,    null: false
    t.integer "date_distribution",     limit: 8,     default: 0,     null: false
    t.boolean "is_retaining_profits",                default: false
    t.integer "redemption_period",     limit: 4,     default: 5,     null: false
    t.float   "redemption_fee",        limit: 24,    default: 0.0
    t.float   "issue_surcharge",       limit: 24,    default: 0.0,   null: false
    t.float   "management_fee",        limit: 24,    default: 0.0,   null: false
    t.text    "comment",               limit: 65535
  end

  create_table "security_index", force: :cascade do |t|
    t.text "comment", limit: 65535
  end

  create_table "security_metal", force: :cascade do |t|
    t.integer "type",    limit: 4,     default: 0, null: false
    t.string  "unit",    limit: 255
    t.text    "comment", limit: 65535
  end

  create_table "security_stock", force: :cascade do |t|
    t.integer "type",              limit: 4,     default: 0,   null: false
    t.float   "dividend",          limit: 24,    default: 0.0, null: false
    t.integer "dividend_interval", limit: 4,     default: 12,  null: false
    t.integer "dividend_exdate",   limit: 8,     default: 0,   null: false
    t.text    "comment",           limit: 65535
  end

  create_table "setting", force: :cascade do |t|
    t.integer "id_user", limit: 4,   default: 0,        null: false
    t.string  "type",    limit: 10,  default: "plugin", null: false
    t.string  "name",    limit: 32
    t.string  "var",     limit: 32
    t.string  "value",   limit: 255
  end

  create_table "split", force: :cascade do |t|
    t.integer "id_security", limit: 4,        null: false
    t.integer "date",        limit: 4,        null: false
    t.string  "ratio",       limit: 11,       null: false
    t.text    "comment",     limit: 16777215
  end

  add_index "split", ["id_security", "date"], name: "SID", unique: true, using: :btree

  create_table "timetag", force: :cascade do |t|
    t.integer "id_user",     limit: 4,     null: false
    t.string  "title",       limit: 255,   null: false
    t.text    "description", limit: 65535, null: false
    t.integer "start",       limit: 4,     null: false
    t.integer "end",         limit: 4,     null: false
  end

  create_table "user", force: :cascade do |t|
    t.string   "login",                limit: 10,                       null: false
    t.boolean  "is_admin",                              default: false, null: false
    t.string   "fullname",             limit: 255
    t.string   "email",                limit: 50
    t.text     "comment",              limit: 16777215
    t.string   "password_digest",      limit: 255
    t.string   "password_auth_token",  limit: 255
    t.string   "password_reset_token", limit: 255
    t.datetime "password_reset_sent"
  end

  create_table "user_session", force: :cascade do |t|
    t.string   "session_id", limit: 255,   null: false
    t.text     "data",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_session", ["session_id"], name: "index_user_session_on_session_id", unique: true, using: :btree
  add_index "user_session", ["updated_at"], name: "index_user_session_on_updated_at", using: :btree

  create_table "version", id: false, force: :cascade do |t|
    t.integer "version", limit: 1
  end

  add_foreign_key "security", "organization", column: "id_organization"
end
