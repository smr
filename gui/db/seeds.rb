#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#


if not Organization.where(:id=>Smr::ID_UNIVERSE_ORGANIZATION).first then
    Organization.new(
        :id=>Smr::ID_UNIVERSE_ORGANIZATION, :name=>'Universe',
        :description=>'This is where you live in. And everything else. The totality of existence.'
    ).save
end

# FIXME: to be tested + create random password
#User.create(
#    :login=>'admin', :id=>1, :is_admin=>1, :fullname=>'SMR Admin',
#    :password=>'admin', :password_auth_token=>'657096b5f9583acaebbf6001d434cd7090d3d8ee',
#    :email=>'root@localhost.localdomain',:comment=>'Default Administrative Account.'
#)

# this "Stock" is used to represent Cash Positions
# - the symbols is fake as long as currencies are not supported
# - id=1 is important as this is used by the code to find this security!
# - NOTE: use save! for debugging
if not Security.where(:id=>Smr::ID_CASH_SECURITY).first then
    Security.new(
        :id=>Smr::ID_CASH_SECURITY,
        :id_organization=>Smr::ID_UNIVERSE_ORGANIZATION,
        :type=>:cash, :fetch_quote=>0, :symbol=>'EUR',
        :brief=>'Cash Balance',
        :description=>'Cash Balance on a Broker Account'
    ).save

    cash_quote = Quote.new(
        :last=>1, :id_security=>Smr::ID_CASH_SECURITY, :date_last=>Time.new(2000,1,1).to_i
    ).save
end

