#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'asset_position'

##
# Total assets of current user at some point in time.
#
# Use #open_positions to access Smr::AssetPosition objects of all positions
# held. Then each knows more details about itself.
class Smr::Asset
    # point in Time looking at the Asset
    attr_reader :date

    # collection of Smr::AssetPosition objects of currently open positions
    attr_reader :open_positions

    # currently invested amount of money
    attr_reader :invested

    # current market value of all assets
    attr_reader :market_value

    # current profit/loss on all assets
    attr_reader :profit_loss

    # current collateral_coverage_value of all assets
    attr_reader :collateral_coverage_value

    # interest accrued on all open_positions
    attr_reader :accrued_interest

    ##
    # age of +Quote+ data before its considered old
    QUOTE_OUTDATED_DAYS = 4

    ##
    # date defines the point of Time this object sits at
    def initialize(id_user, date, options={:id_portfolio=>false, :skip_cash_positions=>false})
        raise 'date must be of Time' unless date.is_a?(Time)


        @id_user = id_user
        @date = date

        limit_portfolio = if options[:id_portfolio] then 'AND po.id=%i' % options[:id_portfolio] else '' end
        skip_cash_positions = if options[:skip_cash_positions] then 'AND s.id != %i' % Smr::ID_CASH_SECURITY else '' end

        @invested = 0.0
        @market_value = 0.0
        @profit_loss = 0.0
        @collateral_coverage_value = 0.0
        @accrued_interest = 0.0

        @_have_old_quotes = false
        @_have_missing_quotes = false

        @open_positions = Array.new

        # ATTENTION: not portable!
        # - thats not the preferred way to do a query.
        # - this one is just too complex for ActiveRecord at the moment
        open_positions = Smr::AssetPositions.new(@date, '
        SELECT
            p.id
       /*   p.id_security,
            p.id_portfolio,   # if those values could be used
            pr.id_order,      # things would be much faster since
            pr.id_position,   # subsequent SmrPosition objects query
            pr.date_created,  # these again... too many queries...
            pr.shares,
            pr.invested */
        FROM
            position p,
            position_revision pr,
            security s,
            portfolio po,
            (SELECT
                MAX(pr1.date_created) AS LASTDATE,
                pr1.id_position AS LASTPOSITION
                FROM
                    position_revision pr1
                WHERE
                    pr1.date_created<=%i
                GROUP BY pr1.id_position
            ) AS blah
        WHERE
            p.id=pr.id_position
            %s
            %s
            AND p.id_portfolio=po.id
            AND s.id=p.id_security
            AND po.id_user=%i
            AND (p.date_closed=0 OR p.date_closed>%i)
            AND pr.date_created=LASTDATE
            AND pr.id_position=LASTPOSITION
            AND po.name NOT LIKE "watchlist:%%"   /* HACK for speed on old DBs */
        GROUP BY p.id
        ORDER BY pr.date_created' % [@date, limit_portfolio, skip_cash_positions, @id_user, @date]).get_all

        # turn Position objects into AssetPosition
        # - for the sake of performance we calculate totals right here
        open_positions.each do |p|
            sp = Smr::AssetPosition.new(p.id, id_user, @date)
            @open_positions << sp

            @invested += sp.invested
            @market_value += sp.market_value if sp.market_value
            @profit_loss += sp.profit_loss if sp.profit_loss
            @collateral_coverage_value += sp.collateral_coverage_value
            @accrued_interest += sp.accrued_interest(:on_holdings=>true, :on_orders=>false)

            @_have_missing_quotes = true unless sp.market_value
            if sp.quote_time and sp.quote_time < QUOTE_OUTDATED_DAYS.days.ago then
                @_have_old_quotes = true
            end
        end
    end

    public

    # tell whether this Asset report includes outdated quotes
    def have_old_quotes?
        @_have_old_quotes
    end

    # tell whether this Asset report is lacking quote data
    def have_missing_quotes?
        @_have_missing_quotes
    end
end # class
