#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'smr/dividend_income'
require 'smr/link'

#
# A position at some point in time.
#
# It provides all information from a positions point of view. Like the Security
# held, Order (s) that were issued and the PositionRevision (s) it created.
#
# A AssetPosition is in one of three logical states of valuation, depending on
# the date given to #new:
#
# <b>open</b>::
#  does valuation with market value at date
# <b>closed, date before closure</b>::
#  does valuation with market value at date with securities held at date
# <b>closed, date after closure</b>::
#  does final gain calculation
#
# Use #is_closed? and #is_viewed_before_closure? to check for the state as a
# number of methods will just return false when called in the wrong state.
#
class Smr::AssetPosition
    include Comparable

    # accessors to underlying Position for compatibility
    attr_reader :id
    attr_reader :id_portfolio
    attr_reader :id_security
    attr_reader :comment

    # Time at which we are looking at the Position.
    attr_reader :date

    # Security held by this position.
    attr_reader :security

    # Portfolio this position is in.
    attr_reader :portfolio

    # User holding this position.
    attr_reader :user

    ##
    # Total number of shares held at this point in time.
    attr_reader :shares

    # Quote found for Security, its the most recent one before given :date
    attr_reader :last_quote

    # Collection of PositionRevision with respect to the point in time we are
    # at.
    attr_reader :revisions

    # Collection of Order (s) in pending state, ie those not executed yet.
    # FIXME: test expiry and canceled state, see #has_pending_orders?
    attr_reader :pending_orders

    ##
    # Charges paid on all orders
    attr_reader :charges

    ##
    # initialize with Position id and User id
    #
    # initilization will fail if +id_position+ is not part of a Portfolio owned
    # by the User specified by +id_user+.
    def initialize(id_position, id_user, date=Time.now)

        @user = User.where(:id=>id_user).first
        @position = Position.where(:id=>id_position).joins(:Portfolio).where('portfolio.id_user=%i'% id_user).first
        raise 'possible security violation: id_position=%i does not belong to id_user=%i.' % [id_position, id_user] if not @position.is_a?(Position)

        @id = id_position
        @id_portfolio = @position.id_portfolio
        @id_security = @position.id_security
        @closed = @position.date_closed
        @comment = @position.comment
        @date = date
        @orders = Array.new
        @pending_orders = Array.new
        @dividend = nil
        @shares = 0.0
        @sum_buy_orders = 0.0
        @sum_sell_orders = 0.0
        @accrued_interest_on_orders = 0.0
        @charges = 0.0

        @viewed_before_closure = false

        # adjust date in case we are closed in the future
        # - subsequent gain calculations produce bogus results otherwise
        if @position.date_closed > 0 and @date < @position.time_closed then
            @viewed_before_closure = true
        end

        # preload things to SELECT them only once
        # - even ActiveRecords CACHE takes time and we can be more intelligent
        @security = @position.Security
        @portfolio = Portfolio.where(:id=>@position.id_portfolio, :id_user=>id_user).first
        @last_quote = @security.last_quote(@date)
        @revisions = @position.PositionRevision.order(:date_created).where('date_created <= %i' % @date).to_a
        @revisions.each do |r|
            if r.id_order != 0 then
                o = r.Order
                @orders << o
                @charges += o.charges
                if o.is_purchase? then
                    @shares += o.shares
                    @sum_buy_orders += o.volume
                    @accrued_interest_on_orders -= o.accrued_interest
                else
                    @shares -= o.shares
                    @sum_sell_orders += o.volume
                    @accrued_interest_on_orders += o.accrued_interest
                end
            end
        end
        @pending_orders=Order.where(:id_position=>@id).pending.order(:date_issued=>:desc)
    end

    public

    ##
    # Descriptive String of the position and its contents, human readable.
    def to_s(options={:with_details=>false})
        if options[:with_details] then
            '%s (%i shares held at %s)' % [@security, shares, @portfolio]
        else @security.to_s end
    end

    ##
    # Currency code this position is held in.
    # - note: currencies are not implemented yet, so this returns the default
    def currency
        Smr::DEFAULT_CURRENCY
    end

    ##
    # Tell whether there are orders waiting for execute, expiry or cancelation.
    def has_pending_orders?
        not @pending_orders.empty?
    end

    ##
    # Smr::DividendIncome object providing Dividend information as of #date.
    def dividend
        if not @dividend then @dividend = Smr::DividendIncome.new(self) end
        @dividend
    end

    ##
    # Wrapper to Position object
    def is_closed?
        @position.is_closed?
    end
    def date_closed
        @position.date_closed
    end
    def time_closed
        @position.time_closed
    end
    def date_record_created
        @position.date_record_created
    end
    def time_record_created
        @position.time_record_created
    end

    ##
    # Tell whether this is a cash deposit, also see Transaction.
    def is_cash_position?
        @position.is_cash_position?
    end

    ##
    # Accrued interest balance.
    #
    # Options specify what is included:
    # - :on_orders is interest paid on BUY orders or received on SELL orders
    # - :on_holdings is interest accrued on #shares held in this position
    #
    # Default behaviour is to return interest paid :on_orders because of historical
    # compatibility.
    def accrued_interest options={}
        options.reverse_merge!({
            :on_orders=>true, :on_holdings=>false
        })
        ai = 0

        ai += @accrued_interest_on_orders if options[:on_orders]
        ai += @security.accrued_interest(@date).to_f * shares if options[:on_holdings]
        return ai
    end

    ##
    # Close the Position.
    #
    # This will close the position right away in case it is empty. +True+ is
    # returned in that case.
    #
    # In case it is not empty a new Order is returned, that will sell it off
    # and close it if executed. That Order may be presented for editing first,
    # ie to add charges, execution price, etc...
    #
    # +False+ is returned in case the position has been closed already, even if
    # that happened 'in the future' (given date > smr_browse_date or Time.now).
    #
    # Closing a Smr::AssetPosition is a one-time-only operation. New orders are
    # not possible once this has been done. Closing is only possible when the
    # number of shares held is zero.
    def close(date=@date)
        return false if is_closed?
        
        if shares.between? -0.01, 0.01
            # at 0 or when a very tiny amount remains, we close.
            # NOTE:
            # - see :precision and :scale on float type columns, it rounds to a scale of 4 digits
            # - http://stackoverflow.com/questions/23120584/why-does-mysql-round-floats-way-more-than-expected
            @position.date_closed = date.to_i
            @position.save!
            return true
        elsif not is_new? and date > @date
            # need to look ahead in time
            p = Smr::AssetPosition.new(@position.id, @user.id, date)
            return true if p.close(date).is_a?(TrueClass)
        else
            return Order.new(
                :id_position=>@id, :type=>'sale', :date_issued=>date.to_i,
                :shares=>shares, :limit=>(last_quote.last or 0),
                :comment=>'Selling off to close position.'
            )
        end
    end

    ##
    # Settle position in case its a cash position.
    #
    # Settlements happens at the end of year of given :date. Usually
    # smr_browse_date.
    def settle
        raise 'only cashpositions can be settled' unless is_cash_position?
        this_year = @revisions.first.time_created
        next_year = this_year + 1.year

        # re-read self at end of year (the year it was opened)
        position_eoy = Smr::AssetPosition.new(@position.id, @user.id, this_year.end_of_year)

        # put current balance into cashposition of next year
        forward_position = Smr::Transaction::find_cashposition(@portfolio.id, @user.id, next_year)
        forward_transaction = Smr::Transaction.new(forward_position, @user, :type=>:cash_booking)
        forward_transaction.book(
            position_eoy.shares,
            :time=>next_year.beginning_of_year + 1.second,
            :comment=>'balance carried over from %s' % this_year.year
        )

        # make settlement booking to zero balance
        # - settling time is right at midnight, end of year to have the last balance shown
        #   on december 31st, while it disappears on january 1st
        settle_transaction = Smr::Transaction.new(@position, @user, :type=>:cash_booking)
        settle_transaction.book(
            position_eoy.shares * -1,
            :time=>this_year.end_of_year + 1.second,
            :comment=>'settlement of %s, balance has been carried over into %s' % [this_year.year, next_year.year]
        )

        # re-read self at end of year and close
        position_eoy = Smr::AssetPosition.new(@position.id, @user.id, this_year.end_of_year + 1.second)
        raise 'cashposition not empty after #settle transaction' unless position_eoy.close.is_a?(TrueClass)
    end

    ##
    # A Position without Order is considered new.
    def is_new?
        @revisions.empty? and not is_closed? or (@revisions.count == 1 and @revisions.first.id_order == 0)
    end

    ##
    # Tell if this is a short position.
    def is_short?
        shares < 0
    end

    ##
    # Tell whether this Position has been closed in the future.
    def is_viewed_before_closure?
        @viewed_before_closure
    end

    ##
    # Total money invested here at date or false if settled at given date.
    def invested
        return false if is_closed? and not is_viewed_before_closure?
        return 0 if revisions.count==0
        revisions.last.invested
    end

    ##
    # charges per 1k of purchase volume
    def charges_per_kilo
        charges / (purchase_volume / 1000)
    end

    ##
    # Price this position has cost per share.
    def cost_price
        return 0.0 if shares.zero? or is_closed?
        invested / shares
    end

    ##
    # Position status as human readable string.
    def status
        if is_closed? then 'closed'
        elsif is_new? then 'new'
        else 'open' end
    end

    ##
    # Time of last Quote.
    def quote_time
        if not @last_quote then return @last_quote.time_last else false end
    end

    ##
    # Market value of position based on last price or false if there is
    # no price.
    def market_value
        if last_quote.last != 0 then shares * last_quote.last else false end
    end

    ##
    # Volume spend to purchase this position.
    def purchase_volume
        @sum_buy_orders
    end

    ##
    # Amount settled or false if position is open
    def settled_volume
        if is_closed? and not is_viewed_before_closure? then @sum_sell_orders else false end
    end

    ##
    # Profit/loss based on total cost of position and market value or
    # settlement or false if it cant be calculated.
    def profit_loss
        if is_closed? and not is_viewed_before_closure? then
            @sum_sell_orders - @sum_buy_orders 
        else
            return false unless market_value
            market_value - invested
        end
    end

    ##
    # Gain based on market value adjusted by dividend received and charges
    # payed or false if it cant be calculated
    def gain
        return false unless profit_loss
        profit_loss + dividend.received - charges + accrued_interest
    end

    ##
    # Dirty value composed of market value, dividend received and charges paid
    # or false if position is closed or it cant be calculated.
    #
    # Its what one made on the position in total. Its dirty since we do not
    # know whether dividend was re-invested in some other position. So do
    # _not_ use this to calculate asset totals.
    def dirty_value
        return false if is_closed? and not is_viewed_before_closure?
        return false unless market_value
        market_value + dividend.received - charges + accrued_interest
    end

    ##
    # Absolute value this position provides as collateral. Its composed usint
    # the #collateral_coverage_ratio of Security.
    def collateral_coverage_value
        return 0.0 if not market_value  or is_cash_position?
        ( market_value * @security.collateral_coverage_ratio).to_f
    end

    ##
    # Smr::Link to this position
    def link
        if is_cash_position?
            Smr::Link.new :cashposition, id
        else Smr::Link.new :position, id end
    end

    ##
    # compares by profit_loss and invested, where losses have priority
    def <=>(other)
        return 0  unless profit_loss
        r=0
        have_loss = false
        both_have_loss = false

        # a value weights more than false (which means we could not calculate
        # profit_loss)
        return 1 if profit_loss!=false and other.profit_loss==false
        return -1 if profit_loss==false and other.profit_loss!=false

        # compare losses first...
        if profit_loss < 0
            have_loss = true
            if profit_loss < other.profit_loss
                r=1
            else profit_loss > other.profit_loss
                r=-1
            end
        end

        if other.profit_loss < 0
            if have_loss then both_have_loss = true end
            have_loss = true

            if profit_loss > other.profit_loss
                r=-1
            else profit_loss < other.profit_loss
                r=1
            end
        end

        # ... then investment, but only both have a loss or there is none at all
        # - compare by absolute values so short positions are inline
        if both_have_loss or not have_loss
            if invested.abs > other.invested.abs
                r=1
            elsif invested.abs < other.invested.abs
                r=-1
            end
        end

        r
    end

    ##
    # Upcoming / expected cashflows. Collection, ordered by date.
    #
    # The :type option can filter cashflows of a specific type. See
    # PositionRevision#types to learn what types of cashflow SMR knows. Note,
    # however, that a Security typemodel is not required to implement all of
    # them. If a filter is unknown to the typemodel it will not return any
    # cashflow.
    #
    # Its empty if the Security typemodel does not support cashflow predictions
    # or just has no cashflow coming up.
    def cashflow(options={:end_date=>false, :type=>:none})
        return Array.new unless @security.has_type_model?
        case @security.type
            when :bond  then amount = shares * 100
            else amount = shares
        end
        @security.get_type.cashflow(
            :amount=>amount,
            :start_date=>@date, :end_date=>options[:end_date],
            :type=>options[:type], :item_link=>Smr::Link.new(:position, @position.id)
        )
    end
end # class
  
##
# Collection of AssetPosition objects as found by an SQL statement at some
# point in time.
#
# ATTENTION::
#  the statement should only select position.id, this class takes care of
#  everything else
class Smr::AssetPositions
    ##
    # Provide Time date and SQL statement as string.
    def initialize(date, sql)
        @date = date
        @smrpositions = Array.new
        @positions = Position.find_by_sql(sql)
    end

    ##
    # Collection of AssetPosition objects.
    def get_all
        @positions
    end
end
