#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'link'

##
# Collection of most recent SmrBlogItem objects of given User.id.
#
# As of now all items from previous 30 days are collected.
class Smr::Blog

    # Optional pattern String used to match Smr::BlogItems.
    attr_reader :pattern

    ##
    # Collects BlogItem objects relevant at given :date. The given User
    # should be authenticated!
    #
    # By default all BlogItem objects within the 30 days preceeding :date are
    # shown.
    #
    # If a :pattern String is given, it will be matched against all models
    # appearing in the blog. Time constraints are ignord when a :pattern is
    # given.
    #
    def initialize(user, date=Time.now,
          options={
              :pattern=>false
          }
        )
        raise ':user must be of User' unless user.is_a?(User)
        raise ':date must be of Time' unless date.is_a?(Time)
        @collection = Array.new
        @pattern = (options[:pattern].blank? ? nil : options[:pattern])

        start_date = (date - 30.days).to_i
        end_date = date.to_i

        # find Comment records
        q = if pattern?
          Comment.where(:id_user=>user.id).ransack({:title_or_comment_cont=>@pattern})
        else
          Comment.where(:id_user=>user.id).where( :date_recorded=>(start_date..end_date) ).ransack
        end
        q.result.to_a.each do |c|
            @collection << Smr::BlogItem.new(
              c.time_recorded, c.class, c.title, c.comment,
              :link=>Smr::Link.new(:comment_edit, c.id)
            )
        end

        # find Order commentary
        q = if pattern?
         Order.joins('LEFT JOIN position p ON p.id = order.id_position')
              .joins('LEFT JOIN portfolio po ON po.id = p.id_portfolio')
              .where('po.id_user=%i' % user.id).ransack({:ransackcolumns_cont=>@pattern})
        else
          Order.where.not(:comment=>'')
              .where( :date_issued=>(start_date..end_date) )
              .joins('LEFT JOIN position p ON p.id = order.id_position')
              .joins('LEFT JOIN portfolio po ON po.id = p.id_portfolio')
              .where('po.id_user=%i' % user.id).ransack
        end
        q.result.to_a.each do |o|
          title = if o.Position.is_cash_position? and o.PositionRevision.first
                     o.PositionRevision.first.describe_type
                  else '%s %s' % [o.type.upcase, o.Security.to_s] end
          @collection << Smr::BlogItem.new(
              o.time_issued, o.class, title, o.comment,
              :link=>Smr::Link.new(:order, o.id)
          )
        end

        # find Quoterecord commentary
        q = if pattern?
          Quoterecord.where(:id_user=>user.id).ransack({:ransackcolumns_cont=>@pattern})
        else
          Quoterecord.where(:id_user=>user.id)
              .where( :date_created=>(start_date..end_date) )
              .where.not(:comment=>'').ransack
        end
        q.result.to_a.each do |qr|
              @collection << Smr::BlogItem.new(qr.time_created, qr.class, qr.to_s, qr.comment)
        end

        # find Document commentary
        q = if pattern?
          Document.where(:id_user=>user.id).ransack({:comment_cont=>@pattern})
        else
          Document.where(:id_user=>user.id)
              .where( :date_upload=>(start_date..end_date) )
              .where.not(:comment=>'').ransack
        end
        q.result.to_a.each do |d|
            @collection << Smr::BlogItem.new(d.time_upload, d.class, 'Commented Upload', d.comment)
        end

        # find Workdesk entries
        q = if pattern?
          Workdesk.where(:id_user=>user.id).ransack({:ransackcolumns_cont=>@pattern})
        else
          Workdesk.where(:id_user=>user.id)
              .where( :date_added=>(start_date..end_date) )
              .where.not(:comment=>'').ransack
        end
        q.result.to_a.each do |wi|
              @collection << Smr::BlogItem.new(wi.time_added, wi.class, wi.to_s, wi.comment)
        end

        @collection.sort_by!{|i| i.date}.reverse!
        true
    end

    ##
    # Tells whether this Blog actually performed a pattern match.
    def pattern?
        not @pattern.blank?
    end

    ##
    # loops over Smr::BlogItem collection
    def each(&block)
        @collection.each(&block)
    end

    ##
    # number of Smr::BlogItem objects the blog has available for viewing.
    def count
        @collection.count
    end

    ##
    # tell whether there is anything in the collection
    def empty?
        @collection.empty?
    end
end
  
  
##
# Represents a single Blog item, ie. for rendering in a view.
class Smr::BlogItem

    # reader for data field
    attr_reader :date
    attr_reader :type
    attr_reader :title
    attr_reader :body
    attr_reader :link

    ##
    # - optionally pass a Smr::Link as :link to reference this BlogItem to
    #   something else
    def initialize(date, type, title, body, options={})
        options.reverse_merge!({
          :link=>false
        })
        raise ':date must be of type Time' unless date.is_a? Time
        raise ':link must be of Smr::Link' if options[:link] and not options[:link].is_a? Smr::Link
        @date=date; @type=type.to_s; @title=title; @body=body;
        @link=options[:link]
    end

    ##
    # Identifier of this item. Unique hopefully and a String.
    def id
        ::Digest::MD5.hexdigest title + date.to_i.to_s
    end
end
