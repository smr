#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

require 'cashflowitem'

##
# Forecast of Cashflows potentially to be received.
#
# Makes use of #cashflow provided by Smr::AssetPosition.
class Smr::CashflowForecast
    def initialize(start_date, end_date, id_user)
        raise ':start_date must be of Time' unless start_date.is_a?(Time)
        raise ':end_date must be of Time' unless end_date.is_a?(Time)

        @start_date=start_date; @end_date=end_date; @id_user=id_user;

        @filter = :none
        @portfolio_id = 0
        @organization_id = 0
        @security_id = 0
        @forecast = Array.new
        @made_forecast = false
    end

 public

    ##
    # loops over CashflowItems we have up to date
    def each(&block)
        generate_forecast unless @made_forecast
        @forecast.each(&block)
    end

    ##
    # tell whether there is something
    def empty?
        generate_forecast unless @made_forecast
        @forecast.empty?
    end

    ##
    # list of filters that may be used on us. Also see set_filter().
    def self.filters
        {
          'No Filter'            => :none,

          # compatibility: see PositionRevision.describe_type
          'Dividend / Interest Income' => :dividend_booking,
          'Redemption' => :redemption_booking,
        }
    end
    def filters
        Smr::CashflowForecast.filters
    end

    ##
    # apply a filter when creating the forecast, see filters()
    def set_filter(filter)
       raise ':filter must be one value from those filters() returns' unless Smr::CashflowForecast.filters.has_value?(filter)
       @made_forecast = false
       @filter=filter
    end

    ##
    # set where to report on?
    # - maybe a numerical :id or Portfolio object
    # - default is 0, meaning it reports on all portfolios
    def set_portfolio(portfolio)
       @made_forecast = false
       if portfolio.is_a? Portfolio
          @portfolio_id = portfolio.id
       elsif portfolio.to_i >= 0
          @portfolio_id = portfolio.to_i
       else
          raise ':portfolio must be numerical >= 0 or a Portfolio instance'
       end
    end

    ##
    # adds a Organization to the forecast
    # - this will look for +all+ Security records belonging to the given
    #   Organization
    # - maybe a numerical :id or Organization object
    # - default is 0, meaning not report on any Organization
    def set_organization(organization)
       @made_forecast = false
       if organization.is_a? Organization then @organization_id = organization.id
       elsif organization.to_i >= 0       then @portfolio_id = organization.to_i
       else  raise ':organization must be numerical >= 0 or a Organization instance'  end
    end

    ##
    # adds a Security to the forecast
    # - maybe a numerical :id or Security object
    def set_security(security)
       @made_forecast = false
       if security.is_a? Security then @security_id = security.id
       elsif security.to_i >= 0       then @security_id = security.to_i
       else  raise ':security must be numerical >= 0 or a Security instance'  end
    end

    ##
    # cumulated cashflow from all CashflowItems
    def total
        generate_forecast unless @made_forecast
        total = 0.0
        @forecast.collect{|i| total += i.total }
        total
    end

    ##
    # number of CashflowItems contained in the forecast
    def count
        generate_forecast unless @made_forecast
        @forecast.count
    end

 protected

    ##
    # compose the actual forecast
    def generate_forecast
        @forecast.clear

        if @organization_id > 0
          Organization.find(@organization_id).Security.each { |s|
              @forecast += s.cashflow(
                  :start_date=>@start_date, :end_date=>@end_date, :type=>@filter,
                  :item_link=>Smr::Link.new(:security, s.id)
              ) if s.has_type_model?
          }
        elsif @security_id > 0
          s = Security.find(@security_id)
          @forecast += s.cashflow(
              :start_date=>@start_date, :end_date=>@end_date, :type=>@filter,
              :item_link=>Smr::Link.new(:security, s.id)
          ) if s.has_type_model?
        else
          a = Smr::Asset.new(
            @id_user,
            @start_date,
            :skip_cash_positions=>true,
            :id_portfolio=>(@portfolio_id > 0 ? @portfolio_id : false)
          )
          a.open_positions.each{ |ap|
              @forecast += ap.cashflow(
                  :end_date=>@end_date, :type=>@filter,
                  :item_link=>Smr::Link.new(:position, ap.id)
              )
          }
        end

        @forecast.sort!
        @made_forecast = true
    end
end
