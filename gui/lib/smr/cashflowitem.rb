#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# Represents a single item of cashflow. Useful for viewing.
class Smr::CashflowItem
    include Comparable

    ##
    # Reader for given attribute, see #new
    attr_reader :date
    attr_reader :what
    attr_reader :amount_per_share
    attr_reader :poolfactor
    attr_reader :comment
    attr_reader :type

    # number of shares (or face value) that produced this cashflow item
    attr_writer :shares

    # Smr::Link to whatever has created this item, may be false
    attr_accessor :link

    ##
    # A piece of cashflow at a specific time.
    #
    # By default its for a single share. But can be scaled by using
    # #shares=X.Y or specifying options[:shares].
    #
    # Note options[:poolfactor]. This is also applied. Make sure to set it
    # correctly if one existed at :date.
    #
    # Pass a Smr::Link in via :link option to make use of the #link method.
    def initialize(date, what, amount_per_share, options={})
      raise ':date must be Time' unless date.is_a? Time
      raise ':amount_per_share must be Integer, Float or Percentage' unless [Integer, Float, Percentage].include? amount_per_share.class

      options.reverse_update({
          :comment=>nil, :type=>false, :poolfactor=>1.0, :shares=>1.0,
          :link=>false,
      })

      @date=date
      @what=what
      @amount_per_share=amount_per_share
      @options = options
      @comment = options[:comment]
      @shares = options[:shares]
      @type = options[:type]
      @link = options[:link]

      @poolfactor = options[:poolfactor]
      @lock_poolfactor = false
    end

    ##
    # Human readable description of #type.
    def describe_type
      PositionRevision.describe_type(@options[:type]) if PositionRevision.types.include?(@options[:type])
    end

    ##
    # Human readable representation of this item.
    def to_s
      '%s: %-25.24s %10.4f (pf=%4.2f)' % [date, describe_type, total, @poolfactor]
    end

    ##
    # Absolute amount of cashflow.
    #
    # It is scaled to #shares and the :poolfactor is applied unless its a
    # redemption payment which causes the poolfactor to shrink in most cases.
    def total
      @lock_poolfactor = true
      if @type == :redemption_booking
          @shares * @amount_per_share
      else @shares * @amount_per_share * poolfactor end
    end

    ##
    # Compares items by :date, then by :total amount
    def <=>(other)
      comp = (date <=> other.date)
      if comp.zero? then (total <=> other.total) else comp end
    end

    ##
    # Adjust :poolfactor to some other value.
    # Works only once and not at all after #total has been called!
    def adjust_poolfactor(value)
      raise '#adjust_poolfactor should never be called twice, check algorithm.' if @lock_poolfactor
      @lock_poolfactor = true
      @poolfactor = value
    end
end
