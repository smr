#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

require 'smr/cashflowitem'
require 'smr/link'

##
# Log of Cashflows happened up to :date
#
# Cashflow is generated from Order executions and Dividend received.
class Smr::CashflowLog
    def initialize(start_date, end_date, id_user)
        raise ':start_date must be of Time' unless start_date.is_a?(Time)
        raise ':end_date must be of Time' unless end_date.is_a?(Time)

        @start_date=start_date.to_i; @end_date=end_date.to_i; @id_user=id_user;

        @filter = :none
        @portfolio_id = 0
        @made_log = false
        @log = Array.new
    end

 public

    ##
    # loops over CashflowItems we have up to date
    def each(&block)
        generate_log if not @made_log
        @log.each(&block)
    end
  
    ##
    # tell whether there is something
    def empty?
        generate_log if not @made_log
        @log.empty?
    end

    ##
    # list of filters that may be used on us. Also see set_filter().
    def self.filters
        f = {
           'No Filter'          => :none,
           'All Cost Charged'   => :charges,
           'Provisions Charged' => :provision,
           'Courtage Charged'   => :courtage,
           'Expenses Charged'   => :expense,
        }
        PositionRevision.types.keys.each{ |t| f[PositionRevision.describe_type(t)]=t.to_sym }
        f
    end
    def filters
        Smr::CashflowLog.filters
    end

    ##
    # apply a filter when creating the log, see filters()
    def set_filter(filter)
       raise 'filter must be one value from those filters() returns' unless filters.has_value?(filter)
       @made_log = false
       @filter=filter
    end

    ##
    # set where to report on?
    # - maybe a numerical :id or Portfolio object
    # - default is 0, meaning it reports on all portfolios
    def set_portfolio(portfolio)
       @made_log = false
       if portfolio.is_a? Portfolio
          @portfolio_id = portfolio.id
       elsif portfolio.to_i >= 0
          @portfolio_id = portfolio.to_i
       else
          raise 'portfolio must be numerical >= 0 or a Portfolio instance'
       end
    end

    ##
    # cumulated cashflow from all Smr::CashflowItems
    def total
        generate_log if not @made_log
        total = 0.0
        @log.collect{|i| total += i.total }
        total
    end

    ##
    # number of Smr::CashflowItems contained in the log
    def count
        generate_log if not @made_log
        @log.count
    end

 protected

    ##
    # compose a log of cashflows from orders and dividend received
    def generate_log
        @made_log = true
        @log.clear
        limit_portfolio = @portfolio_id > 0 ? 'and portfolio.id=%i' % @portfolio_id : ''

        # find cash positions
        cps = Position.where(:id_security=>Smr::ID_CASH_SECURITY)
              .joins(:Portfolio)
              .where('portfolio.id_user=%i %s' % [@id_user, limit_portfolio])
              .to_a

        cps.each do |cp|
          revs = PositionRevision.where(:id_position=>cp.id, :date_created=>@start_date..@end_date)
          revs = revs.where(:type=>PositionRevision.types[@filter]) if PositionRevision.types.include? @filter
          revs.each do |pr|
            if o = pr.Order.get_trigger_order
                # cashflow triggered by some order, usually purchase or sell of shares
                case @filter
                  when :charges
                      tmp = Array.new
                      tmp << '%.2f Provisions' % o.provision if o.provision > 0
                      tmp << '%.2f Courtage' % o.courtage if o.courtage > 0
                      tmp << '%.2f Expenses' % o.expense if o.expense > 0
                      @log << Smr::CashflowItem.new(
                           pr.time_created, 'paid %s' % tmp.join(', '), o.charges * -1,
                          :comment=>pr.Order.to_s,
                          :link=>Smr::Link.new(:order, o.id)
                      ) unless tmp.empty?

                  when :provision
                      @log << Smr::CashflowItem.new(
                          pr.time_created, 'Provisions charged', (o.provision * -1),
                          :comment=>pr.Order.to_s,
                          :link=>Smr::Link.new(:order, o.id)
                      ) if o.provision > 0

                  when :courtage
                      @log << Smr::CashflowItem.new(
                          pr.time_created, 'Courtage charged', (o.courtage * -1),
                          :comment=>pr.Order.to_s,
                          :link=>Smr::Link.new(:order, o.id)
                      ) if o.courtage > 0

                  when :expense
                      @log << Smr::CashflowItem.new(
                          pr.time_created, 'Expenses charged', (o.expense * -1),
                          :comment=>pr.Order.to_s,
                          :link=>Smr::Link.new(:order, o.id)
                      ) if o.expense > 0

                  else
                     @log << Smr::CashflowItem.new(
                       pr.time_created, pr.describe_type,
                       if pr.Order.is_sale? then pr.Order.volume * -1 else pr.Order.volume end,
                       :comment=>o.to_s, :type=>pr.type, :link=>Smr::Link.new(:order, o.id)
                     )
                end
            else
                # cashflow not triggered by anything
                link = case pr.type.to_sym
                  when :cash_booking, :dividend_booking, :redemption_booking, :salary_booking
                      Smr::Link.new(:cashposition, pr.id_position)
                  else Smr::Link.new(:position, pr.id_position)
                end

                @log << Smr::CashflowItem.new(
                    pr.time_created, pr.Order.to_s,
                    if pr.Order.is_sale? then pr.Order.volume * -1 else pr.Order.volume end,
                    :comment=>pr.Order.comment, :type=>pr.type, :link=>link
                ) if @filter == :none or PositionRevision.types.include? @filter
            end
          end
        end

        compat_pre_cashposition_code

        @log.sort_by!{ |i| i.date}.reverse!
    end

    ##
    # constructs cashflow from data before cashpositions where implemented
    # - its slow, but necessary for data before end of March 2015
    # - does nothing for newer data
    # - => therefore CashflowLog behaves slow when browsing older data and
    #   fast on more recent records.
    def compat_pre_cashposition_code
        end_date_compatdata = Time.new(2015,3,31).to_i
        end_date = if @end_date > end_date_compatdata then end_date_compatdata else @end_date end
        limit_portfolio = @portfolio_id > 0 ? 'and po.id=%i' % @portfolio_id : ''

        return false if @start_date > end_date_compatdata

        if [:none, :orders, :charges, :provision, :courtage, :expense].include?(@filter) then
          type = :cash_booking

          # cashflow from transactions
          orders = Order.where(:date_issued=>(@start_date..end_date))
              .where('quote > 0')
              .joins('LEFT JOIN position_revision pr ON pr.id_order = order.id')
              .joins('LEFT JOIN position p ON p.id = order.id_position')
              .joins('LEFT JOIN portfolio po ON po.id = p.id_portfolio')
              .joins('LEFT JOIN security s ON s.id = p.id_security')
              .where('s.id != %i' % Smr::ID_CASH_SECURITY)
              .where('po.id_user=%i %s' % [@id_user, limit_portfolio])
              .order(:date_issued=>:desc)

          orders.each do |o|
                case @filter
                    when :charges
                        tmp = Array.new
                        tmp << '%.2f Provisions' % o.provision if o.provision > 0
                        tmp << '%.2f Courtage' % o.courtage if o.courtage > 0
                        tmp << '%.2f Expenses' % o.expense if o.expense > 0

                        what = 'paid %s' % tmp.join(', ')
                        total = (o.provision + o.courtage + o.expense) * -1

                    when :provision
                        what = 'Provisions charged'
                        total = o.provision * -1

                    when :courtage
                        what = 'Courtage charged'
                        total = o.courtage * -1

                    when :expense
                        what = 'Expenses charged'
                        total = o.expense * -1

                    else # :orders and :none
                        what = o.PositionRevision.first.describe_type
                        total = o.volume
                        total *= -1 if o.is_purchase?
                        type = :order_booking
                end

                next if total == 0.0 # skip everything that did not create any cashflow
                @log << Smr::CashflowItem.new(
                  o.time_executed, what, total,
                  :comment=>o.to_s, :type=>type, :link=>Smr::Link.new(:order, o.id)
                )
            end
        end

        if [:none, :dividend_booking].include?(@filter) then
            # cashflow from dividend received
            Position.select(:id)
                .joins(:Portfolio)
                .where('portfolio.id_user'=>@id_user)
                .where('(position.date_closed=0 OR position.date_closed>=%i) OR (position.date_closed BETWEEN %i AND %i)' % [end_date, @start_date, end_date]).each do |p|

                ap = Smr::AssetPosition.new(p.id, @id_user, Time.at(end_date))
                ap.dividend.payments.each do |dp|
                    next if dp.exdate.to_i < @start_date
                    next if dp.received_total == 0.0 # skips positions sold right before ex-Div date
                    @log << Smr::CashflowItem.new(
                        dp.exdate, PositionRevision.describe_type(:dividend_booking), dp.received_total,
                        :comment=>'from %s' % ap.security.to_s, :type=>:dividend_booking,
                        :link=>Smr::Link.new(:position, ap.id)
                    )
                end
            end
        end
    end
end
