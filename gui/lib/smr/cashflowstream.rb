#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'percentage'
require 'cashflowitem'

##
# Build stream of cashflow from given parameters.
#
# The stream is a collection of Smr::CashflowItem and behaves as such.
#
# It implements caching to improve speed. Keep objects as long as possible.
# See :id_for_caching option.
class Smr::CashflowStream

    # cache streams of cashflow on the class level, see :id_for_caching option
    @@cache = {}

    ##
    # Build the stream of Smr::CashflowItem
    # - :amount_per_share may me be a Integer, a Float (ie. in case of dividend)
    #   or a Percentage (ie. in case of a bond cupon)
    # - set :split_amount_by_interval=>true to have the :amount_per_share split
    #   down to a sub-annual amount by using the :payment_interval interval.
    #   Useful for bonds where the coupon is often specified for a full year
    #   while each sub-annual payment is a fraction of that.
    # - optional :payment_interval and :redemption_interval are in months
    # - optional :last_payment is the Time of the last payment, default
    #   assumption is 10 years forward from Time.now
    # - optional use :redemptions to specify the number of redemption
    #   payments (only really useful with bonds or when a liquidation is
    #   expected)
    # - optional set :last_redemption to the date when this is payed.
    # - optional :id_for_caching is used to store multiple streams in the cache. Useful
    #   when this class is used within a ActiveRecord model.
    #   Make shure to pass a uniq identifier, ie a symbol
    # - optional :item_description is used to put meaning to the Smr::CashflowItem objects
    #   created
    # - optional :item_link may be a Smr::Link which will be added to all
    #   Smr::CashflowItem objects in the stream
    def initialize(amount_per_share, first_payment=Time.now, options={})
        raise ':amount_per_share must be Integer, Float or Percentage' unless [Integer, Float, Percentage].include? amount_per_share.class
        raise ':first_payment must be of Time' unless first_payment.is_a? Time

        options.reverse_merge!({
            :payment_interval=>12, :last_payment=>0, :split_amount_by_interval=>false,
            :redemptions=>0, :redemption_interval=>12, :redemption_price=>100.0, :last_redemption=>0,
            :id_for_caching=>:default, :item_description=>'', :item_link=>false
        })
        options[:last_payment] = Time.now + 10.years if options[:last_payment].to_i == 0

        @amount_per_share = amount_per_share
        @first_payment = first_payment
        @options = options
        @stream = @@cache.fetch(options[:id_for_caching], build_stream)
    end

    ##
    # Return stream scaled to the given :amount. Optionally specify a
    # :start/:end Time frame and/or a :filter on Smr::CashflowItem#type.
    #
    # The :item_link option may be used to set a link on each Smr::CashflowItem
    # before its returned. It should be a Smr::Link.
    def get(amount, options={:filter=>:none})
        @stream.map{ |i|
            next if options[:start] and i.date < options[:start]
            next if options[:end] and i.date > options[:end]
            if options[:filter] and options[:filter] != :none
                next if i.type != options[:filter]
            end

            i.link=options[:item_link] if options[:item_link]
            i.shares=amount
            i
        }.compact
    end

    ##
    # debugging: @stream and @@cache as human readable text table
    def debug_stream
        @stream.each{|i| p ' | %s ' % i.to_s}
        true
    end

    def debug_cache
        @@cache.each{|k,v| p ' | %s => has stream of %s items cached' % [ k, v.count ] }
        true
    end

  protected

    ##
    # The cashflow stream of the entire lifetime of the security.
    #
    # Note that the :total attribute of Smr::Cashflowitem is [mis]used as a
    # factor at this point! The other methods will convert to absolute amounts
    # later, depending on whats requested.
    def build_stream
       s = Array.new

       if @first_payment == Smr::TIME_ZERO
          return @@cache.store(@options[:id_for_caching], s)
       end

       # loop payments
       p = @first_payment
       while p <= @options[:last_payment]
           s << Smr::CashflowItem.new(
               p, 'Interest/Dividend Payment',
                (@options[:split_amount_by_interval] ? @amount_per_share*(@options[:payment_interval]/12.0) : @amount_per_share),
               :type=>:dividend_booking, :comment=>@options[:item_description].to_s, :item_link=>@options[:link]
           )
           p += @options[:payment_interval].months
       end

       # loop redemptions
       # - backwards since we start from @last_redemption
       # - add one second to the redemption to make it
       #   happen after (!) a eventual coupon due the
       #   same day
       #   (all dates are at 0:00.0, or beginning of day)
       if @options[:redemptions] > 0
          r = @options[:last_redemption] + 1.second
          ri = @options[:redemptions].to_f  # redemption interval: 'first is last'
          for i in 1..@options[:redemptions]
              s << Smr::CashflowItem.new(
                      r,
                      'Redemption',
                      (@options[:redemption_price] / 100.0 / @options[:redemptions]),
                      :type=>:redemption_booking, :comment=>@options[:item_description],
                      :poolfactor=>(@options[:redemptions] - ri) / @options[:redemptions]
              )
              r -= @options[:redemption_interval].months
              ri -= 1
          end
       end
       s.sort!

       # adjust poolfactors of items following redemptions
       adjust_to = false
       s.cycle(1) do |i|
        if i.type == :redemption_booking
            adjust_to = i.poolfactor
        elsif adjust_to
            i.adjust_poolfactor(adjust_to)
        end
       end

       @@cache.store(@options[:id_for_caching], s)
    end
end
