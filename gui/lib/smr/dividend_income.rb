#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# Dividend received on a AssetPosition at some point in time.
class Smr::DividendIncome
    ##
    # initialize with Smr::AssetPosition
    def initialize (position)
      raise 'position must be a Smr::AssetPosition' unless position.is_a?(Smr::AssetPosition)
      @position = position
      @payments = query_payments
      @error = Array.new
    end

  public

    ##
    # Collection of DividendIncomeItem objects received.
    attr_reader :payments

    # returns String describing the error when some methode returned False
    def error
      @error.join(', ')
    end

    ##
    # Returns total Dividend received, after tax and other costs.
    def received
      received = 0
      @payments.each{|p| received += p.received_total }
      received
    end

    def empty?
      @payments.empty?
    end

    ##
    # Add Dividend payment to this Position.
    #
    # :id_order should refer to the cash booking which represents the
    # actually received amount (after tax and other costs).
    def add_payment(paid, id_order, exdate=Time.now)
      Dividend.new(
          :date_exdate=>exdate.to_i, :paid=>paid, :id_order=>id_order,
          :id_security=>@position.id_security, :id_position=>@position.id
      ).save!
    end

  protected

    ##
    # Queries eligible Dividend payments to create DividendIncomeItem collection.
    def query_payments
      payments = Array.new

      # OLD records
      # - pre 20150403195501_enhance_dividend.rb migration
      # - slow because revisions must be looped!
      Dividend.where(:id_security=>@position.security.id, :id_order=>0, :id_position=>0)
          .where('date_exdate <= %i' % @position.date).each do |d|
          @position.revisions.each do |pr|
              payments[d.id] = Smr::DividendIncomeItem.new(d, :position_revision=>pr) if pr.date_created <= d.date_exdate
          end
      end

      # NEW records
      # - with received amount being a cash booking
      Dividend.where(:id_position=>@position.id)
          .where('date_exdate <= %i' % @position.date).order(:date_exdate=>:asc).each do |d|
          payments[d.id] = Smr::DividendIncomeItem.new(d)
      end

      payments.compact! || Array.new
    end
end

##
# Represents a single Dividend payment.
class Smr::DividendIncomeItem

  # Unixtime when the security traded ex-Dividend and when payment was received.
  attr_reader :exdate
  attr_reader :valutadate

  # amount received per share and in total at :valutadate
  attr_reader :received
  attr_reader :received_total

  # amount the company has paid at :date_exdate
  attr_reader :paid

  # number of shares and invested amount of money at :date_exdate
  attr_reader :shares
  attr_reader :invested

  ##
  # Provide Dividend and, optionally, PositionRevision record to construct a payment.
  def initialize(dividend, options={:position_revision=>false})
      @exdate=dividend.time_exdate
      @paid=dividend.paid
      @id_order=dividend.id_order

      if options[:position_revision]
          # OLD records
          # - unaware of exdate vs. valutadate
          # - unaware of paid bs. actually received
          @valutadate=@exdate
          @shares=options[:position_revision].shares
          @invested=options[:position_revision].invested
          @received=dividend.paid
          @received_total = @shares * @received
      else
          # NEW records
          # - using cash booking
          booking_rev = dividend.Order.PositionRevision.first
          position_rev = PositionRevision
              .where(:id_position=>dividend.id_position)
              .where('date_created <= %i' % dividend.date_exdate)
              .order(:date_created=>:desc)
              .first
          @valutadate = booking_rev.time_created
          @received_total = dividend.Order.volume
          @shares = position_rev.shares
          @invested = position_rev.invested
          @received = @received_total / @shares
      end
  end

  ##
  # tell whether there is a related Order available
  def has_order?
     @id_order != 0
  end

  ##
  # wrapper to cash booking order, use #has_order? first
  def order
      Order.find(@id_order) if has_order?
  end
end
