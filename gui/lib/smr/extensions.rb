
##
# Namespace for sub-modules included/extended into other Smr classes.
#
# Note some use additional mechanisms such as ActiveSupport::Concern to do
# their extending (see http://api.rubyonrails.org/classes/ActiveSupport/Concern.html)
# which implies additional dependencies.
module Smr::Extensions

end
