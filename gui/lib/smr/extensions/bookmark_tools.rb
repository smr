#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# Enhance Smr models with functionality of Bookmark records
#
# Note: works only for models with relation to Bookmark
module Smr::Extensions::BookmarkTools
   extend ActiveSupport::Concern

    included do
        ##
        # Collection of Bookmark related to this model instance. Useful to build
        # forms.
        def bookmarks_for_form(user)
            raise ':user must be a authenticated User' unless user.is_a? User
            return false unless self.id # catch Security.new.bookmarks_for_form
            Bookmark.for_user(user.id).for_object(self.class, self.id)
        end
    end
end
