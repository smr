#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'active_support/concern'

##
# Generate and define methods making date columns usable as Time
#
# Two methods will be generated:
#
# * "time_<name>"  - returns Time from wrapped "date_<name>"
# * "time_<name>=" - accepts String or Time input, converts to Integer and writes to the "date_<name>" field.
#
# Uses ActiveSupport::Concern mechanism to do that.
module Smr::Extensions::DateTimeWrapper
   extend ActiveSupport::Concern

   included do
        scope :disabled, -> { where(:disabled=>true) }

        fields = self.attribute_names.select{|m| m if m.match /^date_/ }
        fields.each do |f|
            n = f.gsub(/^date_/, 'time_')
#p '==> %s defining #%s wrapping :%s' % [ self, n, f ]
            define_method( n.to_sym ){
                Time.at read_attribute(f.to_sym)
            }

#p '==> %s defining #%s= wrapping :%s=' % [ self, n, f ]
            define_method( ('%s=' % n).to_sym ){ |string|
                t = string.to_time
                if t.between? Smr::TIME_ZERO.beginning_of_day, Smr::TIME_ZERO.end_of_day
                    v = 0
                else v = t end
                write_attribute(f.to_sym, v)
            }
        end
   end
end
