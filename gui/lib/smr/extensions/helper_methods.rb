#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#


##
# Collection if helper methods used throught the application.
#
# Its meant for small workhorses that do generic things that can be useful at
# many places. Views, Controllers, Models, ...
#
# Such methods should NOT interact directly with the database, the session,
# etc... each should be working standalone, based on its parameters only.
#
# Use `include Smr::Extensions::HelperMethods` wherever they are useful.
#
module Smr::Extensions::HelperMethods

    ##
    # mandatory type method: sums outstanding coupons/redemptions happening before
    # end of year, returns absolute number.
    # - see Security::MENDATORY_TYPE_METHODS - therefore this method exists
    # - see Smr::CashflowStream on whats behind cashflows
    def cashflow_this_year(date=Time.now)
        cf = 0
        cashflow(:start_date=>date, :end_date=>date.end_of_year).each{|i| cf += i.total}
        cf
    end
end
