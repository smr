#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# Mandatory methods for mixing into Security typemodels.
#
# This just makes the methods present, it does NOT implement them. Its up to
# the typemodels to implement something that produces a result.
module Smr::Extensions::SecurityTypemodelMandatoryMethods

    ##
    # static definition of whats provided
    MANDATORY_TYPE_METHODS = [
        :describe, :is_expired?,
        :current_yield, :yield_to_call, :yield_to_maturity,
        :shares, :cashflow, :cashflow_this_year, :has_subannual_payments?,
        :accrued_interest,
    ]

    ##
    # Human readable String describing the typemodel.
    def describe
        ''
    end

    ##
    # Number of tradable units in existance.
    def shares
        0
    end

    ##
    # Current Yield. Based on last known Quote unless some other Quote is given.
    def current_yield(quote=false)
        false
    end

    ##
    # Yield to next Call. Based on last known Quote unless some other Quote is given.
    def yield_to_call(quote=false)
        false
    end

    ##
    # Yield to final Maturity. Based on last known Quote unless some other Quote is given.
    def yield_to_maturity(quote=false)
        false
    end

    ##
    # Tell whether the Security is expired at given :date by inspection information known
    # by the type model.
    def is_expired?(date=Time.now)
        false
    end

    ##
    # Returns Smr::CashflowStream.
    def cashflow(options={:start_date=>false, :end_date=>false, :amount=>false, :type=>:none, :item_link=>false})
        Array.new
    end

    ##
    # Tell whether multiple payments can be expected within a fiscal year.
    def has_subannual_payments?
        false
    end

    ##
    # Interest accrued since last payment prior given :date.
    def accrued_interest(date=Time.now)
        0
    end

end
