#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# Namespace for reapers that do the actual fetching. Provides protected helpers
# for processing data.
module Smr::Reapers

  ##
  # List of symbols of reapers that can handle the given :security_type.
  def get_suitable(security_type)
     Smr::Reapers.constants.collect{ |reapername|
         klass = ('Smr::Reapers::%s' % reapername).constantize
         reapername if klass.type_supported? security_type
     }.compact
  end
  module_function :get_suitable

 protected

   ##
   # Translates special characters in :string.
   def clean_special_chars(string)
      replacement_rules = {
          '\r' => '',
          '\n' => '',
          '\t' => '',
      }
      matcher = /#{replacement_rules.keys.join('|')}/
      string.gsub(matcher, replacement_rules).strip
   end
end

# load reapers, each will be part of Smr::Reapers namespace
# NOTE: loading with
#
#          Dir('reapers/*.rb').each{do |f| require f}
#
#       works in development but does nothing in production.
require 'ariva'
require 'finanzen_net'
require 'frankfurt'
require 'kitco'
require 'onvista'
require 'stuttgart'
require 'universal_investment'

##
# Fetch things from the internet. Things such as metadata of a Security or a
# Quote. Therefore this module is to be mixed into the Security model. Its
# methods operate on the attributes of a Security.
module Smr::Fetch

    # Number of fails until :preferred_reaper is reset
    PREFERRED_REAPER_FAILS = 5

    ##
    # tell whether given Smr::Reapers::* object is the preferred one.
    def is_preferred_reaper? reaper
       raise 'need Smr::Reapers::* object' unless reaper.is_a? Smr::Reapers
       (reaper.class.to_s.split('::').last == self.preferred_reaper )
    end

    ##
    # Uses Smr::Reapers to fetch metadata for :symbol and set object attributes
    # appropriately.
    #
    # This will also create a new Security.type model and fills with the
    # things discovered. In case the type model exists, its attributes will be
    # updated.
    #
    # Returns true on success or false on failure.
    #
    # Reapers are called until one succeeds
    def fetch_metadata
        raise ':symbol must be set to fetch anything' if self.symbol.blank?

        Smr::Reapers.constants.each do |reaperreapername|
            reaperclass = ('Smr::Reapers::%s' % reaperreapername).constantize
            next unless reaperclass.type_supported? self.type
            reaper = reaperclass.new(self)
            next unless reaper.respond_to? :metadata

            logger.debug '== lookup metadata => %s with %s' % [ self.symbol, reaper.class ]
            if reaper.metadata
                # metadata is found
                # - cut :brief string at first number because the numerical part
                #   is created form the metadata by the :typemodel
                self.brief = self.brief.split(/\d/).first
                return true
            end
        end
        false
    end

    ##
    # Users Smr::Reapers to obtain a recent quote for :symbol.
    #
    # Returns:
    # - 0 success
    # - 1 failure from the Reaper
    # - 2 Quote was found but is older or of same time is the last known one
    # - 3 no Reaper could handle that Security
    #
    # Reapers are called until one succeeds. The reaper succeeding will be
    # remembered and is tried first at the next invokation of this method.
    # This way each Security will adjust itself to a source thats most
    # reliable, ie where there is active trading.
    #
    def update_quote
        raise ':symbol must be set to update a quote' if self.symbol.blank?
        retval = 3
        reapers = Array.new

        reapers << self.preferred_reaper.to_sym unless self.preferred_reaper.blank?
        reapers << Smr::Reapers.get_suitable(self.type) unless self.preferred_reaper_locked
        reapers = reapers.flatten.uniq

        logger.debug '== lookup quote => %s using reapers %s' % [ self.symbol, reapers.inspect ]
        reapers.each do |reapername|
            logger.debug '  trying  %s' % reapername
            begin reaper = ('Smr::Reapers::%s' % reapername).constantize.new(self)
                next unless reaper.respond_to?(:quote)

                lq = self.last_quote(Time.now, :false_if_not_found=>true, :no_caching=>true)
                if lq and lq.date_last + self.preferred_reaper_delay >= Time.now.to_i
                    logger.debug '  ... delay not passed by, skipping.'
                    next
                end

                q = reaper.quote
            rescue
               logger.fatal 'reaper crashed: %s on security.id=%i,symbol=%s' % [
                    reaper.class, self.id, self.symbol
               ]
               reset_reaper_config if is_preferred_reaper? reaper
               next
            end

            if q.is_a?(Quote)
                if not lq or q.time_last > lq.time_last
                    logger.debug'  ... quote found by %s' % reaper.class
                    begin q.save!
                    rescue
                        logger.fatal 'Quote.safe! failed on security.id=%i,symbol=%s reaper=%s msg=%s' % [
                            self.id, self.symbol, reaper.class, $!
                        ]
                        reset_reaper_config
                        retval = 1
                    else
                        reset_reaper_config reapername if self.preferred_reaper.blank?
                        scale_delay :down
                        retval = 0
                    end
                else
                    scale_delay
                    retval = 2
                end
                break
            else
                if is_preferred_reaper? reaper
                    logger.debug'  ... failcount = %i' % read_attribute(:preferred_reaper_fail_count)
                    write_attribute(:preferred_reaper_fail_count, self.preferred_reaper_fail_count + 1)
                end
                reset_reaper_config if read_attribute(:preferred_reaper_fail_count) >= PREFERRED_REAPER_FAILS
                retval = 1
            end
        end

        logger.debug'  ... preferred is >%s<' % self.preferred_reaper
        logger.debug'  retval = >%s<' % retval

        save! if changed?
        return retval
    end

  protected

    ##
    # scale the :preferred_reaper_delay :up or :down, starting with one hour
    def scale_delay direction=:up
        val = self.preferred_reaper_delay == 0 ? 1800 : self.preferred_reaper_delay

        if direction == :down
            val *= 1/3
        else val *= 2 end
        logger.debug '  ... delay scaled %s to %i seconds' % [direction, val]
        write_attribute :preferred_reaper_delay, val
    end

end # module
