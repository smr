#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'math_engine'
  
##
# Implements a mapping mechanism to keep track of the FigureData records of
# most recent relevance. Its most useful when inherited from the actual
# organization class.
class Smr::FiguresDataMap

    def initialize
        @map = Hash.new{ |h,k| h[k] = Hash.new(&h.default_proc) }
        @last_figure_var = FigureVar.new
        @last_figure_data = FigureData.new
    end

    ##
    # Add a FigureData to the map, return code indicates whether this data is
    # already known (1), known and more recent (2) or not known at all (false).
    #
    # Map items are categorized into (:name, :year, :id, :period). This is a
    # multi-dimensional Hash, see
    # http://stackoverflow.com/questions/10253105/dynamically-creating-a-multi-dimensional-hash-in-ruby
    #
    # Unknown items will be map_added to the map. If a FigureData with the same
    # categorization is passed, the map uses :time to decide whether to update
    # or ignore it.
    #
    def map_add(figure_data)
        unless (figure_data.is_a?(FigureData) or figure_data.is_a?(Smr::AutofigureData))
            raise ':figure_data must be of FigureData or SmrAutofigureData'
        end
        @last_figure_data = figure_data
        @last_figure_var = @last_figure_data.FigureVar

        # define locals to shorten the code below
        name = @last_figure_var.name
        id = @last_figure_var.id.to_i
        year = @last_figure_data.time.year.to_i
        period = @last_figure_data.period

        if not (@map.has_key?(name) and @map[name].has_key?(year) and @map[name][year].has_key?(id) and @map[name][year][id].has_key?(period)) then
            # we do not know this figure yet, map_add to map
            @map[name][year][id][period] = @last_figure_data.time
            return false
        else
            if (@map.has_key?(name) and @map[name].has_key?(year) and @map[name][year].has_key?(id) and @map[name][year][id].has_key?(period)) and @map[name][year][id][period] > figure_data.time then
                # known but older
                return 1
            else
                # known but more recent => update map
                @map[name][year][id][period] = figure_data.time
                return 2
            end
        end
    end

    ##
    # Return name last FigureVar processed by #map_add.
    def get_name
        @last_figure_var.name
    end

    ##
    # Return FigureData id of the last item processed by #map_add.
    def get_figure_data_id
        @last_figure_data.id
    end

    ##
    # Return period of the last item processed by #map_add.
    def get_period
        @last_figure_data.period
    end

    ##
    # Return date as +Time+ of the last item processed by #map_add.
    def get_date
        @last_figure_data.time
    end
end
  
##
# Builds FigureVar and FigureData objects into a form that is easy to process
# in views.
#
# Use #add to push FigureData records into the table. When done, tell the object
# to process all data by calling #render. Thereafter all data is available for
# display through the +get_*+ methods.
#
# Note that methods return false or raise an exception when used their proper
# mode.
class Smr::FiguresDataTable < Smr::FiguresDataMap

    ##
    # mode of operation (input or display)
    #
    # The #add method is only active in input mode while all the +get_*+
    # methods are only available in display mode. You must call #render
    # before you can get any data out. A new object is in input mode.
    attr_reader :input_mode

    ##
    # Most recent figures, see #add and #add_record.
    attr_reader :most_recent_figures

    ##
    # All figures not most recent, see #add and #add_record.
    attr_reader :less_recent_figures

    ##
    # Collection of years the table contains data for.
    attr_reader :years

    def initialize
        super

        @input_mode = true;
        @most_recent_figures = Array.new
        @less_recent_figures = Hash.new{ |h,k| h[k] = Hash.new(&h.default_proc) }
        @years = Array.new

        # the final table created by #render
        @table = Hash.new{ |h,k| h[k] = Hash.new(&h.default_proc) }

        # rowspanning information, see and use #rowspan
        @rowspan = Hash.new
    end

    ##
    # Process a FigureData while in input mode.
    def add(figure_data)
        raise 'add() only works in input mode, it stops working when #render was called' if not @input_mode

        case map_add(figure_data)
            when false then add_record(figure_data)
            when 1     then add_record(figure_data, {:less_recent=>true})
            when 2     then update_data(figure_data)
        end

        true
    end

    ##
    # End input mode and render all data ready for displaying.
    #
    # NOTE: @table is a multi-dimensional array but we use only two dimensions,
    #       see http://stackoverflow.com/questions/10253105/dynamically-creating-a-multi-dimensional-hash-in-ruby
    #
    # Final table will look like when viewed by calling #cell on each
    # +row+,+col+ coordinate:
    #
    #    ______ ________________ ____________ ______________ _______________ _____
    #   |      |                | -  Year -  | - Quarter 1 -| - Quarter 2 - | ... |
    #   | year | FigureVar.name | FigureData |  FigureData  |   FigureData  | ... |
    #   |------|----------------|------------|--------------|---------------|-----|
    #     ...
    #
    # The #text_table method is very useful for debugging.
    #
    def render
        raise 'no data to #render, use add() first' if @most_recent_figures.empty?
        @input_mode = false
        @table.clear if @table.count > 0
        periods = FigureData.new.get_periods


        # add most recent FigureVar
        # - first: sort by FigureVar.time
        # - second: add by period index + offset (because of the table head)
        # ATTENTION: sorting by fd.FigureVar.name probably expensive, consider
        #            fs.id_figure_var as performant alternative
        @most_recent_figures.sort_by! { |fd| [fd.time.year, fd.FigureVar.name] }

        row=1
        @most_recent_figures.each do |fd|
            @table[row][0] = fd.time.year
            @table[row][1] = fd.FigureVar.name

            col = periods.index(fd.period) + 2
            @table[row][col] = fd

            row += 1
        end

        # consolidate rows by years and figure name (first two columns)
        prev_row = @table[1]
        prev_key = 1
        @table.each do |k, row|
            if row[0]==prev_row[0] and row[1]==prev_row[1] then
                # merge into prev_row since row is more recent, see sort_by above
                @table[k] = prev_row.merge(row)
                @table.delete(prev_key) if k > prev_key
            end
            prev_row = @table[k]
            prev_key = k
        end

        # init rowspanning
        for row in 1..@table.count do
            @rowspan.store(row, 0)
        end

        # sort table by years
        # - all elements within a year keep their order as is
        # - our @years accessor is ordered the same way
        # - this also makes a new row-index beginning with 1
        @years.sort!.reverse!
        tmp_table = Hash.new{ |h,k| h[k] = Hash.new(&h.default_proc) }
        tmp = Array.new
        row=1

        @years.each{|y| tmp << @table.select {|k, v| v[0] == y } }

        next_year = true; firstrowyear = 1;
        for yi in 0..@years.count-1 do
            tmp[yi].each do |k,v|
               tmp_table.store(row, v)

               # figure rowspanning
               firstrowyear = row if next_year
               @rowspan[firstrowyear] += 1
               next_year = false
               row += 1
            end
            next_year = true;
        end
        @table = tmp_table

        # blank over-spanned lines with +false+ in first column
        span = 0
        for row in 2..@table.count do
            if @rowspan[row]==0
               if not @rowspan[row-1].is_a?(FalseClass)
                   span = @rowspan[row-1] if @rowspan[row-1] > 0
               end
               @rowspan[row]=false if span>0
            end
            span -= 1
        end

        # create table head
        # - this will be row 0
        # - table starts always with 0 and data follows from row-index 1+
        @table[0][0] = ''
        @table[0][1] = ''
        periods.each_index {|i| @table[0][i+2]=FigureData.new.translate_period(periods[i])}

        true
    end

    ##
    # print table in text format (for debugging only!)
    def text_table
        render
        format = Array.new(columns, '%15s').join('|')

        puts '-' * ((columns+1) * 15)
        for r in 0...rows
            rowstr = Array.new
            rowstr[0] = 'sp:%5s %4s ' % [rowspan(r), cell(r, 0)]
            for c in 1..columns
                rowstr[c] = cell(r, c)
            end
            puts(format % rowstr)
        end
        puts '-' * ((columns+1) * 15)
        true
    end

    ##
    # Returns number of rows after #render.
    def rows
        raise '#rows only works after #render was called' if @input_mode
        @table.count
    end

    ##
    # Returns number of columns after #render.
    # NOTE: the first row sets the number of columns of the table
    def columns
        raise '#columns only works after #render was called' if @input_mode
        @table[0].count
    end

    ##
    # Returns content for a cell identified by row and col, nil if empty.
    def cell(row, col)
        raise '#cell only works after #render was called' if @input_mode

        if c = raw_cell_content(row, col) then
            if c.is_a?(FigureData) or c.is_a?(Smr::AutofigureData)
                c.value
            else
                # leftmost column is always a descriptive string, not to be
                # rounded, scaled, whatever ...
                if col==0 then c.to_s else c end
            end
        else
            nil
        end
    end

    ##
    # Returns unit of value in cell or nil if there is nothing.
    def cell_unit(row, col)
        raise '#cell_unit only works after #render was called' if @input_mode

        if c = raw_cell_content(row, col) then
            if c.is_a?(FigureData) or c.is_a?(Smr::AutofigureData)
                c.FigureVar.unit
            end
        end
    end

    ##
    # Return rowspanning information for given row.
    def rowspan(row)
        raise '#rowspan only works after #render was called' if @input_mode
        if @rowspan.key?(row) then @rowspan[row] else 1 end
    end

    ##
    # Return link to edit FigureVar of given field.
    #
    # False is returned if that field is empty.
    def link(row, col)
        raise '#link only works after #render was called' if @input_mode

        if c = raw_cell_content(row, col) then
           if c.is_a?(FigureData) then return('/figures/%i/edit' % c.id) end
        end

        false
    end

  protected

    ##
    # Add FigureData to internal @most_recent_figures or @less_recent_figures.
    #
    # #less_recent_figures contains all those where a more recent counterpart
    # is available in #most_recent_figures. See #get_summary and #have_summary.
    #
    # #most_recent_figures is a regular +Array+.
    #
    # #less_recent_figures is a multi-dimensional +Hash+ structured as:
    #
    #   @less_recent_figures[:year][:period][:id_figure_data] = Array.new
    #
    def add_record(figure_data, options={ :less_recent=>false })
        y  = figure_data.time.year
        p  = figure_data.period
        id = figure_data.id

        if options[:less_recent] then
            if @less_recent_figures.has_key?(y) and @less_recent_figures[y].has_key?(p) and @less_recent_figures[y][p].has_key?(id) then
               @less_recent_figures[y][p][id] << figure_data
            else
                @less_recent_figures[y][p][id] = [figure_data]
            end
        else
            @most_recent_figures << figure_data
            @years << y if not @years.include?(y)
        end

        true
    end

    ##
    # Update FigureData item in #most_recent_figures, make sure the old data is
    # kept in #less_recent_figures.
    #
    # FIXME: to be implemented!
    def update_data(figure_data)
        #@most_recent_figures.find(:id_figure_var=>figure_data.id_figure_var, ...)
        true
    end

    ##
    # Returns content for a cell identified by row and col.
    #
    # The content can be +nil+ if that cell is empty or whatever is there, ie.
    # a FigureData, a +String+, etc....
    def raw_cell_content(row, col)
        if @table.has_key?(row) and @table[row].has_key?(col) then
                @table[row][col]
        else nil end
    end
end
  
##
# Creates many (!) AutofigureData objects from a FigureVar record.
#
# A Autofigure is based on a FigureVar record with the :expression field
# containing a math expression. That is each FigureVar becomes a Autofigure
# by setting the :expression field. It will return to being a ordinary
# FigureVar by emptying the the :expression field.
#
# Each Autofigure will create as many AutofigureData objects as possible,
# depending on the information available to solve the given :expression. It
# might be hundreds if there is input data for, say, 100 fiscal quarters.
#
class Smr::Autofigure

    # collection of human readdable errors, emtpy if all went well
    attr_reader :errors

    # Math functions users can use in the :expression field
    SUPPORTED_FUNCTIONS = [:ceil, :floor, :sqrt, :cos, :sin, :tan, :exp, :log]

    ##
    # Init with FigureVar and a list of variables contained in its :expression.
    # See SmrDaemonClient#parse_math_expressions.
    def initialize(figure_var)
        raise 'figure_var must be a FigureVar object' unless figure_var.is_a?(FigureVar)
        raise 'figure_var must have :expression field set' if figure_var.expression.blank?
        @autofigure = figure_var
        @expression_vars = Hash.new
        @errors = Array.new

        # find variables used in expression
        # - that is FigureVar objects that match by name used in :expression
        # - internally variables are tracked by :id, not by :name (avoids db
        #   queries later, when building maps)
        variables = @autofigure.expression.strip.split(/\W+/).delete_if do |x|
              x.to_f != 0 or SUPPORTED_FUNCTIONS.include?(x.to_sym)
        end
        FigureVar.select(:id, :name).where(:name=>variables).each do |v|
            @expression_vars[v.id]=v.name
        end

        # collects FigureData by year and period, to know what we have (=>
        # #add()) and still need (=> #get_missing_variables)
        @datamatrix = Hash.new

        # collects Time if most recent FigureData per row in @datamatrix,
        # necessary to know how to time the SmrAutofigureData objects
        @timematrix = Hash.new

        @id_security = nil
    end

    ##
    # Add FigureData as input for solving the expression.
    #
    # Returns +true+ if it was added or +false+ if it did not help to fill this
    # expression.
    #
    # Note: data is overwritten. A FigureData of same +:year+ and +:period+
    # will overwrite the previously passed one. So mind the order.
    def add(figure_data)
        raise 'figure_data must be a FigureData object' unless figure_data.is_a?(FigureData)

        if @id_security and @id_security != figure_data.id_security
            raise 'add()ing FigureData of another Security should not happen, should it?'
        else @id_security = figure_data.id_security end

        i = '%s_%s' % [figure_data.time.year, figure_data.period ]
        if @expression_vars.keys.include?(figure_data.id_figure_var)
            newdata = { figure_data.id_figure_var=>figure_data.value }
            @datamatrix[i] = if @datamatrix[i] then @datamatrix[i].merge(newdata) else newdata end

            if not @timematrix[i] or @timematrix[i] < figure_data.time then
                @timematrix[i] = figure_data.time
            end

            return true
        end

        return false
    end

    ##
    # Tells whats missing to solve all expressions.
    def get_missing_variables
        missing = Array.new
        @datamatrix.each do |i,d|
            diff = @expression_vars.keys - d.keys
            if not diff.empty?
                missing << i.to_s + '_' + @expression_vars.select{|k,v| diff.include?(k) }.values.join('-')
            end
        end
        missing
    end

    ##
    # Returns collection of AutofigureData objects with solved
    # expressions.
    #
    # Objects with non-solvable expressions due to missing data are
    # skipped silently (see #get_missing_variables).
    #
    # Exceptions from MathEngine will be catched and collected in #errors.
    def get
        @datamatrix.collect do |i,d|
            diff = @expression_vars.keys - d.keys
            if diff.empty?
                e = MathEngine.new
                e.context.include_library Smr::AutofigureMathFunctions.new

                af, time, period, id = @autofigure, @timematrix[i], i.split('_').second, @id_security

              begin
                # init variables in MathEngine instance, then ...
                d.each{ |k,v|
                  e.context.set(@expression_vars[k].to_sym,v)
                }

                # ... solve the expression as result of a new AutofigureData
                Smr::AutofigureData.new(af, e.evaluate(af.expression), id, period, time)
              rescue #=> exception
# p exception.backtrace
# p '=E=> %s' %  e.inspect
                 @errors << 'solving %s(%s-%s) failed with: %s' % [af.name, time.year, period, $! ]
                 nil
              end
            end
        end.compact
    end
end
  
##
# Like a FigureData object in behaviour but without related database record.
#
# A AutofigureData object is ment to behave exactly like a FigureData
# object. Except that it can`t be saved or updated or trigger any other
# database operation. Its an entirely artificial record so to say.
class Smr::AutofigureData
    # compatibility with FigureData, always +nil+
    attr_reader :id

    # compatibility with FigureData, empty
    attr_reader :analyst, :is_expected, :is_audited, :comment

    # where this autofigure was derived from
    attr_reader :id_figure_var, :id_security, :period, :expression

    # unix timestamp when this autofigure was made, also see time()
    attr_reader :date

    # result of the expression or +nil+ if not yet solved
    # Note: must be calculated/set externaly, Smr::Autofigure#get does this
    attr_accessor :value

    ##
    # Give FigureVar out of which this Smr::AutofigureData was derived and the result
    # from solving the expression.
    # - :id_security+ is to know what this is for
    # - the given FigureVar must have a :expression, otherwise its not a autofigure
    def initialize(autofigure, result, id_security, period, time)
        raise ':autofigure must be a FigureVar' unless autofigure.is_a? FigureVar
        raise ':autofigure :expression must be set' if autofigure.expression.blank?
        raise ':result must be numerical' unless result.is_a? Numeric
        raise ':time must be of Time' unless time.is_a? Time
        @autofigure = autofigure
        @expression = autofigure.expression
        @value = result

        # carry on meaningful fields
        @id_figure_var = autofigure.id
        @id_security = id_security
        @date = time.to_i
        @period = period
        @analyst = self.class
    end

    ##
    # Time when this autofigure was made.
    def time
        Time.at(@date)
    end

    ##
    # Wrapper for compatibility with FigureData.
    def get_periods
        FigureData.new.get_periods
    end

    ##
    # Wrapper for compatibility with FigureData.
    def translate_period(period=@period)
        FigureData.new.translate_period(period)
    end

    ##
    # Wrapper for compatibility with FigureData.
    def FigureVar
        @autofigure
    end
end

##
# additional methods available to users in FigureVar#expression field.
class Smr::AutofigureMathFunctions
  def ceil(v)
      v.to_f.ceil
  end

  def floor(v)
      v.to_f.floor
  end
end
