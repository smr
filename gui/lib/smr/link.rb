#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

##
# Link to some item in the application or to the outside.
class Smr::Link

  # reader to init value
  attr_reader :type
  attr_reader :destination
  attr_reader :name

  ##
  # For :type see #types on whats possible. Giving an unknown type will raise
  # a exception.
  #
  # The :destination parameter is whatever makes sense for the given type.
  # Usually its the numerical id.
  #
  # The :name parameter is shown by #to_s if given.
  def initialize(type, destination, name=false)
    raise 'given :type=%s is not supported' % type unless types.has_value? type
    @type=type
    @destination=destination
    @name=name
  end

  ##
  # supported types
  def self.types
      {
        'External Link'   => :url,
        'Asset Position'  => :position,
        'Cash Deposit'    => :cashposition,
        'Order'           => :order,
        'Blog Commentary' => :comment,
        'Edit Blog Commentary' => :comment_edit,
        'Security'        => :security,
        'Organization'    => :organization,
#        'Document'       => :document
        'Portfolio'       => :portfolio,
        'Figure'          => :figurevar,
        'User'            => :user,
      }
  end
  def types
      Smr::Link.types
  end

  ##
  # URL String to the destination of this link
  def to_url
      url_helpers = Rails.application.routes.url_helpers
      case @type
          when :url then @destination
          when :position     then url_helpers.position_path :id=>@destination
          when :cashposition then url_helpers.cashposition_path :id=>@destination
          when :order        then url_helpers.order_path :id=>@destination
          when :comment      then url_helpers.blog_path :id=>@destination
          when :comment_edit then url_helpers.new_blog_path :id=>@destination # FIXME: the :new action should not do this
          when :security     then url_helpers.portrait_security_path :id=>@destination
          when :organization then url_helpers.portrait_organization_path :id=>@destination
          when :portfolio    then url_helpers.show_portfolio_path :id_portfolio=>@destination
          when :figurevar    then url_helpers.figures_path   # :id=>@destination - to be implemented
          when :user         then url_helpers.root_path  # :id=>@destination - to be implemented
      end
  end

  ##
  # String to present this link to humans.
  def to_s
      return @name if @name
      if @type == :url
          @destination.to_s
      else '%s #%i' % [types.key(@type), @destination] end
  end
end

