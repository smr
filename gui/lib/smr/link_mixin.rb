#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

module Smr  #:nodoc:

    ##
    # Mix Smr::Link into models.
    # NOTE: use only if the model is supported by Smr::Link.types. Otherwise it
    #       will not know what to do with it
    module LinkMixin

        ##
        # Returns Smr::Link pointing to this model object.
        def link
            Smr::Link.new(self.class.to_s.downcase.to_sym, self.id)
        end

    end

end # module
