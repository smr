#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
  
module Smr  #:nodoc:
  ##
  # Collection of Quoterecord objects owned by a given user, related to given
  # security at given date.
  #
  # == Security
  # The +id_user+ parameter should <b>not</b> come from user input, but rather
  # result from a successful authentication process.
  class Quoterecords < Array
      ##
      # provide Security and +Time+ date
      def initialize(id_user, security, date=Time.now.end_of_day)
          raise ':security must be a Security' unless security.is_a?(Security)
          @@id_user = id_user
          @@date = date
          @@security = security
  
          Quoterecord.joins(:Quote)
              .where(:id_user=>@@id_user, :id_security=>@@security.id)
              .where('quote.date_last <= %i' % date)
              .order('quote.date_last DESC').each do |qr|
            push(qr)
          end
      end

      ##
      # return column names
      def self.get_columns
          Quoterecord.get_columns
      end
      
      ##
      # translate column name into human readable +String+
      def self.translate_column(column)
          Quoterecord.translate_column(column)
      end
  
      ##
      # return quote of last pivotal point for +column+
      def get_pivot_point(column)
          qr = Quoterecord.select(:id_quote).where(
                  :id_user=>@@id_user, :id_security=>@@security.id, :column=>column,
                  :is_pivotal_point=>true, :date_created=>0..@@date.to_i
              ).order(:date_created=>:desc).limit(1).first
          qr.Quote.last if qr.is_a?(Quoterecord)
      end
  
      ##
      # return column name of last recorded record
      def get_last_recorded_column
          self.first.column if not empty?
      end
  
      ##
      # view given Quote in context of existing Quoterecord data
      #
      # returns a message string with its findings and a new Quoterecord object,
      # contained in a two field array or false on failure.
      #
      # This new object is linked to Quote and has fields set according to the
      # findings.
      #
      def inspect_quote(quote)
          raise ':quote must be of Quote' unless quote.is_a?(Quote)
  
          message = []
          threshold = QuoterecordThreshold.where(:id_user=>@@id_user, :id_security=>@@security.id).first || QuoterecordThreshold.new(:quote_sensitivity=>0, :hit_sensitivity=>0)
          new_quoterecord = Quoterecord.new(:id_user=>@@id_user, :id_security=>@@security.id)
  
          # check sensitivity hit against most recent quote record (=mrqr)
          if empty?
              message << 'no previous quoterecords, initiate recording with this one'
          else
              mrq = self.first.Quote  # most recent Quote of this Quoterecords list
              if mrq.last > 0.0
                  if quote.last > mrq.last + threshold.quote_sensitivity then
                      message << 'UP hit'
                      new_quoterecord.is_uphit = true
                  elsif quote.last < mrq.last - threshold.quote_sensitivity then
                      message << 'DOWN hit'
                      new_quoterecord.is_downhit = true
                  end
              end
          end
  
          # check quote against previous pivotal points
          self.class.get_columns.each do |col|
              next if not pivot_point = get_pivot_point(col)
              t_col = self.class.translate_column(col)
  
              case col
                  when 'upward', 'natural_rally', 'secondary_rally'
                      if quote.last > pivot_point + threshold.hit_sensitivity then
                         message << 'pivotal point hit in %s column' % t_col
                      elsif (pivot_point-threshold.hit_sensitivity..pivot_point+threshold.hit_sensitivity) === quote.last
                         message << 'close to pivotal point in %s column' % t_col
                      end
                
                  when 'downward', 'natural_reaction', 'secondary_reaction'
                      if quote.last < pivot_point - threshold.hit_sensitivity then
                         message << 'pivotal point hit in %s column' % t_col
                      elsif (pivot_point-threshold.hit_sensitivity..pivot_point+threshold.hit_sensitivity) === quote.last
                         message << 'close to pivotal point in %s column' % t_col
                      end
              end
          end
  
          return message.join(', '), new_quoterecord
      end
  
  end
  
end # module
