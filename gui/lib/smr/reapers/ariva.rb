#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'open-uri'
require 'nokogiri'

module Smr::Reapers # :nodoc:

    class Ariva
        include Smr::Reapers
        QUERY_INDICES_URL = 'https://www.ariva.de/aktien/indizes'

        def initialize(security)
            raise 'Security object required' unless security.is_a? Security
            raise 'Security.type not supported by this reaper' unless Ariva.type_supported? security.type
            @security = security
        end

        ##
        # List of Security#types this reaper can retrieve data for.
        def Ariva.security_types
            [ :index ]
        end

        ##
        # tell whether :type can be handled by this reaper.
        def Ariva.type_supported?(type)
            Ariva.security_types.include? type.to_sym
        end

        ##
        # Retrieve current Quote for Security and safe! it on success.
        def quote
            src = open(QUERY_INDICES_URL % @security.symbol)
            page = Nokogiri::HTML(src) do |config|
                config.strict.noblanks
                config.strict.noent
            end

            # check whether :symbol was found actually
            dataline = false
            page.css('div#CONTENT table tbody tr').each do |l|
                if clean_special_chars(l.text).start_with? @security.symbol
                    dataline = l
                    break
                end
            end
#p '==> see: %s' % dataline
            return false unless dataline

            q = Quote.new(:id_security=>@security.id)

            q.exchange = clean_special_chars(dataline.element_children[0].text).gsub(' ','')
            q.last = clean_special_chars(dataline.element_children[1].text).gsub('.','').gsub(',','.').to_f

            timestr = clean_special_chars(dataline.element_children[4].text).gsub(' ','')
            timestr += Time.now.year.to_s if timestr.include? '.'
            q.time_last = Time.parse timestr
            q.time_last += 20.hours if q.time_last.hour == 0 # treat as 'close'

#p '==> %s have %s' % [q.time_last, q.inspect]
            return (q.last>0 and q.time_last) ? q : false
        end
    end
end # module
