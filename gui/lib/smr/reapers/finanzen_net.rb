#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'open-uri'
require 'nokogiri'

##
# Retrieve Quote and Security from Finanzen.net
class Smr::Reapers::Finanzennet
    include Smr::Reapers
    QUERY_URL = 'https://www.finanzen.net/suchergebnis.asp?strSuchString=%s'

    def initialize(security)
        raise 'Security object required' unless security.is_a? Security
        raise 'Security.type=%s not supported by this reaper' % security.type unless Finanzennet::type_supported? security.type
        @security = security
        @security.save! unless @security.id  # it must be a database record
    end

    ##
    # List of Security#types this reaper can retrieve data for.
    def Finanzennet.security_types
        [ :unknown, :stock, :bond, :fund ]
    end

    ##
    # tell whether :type can be handled by this reaper.
    def Finanzennet.type_supported?(type)
        Finanzennet.security_types.include? type.to_sym
    end

    ##
    # Retrieve metadata for Securtity#symbol and creates correct type model.
    #
    # Returns true on success or false on failure.
    def metadata
        #src = open(QUERY_URL % @security.symbol, 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36')
        begin
            src = open(QUERY_URL % @security.symbol)
        rescue OpenURI::HTTPError
            return false
        end

        page = Nokogiri::HTML(src) do |config|
#                config.strict.noblanks
        end

        # check result URL: nothing found shows a search page sometimes
        property = page.at_xpath('//meta[@property="og:url"]')
        unless property['content'] == 'http://www.finanzen.net/suchergebnis.asp'
            @security.url = property['content']
        else return(false) end

        meta = page.at_xpath('//meta[@name="keywords"]')
        meta = page.at_xpath('//meta[@name="Keywords"]') if meta.blank?
        keywords = meta['content'].split(%r{,\s*})
        if not (keywords & ['Unternehmensanleihen', 'Staatsanleihen', 'Anleihen-Kurse']).empty?
            type = :bond
        elsif not (keywords & ['Aktienfonds', 'Immobilienfonds', 'Rentenfonds', 'Geldmarktfonds', 'Dachfonds', 'Fonds', 'ETFs']).empty?
            type = :fund
        elsif not (keywords & ['Aktien', 'Realtimekurse', 'Aktienkurse']).empty?
            type = :stock
        else
            return false
        end

        # create and fill the :typemodel
        typemodel = @security.create_type(type)

        css_selector = case @security.type
            when :fund  then 'table.table-small tr td:first'
            when :stock then 'div.box table.table tr td:first'
            else 'div.box table.table tr td:first'
        end

        page.css(css_selector).each do |dataline|
            clean_header = clean_special_chars dataline.text
            next unless dataline.next_element
            clean_data = clean_special_chars dataline.next_element.text
            next if clean_data.blank? or clean_data.eql?('-')

            case @security.type
                when :bond then handle_bond(clean_header, clean_data, typemodel)

                when :stock
                    property = page.at_xpath('//meta[@property="og:title"]')
                    @security.brief = property['content'].split('|').first.chop
                    handle_stock(clean_header, clean_data, typemodel)

                when :fund then handle_fund(clean_header, clean_data, typemodel)

                else return false
           end
        end

        if @security.changed? or typemodel.changed?
            (@security.save and typemodel.save)
        else false end
    end

 protected

    def handle_bond(header, data, typemodel)
#p '==handle bond=> |%s| = |%s|' % [header, data]
        case header
            when 'Name'       then @security.brief = data
            when 'ISIN'       then @security.symbol = data
            when 'Kupon in %' then typemodel.coupon = data.gsub(',', '.')
            when 'Fälligkeit' then
                typemodel.time_maturity = data
                typemodel.redemption_installments = 1
            when 'Erstes Kupondatum'    then typemodel.time_first_coupon = data
            when 'Letztes Kupondatum'   then typemodel.time_last_coupon = data
            when 'Zinstermine pro Jahr' then typemodel.coupon_interval = 12 / data.to_i
            when 'Emissionswährung'     then typemodel.currency = data if data != Smr::DEFAULT_CURRENCY
            when 'Emissionsvolumen*'    then typemodel.issue_size = data.gsub('.', '').to_i
            when 'Stückelung'           then typemodel.denomination = data
            when 'Nachrang' then  if data.downcase == 'ja' then typemodel.is_callable = true end
        end
    end

    def handle_stock(header, data, typemodel)
#p '==handle stock==> |%s| = |%s|' % [header, data]
        case header
            when 'ISIN'      then @security.symbol = data
            when 'Letzte Dividende' then typemodel.dividend = data.gsub(',', '.')
        end
    end

    def handle_fund(header, data, typemodel)
#p '==handle fund==> |%s| = |%s|' % [header, data]
        case header
            when 'Name'         then @security.brief = data.chomp
            when 'ISIN'         then @security.symbol = data
            when 'Auflagedatum' then typemodel.time_inception = data
            when 'Währung'      then typemodel.currency = data if data != Smr::DEFAULT_CURRENCY
            when 'Geschäftsjahresende' then typemodel.time_fiscalyearend = data
            when 'Ausgabeaufschlag'
                if (v = data.gsub(',', '.')).to_i > 0 then typemodel.issue_surcharge = v end
            when 'Verwaltungsgebühr'
                if (v = data.gsub(',', '.')).to_i > 0 then typemodel.management_fee = v end
#                when 'Depotbankgebühr'      then typemodel. ? not implemented
#                when 'Pauschalgebühr'      then typemodel. ? not implemented
            when 'Mindestanlage'
                if (v = data.gsub('.', '').gsub(',', '.')).to_i > 0 then typemodel.investment_minimum = v end
            when 'Ausschüttung'  then typemodel.is_retaining_profits = true if data.downcase.include?('thesaur')
            when 'Kategorie'
                typemodel.type = :equity   if data.include? 'Aktienfonds'
                typemodel.type = :bond     if data.include? 'Rentenfonds'
                typemodel.type = :balanced if data.include? 'Mischfonds'
                typemodel.type = :real_estate   if data.include? 'Immobilienfonds'
                typemodel.type = :fund_of_funds if data.include? 'Rentenfonds'
                typemodel.type = :commodity     if data.include? 'Rohstoff'
        end
    end
end
