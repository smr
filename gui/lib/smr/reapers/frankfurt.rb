#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'open-uri'
require 'nokogiri'

##
# Retrieve Quote from Frankfurt Stock Exchange
class Smr::Reapers::Frankfurt
    include Smr::Reapers
    QUERY_URL = 'http://www.boerse-frankfurt.de/suchergebnisse?_search=%s'

    def initialize(security)
        raise 'Security object required' unless security.is_a? Security
        raise 'Security.type not supported by this reaper' unless Frankfurt.type_supported? security.type
        @security = security
    end

    ##
    # List of Security#types this reaper can retrieve data for.
    def Frankfurt.security_types
        [ :unknown, :bond ]
    end

    ##
    # tell whether :type can be handled by this reaper.
    def Frankfurt.type_supported?(type)
        Frankfurt.security_types.include? type.to_sym
    end

    ##
    # Retrieve current Quote for Security and safe! it on success.
    def quote
        src = open(
            QUERY_URL % @security.symbol,
            'Cookie'=>'openmarketdisclameraccepted=bond; LS4_push3boersefrankfurtde=|799|; LS4_http%3A%2F%2Fpush3.boerse-frankfurt.de=|799_push3boersefrankfurtde|; LS4_799_push3boersefrankfurtde=1457720776555|S|boerse__frankfurt_de_799_push3boersefrankfurtde'
        )
        page = Nokogiri::HTML(src) do |config|
            config.strict.noblanks
        end

        # check whether :symbol was found actually
        return false unless page.css('span.stock-subline').text.include? @security.symbol

        q = Quote.new(:id_security=>@security.id, :exchange=>'FRA')

        # look for quote and supplementary details
        page.css('div.table-responsive table.table tbody tr').each do |tableline|
            label = clean_special_chars tableline.first_element_child.text
            value = clean_special_chars tableline.last_element_child.text
#p '==> %s: %s' %  [ label, value ]

            case label
                when 'Letzter Preis'
                    q.last = value.gsub(/\s%/, '').gsub(',','.').to_f
                when 'Umsatz nominal'
                    q.volume = value.gsub('.','').to_i
                when 'Tageshoch'
                    q.high = value.gsub(',','.').to_f
                when 'Tagestief'
                    q.low = value.gsub(',','.').to_f
                when 'Datum / Zeit'
                    q.time_last = Time.parse( value.gsub('/','') )
            end
        end

        return (q.last>0 and q.time_last) ? q : false
    end
end
