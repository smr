#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'open-uri'
require 'nokogiri'

##
# Retrieve Quote from Kitco Precious Metals
class Smr::Reapers::Kitco
    include Smr::Reapers
    QUERY_URL = 'http://www.kitco.com/market/'

    def initialize(security)
        raise 'Security object required' unless security.is_a? Security
        raise 'Security.type=%s not supported by this reaper' % security.type unless Kitco.type_supported? security.type
        @security = security
    end

    ##
    # List of Security#types this reaper can retrieve data for.
    def Kitco.security_types
        [ :metal ]
    end

    ##
    # tell whether :type can be handled by this reaper.
    def Kitco.type_supported?(type)
        Kitco.security_types.include? type.to_sym
    end

    ##
    # Retrieve current Quote for Security
    def quote
        src = open QUERY_URL
        page = Nokogiri::HTML(src) do |config|
            config.strict.noblanks
        end

        q = Quote.new(:id_security=>@security.id, :exchange=>'World Spot Price')

        # look for exchange rate to EUR since site shows USD
        rate = clean_special_chars(
            page.css('div.exchange_rates span.img_flag_euro').first.parent.parent.next_sibling.css('td.index').text
        ).to_f

        # look for quote
        id = case @security.symbol.downcase
            when 'silver'    then 'AG'
            when 'gold'      then 'AU'
            when 'platinum'  then 'PT'
            when 'palladium' then 'PD'
            when 'rhodium'   then 'RH'
        end

        q.time_last = Time.strptime(
            '%s %s' % [
                page.css('td#wsp-%s-date' % id).text,
                page.css('td#wsp-%s-time' % id).text
             ],
             '%m/%d/%Y %H:%M'
        )

        str = clean_special_chars(
            page.css('td#wsp-%s-bid' % id).text
        )
        q.last = str.to_f * rate unless str.blank?

        return (q.last>0 and q.time_last) ? q : false
    end
end
