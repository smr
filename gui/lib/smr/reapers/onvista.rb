#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'open-uri'
require 'nokogiri'

##
# Retrieve Quote data from Onvista.
class Smr::Reapers::Onvista
    include Smr::Reapers
    QUERY_URL = 'https://www.onvista.de/suche?searchValue=%s'

    def initialize(security)
        raise 'Security object required' unless security.is_a? Security
        raise 'Security.type not supported by this reaper' unless Onvista.type_supported? security.type
        @security = security
    end

    ##
    # List of Security#types this reaper can retrieve data for.
    def Onvista.security_types
        [ :unknown, :stock, :fund ]
    end

    ##
    # tell whether :type can be handled by this reaper.
    def Onvista.type_supported?(type)
        Onvista.security_types.include? type.to_sym
    end

    ##
    # Retrieve current Quote for Security and safe! it on success.
    def quote
        begin
            src = open(QUERY_URL % @security.symbol)
        rescue #Exception => e
#p '==> %s' % e
            return false
        end

        page = Nokogiri::HTML(src) do |config|
            config.strict.noblanks
        end
        have_type, q = false, false

        # check whether :symbol was found actually
        if page.css('div.WERTPAPIER_DETAILS').text.include?(@security.symbol)
            have_type = :stock
        elsif page.css('main#fund-snapshot table td').text.include?(@security.symbol)
            have_type = :fund
        else
            return false
        end
#p '==have_type=> %s' % have_type
        # look for quote and supplementary details
        case have_type
            when :stock then
                page.css('article.HANDELSPLAETZE table tr').each do |dataline|
                    next if dataline.child.name == 'th'           # skip table header
                    next if dataline.text.include? 'geschlossen'  # skip currently closed exchange
#p '==stock line=> '; dataline.element_children.each{ |column| p '  > %s %s: %s' %  [ column.name, column[:class], column.text ] }
                    q = Quote.new(:id_security=>@security.id)

                    if match = /[.\d]{1,},\d{2,}\s[A-Z]{3}/.match(dataline.element_children[2].text) then
                        (quotestr, currency) = match.to_s.split(' ')
                        return false if currency != Smr::DEFAULT_CURRENCY   # drop until currency support implemented
#p '--> matched: >%s< >%s<' % [ quotestr, currency ]
                        q.last = quotestr.gsub('.','').gsub(',','.').to_f
                        q.time_last = Time.parse(dataline.element_children[5].text + dataline.element_children[6].text)
                        #(q.bid, q.ask) = dataline.element_children[7].text.split('/')  # not supported by model
                        q.exchange = dataline[:class]
#p '--> %s' % q.inspect
                        break
                    else next end  # skip to next line when column 2 not looks like a quote
                end
             when :fund then
                page.css('div span.price').each do |dataline|
                    q = Quote.new(:id_security=>@security.id)
                    if match = /[.\d]{1,},\d{2,}\s[A-Z]{3}/.match(dataline.text) then
                        (quotestr, currency) = match.to_s.split(' ')
                        q.last = quotestr.gsub('.','').gsub(',','.').to_f
                    end
                end
                page.css('div span.datetime').each do |dataline|
                    begin
                        q.time_last = Time.parse(dataline.text)
                        q.exchange = 'KAG'
                    rescue
                        return(false)
                    end
                end
        end

        return q
    end
end
