#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'open-uri'
require 'nokogiri'

##
# Retrieve Quote from Stuttgart Stock Exchange
class Smr::Reapers::Stuttgart
    include Smr::Reapers
    QUERY_URL = 'https://www.boerse-stuttgart.de/de/rd/erweiterte_kurssuche/suchergebnis/?searchterm=%s'

    def initialize(security)
        raise 'Security object required' unless security.is_a? Security
        raise 'Security.type not supported by this reaper' unless Stuttgart.type_supported? security.type
        @security = security
    end

    ##
    # List of Security#types this reaper can retrieve data for.
    def Stuttgart.security_types
        [ :unknown, :bond ]
    end

    ##
    # tell whether :type can be handled by this reaper.
    def Stuttgart.type_supported?(type)
        Stuttgart.security_types.include? type.to_sym
    end

    ##
    # Retrieve current Quote for Security and safe! it on success.
    def quote
        src = open(QUERY_URL % @security.symbol)
        page = Nokogiri::HTML(src) do |config|
            config.strict.noblanks
        end

        # check whether :symbol was found actually
        return false unless page.css('div.head h1 span').text.include? @security.symbol

        q = Quote.new(:id_security=>@security.id, :exchange=>'STU')

        # look for quote and supplementary details
        page.css('div.content_box_content table tr').each do |tableline|
            if tableline.element_children[1]
                label =  clean_special_chars tableline.first_element_child.text
                value1 = clean_special_chars tableline.element_children[1].text
                value2 = clean_special_chars tableline.last_element_child.text
#p '==> %s: val1=%s val2=%s' %  [ label, value1, value2 ]
            else next end

            case label
                when 'Last / Rendite'
                    q.last = value1.gsub(',','.').to_f
                when 'Tagesvolumen (nominal)'
                    q.volume = value1.gsub('.','').to_i
                when 'Tageshoch / -tief'
                    q.high = value1.gsub(',','.').to_f
                    q.low = value2.gsub(',','.').to_f
                when 'Kurszeit'
                    q.time_last = Time.parse( value1 + ' ' + value2 )
            end
        end
#p '==> %s have %s' % [q.time_last, q.inspect]
        return (q.last>0 and q.time_last) ? q : false
    end
end
