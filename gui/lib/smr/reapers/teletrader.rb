#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'open-uri'
require 'nokogiri'

##
# Classes to fetch things from various sources. The constructors all need a
# Security instance as parameter and operate on it.
module Smr::Reapers

    class Teletrader
        QUERY_URL = 'http://www.teletrader.com/search/%s'

        def initialize(security)
            raise 'Security object required' unless security.is_a? Security
            @security = security
        end

        def metadata
            src = open(QUERY_URL % @security.symbol)
#            src = File.open('/tmp/bp.teletrader.com.html')

            page = Nokogiri::HTML(src) do |config|
                config.strict.noblanks
            end

            # inspect meta keywords to figure what we got here
            meta = page.at_xpath('//meta[@name="keywords"]')
            keywords = meta['content'].split(%r{,\s*})

            return false unless keywords.include?(@security.symbol)

            @security.brief = page.at_css('div#fullName').text
            @security.url = QUERY_URL % @security.symbol

            # determine :type
            case
                 when keywords.include?('bond') then @security.type = :bond
                 when keywords.include?('dividend') then @security.type = :stock
            end

            # create and fill the :typemodel
            # - must save security first to have its :id created
            @security.save! unless @security.id
            if typemodel = @security.get_type
                case @security.type.to_sym

                    # Bonds
                    when :bond
                        page.css('div.quote-details > h2 + table.data-view > tbody > tr > th').each do |dataline|
                            clean_header = dataline.text.gsub(/[:\r\n]/, '')
                            clean_data = dataline.next_sibling.next_sibling.text.gsub(/[,\r\n]/, '')
#p '==      bond=> %s = %s' % [clean_header, clean_data]

                            case clean_header
                                when 'Currency' then typemodel.currency = clean_data if clean_data != Smr::DEFAULT_CURRENCY
                                when 'Expiration date' then
                                    typemodel.time_maturity = Time.strptime(clean_data, '%m/%d/%Y')
                                    typemodel.redemption_installments = 1
                                when 'Face value' then
                                    if (dval = (clean_data[/\d*/,0]).to_i) > 0 then typemodel.denomination = dval end
                                when 'Coupon' then typemodel.coupon = clean_data.gsub('%', '')
                                when 'Coupon type'
                                    if clean_data.downcase.include? 'float'
                                        typemodel.type = :floating_rate
                                    elsif clean_data.downcase.include? 'zero'
                                        typemodel.type = :zero_rate
                                    end
                                when 'Floater'
                                    typemodel.type = :floating_rate if clean_data.downcase == 'yes'
                                when 'Coupon frequency' then
                                    if (ival = (clean_data[/\d*/,0]).to_i) > 0 then typemodel.coupon_interval = ival end
                                when 'Expiration value' then typemodel.redemption_price = clean_data
                            end
                        end

                    # Stock
                    when :stock
                        page.css('div.quote-details > h2 + table.data-view > tbody > tr > th').each do |dataline|
                            clean_header = dataline.text.gsub(/[:\r\n]/, '')
                            clean_data = dataline.next_sibling.next_sibling.text.gsub(/[,\r\n]/, '')
#p '==      stock=> %s = %s' % [clean_header, clean_data]
                            case clean_header
                                when 'Issue type'
                                    if clean_data.downcase.include? 'preferred'
                                        typemodel.type = :preferred
                                    elsif clean_data.downcase.include? 'bearer'
                                        typemodel.type = :bearer
                                    elsif clean_data.downcase.include? 'registered'
                                        typemodel.type = :registered
                                    elsif clean_data.include? 'ADR'
                                        typemodel.type = :ADR
                                    end
                            end
                        end

                end

                if typemodel.changed?
                    typemodel.save
                else false end
            end
        end
    end

end # module
