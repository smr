#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'open-uri'
require 'nokogiri'

##
# Retrieve Quote from Universal Investment Fund Platform
class Smr::Reapers::UniversalInvestment
    include Smr::Reapers
    QUERY_URL = 'http://fondsfinder.universal-investment.com/de/DE/Funds/%s'

    def initialize(security)
        raise 'Security object required' unless security.is_a? Security
        raise 'Security.type not supported by this reaper' unless UniversalInvestment.type_supported? security.type
        @security = security
    end

    ##
    # List of Security#types this reaper can retrieve data for.
    def UniversalInvestment.security_types
        [ :unknown, :fund ]
    end

    ##
    # tell whether :type can be handled by this reaper.
    def UniversalInvestment.type_supported?(type)
        UniversalInvestment.security_types.include? type.to_sym
    end

    ##
    # Retrieve current Quote for Security and safe! it on success.
    def quote
        begin
            src = open(
                QUERY_URL % @security.symbol,
                'Cookie'=>'fondsfinder.test.cookie=True; ai_user=yen/n|2016-03-11T20:52:28.552Z; distributionstatuscodes=BCBC7A7A-3CE7-E411-8103-0050C0185220; distributioncountrycode=DE; ARRAffinity=787630b808b4f4ce9564ab22862418932f67da5c66461b0fea1fd0dc7fef5d9a; BT_ctst=; BT_sdc=eyJldF9jb2lkIjoiTkEiLCJyZnIiOiJodHRwOi8vbG9jYWxob3N0OjIzNDMvIiwidGltZSI6MTQ1NzcyOTU0ODYyOCwicGkiOjUsImV0Y2NfY21wIjoiTkEifQ%3D%3D; BT_pdc=eyJldGNjX2N1c3QiOjAsImVjX29yZGVyIjowLCJldGNjX25ld3NsZXR0ZXIiOjB9; ai_session=tQc+O|1457780881671|1457781884398'
            )
        rescue OpenURI::HTTPError
            return false
        end

        page = Nokogiri::HTML(src) do |config|
            config.strict.noblanks
        end

        # check whether :symbol was found actually
        return false unless  page.css('div.gi-ui-fonds-info-panel').text.include? @security.symbol

        q = Quote.new(:id_security=>@security.id, :exchange=>'Universal Investment')

        # look for date
        # - valuations are not ready before 18:00 the same day
        q.time_last = Time.parse(
            /Stand:\s(\d{2}.\d{2}.\d{4})/.match( page.css('p.gi-ui-stand').text )[1]
        ) + 18.hours

        # look for quote and supplementary details
        page.css('table.table tbody tr').each do |tableline|
            label = tableline.first_element_child.text
            value = tableline.last_element_child.text
#p '==> %s: %s' %  [ label, value ]

            case label
                when 'Anteilwert'
                    (quote, currency) = value.split(' ')
                    q.last = quote.gsub(',','.').to_f
            end
        end

        if q.time_last and q.last > 0 then q else false end
    end
end
