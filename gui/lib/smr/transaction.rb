#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

module Smr  #:nodoc:
  ##
  # Businesslogic to buy/sell securities in SMR.
  #
  # Works mostly on top of the Order model but interacts with Position and
  # Security. Essentially a Transaction creates new PositionRevision(s).
  #
  # Methods return False in case #usage_constrains_ok is not happy. #error
  # has a String desribing the error in that case.
  class Transaction

      # Quote.quote to use for executing cash bookings
      # - defined as constant, though I do not see why this should ever change
      CASHPOSITION_EXEC_QUOTE = 1.0

      # cash Position used for booking this Transaction
      attr_reader :cash_position

      # Order used or issued by this Transaction
      attr_reader :order

      ##
      # Perform a transaction on given Position (or Smr::AssetPosition).
      # - User is asked for the sake of security, it should be authenticated!
      # - :order will use existing Order as basis for the transaction. The
      #   Order must belong to this Position and should be in pending state.
      # - :order_is_redemption says the given :order was not initiated by the
      #   SMR User but rather by the issuer of the Security to redeem it. The
      #   resulting cash booking is categorized accordingly then.
      # - if given Position is a cash position, #buy and #sell will directly
      #   operate on it. See Smr::ID_CASH_SECURITY.
      # - charges specified by #set_charges will be deducted from the cash
      #   booking
      # - if given, :type is one of the types supported by PositionRevision, :shares is default
      def initialize(position, user,
            options={
                :type=>:shares, :issue_time=>false,
                :order=>false, :order_is_redemption=>false,
                :triggered_by_order=>false
            }
          )
        @position = position
        @user = user
        @time = options[:issue_time] || Time.now
        @order = prepare_order(options[:triggered_by_order], options[:order])
        @order_is_redemption = options[:order_is_redemption] || false
        @cash_position = prepare_cash_position
        @error = Array.new
        @type =  options[:type] || :shares
      end

      ##
      # Tell whether this Transaction is a cash booking.
      def is_cash_booking?
          @position.is_cash_position?
      end

      ##
      # Human readable string describing this transaction
      def to_s
        if not @error.empty?
            'Transaction has errors: ' + @error.join(', ')
        elsif @order.is_pending?
            'Transaction in progress'
        elsif @order.is_executed?
            'Transaction complete, %s %.4f shares and booked against cash.' \
            % [ @order.type, @order.shares ]
        end
      end

      ##
      # Set charges to be booked with this order.
      #
      # All charges will be deducted from the cash booking.
      def set_charges(provision, courtage, expense)
        return false unless usage_constrains_ok
        @order.provision = provision
        @order.courtage = courtage
        @order.expense = expense
      end

      ##
      # Set accrued interest to be booked with this order.
      #
      # Accrued interest is handled as follows:
      # - on a BUY order :amount is considered being paid
      # - on a SELL order :amount is considered being received
      def set_accrued_interest(amount)
        return false unless usage_constrains_ok
        @order.accrued_interest = amount.abs
      end

      ##
      # Set a commentary text. It might be a verbose explanation.
      def set_comment(text)
        return false unless usage_constrains_ok
        @order.comment = text
      end

      ##
      # Make a PURCHASE on the Position.
      def buy(shares, limit, exchange=nil)
        return false unless usage_constrains_ok
        @order.type = 'buy'
        @order.shares = shares.abs
        @order.limit = limit
        @order.exchange = if is_cash_booking? then 'cash' else exchange end
        @order.save
      end

      ##
      # Make a SALE on the Position.
      def sell(shares, limit, exchange=nil)
        return false unless usage_constrains_ok
        @order.type = 'sale'
        @order.shares = shares.abs
        @order.limit = limit
        @order.exchange = if is_cash_booking? then 'cash' else exchange end
        @order.save
      end

      ##
      # Make a cash booking using :amount.
      #
      # The :amount can be positive or negative. Works only if given Position
      # is a cash position.
      #
      # Specify optional :charges and :accrued_interest as absolute number. It
      # will be added/deducted from :amount, depending its sign.
      #
      # There is no need to call #execute when #book is used.
      def book(amount, options={:time=>false, :charges=>false, :accrued_interest=>false, :comment=>false})
        return false unless usage_constrains_ok
        if not is_cash_booking? then
            @error << '#book works only if a cash position was given to ::new'
            return false
        end

        # #book is always to handle cash of some sort, so its default
        # is the "cash equivalent" of :shares.
        # Otherwise fall through and use options[:type] as was given to #new.
        @type = :order_booking if @type == :shares

        time = options[:time] || Time.now
        charges = options[:charges] || 0.0
        accrued_interest = options[:accrued_interest] || 0.0
        set_comment(options[:comment]) if options[:comment]

        if amount < 0.0 then
            sell(amount.abs + accrued_interest + charges, CASHPOSITION_EXEC_QUOTE)
        else
            buy(amount + accrued_interest - charges, CASHPOSITION_EXEC_QUOTE)
        end
        execute(CASHPOSITION_EXEC_QUOTE, time)
      end

      ##
      # Apply Order to Position at given quote and time.
      #
      # This is final and can not be reversed as it will also book the
      # transaction against cash.
      def execute(quote, time=Time.now)
        return false unless usage_constrains_ok
        @order.quote = quote
        @order.save! unless @order.id # trigger :id being created

        PositionRevision.create!(
            :type=>@type,
            :id_order=>@order.id,
            :id_position=>@position.id,
            :date_created=>time.to_i,
        )

        process_revisions
        book_order_against_cash(time)
      end

      ##
      # Cancel the Transaction. Can not be reversed.
      def cancel(time=Time.now)
        @order.is_canceled = 1
        @order.save!
      end

      # returns String describing the error when some methode returned False
      def error
        @error.join(', ')
      end

      ## #
      # Create new or use existing cashposition in Portfolio, depending on
      # :time and presence of such a Position.
      #
      # Implemented as class method so it can be used externally to find
      # cashpositions.
      #
      # User.id is checked here to improve security.
      #
      # Specification of cash type Position records:
      # - a cash Position lives 1 year: Jan 1st to Dec 31st
      #   (the :time parameter is of Time and used to determine the year)
      # - when a cash position is closed, that year is settled! no more orders
      #   in that account / that year are possible
      # - cash positions are created silently, if not present for that year
      # - cash positions are to be closed manually (so it is still possible to
      #   enter order data of past years)
      #
      def Transaction::find_cashposition(id_portfolio, id_user, time=Time.now)
        start_date=time.beginning_of_year.to_i
        end_date=time.end_of_year.to_i
        cp = false

        cps = Position.where(:id_portfolio=>id_portfolio, :id_security=>Smr::ID_CASH_SECURITY)
                     .joins(:Portfolio).where('portfolio.id_user=%i' % id_user).order(:id).all.to_a
        if cps then
            cps.keep_if { |cp|
                cp.time_record_created.year == time.year
            }
            cp = cps.first   # should we ever have multiple ones open the same year? multi currency?
        end

        cp = Position.create!(
           :id_portfolio=>id_portfolio,
           :id_security=>Smr::ID_CASH_SECURITY,
           :date_record_created=>time,
           :comment=>'Jan - Dec %i' % time.year
        ) unless cp

        return cp
      end

    protected

      ##
      # usability is limited by conditions verified here
      def usage_constrains_ok
        if @order.is_canceled? then
            @error << 'Order #%i has been canceled already' % @order.id
        elsif @order.is_executed? then
            @error << 'Order #%i has been executed already' % @order.id
        elsif @position.is_closed? then
            @error << 'Position #%i is closed. Orders can only be added to open positions.' % @position.id
        elsif @cash_position.is_closed? then
            @error << 'Cash position has been settled. No further transactions are possible for the period of %s.' % @cash_position.comment
        end
        @error.empty?
      end

      ##
      # create or use an Order
      def prepare_order(triggered_by_order=false, order=false)
        if order and order.is_a?(Order) then
            raise 'possible security violation: given order(:id=>%i) does not belong to position in a portfolio owned by user(:id=>%i)' % [order.id, @user.id] if order.Position.Portfolio.id_user != @user.id
            o = order
        else
            o = Order.new(
                :date_issued=>@time.to_i,
                :date_expire=>(@time+1.month).end_of_day.to_i,
                :id_position=>@position.id,
                :expense=>0, :courtage=>0, :provision=>0
            )
        end
        o.triggered_by_order = triggered_by_order if triggered_by_order
        return o
      end

      ###
      # utilize #find_cashposition in context of a instance
      def prepare_cash_position
         return Transaction::find_cashposition(@position.id_portfolio, @user.id, @time)
      end

      ##
      # Re-calculate all PositionRevision records related to this Position.
      def process_revisions
          @order.save
          shares = 0.0
          invested = 0.0
          PositionRevision.where(:id_position=>@position.id).order(:date_created=>:asc).each do |pr|
              o = pr.Order

              if o.is_sale? then
                  shares   -= o.shares
                  invested -= o.shares * o.quote
              else
                  shares   += o.shares;
                  invested += o.shares * o.quote
              end

              pr.shares = shares
              pr.invested = invested
              pr.save!
          end
      end

      ##
      # Make cash booking on cash Position in same Portfolio.
      #
      # Returns
      # - 1 if cash booking was successfull
      # - 2 if this Transaction is a cash booking itself
      # - 3 if the Order in this Transaction has been booked already
      #
      def book_order_against_cash(time)
        if @order.quote==0.0 or @order.shares==0.0 then
            raise 'order not sufficiently prepared'
        end

        # do nothing if this is a cash booking or the order has already triggered a cash booking
        return 2 if is_cash_booking?
        return 3 if Order.where(:triggered_by_order=>@order.id).count > 0

        # add new order onto it, execute right away
        ct = Transaction.new(
            @cash_position, @user, :triggered_by_order=>@order.id,
            :type=>(@order_is_redemption ? :redemption_booking : :shares ),
            :issue_time=>@time
        )
        vol = @order.volume
        vol *= -1 if @order.is_purchase?
        ct.book(vol, :charges=>@order.charges, :accrued_interest=>@order.accrued_interest, :time=>time)

        return 1
      end

  end

end # module
