#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
  
module Smr  #:nodoc:
  ##
  # A UploadedFile with relations to Portfolio, Position, Order and/or Security.
  #
  # == Security
  # For the sake of security its required to supply id of User that is used to
  # make sure the requested document does belong to that user. The +id_user+
  # parameter should <b>not</b> come from user input, but rather result from a
  # successful authentication process.
  #
  
  # == Attention
  # - right now this is only for viewing perposes
  # - to store new data records use the model classes directly. These are
  #   Document, DocumentData and DocumentAssign.
  #
  class UploadedFile
    ##
    # initialize with Document id and User id
    def initialize(id_document, id_user)
        @id = id_document
        @id_user = id_user
  
        @document = Document.where(:id=>@id, :id_user=>@id_user).limit(1).first
    end
  
    public
  
    def id
        @document.id
    end
  
    ##
    # returns Document.time_upload
    def time_upload
        @document.time_upload
    end
  
    def comment
        @document.comment
    end
  
    def filename
        @document.filename
    end
  
    def size
        @document.size
    end
  
    def mimetype
        @document.mimetype
    end
  
    def has_relations?
        if DocumentAssign.where(:id_document=>@id, :is_assigned=>1).count > 0 then return true
        else return false end
    end
  
    ##
    # tell whether a Document record was found
    def empty?
        not @document.is_a?(Document)
    end
  
    ##
    # return human readable string of relations of this document
    #
    # TODO: make link to each relation
    def relations_text
        rel = DocumentAssign.where(:id_document=>@id, :is_assigned=>1).first
        t = Array.new
  
        return nil unless rel
  
        t << 'security %s' % rel.Security if rel.id_security
        t << 'order #%i' % rel.id_order if rel.id_order
        t << 'portfolio %s' % rel.Portfolio if rel.id_portfolio
        t << 'position %s held at %s' % [rel.Position.Security, rel.Position.Portfolio] if rel.id_position
        
        return 'related to ' + t.join(', ')
    end
  
    ##
    # return Portfolio object(s) if any were assigned
    # FIXME: not yet implemented
    def portfolio
        true
    end
  
    ##
    # return Position object(s) if any were assigned
    # FIXME: not yet implemented
    def position
        true
    end
  
    ##
    # return Order object(s) if any were assigned
    # FIXME: not yet implemented
    def order
        true
    end
  
    ##
    # return Security object(s) if any were assigned
    # FIXME: not yet implemented
    def security
        true
    end
  
    ##
    # return BLOB (binary large object) data of that document
    def data
        DocumentData.where(:id_document=>@id).first.content
    end
  
    ##
    # assigns this document to an Order, a Position, a Security or a Portfolio.
    #
    # Give a symbol and the id that should be assigned. Like
    # +assign(:order=>5)+ or more than on with +assign(:order=>6,
    # :position=>3)+.
    #
    # Unassign by specifying zero, ie +assign(:order=>0)+.
    def assign(assignments = {})
        da = DocumentAssign.find_by_id_document(@id) || DocumentAssign.new(:id_document=>@id, :is_assigned=>1)
  
        return false if assignments.empty?
  
        da.id_order = assignments[:order] if assignments[:order]; da.id_order=nil if assignments[:order] == 0
        da.id_position = assignments[:position] if assignments[:position]; da.id_position=nil if assignments[:position] == 0
        da.id_portfolio = assignments[:portfolio] if assignments[:portfolio]; da.id_portfolio=nil if assignments[:portfolio] == 0
        da.id_security = assignments[:security] if assignments[:security]; da.id_security=nil if assignments[:security] == 0
  
        da.save!
    end

    ##
    # Make new Smr::UploadedFile from POSTed data.
    #
    # This creates the records which can later be viewed as Smr::UploadedFile. On
    # success it will return a new Smr::UploadedFile based on the new records.  
    # - +upload_time+ should be of class Time
    # - +file+ is expected to be of class ActionDispatch::Http::UploadedFile.
    #
    # NOTE: this is a class method, not an instance method!
    def self.store(id_user, file, comment='', upload_time=Time.now)
        raise 'file must be of ActionDispatch::Http::UploadedFile' unless file.is_a?(ActionDispatch::Http::UploadedFile)
        d = Document.new(
            :id_user=>id_user,
            :date_upload=>upload_time.to_i,
            :size=>file.size,
            :mimetype=>file.content_type,
            :filename=>file.original_filename,
            :comment=>comment
        )
        d.save!
        DocumentData.new(:id_document=>d.id, :content=>file.read).save!
        return UploadedFile.new(d.id, id_user)
    end
  end
  
  ##
  # Collection of UploadedFile objects owned by a given user.
  #
  # Optionally only those related to :id_position.
  #
  class UploadedFiles < Array
    # Ransack Gem query object, ie for use in controllers or views.
    attr_reader :query

    ##
    # == Security
    # The +id_user+ parameter should <b>not</b> come from user input, but rather
    # result from a successful authentication process.
    def initialize(id_user, options={})
        options.reverse_merge!({
            :id_position=>false, :id_order=>false,
            :date=>Time.now,
            :ransack_query=>false
        })

        @query = Document.where(
            :id_user=>id_user,
            :date_upload=>0..options[:date].to_i
        ).ransack(options[:ransack_query])

        if options[:id_position]
            @query = Document.where(
                :id_user=>id_user,
                :date_upload=>0..options[:date].to_i
            ).joins(:DocumentAssign).where(
                :document_assign=>{ :id_position=>options[:id_position] }
            ).order(
                :date_upload=>:desc
            ).ransack(options[:ransack_query])
        elsif options[:id_order]
            @query = Document.where(
                :id_user=>id_user
            ).joins(:DocumentAssign).where(
                :document_assign=>{ :id_order=>options[:id_order] }
            ).order(
                :date_upload=>:desc
            ).ransack(options[:ransack_query])
        else
            @query = Document.where(
                :id_user=>id_user,
                :date_upload=>0..options[:date].to_i
            ).order(
                :date_upload=>:desc
            ).ransack(options[:ransack_query])
        end
        @query.result.to_a.each { |id| push(UploadedFile.new(id, id_user)) }
        compact!
    end
  end
  
end # module
