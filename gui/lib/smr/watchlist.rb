#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#

module Smr  #:nodoc:
  ##
  # Total assets of current user at some point in time.
  #
  # Use #open_positions to access Smr::AssetPosition objects of all positions
  # held. Then each knows more details about itself.
  class Watchlist

      # Ransack Gem query object, ie for use in controllers or views.
      attr_reader :query

      ##
      # Makes the watchlist with Quote information at :date. The given User
      # should be authenticated!
      #
      # By default a set of 15 randomly selected securities is shown. To see
      # more, provide a :ransack_query.
      # This is for the sake of speed at the first look. There might be
      # hundrets to throusands of Security records in the database. Its no good
      # to select all of them at once.
      #
      # Option :ransack_query is the query produced by Ransack's #search_form
      # helper method. It will be used to select Security records.
      #
      # Optional :exclude_securities may give a collection if Security.id
      # numbers that should not be shown in the watchlist.
      #
      #
      # Note: if the :ransack_query contains #sorts, only the first (!) one
      # will actually be used and passed on to Smr::WatchlistItem as
      # :compare_by.
      def initialize(user, date,
            options={
                :exclude_securities=>false,
                :provides_collateral=>false, :provides_cashflow=>false,
                :provides_huge_coupon=>false, :provides_subannual_payments=>false,
                :matures_within_year=>false,
                :ransack_query=>false
            }
          )
          raise ':user must be of User' unless user.is_a?(User)
          raise ':date must be of Time' unless date.is_a?(Time)

          @user = user
          @date = date
          @items = Array.new
          exclude_securities = [ Smr::ID_CASH_SECURITY ]
          exclude_securities += options[:exclude_securities] if options[:exclude_securities]
          @filters = options[:filters] if options[:filters]


          if options[:ransack_query]
            @query = Security.ransack(options[:ransack_query])
          else
            c = Security.count
            @query = Security.where(:id=>Array.new(15).map{|e| rand(c)}).ransack
          end

          @query.result.includes(:Organization,:SecurityBond,:SecurityStock).to_a.uniq.each do |s|
            # apply filters
            next if exclude_securities.include?(s.id)
            next if s.is_expired?(date) or s.has_trading_ceased?(date)
            next if options[:provides_collateral] and s.collateral_coverage_ratio == 0
            next if options[:provides_cashflow] and (s.cashflow_this_year == false or s.cashflow_this_year == 0)
            next if options[:provides_subannual_payments] and not s.has_subannual_payments?
            if s.type == :bond
                next if options[:matures_within_year] and not s.get_type.time_maturity.between?(@date, @date + 1.year)
                next if options[:provides_huge_coupon] and s.get_type.coupon < 9
            elsif options[:matures_within_year] or options[:provides_huge_coupon]
                next  # skip securities that can be filtered with these options
            end

            # add to list
            @items << Smr::WatchlistItem.new(
                s, date,
                :compare_by=>(if not @query.sorts.empty? then @query.sorts.first.name.to_sym else :date end)
            )
          end
          @items.sort!
          @items.reverse! if not @query.sorts.empty? and @query.sorts.first.dir == 'desc'
      end
  
   public

      ##
      # loops over WatchlistItem collection
      def each(&block)
          @items.each(&block)
      end

      def collect(&block)
          @items.collect(&block)
      end

      def count
          @items.count
      end
  end

  ##
  # Represents a single item of a watchlist, suitable for easy
  # rendering in template.
  class WatchlistItem
      include Comparable

      # unique identifier for this item, right now wrapped to Security#id
      attr_reader :id

      # Time of this item.
      attr_reader :date

      # Quote of last trade known before :date, may be "old", wrapped to Security#last_quote.
      attr_reader :quote

      # underlying Security
      attr_reader :security

      ##
      # A WatchlistItem is basically a Security shown at a :date.
      #
      # Use the :compare_by option to tune how a collection of these items
      # should be ordered. Default is #date, possible are all attributes of
      # Security.
      def initialize(security, date, options={:compare_by=>:date})
          raise ':security must be of type Security' unless security.is_a?(Security)
          raise ':date must be of type Time' unless date.is_a?(Time)
  
          @security = security; @date=date
          @quote = @security.last_quote(@date)
          @id = @security.id
          @compare_by = options[:compare_by] || :date
      end

      ##
      # Evaluates :compare_by option, see #new.
      def <=>(other)
        if @compare_by != :date
            mine = @security.send(@compare_by)
            the_other = other.security.send(@compare_by)

            if mine==false or the_other==false then  # probably from Security.method_missing
               if mine or the_other then 1
               else 0 end
            else mine <=> the_other end
        else
            date <=> other.date
        end
      end
  end
end # module
