require 'security_identifiers'

namespace :smr do
  desc "Run quote update cycle. Useful for scheduling, ie with cron."
  task :update_quotes => :environment do
      require 'fetch_quote_job'

      Security.where(:fetch_quote=>true).each do |s|
          next if s.has_trading_ceased? or s.is_expired?
          Smr::FetchQuoteJob.perform_later(s)
      end
  end

  desc "Create Securities from ISIN symbols in :file."
  task :import_symbols, [:file] => :environment do |t, args|
      symbols, item, found, imported, known, expired, invalid, no_metadata = 0, 0, 0, 0, 0, 0, 0, 0
      stop_each = rand 1402

      unless args[:file]
        puts 'Usage: rake smr:import_symbols[/path/to/file]'
        exit 1
      end

      matches = File.open(args[:file], 'r').read.scan(/[A-Z]{2}[A-Z0-9]{9}\d{1}?/)
      symbols = matches.count

      matches.each do |m|
        puts '%i/%i => %s' % [item, symbols, m]
        item+=1

        unless SecurityIdentifiers::ISIN.new(m).valid?
            puts '  ISIN code invalid, skipping.'; invalid+=1
            next
        end
        # use original string until https://github.com/invisiblelines/security_identifiers/issues/5 is solved
        isin = m

        puts "--- sleep #{sleep rand(300) } --- " if item % stop_each == 0

        if Security.where(:symbol=>isin).count == 0
            s = Security.new :symbol=>isin
            if s.fetch_metadata
                puts '  found %s' % s; found+=1
                if s.is_expired?
                    puts '  is expired, dropping.'; expired+=1
                    s.destroy
                else
                    s.description = 'created by smr:import_symbols'
                    s.save!
                    imported+=1
                end
            else
                puts '  no metadata found, dropping.'; no_metadata+=1
                s.destroy
                next
            end
        else puts '  is known, skipping.'; known+=1 end
#Security.where(:symbol=>isin).destroy_all
      end

      puts
      puts 'stats: symbols=%i invalid_symbols=%i found=%i not_found=%i skipped_known=%i skipped_expired=%i imported=%i' % [
        symbols, invalid, found, no_metadata, known, expired, imported
      ]
  end

  desc 'Create fixtures for testing from data in an existing database. Defaults to development database.  Set RAILS_ENV to override.'
  task :extract_fixtures => :environment do
    sql  = 'SELECT * FROM `%s`'
    skip_tables = ['schema_info']
    ActiveRecord::Base.establish_connection
    (ActiveRecord::Base.connection.tables - skip_tables).each do |table_name|
      i = '000'
      File.open("#{Rails.root}/test/fixtures/#{table_name}.yml", 'w') do |file|
        data = ActiveRecord::Base.connection.select_all(sql % table_name)
        file.write data.inject({}) { |hash, record|
          hash["#{table_name}_#{i.succ!}"] = record
          hash
        }.to_yaml
      end
    end
  end
end
