 #
 # This file is part of SMR.
 #
 # SMR is free software: you can redistribute it and/or modify it under the
 # terms of the GNU General Public License as published by the Free Software
 # Foundation, either version 3 of the License, or (at your option) any later
 # version.
 #
 # SMR is distributed in the hope that it will be useful, but WITHOUT ANY
 # WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 # A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License along with
 # SMR.  If not, see <http://www.gnu.org/licenses/>.
 #
 require 'test_helper'
 require 'digest/sha1'
 require 'csv'
 
module Smr  #:nodoc:
    ##
    # Typical admin privileged session to test administrating things.
    #
    class AdminSessionTest < ActionDispatch::IntegrationTest
        LOGIN = 'admin'
        PASSWORD = 'admin'
  
        ##
        # try to login as privileged admin user
        test "login as admin" do
            # see if it takes us to the login page
            get root_path
            assert_response :redirect, 'accessing / should redirect to /login'
            follow_redirect!
            assert_response :success
            assert_equal new_session_path, path
           
            smr_login(LOGIN, PASSWORD)
        end
  
        ##
        # manage (add, edit, delete) securities
        test "manage securities" do
            smr_login(LOGIN, PASSWORD)
  
            get objects_security_index_path
            assert_response :success
            assert_not_nil assigns(:securities)

            get export_securities_path(:format=>'csv')
            assert_response :success
            assert_equal 'text/csv', response.content_type
            assert CSV.parse(response.body)

            # FIXME: tbd
        end
  
        ##
        # manage (add, edit, delete) portfolios
        test "manage portfolios" do
            smr_login(LOGIN, PASSWORD)
  
            get objects_portfolio_index_path
            assert_response :success
            assert_not_nil assigns(:portfolios)
            # FIXME: tbd
        end
  
        ##
        # manage (add, edit, delete) FigureVar records
        test "manage figures" do
            smr_login(LOGIN, PASSWORD)
  
            get objects_figurevar_index_path
            assert_response :success
            assert_not_nil assigns(:figurevars)
            # FIXME: tbd
        end

        ##
        # manage (add, edit) User
        test "manage user" do
            smr_login(LOGIN, PASSWORD)

            get objects_user_index_path
            assert_response :success
            assert_not_nil assigns(:users)
            # FIXME: tbd
        end

        ##
        # manage (add, edit) Organization
        test "manage organization" do
            smr_login(LOGIN, PASSWORD)

            get objects_organization_index_path
            assert_response :success
            assert_not_nil assigns(:organizations)
            # FIXME: tbd
        end

        ##
        # manage (add, edit) Bookmark
        test "manage bookmark" do
            smr_login(LOGIN, PASSWORD)

            get objects_bookmark_index_path
            assert_response :success
            assert_not_nil assigns(:bookmarks)
            # FIXME: tbd
        end
    end

end # module
