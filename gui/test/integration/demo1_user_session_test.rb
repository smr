#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'
require 'digest/sha1'
        
module Smr  #:nodoc:
    ##
    # typical user session doing things except trading positions.
    class Demo1UserSessionTest < ActionDispatch::IntegrationTest
        LOGIN = 'demo1'
        PASSWORD = 'demo'
    
        ##
        # try to login as demo1 user
        test "login as demo1" do
            # see if it takes us to the login page
            get root_path
            assert_response :redirect, 'accessing / should redirect to /login'
            follow_redirect!
            assert_response :success
            assert_equal new_session_path, path
           
            smr_login(LOGIN, PASSWORD)
        end
    
        ##
        # try to list documents, upload one, download it again, delete it
        test "work with documents" do
            smr_login(LOGIN, PASSWORD)
    
            get documents_path
            assert_response :success
            assert_not_nil assigns(:portfolios)
            assert_not_nil assigns(:document)
            assert_not_nil docs = assigns(:documents)
            assert docs.is_a?(UploadedFiles) 
            assert docs.empty?
    
            upload_file = fixture_file_upload('quote.yml', 'text/plain')
            upload_sha1 = Digest::SHA1.hexdigest(upload_file.read)
    
            # upload
            post documents_path,
                {:action=>'create', :document=>{:comment=>'IntegrationTest', :content=>upload_file}, :id_portfolio=>'' },
                {:html=>{:multipart=>true}, :referer=>documents_path }
            assert_response :redirect
            follow_redirect!
            assert_not_nil docs = assigns(:documents), 'no document shown after upload'
            assert docs.is_a?(UploadedFiles) 
            assert_not docs.empty?
            assert d = docs.each.first
            assert d.is_a?(UploadedFile)
    
            # download nothing
            get download_path, {}, {'HTTP_REFERER'=>documents_path}
            assert_redirected_to documents_path
    
            # download what we uploaded
            get download_path, {:id=>d.id}, {'HTTP_REFERER'=>documents_path}
            assert_equal @response.content_type, 'text/plain'
            assert_equal Digest::SHA1.hexdigest(@response.body), upload_sha1
    
            # delete what we uploaded
            get delete_document_path, {:id=>d.id}, {'HTTP_REFERER'=>documents_path}
            assert_redirected_to documents_path
        end
    
        ##
        # view cashflows from dividends and orders
        test "view cashflow" do
            smr_login(LOGIN, PASSWORD)
    
            get cashflow_index_path(:timeframe=>:all_time, :portfolio=>0)
            assert_response :success
            assert_equal cashflow_index_path, path
            assert_not_nil log = assigns(:log)
            assert_not_nil forecast = assigns(:forecast)

            assert log.is_a?(CashflowLog)
            assert log.count > 0, 'CashflowLog is empty'
            log.each do |i|
                assert i.is_a?(CashflowItem)
            end

            assert forecast.is_a?(CashflowForecast)
            assert forecast.count > 0, 'CashflowForecast is empty'
            forecast.each do |i|
                assert i.is_a?(CashflowItem)
            end
        end
    
        ##
        # view and add FigureData
        test "work with figure data" do
            smr_login(LOGIN, PASSWORD)
    
            get figures_path
            assert_response :success
            assert_equal figures_path, path
            assert assigns(:securities).is_a?(Array)
            assert assigns(:selected_security).is_a?(Security)
            assert assigns(:have_data).is_a?(FalseClass)
    
            # select a security that has figure data
            get figures_path, {:id_security=>25, :show=>'this'}
            assert_response :success
            assert assigns(:have_data).is_a?(TrueClass)
            assert assigns(:datatable).render  # Raises if no data
    
            # add new FigureData
            # - no fixtures on BASF AG, therefore nothing should be shown at first
            #   and then only FigureData we added from here
            figure_data = {
                :id=>0,             # new
                :id_security=>15,      # BASF AG
                :id_figure_var=>4,  # Demo1Var1
                :period=>'year', :time=>1.days.ago.strftime('%Y-%m-%d'),
                :analyst=>'integration test', :value=>1.23, :is_expected=>1,
                :is_audited=>1, :comment=>'made by demo1 integration test'
            }
    
            get new_figure_path, :id_security=>figure_data[:id_security], :show=>'this'
            assert_response :success
            assert assigns(:figuredata).is_a?(FigureData)
            assert assigns(:have_data).is_a?(FalseClass)
            post(figures_path, 
                {:action=>'create', :figure_data=>figure_data},
                {:referer=>figures_path}
            )
            follow_redirect!
            assert_equal figures_path, path
            assert assigns(:selected_security).is_a?(Security)
            assert_equal assigns(:selected_security).id, figure_data[:id_security]
            assert assigns(:have_data).is_a?(TrueClass)
    
            # try again, but store FigureData on a FigureVar that belongs to
            # another user
            # note: thats something the Gui does not allow to do
            figure_data[:id_figure_var] = 1  # FirstVar, belongs :id_user=1 (admin)
            figure_data[:id_security] = 6       # CocaCola Inc.
            get new_figure_path, :id_security=>figure_data[:id_security], :show=>'this'
            assert_response :success
            assert assigns(:have_data).is_a?(FalseClass)
            post(figures_path, 
                {:action=>'create', :figure_data=>figure_data},
                {:referer=>figures_path}
            )
            follow_redirect!
            assert assigns(:have_data).is_a?(FalseClass)
        end
    
        ##
        # use the personal blog
        test "read and blog articles" do
            smr_login(LOGIN, PASSWORD)
    
            get blog_index_path
            assert_response :success
            assert_equal blog_index_path, path
            assert_not_nil blog = assigns(:blogroll)
            assert blog.is_a?(Blog)
            assert blog.empty?
    
            # blog new article
            get new_blog_path
            assert_response :success
            assert_equal new_blog_path, path
            assert_not_nil article = assigns(:article)
            assert article.is_a?(Comment)
            post blog_index_path,
                {:action=>'create', :comment=>{:title=>'IntegrationTest Title', :comment=>'IntegrationTest Text'} },
                {:referer=>blog_index_path }
            follow_redirect!
            assert_equal blog_index_path, path
            assert_not_nil blog = assigns(:blogroll)
            assert blog.is_a?(Blog)
            assert_not blog.empty?
            item = blog.each.first
            assert item.is_a?(BlogItem)
            assert_equal 'Comment', item.type
            assert_equal 'IntegrationTest Title', item.title
            assert_equal 'IntegrationTest Text', item.body
        end

        ##
        # use quote records
        test "work with quoterecords" do
            smr_login(LOGIN, PASSWORD)
    
            get quoterecords_path
            assert_response :success

            # get quoterecords on E.On AG (there should not be any) for 2007-08-07
            post set_session_params_path,
                {:smr_browse_date=>'2007-08-07'},
                {:referer=>quoterecords_path}
            follow_redirect!
            get quoterecords_path, { :id_security=>25, :show=>'this' }
            assert_response :success
            assert assigns(:selected_security).is_a?(Security)
            assert_equal 25, assigns(:selected_security).id

            assert_not_nil quote = assigns(:intraday_quotes).first
            assert quote.is_a?(Quote)

            # post a new quoterecord from :intraday_quotes observations
            marker_qr=Digest::SHA1.hexdigest(Time.now.to_s)
            post quoterecords_path,
                {:action=>'create',
                  :quoterecord => {
                    :id=>'', :id_security=>quote.id_security, :id_quote=>quote.id,
                    :column=>'upward', :comment=>marker_qr,
                    :is_pivotal_point=>0, :is_uphit=>0, :is_downhit=>0, :is_signal=>0
                  },
                  :commit=>'record this one'
                },
                {:referer=>quoterecords_path}
            assert_response :success
            assert_equal quoterecords_path, path

            # now we should see that one listed in previous records
            assert_not_nil qrs = assigns(:quoterecords)
            assert qrs.is_a?(Smr::Quoterecords)
            qr = qrs.first
            assert_equal qr.comment, marker_qr
            assert_equal qr.column, 'upward'

            # edit that record
            get quoterecord_path(:id=>qr.id)
            assert_response :success
            assert_not_nil qr_edit = assigns(:quoterecord)
            assert_equal qr_edit.comment, qr.comment
            marker_qr_edit=Digest::SHA1.hexdigest(marker_qr + Time.now.to_s)
            post quoterecords_path,
                {:action=>'create',
                  :quoterecord => {
                    :id=>qr_edit.id, :column=>'downward', :comment=>marker_qr_edit,
                    :is_pivotal_point=>0, :is_uphit=>0, :is_downhit=>0, :is_signal=>0
                  },
                  :commit=>'record this one'
                },
                {:referer=>quoterecords_path}
            assert_response :success
            assert_not_nil qrs_edited = assigns(:quoterecords)
            assert_equal qrs_edited.first.comment, marker_qr_edit
        end

        ##
        # record rules on quoterecords
        test "work with quoterecord rules" do
            smr_login(LOGIN, PASSWORD)
    
            get quoterecord_rules_path
            assert_response :success
            assert assigns(:rules).empty?

            # make new rule
            marker_rule1=Digest::SHA1.hexdigest('obey!' + Time.now.to_s)
            get new_quoterecord_rule_path
            assert_response :success
            post quoterecord_rules_path,
                {   :action=>'create',
                    :quoterecord_rule=>{:id=>'', :order=>1, :name=>'rule1', :rule=>marker_rule1},
                    :related_columns=>['secondary_rally', 'downward'],
                    :commit=>'Save Rule'
                },
                {:referer=>new_quoterecord_rule_path}
            follow_redirect!
            assert_response :success
            rules = assigns(:rules)
            assert_not rules.empty?, 'we should have a rule by now'
            assert_equal rules.first.name, 'rule1'
            assert_equal rules.first.rule, marker_rule1
            assert rules.first.get_related_columns.include?('downward'), 'column not related to new rule'

            # edit our rule
            marker_edited=Digest::SHA1.hexdigest('I said obey!' + Time.now.to_s)
            get new_quoterecord_rule_path, :id=>rules.first.id
            assert_not_nil edit_rule = assigns(:rule)
            assert_equal edit_rule.rule, marker_rule1
            post quoterecord_rules_path,
                {   :action=>'create',
                    :quoterecord_rule=>{:id=>edit_rule.id, :order=>1, :name=>edit_rule.name, :rule=>marker_edited},
                    :related_columns=>['secondary_reaction', 'upward'],
                    :commit=>'Save Rule'
                },
                {:referer=>new_quoterecord_rule_path}
            follow_redirect!
            assert_response :success
            assert_not_nil edited_rule = assigns(:rules).first
            assert_equal edited_rule.name, 'rule1'
            assert_equal edited_rule.rule, marker_edited
            assert edited_rule.get_related_columns.include?('upward'), 'edited column relation not present'
        end

        ##
        # add a new portfolio and transfere existing position into it
        test "add portfolio and transfer position" do
            smr_login(LOGIN, PASSWORD)
            get new_objects_portfolio_path
            assert_response :success
            assert_not_nil p = assigns(:portfolio)
            assert p.is_a?(Portfolio)

            # create portfolio
            post objects_portfolio_index_path,
                {   :action=>'create',
                    :portfolio=>{:id=>'', :name=>'demo1_testportfolio', :taxallowance=>100, :comment=>'demo1_testcomment'},
                    :time_created=>'2015-04-08 15:26:58 -0400',
                    :commit=>'Save Portfolio'
                },
                {:referer=>new_objects_portfolio_path}
            follow_redirect!
            assert_response :success
            assert_equal objects_portfolio_index_path, path

            # find & edit existing and new Portfolio
            existing = true
            new = true
            assert_not_nil plist = assigns(:portfolios)
            plist.each{|p|
                if p.name == 'Local Broker' then existing = p else new = p end
            }
            assert existing.is_a?(Portfolio), 'existing portfolio no located'
            assert new.is_a?(Portfolio), 'new portfolio not located'

            get edit_objects_portfolio_path(:id=>existing.id)
            assert_response :success
            assert_not_nil editing = assigns(:portfolio)
            assert_equal existing.name, editing.name

            # submit the transfer form, targeting the new portfolio
            post objects_patch_portfolio_path,
                {
                    :id_portfolio_transfer=>new.id,
                    :positions=>['14'],              # we just *know* position 14 s;-) (from fixtures)
                    :commit=>'Transfer Positions'
                },
                {:referer=>edit_objects_portfolio_path(:id=>existing.id)}
            follow_redirect!
            assert_response :success
            assert_equal objects_portfolio_index_path, path
            assert_equal 1, Position.where(:id=>14, :id_portfolio=>new.id).count, 'transfered position #14 not in target portfolio'
        end

        ##
        # view an existing order
        test "view order" do
            smr_login(LOGIN, PASSWORD)

            get order_path(:id=>145)
            assert_response :success
            assert_equal order_path(:id=>145), path
            assert_not_nil order = assigns(:order)
            assert_not_nil documents = assigns(:documents)

            assert order.is_a?(Order)
            assert documents.is_a?(Smr::UploadedFiles)

            get order_path(:id=>999999)
            assert_response :redirect, 'viewing nonexisting Order does not redirect'
            follow_redirect!
            assert_response :success
            assert_equal root_path, path, 'expected redirect to /'
        end

        ##
        # view report on closed positions and asset statistics
        test "view report" do
            smr_login(LOGIN, PASSWORD)

            get report_index_path(:timeframe=>:all_time, :portfolio=>0)
            assert_response :success
            assert_equal report_index_path, path
            assert_not_nil asset_totals = assigns(:asset_totals)
            assert_not_nil closed_positions = assigns(:closed_positions)
            assert_not_nil realized_gain = assigns(:realized_gain)
            assert_not_nil revenue = assigns(:revenue)
            #assert_not_nil charges_paid = assigns(:charges_paid)  # disabled, see report controller
            #assert_not_nil dividend_received = assigns(:dividend_received)

            assert closed_positions.count > 0, 'No :closed_positions found'
            closed_positions.each do |p|
                assert p.is_a?(Smr::AssetPosition)
            end

            assert asset_totals.count > 0, 'No reporting on :asset_totals'
            asset_totals.each do |t|
                assert t.is_a?(Smr::Asset)
            end
        end

        ##
        # portrait a Security and a Organization
        test "view portraits" do
            smr_login(LOGIN, PASSWORD)

            get portrait_security_path(:id=>106)  # of Country A 5.0% 2000(00)
            assert_response :success
            assert_not_nil security = assigns(:security)
            assert_not_nil positions = assigns(:positions)
            assert_not_nil cashflow = assigns(:cashflow)
            assert security.is_a? Security
            assert_equal 106, security.id, 'portrait on wrong Security'
            assert positions.first.is_a? Smr::AssetPosition
            if security.cashflow_this_year > 0
              assert cashflow.total > 0, 'no cashflow reported'
            end

            get portrait_organization_path(:id=>5)  # of Country A
            assert_response :success
            assert_not_nil organization = assigns(:organization)
            assert_not_nil positions = assigns(:positions)
            assert_not_nil cashflow = assigns(:cashflow)
            assert organization.is_a? Organization
            assert_equal 5, organization.id, 'portrait on wrong Organization'
            assert positions.first.is_a? Smr::AssetPosition
            if security.cashflow_this_year > 0
              assert cashflow.total > 0, 'no cashflow reported'
            end
        end

        ##
        # perform research work on Security and Organization
        test "use workdesk" do
            smr_login(LOGIN, PASSWORD)

            get workdesk_index_path, {}, {'HTTP_REFERER'=>workdesk_index_path}
            assert_response :success
            assert_not_nil stack = assigns(:stack)
            assert stack.empty?

            # add Country A 5.0% 2000(00)
            get workdesk_add_path(:id_security=>106), {}, {'HTTP_REFERER'=>workdesk_index_path}
            follow_redirect!
            assert_response :success

            # add Country A
            get workdesk_add_path(:id_organization=>5), {}, {'HTTP_REFERER'=>workdesk_index_path}
            follow_redirect!
            assert_response :success

            assert_not_nil stack = assigns(:stack)
            assert_equal 2, stack.count
        end
    end
 
end # module
