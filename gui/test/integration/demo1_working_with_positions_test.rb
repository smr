#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'
require 'digest/sha1'

module Smr  #:nodoc:
    ##
    # typical user session doing work on positions, such as trading securities.
    class Demo1WorkingWithPositionsTest < ActionDispatch::IntegrationTest
        LOGIN = 'demo1'
        PASSWORD = 'demo'

        ##
        # try to login as demo1 user
        test "login as demo1" do
            # see if it takes us to the login page
            get root_path
            assert_response :redirect, 'accessing / should redirect to /login'
            follow_redirect!
            assert_response :success
            assert_equal new_session_path, path

            smr_login(LOGIN, PASSWORD)
        end

        ##
        # view a open position
        test "view open position" do
            smr_login(LOGIN, PASSWORD)

            # take second position from asset listing
            assert_not_nil asset_position_entry = assigns(:open_positions).second
            assert asset_position_entry.is_a?(AssetPosition)
            get position_path(:id=>asset_position_entry.id)
            assert_response :success
            assert_equal position_path, path
            assert_not_nil p = assigns(:position)
            assert p.is_a?(AssetPosition)

            # position should be open since we took it from open_positions
            assert_not p.is_closed?
            assert_not p.is_new?
            assert p.invested.is_a?(Float)
            assert (p.market_value.is_a?(Float) and p.market_value>0.0)
            assert p.profit_loss.is_a?(Float)
            assert p.dividend.received
            assert (p.charges.is_a?(Float) and p.charges >= 0.0)
            assert p.gain.is_a?(Float)

            # it must have at least one revision since its open but not new
            assert_not_nil first_revision = p.revisions.first
            assert first_revision.is_a?(PositionRevision)
            assert first_revision.Order.is_a?(Order)
            assert_not_equal first_revision.shares, 0.0


            get position_path(:id=>30)
            assert_redirected_to root_path, 'no redirect when using position_controller with cash position'
        end

        ##
        # view a closed position at two points in time: /after/ it has been
        # closed and /before/, when it was open.
        test "view closed position" do
            smr_login(LOGIN, PASSWORD)

            get position_path(:id=>15, :smr_browse_date=>'2013-10-20')
            assert_response :success
            assert_equal position_path, path

            # Position #15 (E.On) has been sold on Oct 18 2013, we look two days later
            post set_session_params_path,
                { :smr_browse_date=>'2013-10-20' },
                { :referer=>position_path(:id=>15) }
            assert_redirected_to position_path(:id=>15)
            follow_redirect!

            assert_not_nil p = assigns(:position)
            assert p.is_a?(AssetPosition)
            assert p.is_closed?, 'position %s not closed browsing after closure' % p
            assert_not p.is_viewed_before_closure?
            assert_not p.is_new?
            assert_equal 0, p.shares

            # look again, two days before it was closed
            post set_session_params_path,
                { :smr_browse_date=>'2013-10-16' },
                { :referer=>position_path(:id=>15) }
            assert_redirected_to position_path(:id=>15)
            follow_redirect!

            assert_not_nil p = assigns(:position)
            assert p.is_closed?, 'position %s not closed browsing before closure' % p
            assert p.is_viewed_before_closure?
            assert_not p.is_new?
            assert_equal 100, p.shares
        end

        ##
        # issue and cancel orders on existing position
        test "issue orders on existing position" do
            smr_login(LOGIN, PASSWORD)
            get new_position_path
            assert :success

            # the form should offer option 3: new order in portfolio OR new order
            # on existing position
            assert_not assigns(:position)
            assert_not assigns(:portfolio)
            assert open_positions = assigns(:open_positions)
            assert securities = assigns(:securities)
            assert portfolios = assigns(:portfolios)

            # issue new order on existing open position
            marker_order1=Digest::SHA1.hexdigest(Time.now.to_s)
            post position_index_path(:id_position=>open_positions.first.id),
                { :action=>'create',
                  :time_issued=>Time.now.to_s, :time_expire=>(Time.now+2.days).to_s,
                  :order=>{ :shares=>100, :limit=>6.66, :provision=>1.11,
                            :expense=>2.22, :courtage=>0.0,
                            :comment=>marker_order1}
                },
                {:html=>{:multipart=>true}, :referer=>position_path(:id=>open_positions.first.id) }
            assert :success

            get position_path(:id=>open_positions.first.id)
            assert :success
            assert p = assigns(:position)
            assert p.is_a?(AssetPosition)
            assert p.has_pending_orders?
            neworder1 = p.pending_orders.where(:comment=>marker_order1).first
            assert neworder1.is_a?(Order), 'issued order not found in pending state, got: %s' % neworder1.inspect

            # issue and execute new order in one step
            marker_order2=Digest::SHA1.hexdigest(Time.now.to_s)
            post position_index_path(:id_position=>open_positions.first.id),
                { :action=>'create',
                  :execute_now=>1, :execute_quote=>7.76, :execute_time=>Time.now+2.hours,
                  :time_issued=>Time.now.to_s, :time_expire=>(Time.now+2.days).to_s,
                  :order=>{ :type=>'buy', :shares=>200, :limit=>7.77,
                            :provision=>0.00, :expense=>0.001, :courtage=>3.33,
                            :comment=>marker_order2}
                },
                {:html=>{:multipart=>true}, :referer=>position_path }
            assert :success

            get position_path(:id=>open_positions.first.id)
            assert :success
            assert p_after_execute = assigns(:position)
            assert p_after_execute.is_a?(AssetPosition)
            assert_not flash.empty?, 'have flash messages: ' % flash.inspect
            assert p_after_execute.revisions.any? {|r| r.Order.comment == marker_order2},
                'executed new order did not create new revision'

            # check whether executing changed number of shares, invested, etc... in
            # position ... and.... well... it should be correctly calculated
            assert_equal p.shares + 200, p_after_execute.shares,
                'wrong number of shares in position after order execute'
            assert_equal p.invested + 200*7.76, p_after_execute.invested,
                'wrong invested amount in position after order execute'
            assert_equal p.charges + 0.001 + 3.33, p_after_execute.charges,
                'wrong charges in position after order execute'

            # cancel issued neworder1
            post cancel_order_path,
                { :action=>'cancel_order', :id_order=>neworder1.id},
                {:html=>{:multipart=>true}, :referer=>position_path(:id=>p.id) }
            assert_redirected_to position_path(:id=>p.id)
            follow_redirect!
            assert p_after_cancel = assigns(:position)
            assert p_after_cancel.is_a?(AssetPosition)
            assert_nil p_after_cancel.pending_orders.where(:comment=>marker_order1).first,
                'failed to cancel pending order'
        end

        ##
        # controller option 3) make new position in selectable portfolio holding selectable security
        test "issue order for new position in some selected portfolio" do
            smr_login(LOGIN, PASSWORD)
            get new_position_path
            assert :success

            # issue new order that triggers creation of new position, execute right away
            marker_order1=Digest::SHA1.hexdigest(Time.now.to_s)
            post position_index_path,
                { :action=>:create,
                  :id_position=>'', :id_security=>13, :id_portfolio=>20, # Addidas into Local Broker owned by demo1
                  :time_issued=>Time.now.to_s, :time_expire=>(Time.now+2.days).to_s,
                  :order=>{ :type=>'buy', :shares=>100, :limit=>75, :addon=>'none', :exchange=>'TRG',
                            :provision=>9.95, :expense=>0, :courtage=>0,
                            :accrued_interest=>0,
                            :comment=>marker_order1},
                  :execute_now=>1, :execute_quote=>75, :execute_time=>(Time.now + 2.minutes).to_s
                },
                {:html=>{:multipart=>true}, :referer=>asset_index_path }
            assert :success
            follow_redirect!

            assert p = assigns(:position)
            assert p.is_a?(AssetPosition)
            assert_equal marker_order1, p.revisions.first.Order.comment, 'marker_order1 not found in revision'
            assert_equal 100, p.shares, 'incorrect number of shares after execute'
        end

        ##
        # add dividend payment by following the workflow a user would use
        test "add dividend payment to open position" do
           id_position=33  # Company A Preferred Stock
           dividend_data = { :id_position=>33, :time_exdate=>"2015-05-28", :paid=>1.25}

           smr_login(LOGIN, PASSWORD)

            get position_path(:id=>id_position)
            assert :success
            get position_path(:id=>id_position) + '?add_dividend=true'
            assert :success
            assert d = assigns(:dividend)
            assert d.is_a?(Dividend)
            assert_equal id_position, d.id_position

            post(cashflow_index_path,
                { :action=>'create', :dividend=>dividend_data,
                  :valutadate=>"2015-05-30", :received=>1.0,
                  :redirect=>"position", :commit=>"Save Payment Received",
                },
                {:referer=>position_path(:id=>id_position) }
            )
            follow_redirect!
            assert_equal position_path(:id=>id_position), path
        end

        ##
        # utilize a cash position: view, make booking
        test "work with cash position" do
            smr_login(LOGIN, PASSWORD)

            get cashposition_path(:id=>15)
            assert_redirected_to root_path, 'no redirect when using cashposition_controller with regular position'

            get cashposition_path(:id=>30)
            assert_response :success
            assert_equal cashposition_path(:id=>30), path

            assert_not_nil cp = assigns(:position)
            assert cp.is_a?(Smr::AssetPosition)
            assert cp.is_cash_position?
            assert_equal -14602.7, cp.invested, 'wrong balance of cash account'

            # make booking: one deposit, one withdrawal, re-test balance
            booking_data = { :id_cashposition=>30, :amount=>100, :date=>'2015-03-25', :comment=>'booking deposit test', :type=>'cash_booking' }
            post(cashposition_index_path(:id=>30),
                {:action=>'create', :booking=>booking_data},
                {:referer=>cashposition_path(:id=>30) }
            )
            follow_redirect!
            assert_equal cashposition_path(:id=>30), path
            assert_not_nil cp = assigns(:position)
            assert_equal -14502.7, cp.invested, 'wrong balance after booking deposit'

            booking_data = { :id_cashposition=>30, :amount=>-1000, :date=>'2015-03-26', :comment=>'booking withdrawal test', :type=>'cash_booking' }
            post(cashposition_index_path(:id=>30),
                {:action=>'create', :booking=>booking_data},
                {:referer=>cashposition_path(:id=>30) }
            )
            follow_redirect!
            assert_equal cashposition_path(:id=>30), path
            assert_not_nil cp = assigns(:position)
            assert_equal -15502.7, cp.invested, 'wrong balance after booking withdrawal'
        end

        ##
        # settle a cash position
        test "settle cash position" do
            smr_login(LOGIN, PASSWORD)

            # hit the :settle link
            get settle_cashposition_path(:id=>30), nil, :referer=>cashposition_path(:id=>30)
            follow_redirect!

            assert_equal cashposition_path(:id=>30), path
            assert_not_nil cp = assigns(:position)
            assert cp.is_closed?, 'asset position not closed after settling'

            # browse assets at end of the year of settlement: settled cashposition should be shown
            assets = Smr::Asset.new(2, Time.new(cp.revisions.first.time_created.year, 12, 31))
            assets.open_positions.each{|p| cp = p if p.is_cash_position? }

            assert cp, 'no cashposition in asset listing on Dec 31st'
            assert_equal 30, cp.id, 'got cashposition with unexpected :id'
            assert cp.is_closed?, 'asset position not closed at Dec 31st'
            assert (cp.shares != 0), 'asset position has zero balance. it should hold or owe something on Dec 31st.'

            # browse assets the year following settlement: another cashposition should be shown
            assets_next_year = Smr::Asset.new(2, Time.new( (cp.revisions.first.time_created + 1.year).year, 1, 10))
            next_cp = false
            assets_next_year.open_positions.each{|p| next_cp = p if p.is_cash_position? }
            assert next_cp, 'no cashposition in asset listing of following year'
            assert_not_equal cp.id, next_cp.id, 'asset listing shows settled cashposition in following year'
        end

        ##
        #  investigate a common stock position (using SecurityStock type model)
        test "work with common stock position" do
            smr_login(LOGIN, PASSWORD)

            get position_path(:id=>31)
            assert_response :success
            assert_not_nil p = assigns(:position)
            assert p.is_a?(Smr::AssetPosition)
            assert p.security.get_type.is_a?(SecurityStock)
            assert_equal :stock, p.security.type
        end

        ##
        #  investigate a debt position (using SecurityBond type model)
        test "work with debt position" do
            smr_login(LOGIN, PASSWORD)

            get position_path(:id=>35)
            assert_response :success
            assert_not_nil p = assigns(:position)
            assert p.is_a?(Smr::AssetPosition)
            assert p.security.get_type.is_a?(SecurityBond)
            assert_equal :bond, p.security.type
        end
    end

end # module
