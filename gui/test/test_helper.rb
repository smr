#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
ENV["RAILS_ENV"] = "test"
include ActionDispatch::TestProcess
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
    # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
    #
    # Note: You'll currently still have to declare fixtures explicitly in integration tests
    # -- they do not yet inherit this setting
    fixtures :all

    ##
    # seed the database, call in tests that rely on seed data
    def smr_seed
        assert Rails.application.load_seed, 'db:seed failed!'
    end

    # Add more helper methods to be used by all tests here...

    ##
    # log user into Smr
    #
    # Success should redirect us to '/' showing Assets overview as its the
    # default page.
    #
    # See ActionDispatch::IntegrationTest documentation in case multiple
    # parallel sessions are required at some point.
    def smr_login(loginname, password)
        post sessions_path, :login=>loginname, :password=>password
        assert_response :redirect
        follow_redirect!
        assert_response :success

        assert_equal root_path, path
        assert_not_nil assigns(:current_user), 'login failed, no current_user'
        assert_not_nil assigns(:assets), 'no Assets listed after login'
        assert_not_nil assigns(:open_positions), 'no Open Positions listed after login'
    
        # see we are logged in as the one we think we are and got a token
        current_user = assigns(:current_user)
        assert_equal current_user.login, loginname
        assert current_user.password_auth_token
    end

    ##
    # evaluates SMR_ONLINE_TEST environment variable.
    #
    # - true          => run tests that need internet connectivity
    # - false / unset => skip such tests
    def smr_online_test?
       ENV['SMR_ONLINE_TEST'] == 'true' ? true : false
    end
end
