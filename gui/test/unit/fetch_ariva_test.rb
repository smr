#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

class FetchTest < ActiveSupport::TestCase

    test "ariva reaper" do
        return true unless smr_online_test?

        to_test = [
            Security.create!(:symbol=>'DAX', :type=>:index),
            Security.create!(:symbol=>'Hang Seng', :type=>:index),
        ]

        to_test.each do |s|
            assert (s.create_type :index).is_a?(SecurityIndex)

            reaper = Smr::Reapers::Ariva.new s
            q = reaper.quote
            assert q.is_a?(Quote), 'no Quote found for %s' % s.symbol
            assert q.last.is_a? Float
            assert_not_equal 0, q.last, 'got 0 Quote for %s' % s.symbol
            assert q.exchange
        end
    end
end
