#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

class FetchTest < ActiveSupport::TestCase

    test "finanzen.net reaper" do
        return true unless smr_online_test?

        to_test = [
           Security.new(:symbol=>'CH0224397056', :description=>'Swiss 2014-64 CHF'),
           Security.new(:symbol=>'ES00000128E2', :description=>'Spain 2016-64 EUR'),
           Security.new(:symbol=>'DE000BAY0017', :description=>'Bayer AG'),
        ]

        to_test.each do |s|
            reaper = Smr::Reapers::Finanzennet.new s
            assert reaper.metadata, 'no metadata found for %s' % s.symbol
            assert_not s.brief.empty?, 'No descriptions set'

            typemodelclass = case s.symbol
                when 'DE000BAY0017' then SecurityStock
                else SecurityBond
            end
            assert s.get_type.is_a?(typemodelclass), 'typemodel faulty?'
        end
    end

end
