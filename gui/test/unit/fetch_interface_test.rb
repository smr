#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

class FetchTest < ActiveSupport::TestCase

    test "fetching interface" do
        return true unless smr_online_test?

        to_test = [
            (Security.find_by_symbol('A1ML7J') || Security.new(:symbol=>'A1ML7J', :description=>'Vonovia SE')),
            (Security.find_by_symbol('US084670BK32') || Security.new(:symbol=>'US084670BK32', :description=>'Berkshire 2043')),
            (Security.find_by_symbol('DE0008490723') || Security.new(:symbol=>'DE0008490723', :description=>'morgen Aktien Global UI')),
        ]

        to_test.each do |s|
            assert s.fetch_metadata, 'no metadata found for %s' % s.to_s_with_symbol
            assert s.has_type_model?, 'no type model created for %s' % s.to_s_with_symbol
            assert_equal 0, s.update_quote, 'no Quote found for %s' % s.to_s_with_symbol
        end
    end

    test "reapers must raise unknown security type" do

        module BogusSecurityType
            def type; return :bogus_type_not_known_by_any_reaper; end
        end
        s =  Security.new(:description=>'bla', :symbol=>'blubb')
        s.extend BogusSecurityType

        Smr::Reapers.constants.each do |reapersym|
          assert_raise RuntimeError, 'reaper "%s" does not raise at #new' % reapersym do
            ('Smr::Reapers::%s' % reapersym).constantize.new(s)
          end
        end
    end

end
