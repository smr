#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

class FetchTest < ActiveSupport::TestCase

    test "stuttgart reaper" do
        return true unless smr_online_test?

        to_test = [
            Security.new(:symbol=>'DE0001102341', :description=>'BRD 2046'),
            Security.new(:symbol=>'CH0346828400', :description=>'Gaz Capital 2021'),
            Security.new(:symbol=>'XS1294343337', :description=>'OMV 2025'),
        ]

        to_test.each do |s|
            reaper = Smr::Reapers::Stuttgart.new s
            q = reaper.quote
            assert q.is_a?(Quote), 'no Quote found for %s' % s.symbol
            assert q.last.is_a? Float
            assert_not_equal 0, q.last, 'a Quote should never be 0'
            assert q.exchange
        end
    end

end
