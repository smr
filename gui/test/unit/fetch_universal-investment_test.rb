#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

class FetchTest < ActiveSupport::TestCase

    test "universal_investment reaper" do
        return true unless smr_online_test?

        to_test_success = [
            Security.new(:symbol=>'DE0008490673', :description=>'Degussa Bank - Universal-Rentenfonds'),
            Security.new(:symbol=>'DE0008490723', :description=>'morgen Aktien Global UI'),
        ]
        to_test_failure = [
            Security.new(:symbol=>'LU0731782404', :description=>'Fidelity - Global Dividend Fund A'),      # not available via universal!
        ]


        to_test_success.each do |s|
            reaper = Smr::Reapers::UniversalInvestment.new s
            q = reaper.quote
            assert q.is_a?(Quote), 'no Quote found for %s' % s.symbol
            assert q.last.is_a? Float
            assert_not_equal 0, q.last, 'a Quote should never be 0'
            assert q.exchange
        end

        to_test_failure.each do |s|
            reaper = Smr::Reapers::UniversalInvestment.new s
            assert_not reaper.quote, 'lookup for %s should fail properly with False' % s.symbol
        end
    end

end
