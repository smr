#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'
require 'figures'

class FiguresTest < ActiveSupport::TestCase

    test "test autofigures" do
        var1 = FigureVar.create! :name=>'var1'
        var2 = FigureVar.create! :name=>'var2'
        security = Security.first

        af = Smr::Autofigure.new(
          FigureVar.create! :name=>'autofigure1', :expression=>'floor( (var1 + var2) / 4 )'
        )
        af.add FigureData.create!(:value=>3.22, :period=>:year, :id_figure_var=>var1.id, :date=>Time.now.to_i, :id_security=>security.id)
        af.add FigureData.create!(:value=>5.14, :period=>:year, :id_figure_var=>var2.id, :date=>Time.now.to_i, :id_security=>security.id)
        assert af.get_missing_variables.empty?
        assert_not af.get.empty?
        assert_equal 2, af.get.first.value, 'solved expression result incorrect'


        af = Smr::Autofigure.new(
          FigureVar.create! :name=>'autofigure2', :expression=>'ceil( var2 ' # parse error, missing ')'
        )
        af.add FigureData.create!(:value=>5.14, :period=>:q1, :id_figure_var=>var2.id, :date=>Time.now.to_i, :id_security=>security.id)
        assert af.get.empty?, 'expected empty result, ie. no half-broken autofigure'
        assert_not af.errors.empty?, 'expected error message from autofigure'


        # the db knows this variable, but there is no FigureData available
        some_missing_var = FigureVar.create! :name=>'some_missing_var'
        af = Smr::Autofigure.new(
          FigureVar.create! :name=>'autofigure3', :expression=>'var1 + some_missing_var'
        )
        af.add FigureData.create!(:value=>2, :period=>:q2, :id_figure_var=>var1.id, :date=>Time.now.to_i, :id_security=>security.id)
        assert af.get.empty?, 'expected empty result, ie. no half-broken autofigure'
        assert af.errors.empty?, 'expected no error message from autofigure with missing variable'

        # negative value in equation
        af = Smr::Autofigure.new(
          FigureVar.create! :name=>'autofigure4', :expression=>'var1 + var2'
        )
        af.add FigureData.create!(:value=>2, :period=>:year, :id_figure_var=>var1.id, :date=>Time.now.to_i, :id_security=>security.id)
        af.add FigureData.create!(:value=>-3, :period=>:year, :id_figure_var=>var2.id, :date=>Time.now.to_i, :id_security=>security.id)
        assert af.errors.empty?, af.errors
        assert_equal -1, af.get.first.value, 'expected negative expression result, its incorrect at least'
    end

    test "test free-cashflow equation" do
        var1 = FigureVar.create! :name=>'cashflow_operations'
        var2 = FigureVar.create! :name=>'cashflow_investing'
        var3 = FigureVar.create! :name=>'shares'
        security = Security.first

        af = Smr::Autofigure.new(
          FigureVar.create! :name=>'Free Cashflow', :expression=>'floor((cashflow_operations - cashflow_investing) / shares * 10)/10'
        )
        af.add FigureData.create!(:value=>600, :period=>:year, :id_figure_var=>var1.id, :date=>Time.now.to_i, :id_security=>security.id)
        af.add FigureData.create!(:value=>200, :period=>:year, :id_figure_var=>var2.id, :date=>Time.now.to_i, :id_security=>security.id)
        af.add FigureData.create!(:value=>10,  :period=>:year, :id_figure_var=>var3.id, :date=>Time.now.to_i, :id_security=>security.id)
        assert af.get_missing_variables.empty?

        result = af.get
        assert af.errors.empty?, 'solving produced errors: %s' % af.errors.join(' :: ')
        assert_not result.empty?
        assert_equal BigDecimal.new(40), af.get.first.value, 'solved expression result incorrect'
    end
end
