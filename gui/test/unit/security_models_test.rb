#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

class SecurityModelsTest < ActiveSupport::TestCase

    ##
    # The Security model offers a number of possible types. Each :type may have
    # a corresponding table with specific fields describing it very detailed
    # and a Active::Records model class on top.
    #
    # The model class may offer various methods that do useful things with the
    # additional data. A few such methods should always be implemented so that
    # Securities can be shown in a unified form.
    #
    # This test tries to instantiate the model class(es) and verifies the
    # existence of mandatory methods.
    #
    # These methods are expected to return False on otherwise empty Security /
    # Security<TYPE> instances.
    test "mandatory methods consistency" do
        smr_seed

        Security.types.each do |t|
            next if [ :unknown, :cash ].include?(t)

            assert s = Security.create(:brief=>'test type=%s'% t, :symbol=>'1234%s' % t)
            assert tm = s.create_type(t)
            if s.has_type_model? then
                Smr::Extensions::SecurityTypemodelMandatoryMethods::MANDATORY_TYPE_METHODS.each do |m|
                    assert st = s.get_type, 'obtaining typemodel instance failed for Security of type=%s' % s.type
                    assert st.methods.include?(m), 'method #%s not implemented for Security of type=%s' % [m, s.type]

                    retval = st.send(m)
                    if m == :cashflow
                        assert retval.is_a?(Array), '#cashflow should return Array. Got: %s for model %s.' % [retval.class, st.class]
                    elsif m == :cashflow_this_year
                        assert retval, '#cashflow_this_year returned nil on model %s' % st.class
                        assert retval.to_f >= 0, '#cashflow_this_year should never be negative for %s' % st.class
                    elsif m == :describe
                        assert retval.is_a?(String), 'expected #describe to return String for model %s' % st.class
                    else
                        assert (not retval or retval==0), 'expected nil, False or 0 for %s.%s. Got: %s' % [st.class, m, retval.inspect]
                    end
                end
            else
                assert false, 'Security created with type=%s does not offer typemodel' % t
            end
        end
    end

    ##
    # The #cashflow method of each type should implement some mandatory options.
    # FIXME: to be implemented - dont know how
#    test "mandatory cashflow method implementation consistency" do
#       Security.types.each do |t|
#           assert s = Security.create(:type=>t.total, :brief=>'test type=%s'% t.date, :symbol=>'1234')
# p '==> %s' % s.inspect
#       end
#    end

    ##
    # correctness of cashflow produced by SecurityBond model for open-end + fixed-rate bond
    test "bond cashflow with quarterly payments" do
        coupon = 0.04
        interval = 3  # every three months or four times a year
        bond = SecurityBond.new(
            :coupon=>coupon, :coupon_interval=>interval,
            :time_first_coupon=>Time.now - 1.year,
            :denomination=>100
        )

        bond.cashflow.each do |c|
            assert c.is_a? Smr::CashflowItem
            assert c.date.is_a? Time
            assert c.total.is_a? Float
            assert_equal coupon * (interval / 12.0), c.total, 'incorrect :coupon amount'
            assert_equal bond.time_first_coupon.day, c.date.day, 'day of payment not matching day of :time_first_coupon'
        end
    end

    ##
    # verify yield to maturity (which is an XIRR - date weighted internal rate of return)
    test "bond yield to maturity" do
        bond = SecurityBond.new(
            :coupon=>5.0, :coupon_interval=>12,
            :time_first_coupon=>Time.new(2001,05,01),
            :time_last_coupon=>Time.new(2005,05,01),
            :time_maturity=>Time.new(2005,05,01), :redemption_installments=>1
        )

        ytm = bond.yield_to_maturity(Quote.new(:time_last=>Time.new(2001,05,01), :last=>100))
        assert ytm.is_a?(Percentage), 'YTM calculation did not return a Percentage'
        assert_in_delta Percentage.new(0.06453685).to_f, ytm.to_f, 0.000001, 'YTM calculation incorrect?'
    end

    ##
    # verify yield to maturity for purchases above par value
    # - this tests a bug in Finance 2.0.0, see https://github.com/bcsgsvn/finance/pull/39
    # - until a fix is released, apply this patch
    #     https://github.com/eligoenergy/finance/commit/b8126ede94e5ff79ce431054a9d3533c6bb433ed
    #   manually to make this test pass
    test "bond yield to maturity with quote above par" do
        bond = SecurityBond.new(
            :coupon=>6.75, :coupon_interval=>12,
            :time_first_coupon=>Time.new(2012,04,26),
            :time_last_coupon=>Time.new(2018,04,26),
            :time_maturity=>Time.new(2018,04,26), :redemption_installments=>1
        )

        ytm = bond.yield_to_maturity(Quote.new(:time_last=>Time.new(2015,12,30), :last=>105.3))
        assert ytm.is_a?(Percentage), 'YTM calculation did not return a Percentage'
        assert ytm.to_f > 0.0, 'positive YTM expected'
        assert_in_delta Percentage.new(0.06379494).to_f, ytm.to_f, 0.000001, 'YTM calculation incorrect?'
    end

    ##
    # verify yield to maturity when lifetime remaining is below 1
    # year: it should be absolut, *not* annualized
    test "bond yield to maturity with redemption below 1 year away" do
        bond = SecurityBond.new(
            :coupon=>5.0, :coupon_interval=>12,
            :time_first_coupon=>Time.now - 1.year + 1.month - 1.day,
            :time_last_coupon=>Time.now + 1.month - 1.day,
            :time_maturity=>Time.now + 1.month,
            :redemption_installments=>1
        )

        ytm = bond.yield_to_maturity(Quote.new(:time_last=>Time.now, :last=>90))
        assert ytm.is_a?(Percentage), 'YTM calculation did not return a Percentage'
        assert_in_delta Percentage.new(0.1666).to_f, ytm.to_f, 0.02, 'YTM calculation incorrect?'
    end

    ##
    # Verify cashflow of open-end bond: no :date_maturity, no :date_last_coupon
    test "bond with undefined maturity" do
        bond = SecurityBond.new(
            :coupon=>3.0, :coupon_interval=>12,
            :time_first_coupon=>Time.new(2011,06,01),
        )

        assert_not bond.cashflow.empty?, 'open-ended bond can not have empty cashflow'
        assert bond.cashflow.first.is_a? Smr::CashflowItem

        cf = bond.cashflow(:end_date=>Time.new(2014,05,01))
        assert cf.last.date <= Time.new(2014,05,01), '#cashflow not respecting given :end_date'
    end

    ##
    # verify yield to (next) call
    test "bond yield to call" do
        bond = SecurityBond.new(
            :coupon=>5.0, :coupon_interval=>12,
            :time_issue=>Time.new(2000,01,01),
            :time_first_coupon=>Time.new(2001,01,01),
            :time_last_coupon=>Time.new(2015,01,01),
            :time_first_call=>Time.new(2010,01,01), :call_price=>105.0, :call_interval=>12,
            :time_maturity=>Time.new(2015,01,01), :redemption_price=>100.0, :redemption_installments=>1,
        )

        q = Quote.new(:time_last=>Time.new(2011,07,01), :last=>100)
        ytc = bond.yield_to_call q
        ytm = bond.yield_to_maturity q

        assert ytc.is_a?(Percentage), 'YTC calculation did not return a Percentage'
#     p '==> %s' %  bond.time_next_call(q.time)
#     p '==> %f , %f' %  [ ytc, ytm]
#        assert ytc > ytm, 'YTC should be greater than YTM'
        assert_in_delta Percentage.new(0.05000000).to_f, ytc.to_f, 0.000001, 'YTC calculation incorrect?'
    end

    ##
    # verify interest accrued since last coupon
    test "bond accrued interest" do
        bond = SecurityBond.new(
            :coupon=>5.0, :coupon_interval=>12,
            :time_issue=>Time.new(2000,01,01),
            :time_first_coupon=>Time.new(2001,01,01),
            :time_last_coupon=>Time.new(2015,01,01),
            :time_maturity=>Time.new(2015,01,01), :redemption_installments=>1,
        )

        i = bond.accrued_interest(Time.new(2002,07,01))

        assert i.is_a? Percentage
        assert_in_delta Percentage.new(0.024657534).to_f, i.to_f, 0.000001, 'accrued interest calculation incorrect?'
    end

    ##
    # correctness of cashflow produced by SecurityStock model
    test "stock dividend cashflow with annual payments" do
        dividend = 2.0
        stock = SecurityStock.new(
            :dividend=>dividend, :dividend_interval=>12,
            :time_dividend_exdate=>Time.now,
        )

        stock.cashflow.each do |c|
            assert c.is_a? Smr::CashflowItem
            assert c.date.is_a? Time
            assert c.total.is_a? Float
            assert_equal dividend, c.total, 'incorrect :dividend amount'
            assert_equal [stock.time_dividend_exdate.day, stock.time_dividend_exdate.month],
                         [c.date.day, c.date.month],
                         'day/month of payment not matching day/month of :dividend_exdate'
        end
    end

    test "stock dividend cashflow with quarterly payments" do
        dividend = 0.625
        stock = SecurityStock.new(
            :dividend=>dividend, :dividend_interval=>3,
            :time_dividend_exdate=>Time.now,
        )

        assert_equal 4 * 10, stock.cashflow.count, 'payment count does not match, expected 4 semi-annual payments for 10 years'
        assert_equal dividend, stock.cashflow.first.total, 'first payment does not match :dividend'
        assert_equal dividend, stock.cashflow.last.total, 'last payment does not match :dividend'

        sum_4quarters=0
        stock.cashflow[0..3].each{|i| sum_4quarters += i.total}
        assert_equal dividend * 4, sum_4quarters, 'cashflow of 4 quarters not matching 4 times :dividend'
    end

    ##
    # correctness of cashflow produced by the SecurityFund model
    test "fund cashflow from yearly distributions" do
        distribution = 3.0
        fund = SecurityFund.new(
            :distribution=>distribution, :distribution_interval=>12,
            :time_distribution=>Time.now,
        )

        # try annual payments
        fund.cashflow.each do |c|
            assert c.is_a? Smr::CashflowItem
            assert c.date.is_a? Time
            assert c.total.is_a? Float
            assert_equal distribution, c.total, 'incorrect :distribution amount'
            assert_equal [fund.time_distribution.day, fund.time_distribution.month],
                         [c.date.day, c.date.month],
                         'day/month of payment not matching day/month of :date_distribution'
        end
   end
end
