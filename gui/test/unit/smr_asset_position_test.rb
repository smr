#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

module Smr  #:nodoc:
    class AssetPositionTest < ActiveSupport::TestCase

        test "correct number of shares" do
            to_test = [
                # Cisco Systems Inc.
                {:id=>1, :at=>Time.new(2003,5,23), :shares=>15.0},
                {:id=>1, :at=>Time.new(2005,5,23), :shares=>-5.0},

                # IBM Inc.
                {:id=>10, :at=>Time.new(2003,5,23), :shares=>0.0},
                {:id=>10, :at=>Time.new(2005,5,23), :shares=>25.0},

                # McDonalds Inc.
                {:id=>20, :at=>Time.new(2010,5,23), :shares=>0.0},

                # DWS Vermoegensbildungsfonds A
                {:id=>6, :at=>Time.new(2004,5,23), :shares=> 15.0},
                {:id=>6, :at=>Time.new(2004,8,23), :shares=> 112.3456},

                # Altana AG
                {:id=>11, :at=>Time.new(2014,6,20), :shares=>70.0},
            ]

            to_test.each do |tp|
                p = AssetPosition.new(tp[:id], 1, tp[:at])
                assert p, 'no Position for id_position=%i, id_user=1?' % tp[:id]
                assert_in_delta tp[:shares], p.shares, 0.0001, 'wrong number of shares in position #%i' % tp[:id]
            end
        end

        ##
        # tests amount of money invested
        # - that covers less code than the gain test but helps debugging when the
        #   later fails
        test "correct invested amount" do
            to_test = [
                # Altana AG
                {:id=>11, :at=>Time.new(2014,6,20), :invested=>2458.8},
            ]

            to_test.each do |t|
                p = AssetPosition.new(t[:id], 1, t[:at])
                assert p, 'no Position for id_position=%i, id_user=1?' % t[:id]
                assert_in_delta t[:invested], p.invested, 0.0001, 'wrong invested amount of money in position #%i' % t[:id]
            end
        end

        ##
        # make sure positions are closed properly
        test "positions closed" do
            to_test = [
                # DaimlerChrysler AG
                {:id=>2, :at=>Time.new(2005,5,23)},

                # Allianz AG
                {:id=>7, :at=>Time.new(2005,5,23)}
            ]

            to_test.each do |tp|
                p = AssetPosition.new(tp[:id], 1, tp[:at])
                assert p, 'no Position for id_position=%i, id_user=1?' % tp[:id]
                if not p.is_closed? then
                    assert false, 'position #%i is not closed at %s' % [ tp[:id], tp[:at] ]
                end
            end
        end

        ##
        # tests gain and all the code necessary to calculate this correctly
        test "correct gain" do
            to_test = [
                # Altana (open)
                {:id=>18, :at=>Time.new(2005,10,12).end_of_day, :gain=>27.0, :id_user=>3},

                # Allianz AG (closed, viewed before closure)
                {:id=>7, :at=>Time.new(2005,5,23).end_of_day, :gain=>462.30},

                # Allianz AG (closed, viewed after closure, settled)
                {:id=>7, :at=>Time.new(2008,5,23).end_of_day, :gain=>3201.40},

                # No Quote (opened with new, order present but not executed, no shares held, no quote data)
                {:id=>25, :at=>Time.new(2014,11,15).end_of_day, :gain=>false},
            ]

            to_test.each do |tp|
                p = AssetPosition.new(tp[:id], tp[:id_user]||1, tp[:at])
                assert p, 'no Position for id_position=%i, id_user=1?' % tp[:id]

                if tp[:gain].is_a?(FalseClass)
                    assert p.gain.is_a?(FalseClass), 'gain should be false on position #%i at %s' % [ tp[:id], tp[:at] ]
                else
                    assert_in_delta tp[:gain], p.gain, 0.0001, 'position #%i has incorrect gain at %s' % [ tp[:id], tp[:at] ]
                end
            end
        end

        ##
        # tests sorting operator
        test "sorting positions" do
            # "IBM Inc." versus "CocaCola Inc."
            # - both have a profit, CocaCola is the larger investment thus its more risky
            a = AssetPosition.new(10, 1, Time.new(2014,11,15).end_of_day)
            b = AssetPosition.new(4, 1, Time.new(2014,11,15).end_of_day)
            assert_equal -1, a<=>b

            # "DWS Vermoegensbildungsfonds A" versus "Altana"
            # - dws has larger investment but Altana produces a loss
            a = AssetPosition.new(6, 1, Time.new(2014,11,15).end_of_day)
            b = AssetPosition.new(11, 1, Time.new(2014,11,15).end_of_day)
            assert_equal -1, a<=>b

            # "Cisco Systems Inc." versus "Cisco Systems Inc."
            # - the first one is a short position, the second is long
            # - the long position is smaller given its absolute value in the fire
            a = AssetPosition.new(1, 1, Time.new(2014,11,15).end_of_day)
            b = AssetPosition.new(12, 1, Time.new(2014,11,15).end_of_day)
            assert_equal 1, a<=>b
        end

        ##
        # future cashflow consinst of cupons and redemptions, in proper order
        test "upcoming cashflow" do
            # Country A 5.0% 2000(00) - a Bond type security, held in position by user Demo1
            bond = AssetPosition.new(35, 2, Time.new(2015,6,6).end_of_day)
            cf = bond.cashflow
            sum = 0
            cf.each do |c|
                assert c.is_a? Smr::CashflowItem
                assert c.date.is_a? Time
                assert c.total.is_a? Float
                sum += c.total
            end
            assert_equal 86, cf.count, 'expecting 85 coupons + one redemption payment'
            assert_equal (85*250 + 5000), sum, 'wrong total of cashflow'
            assert_equal 5000, cf.last.total, 'redemption payment not found'
        end

        ##
        # verify amount of interest accrued on specific dates
        test "correct accrued interest" do
            # Country A 5.0% 2000(00) - a Bond type security, held in position by user Demo1
            bond = AssetPosition.new(35, 2, Time.new(2015,6,6).end_of_day)
            assert_equal -50, bond.accrued_interest, 'should have :on_orders accruals only'
            assert_in_delta(
                -25.3424,
                bond.accrued_interest(:on_holdings=>true),
                0.0001,
                'should contain :on_orders + :on_holdings accruals'
            )
            assert_in_delta(
                24.6575,
                bond.accrued_interest(:on_orders=>false, :on_holdings=>true),
                0.0001,
                'should have :on_holdings accruals only'
            )
        end
    end
end # module
