#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

module Smr  #:nodoc:

    class AssetTest < ActiveSupport::TestCase
        ##
        # test time machine by verifying investment sums at certain dates
        #
        # this is using the admin user with id_user=>1
        test 'invested' do
            to_test = [
                {:at=>Time.new(2003,5,23), :invested=>225.0},
                {:at=>Time.new(2004,5,23), :invested=>12245.85},
                {:at=>Time.new(2014,5,23), :invested=>10570.05},
                {:at=>Time.new(2014,6,20), :invested=>12068.85},
            ]

            to_test.each do |ta|
                a = Smr::Asset.new(1, ta[:at])
                assert_in_delta(ta[:invested], a.invested, 0.0001, 'wrong investment total on %s ' % ta[:at])
            end
        end

        ##
        # correctnes of #collateral_coverage_value using demo1 account
        test 'collateral coverage value' do
            a = Smr::Asset.new(2, Time.new(2015,6,10))   # user: demo1
            assert a.collateral_coverage_value.is_a?(Float)
            assert_equal 4000.0, a.collateral_coverage_value, 'wrong amount, see demo1 position #35 in Country A 5.0% 2000(00)'
        end
    end

end # module
