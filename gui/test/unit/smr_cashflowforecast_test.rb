#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

module Smr  #:nodoc:
    require 'cashflowforecast'

    class CashflowForecastTest < ActiveSupport::TestCase

        ##
        # looking at positions of user demo1, from :at date until end of year.
        test 'forecast at specific date' do
            to_test = [
                {:at=>Time.new(2003,5,24), :total=>0.0},
                {:at=>Time.new(2015,5,24), :total=>250.0},
                {:at=>Time.new(2015,12,1), :total=>150.0},
                {:at=>Time.new(2016,3,24), :total=>900.0},

                # 'Country A 5.0% 2011(2025-27) USD' matures from 2025 to 2027
                # - first we see an increase due to redemptions
                # - then we see smaller cupon payments due to reduced face value
                # - finaly we see a drop in the year after the last redemption
                #   as cupons from that paper ceased
                {:at=>Time.new(2025,3,24), :total=>2525.0000},
                {:at=>Time.new(2026,3,24), :total=>2441.6666},
                {:at=>Time.new(2027,3,24), :total=>2358.3333},
                {:at=>Time.new(2028,3,24), :total=>500.0},
            ]

            to_test.each do |t|
                # startdate of January 1st 2001 means we ask for cashflows from "All Time"
                cf = CashflowForecast.new(t[:at], t[:at].end_of_year, 2)

                if t[:total] == false then
                    assert_nil cf.each.first, 'cashflow that should not be here %s' % t[:at]
                else
                    assert_in_delta t[:total], cf.total, 0.0001, 'wrong total forecasted for %s ' % t[:at]
                end
            end
        end

#        test 'forecast filtered at specific date' do
#            to_test = [
#                {:at=>Time.new(2003,5,23), :total=>false, :filter=>:dividend},
#                {:at=>Time.new(2004,9,23), :total=>-15634.046, :filter=>:orders},
#                {:at=>Time.new(2007,5,23), :total=>880.55, :filter=>:dividend},
#                {:at=>Time.new(2008,1,1), :total=>-24.47, :filter=>:provision},
#                {:at=>Time.new(2015,4,1), :total=>-44.32, :filter=>:provision},
#                {:at=>Time.new(2015,4,1), :total=>-35.57, :filter=>:courtage},
#                {:at=>Time.new(2015,4,1), :total=>-95.93, :filter=>:expense},
#            ]
#
#            to_test.each do |t|
#                cf = CashflowLog.new(Time.new(2001,1,1), t[:at], 1)
#                cf.set_filter(t[:filter])
#
#                if t[:total] == false then
#                    assert_nil cf.each.first, 'cashflow that should not be here on %s' % t[:at]
#                else
#                    assert_in_delta t[:total], cf.total, 0.0001, 'wrong cashflow total on %s ' % t[:at]
#                end
#            end
#        end
#
#        test 'unknown filter' do
#            cf = CashflowLog.new(Time.new(2001,1,1), Time.now, 1)
#            assert_raise RuntimeError do
#                cf.set_filter(:this_filter_does_not_exist_and_never_will)
#            end
#        end
    end

end # module
