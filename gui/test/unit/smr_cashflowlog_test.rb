#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

module Smr  #:nodoc:
    require 'cashflowlog'

    class CashflowLogTest < ActiveSupport::TestCase

        test 'cashflow at specific date' do
            to_test = [
                {:at=>Time.new(2001,6,7), :total=>false},
                {:at=>Time.new(2003,5,23), :total=>-225.0},
                {:at=>Time.new(2004,9,23), :total=>-15432.496},
                {:at=>Time.new(2008,1,1), :total=>-4405.2959},
                {:at=>Time.new(2015,3,1), :total=>-8927.4960}
            ]

            to_test.each do |t|
                # startdate of January 1st 2001 means we ask for cashflows from "All Time"
                cf = CashflowLog.new(Time.new(2001,1,1), t[:at], 1)

                if t[:total] == false then
                    assert_nil cf.each.first, 'cashflow that should not be here %s' % t[:at]
                else
                    assert_in_delta t[:total], cf.total, 0.0001, 'wrong cashflow total on %s ' % t[:at]
                end
            end
        end

        test 'cashflow filtered at specific date' do
            to_test = [
                {:at=>Time.new(2003,5,23), :total=>false, :filter=>:dividend_booking},
                {:at=>Time.new(2004,9,23), :total=>-15432.496, :filter=>:none},
                {:at=>Time.new(2007,5,23), :total=>880.55, :filter=>:dividend_booking},
                {:at=>Time.new(2008,1,1), :total=>-24.47, :filter=>:provision},
                {:at=>Time.new(2015,4,1), :total=>-44.32, :filter=>:provision},
                {:at=>Time.new(2015,4,1), :total=>-35.57, :filter=>:courtage},
                {:at=>Time.new(2015,4,1), :total=>-95.93, :filter=>:expense},
            ]

            to_test.each do |t|
                cf = CashflowLog.new(Time.new(2001,1,1), t[:at], 1)
                cf.set_filter(t[:filter])

                if t[:total] == false then
                    assert_nil cf.each.first, 'cashflow that should not be here on %s' % t[:at]
                else
                    assert_in_delta t[:total], cf.total, 0.0001, 'wrong cashflow total on %s ' % t[:at]
                end
            end
        end

        test 'unknown filter' do
            cf = CashflowLog.new(Time.new(2001,1,1), Time.now, 1)
            assert_raise RuntimeError do
                cf.set_filter(:this_filter_does_not_exist_and_never_will)
            end
        end
    end

end # module
