#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'
require 'cashflowstream'

module Smr  #:nodoc:

  class CashflowstreamTest < ActiveSupport::TestCase

    ##
    # correctness of cashflow produced for a typical bond
    test "bond cashflow with annual payments" do
        facevalue  = 100
        coupon = Percentage.new(5.0)
        cf = Smr::CashflowStream.new(
            coupon,
            Time.new(2000,05,01),
            :last_payment=>Time.new(2002,05,01),
            :last_redemption=>Time.new(2002,05,02), :redemptions=>1
        )

        # try annual payments
        num_coupons = cf.get(facevalue, :filter=>:dividend_booking).count
        assert_equal 3, num_coupons, 'need one coupon payment per year plus'
        cf.get(facevalue, :filter=>:dividend_booking).each do |c|
            assert_equal coupon * facevalue, c.total, 'CashflowItem total must match coupon'
        end
        assert_equal facevalue, cf.get(facevalue, :filter=>:redemption_booking).last.total, 'cashflow redemption payment be last and match bonds denomination'
    end

    ##
    # correctness of cashflow produced for a bond with semi-annual payments
    test "bond cashflow with semi-annual payments" do
        facevalue  = 100
        coupon = Percentage.new(5.0)
        cf = Smr::CashflowStream.new(
            coupon,
            Time.new(2000,05,01),
            :last_payment=>Time.new(2002,05,01), :payment_interval=>6,
            :last_redemption=>Time.new(2002,05,02), :redemptions=>1
        )

        num_coupons = cf.get(facevalue, :filter=>:dividend_booking).count
        assert_equal 5, num_coupons, 'need two coupon payments per year, except for the last year as :last_redemption is before november.'
        cf.get(facevalue, :filter=>:dividend_booking).each do |c|
            assert_equal coupon * facevalue / 2, c.total, 'CashflowItem total must match half of a coupon'
        end
        assert_equal facevalue, cf.get(facevalue, :filter=>:redemption_booking).last.total, 'cashflow redemption payment be last and match bonds denomination'
    end

    ##
    # correctness of cashflow produced for a bond with monthly payments
    test "bond cashflow with monthly payments" do
        facevalue  = 100
        coupon = Percentage.new(5.0)
        cf = Smr::CashflowStream.new(
            coupon,
            Time.new(2000,05,01),
            :last_payment=>Time.new(2002,05,01), :payment_interval=>1,
            :last_redemption=>Time.new(2002,05,02), :redemptions=>1
        )

        num_coupons = cf.get(facevalue, :filter=>:dividend_booking).count
        assert_equal 25, num_coupons, 'need twelve coupon payments per year, except for the first and last year as its may to may.'
        cf.get(facevalue, :filter=>:dividend_booking).each do |c|
            assert_equal coupon * facevalue / 12, c.total, 'CashflowItem total must match 1/12th of a coupon'
        end
        assert_equal facevalue, cf.get(facevalue, :filter=>:redemption_booking).last.total, 'cashflow redemption payment be last and match bonds denomination'
    end


    ##
    # Verify cashflow handles the poolfactor correctly
    test "bond with multiple redemption payments and coupons in between" do
        facevalue  = 100
        coupon = Percentage.new(0.05)
        stream = Smr::CashflowStream.new(
            coupon,
            Time.new(2011,11,02),
            :last_payment=>Time.new(2017,11,02), :payment_interval=>6,
            :last_redemption=>Time.new(2017,11,02), :redemptions=>3
        )
        cf = stream.get(facevalue)

        assert_equal :dividend_booking, cf.first.type
        assert_equal 2.5, cf.first.total   # cupons with poolfactor=1.0, half year 1
        assert_equal 2.5, cf.second.total  # cupons with poolfactor=1.0, half year 2
        assert_equal :redemption_booking, cf.last.type
        assert_equal 0.0, cf.last.poolfactor
        assert_in_delta 33.3333, cf.last.total, 0.0001, 'expected last redemption to be 1/3rd of facevalue'
    end

    test "bond with multiple redemptions with subannual coupons - held after first payment" do
        facevalue  = 100
        coupon = Percentage.new(0.05)
        stream = Smr::CashflowStream.new(
            coupon,
            Time.new(2011,11,02),
            :last_payment=>Time.new(2017,11,02), :payment_interval=>6,
            :last_redemption=>Time.new(2017,11,02), :redemptions=>3
        )
        cf = stream.get(facevalue, :start=>Time.new(2016,01,02))

        assert_equal :dividend_booking, cf.first.type
        assert_in_delta 0.6666, cf.first.poolfactor, 0.0001, 'the cupon following Feb 1st 2016 has reduced poolfactor'
        assert_in_delta 1.6666, cf.first.total, 0.0001,      'the cupon following Feb 1st 2016 has total reduced by the poolfactor'
        assert_equal :redemption_booking, cf.last.type
        assert_equal 0.0, cf.last.poolfactor
        assert_in_delta 33.3333, cf.last.total, 0.0001, 'expected last redemption to be 1/3rd of facevalue'
    end
  end
end # module
