#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

module Smr  #:nodoc:

    class DateTimeWrapperTest < ActiveSupport::TestCase

        # models needing time wrappers
        MODELS = [
            Comment, Bookmark, Dividend, Document, NewsFeed, NewsItem,
            NewsRead, Order, Portfolio, Position, PositionRevision, Quote,
            Quoterecord, QuoterecordRule, SecurityBond, SecurityDerivative,
            SecurityFund, Security, SecurityStock, Timetag, Workdesk
        ]

        ##
        # Inspect models for existence of our extension.
        test 'inspect some models' do
            MODELS.each { |m|
                assert m.ancestors.include?(Smr::Extensions::DateTimeWrapper), ('model %s is not extended with Smr::Extensions::DateTimeWrapper' % m)
            }
        end

        ##
        # See if certain models have working time methods.
        #
        # Note: its just sampled for now, the most important models and
        # methods. Many other tests will fail if a time wrapper does not work.
        test 'run some time methods' do

            to_test = {
                Dividend         => [ :time_exdate, :time_exdate= ],
                SecurityBond     => [ :time_first_coupon, :time_first_coupon=, :time_maturity, :time_maturity= ],
                Order            => [ :time_issued, :time_expire ],
                Portfolio        => [ :time_created ],
                Position         => [ :time_closed ],
                PositionRevision => [ :time_created ],
                Quote            => [ :time_last ],
                Security         => [ :time_trading_ceased ],
                SecurityStock    => [ :time_dividend_exdate ],
                Timetag          => [ :time_start, :time_end ],
            }

            to_test.keys.each do |model|
                to_test[model].each do |m|
                    assert model.new.methods.include?(m),  '%s has no %s' % [ model, m]
                    if m.match /\=$/ then
                        assert model.new.send(m, '2012-10-23'), '%s.%s fails to accept + convert + write String'  % [ model, m]
                    else
                        assert model.new.send(m).is_a?(Time), '%s.%s returns no Time'  % [ model, m]
                    end
                 end
            end
        end
    end

end # module
