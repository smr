#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

module Smr  #:nodoc:
    class DividendIncomeTest < ActiveSupport::TestCase

        # User.id for Dividend tests (see fixtures)
        ID_USER = 1

        test "dividend received on correct number of shares" do
            to_test = [
                # Allianz AG
                {:id=>7, :at=>Time.new(2004,2,7).end_of_day, :shares=>10, :received=>1.5, :total=>15},
                {:id=>7, :at=>Time.new(2004,2,10).end_of_day, :shares=>35, :received=>2, :total=>70},
                {:id=>7, :at=>Time.new(2004,2,11).end_of_day, :shares=>35, :received=>1.83, :total=>64.05},
                {:id=>7, :at=>Time.new(2004,2,12).end_of_day, :shares=>35, :received=>1.5, :total=>52.5},
            ]

            to_test.each do |td|
                d = Smr::DividendIncome.new( Smr::AssetPosition.new(td[:id], ID_USER, td[:at]) )
                p = d.payments.last

                assert p.is_a?(DividendIncomeItem), 'no dividend payments, should have some'
                assert_equal p.shares,         td[:shares],   'wrong number of shares in position #%i at %s' % [td[:id], td[:at]]
                assert_equal p.received,       td[:received], 'received payment incorrect for position #%i at %s' % [td[:id], td[:at]]
                assert_equal p.received_total, td[:total],    'total payment incorrect for position #%i at %s' % [td[:id], td[:at]]
            end
        end

        test "no dividend received" do
            to_test = [
                # Altana
                {:id=>11, :at=>Time.new(2005,5,4).end_of_day},
            ]

            to_test.each do |td|
                d = Smr::DividendIncome.new( Smr::AssetPosition.new(td[:id], ID_USER, td[:at]) )
                assert d.payments.empty?, 'found dividend payments which should not be here, position #%i at %s' % [td[:id], td[:at]]
            end
        end

        test "add payment to position" do
            srand(Time.now.to_i)

            paid = rand(1000)/100.0       # what the company paid, per share
            received = paid * 0.74        # after tax and/or other cost, per share
            exdate = Time.now - 2.days
            valutadate = exdate + 1.days

            ap = Smr::AssetPosition.new(4, ID_USER, valutadate)  # fixtures: Coca Cola
            apt = Smr::Transaction.new(ap, User.find(ID_USER))

            cp = Smr::Transaction.new(apt.cash_position, User.find(ID_USER))
            assert cp.book(received*ap.shares, :time=>valutadate), 'booking received amount failed: %s' % cp.error

            d = Smr::DividendIncome.new(ap)
            assert d.add_payment(paid, cp.order.id, exdate), 'add payment failed: %s' % d.error

            payment = Smr::DividendIncome.new(ap).payments.last

            assert_equal exdate.to_date, payment.exdate.to_date
            assert_equal valutadate.to_date, payment.valutadate.to_date
            assert_equal paid, payment.paid
            assert_in_delta received, payment.received, 0.00001
        end
    end
end # module
