#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

module Smr  #:nodoc:

    class LinkTest < ActiveSupport::TestCase

        ##
        # make and use various types of link
        test 'basic usage' do

            # external link
            url = 'http://www.example.com'
            l = Smr::Link.new(:url, url)
            assert_equal url, l.to_url

            assert_raises RuntimeError do
                Smr::Link.new(:some_unknown_type, 123)
            end

            # internal links to various things
            assert_equal '/position/123', Smr::Link.new(:position, 123).to_url
        end
    end

end # module
