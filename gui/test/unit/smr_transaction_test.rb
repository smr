#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

module Smr  #:nodoc:

    class TransactionTest < ActiveSupport::TestCase
        # User.id for transaction tests
        ID_USER = 1

        # Security.id to buy/sell during test
        ID_SECURITY = 7     # 'Siemens AG' - has no position in fixtures data

        ##
        # make a new and empty AssetPosition
        def make_position options={:cash_position=>false}
            smr_seed
            pf = Portfolio.create(
                :id_user=>ID_USER, :name=>'TransactionTest', :type=>'', :date_created=>Time.now.to_i
            )
            p = Position.new(
                :id_security=>(options[:cash_position] ? Smr::ID_CASH_SECURITY : ID_SECURITY),
                :id_portfolio=>pf.id,
                :comment=>(options[:cash_position] ? 'make cash booking' : 'transaction test')
            )
            assert p.save, 'could not create new position'
            p
        end

        ##
        # see if the database is populated with the things needed here
        test 'database setup properly' do
            smr_seed
            assert Security.where(:id=>Smr::ID_CASH_SECURITY).first, 'Smr::ID_CASH_SECURITY not present in database'
            assert Security.where(:id=>ID_SECURITY).first, 'ID_SECURITY not present in database'
            assert User.where(:id=>ID_USER).first, 'ID_USER not present in database'
        end

        ##
        # purchase security and sell it again
        test 'purchase and sell' do
            p = make_position

            # BUY security
            t1 = Smr::Transaction.new(p, User.find(ID_USER))
            assert t1.is_a?(Smr::Transaction)
            assert t1.buy(100, 11.0, 'TST')
            assert t1.set_charges(1, 2, 3)
            assert t1.set_accrued_interest(100)
            assert t1.set_comment('transaction test: purchase')
            assert_equal 1, t1.execute(10.50)
            assert_equal 100, t1.order.PositionRevision.first.shares

            # SELL again
            t2 = Smr::Transaction.new(p, User.find(ID_USER))
            assert t2.sell(80, 15.0, 'TST')
            assert t2.set_charges(3, 2, 1)
            assert t2.set_accrued_interest(150)
            assert t2.set_comment('transaction test: sell')
            assert_equal 1, t2.execute(15.00, Time.now + 2.days)
            assert_equal 20, t2.order.PositionRevision.first.shares

            # investigate position
            ap = Smr::AssetPosition.new(p.id, ID_USER, Time.now + 3.days)
            assert_equal 20, ap.shares, 'Number of shares in AssetPosition wrong'
            assert_not ap.is_closed?

            # investigate the bookings
            # - must have two orders on one cash position
            # - the purchase security order created a sale of cash
            # - the sell security order created a purchase of cash
            assert_not_equal t1.order, t2.order
            assert_equal t1.cash_position, t2.cash_position
            rev1 = ap.revisions.first
            rev2 = ap.revisions.second
            booking1 = Order.where(
                :type=>'sale', :triggered_by_order=>rev1.id_order,
                :shares=>(t1.order.volume + t1.order.charges + t1.order.accrued_interest)
            ).first
            booking2 = Order.where(
                :type=>'buy', :triggered_by_order=>rev2.id_order,
                :shares=>(t2.order.volume - t2.order.charges + t2.order.accrued_interest)
            ).first
            assert booking1.is_a?(Order), 'booking for t1 failed, got: %s' % booking1.inspect
            assert booking2.is_a?(Order), 'booking for t2 failed, got: %s' % booking2.inspect

            # Cash Position should be shown in AssetPosition list
            assets = Smr::Asset.new(ID_USER, Time.now, :id_portfolio=>p.id_portfolio)
            cp = assets.open_positions.find{|p| p.id_security == ID_CASH_SECURITY }
            assert cp.is_a?(Smr::AssetPosition), 'cash position not found among Smr::Asset open positions'
        end

        ##
        # purchase security and sell it again using a Smr::AssetPosition
        test 'purchase and sell on AssetPosition' do
            p = make_position
            ap = Smr::AssetPosition.new(p.id, ID_USER, Time.now - 3.days)
            t1 = Smr::Transaction.new(ap, User.find(ID_USER))
            assert t1.is_a?(Smr::Transaction)
            assert t1.buy(200, 20.0, 'TST')
            assert t1.set_charges(1, 2, 3)
            assert t1.set_comment('transaction test: purchase')
            assert_equal 1, t1.execute(20, Time.now - 2.days)
            assert_equal 200, t1.order.PositionRevision.first.shares

            # sell again
            t2 = Smr::Transaction.new(ap, User.find(ID_USER))
            assert t2.sell(100, 25.0, 'TST')
            assert t2.set_charges(3, 2, 1)
            assert t2.set_comment('transaction test: sell')
            assert_equal 1, t2.execute(25.00, Time.now - 1.days)
            assert_equal 100, t2.order.PositionRevision.first.shares

            # investigate position
            ap_after_transactions = Smr::AssetPosition.new(p.id, ID_USER, Time.now)
            assert_equal 100, ap_after_transactions.shares, 'Number of shares in AssetPosition wrong'
            assert_not ap_after_transactions.is_closed?

            # investigate the bookings (see test above)
            assert_not_equal t1.order, t2.order
            assert_equal t1.cash_position, t2.cash_position
            rev1 = ap_after_transactions.revisions.first
            rev2 = ap_after_transactions.revisions.second
            booking1 = Order.where(:type=>'sale', :triggered_by_order=>rev1.id_order, :shares=>(t1.order.volume+t1.order.charges)).first
            booking2 = Order.where(:type=>'buy', :triggered_by_order=>rev2.id_order, :shares=>(t2.order.volume-t2.order.charges)).first
            assert booking1.is_a?(Order), 'booking for t1 failed, got: %s' % booking1.inspect
            assert booking2.is_a?(Order), 'booking for t2 failed, got: %s' % booking2.inspect
        end

        ##
        # cancel a transaction
        test 'cancel transaction' do
            p = make_position
            t1 = Smr::Transaction.new(p, User.find(ID_USER))
            assert t1.buy(99.99, 2.0, 'TST')
            assert t1.set_comment('transaction test: cancel transaction')
            t1.cancel
            assert t1.order.is_canceled

            # doing things on a canceled Transaction should not work
            assert_not t1.set_charges(1, 2, 3)
            assert_not t1.set_comment('transaction test: change after cancel()')
            assert_not t1.buy(2, 1, 'TST')
            assert_not t1.sell(2, 1, 'TST')
            assert_not t1.execute(3)
            assert t1.error.is_a?(String)
        end

        ##
        # If a cash position is closed, the corresponding period is settled. No
        # further transactions should be possible then.
        test 'transaction in settled period' do
            p = make_position
            t = Smr::Transaction.new(p, User.find(ID_USER))

            id_cp = t.cash_position.id
            cp = Smr::AssetPosition.new(id_cp, ID_USER)

            # book a small amount in case we got a new empty (!) cashposition
            assert_equal 2, Smr::Transaction.new(cp, User.find(ID_USER)).book(1, :comment=>'small cash booking'), 'wrong return code for cash booking'

            # re-read the cash position and settle
            cp = Smr::AssetPosition.new(id_cp, ID_USER)
            cp.settle

            # make another transaction that should get the same cashposition, but re-read after #settle
            t = Smr::Transaction.new(p, User.find(ID_USER))
            assert_equal id_cp, t.cash_position.id, 'second transaction got another cashposition'
            cp = Smr::AssetPosition.new(t.cash_position.id, ID_USER)

            assert cp.is_closed?
            retval = t.buy(2, 1, 'TST')
            assert_not retval, '#buy returned %s object, expect FalseClass' % retval.class
            assert t.error.is_a?(String)
            assert t.error.include?('settled'), t.error
        end

        ##
        # Issues a transaction in current year that happend in the year before.
        # Both, the transaction /and/ the cash booking must happen in the
        # previous year.
        test 'transaction in previous year' do
            current_year = Time.now
            prev_year = current_year - 1.year
            ap = Smr::AssetPosition.new(make_position.id, ID_USER, current_year)

            # purchase shares in past year while the assetposition is at current year
            t = Smr::Transaction.new(ap, User.find(ID_USER), :issue_time=>prev_year )
            assert t.buy 80, 12.5, 'some exchange'
            assert_equal  1, t.execute(12.5, prev_year), '#execute should return 1 to indicate successful booking'

            # where is the cash booking?
            cp_current_year = Smr::Transaction.find_cashposition ap.portfolio.id, ID_USER, current_year
            assert cp_current_year.PositionRevision.all.empty?, 'current year not supposed to have cash bookings'

            cp_prev_year = Smr::Transaction.find_cashposition ap.portfolio.id, ID_USER, prev_year
            assert_not cp_prev_year.PositionRevision.all.empty?, 'past year must have cash bookings'
            found_booking = false
            cp_prev_year.PositionRevision.all.each do |b|
                found_booking=true if b.shares == (80*12.5*-1) and b.date_created == prev_year.to_i
            end
            assert found_booking, 'cash booking not found in previous year'
        end

        ##
        # Cash bookings are done on a cash position only. There is no security
        # Order involved here.
        test 'make a cash booking' do
            smr_seed
            cp = make_position :cash_position=>true

            t1_exectime = Time.now - 3.days
            t1 = Smr::Transaction.new(cp, User.find(ID_USER), :issue_time=>t1_exectime - 1.days)
            t1.set_comment('booking positive cashflow')
            assert t1.book(100, :time=>t1_exectime), t1.error

            t2_exectime = Time.now - 1.days
            t2 = Smr::Transaction.new(cp, User.find(ID_USER), :issue_time=>t2_exectime - 1.days)
            t2.set_comment('booking negative cashflow')
            assert t2.book(-50, :time=>t2_exectime), t2.error

            # investigate cash position and bookings
            assert_equal t1.cash_position, t1.cash_position
            assert_equal t1_exectime.to_i, cp.PositionRevision.order(:date_created).first.date_created
            assert_equal t2_exectime.to_i, cp.PositionRevision.order(:date_created).second.date_created
            assert_equal 50, cp.PositionRevision.order(:date_created).second.shares, 'booking calculation wrong'
        end

        ##
        # the booking must happen in the previous year, *not* in the current one
        test 'make a cash booking after year changed' do
            smr_seed
            p = make_position

            cp_past_year =Smr::Transaction.find_cashposition(p.id_portfolio, User.find(ID_USER).id, Time.now - 1.year)
            initial_balance_past_year = (cp_past_year.PositionRevision.count > 0 ? cp_past_year.PositionRevision.order(:date_created).last.shares : 0)

            t1_exectime = Time.now - 1.year + 1.day
            t1 = Smr::Transaction.new(cp_past_year, User.find(ID_USER), :issue_time=>t1_exectime)
            t1.set_comment('booking positive cashflow in past year')
            assert t1.book(100, :time=>t1_exectime), t1.error

            cp_current_year =Smr::Transaction.find_cashposition(p.id_portfolio, User.find(ID_USER).id, Time.now)
            initial_balance_current_year = (cp_current_year.PositionRevision.count > 0 ? cp_current_year.PositionRevision.order(:date_created).last.shares : 0)

            t2_exectime = Time.now + 1.second
            t2 = Smr::Transaction.new(cp_current_year, User.find(ID_USER), :issue_time=>t2_exectime)
            t2.set_comment('booking negative cashflow in current year')
            assert t2.book(-50, :time=>t2_exectime), t2.error

            # investigate cash position and bookings
            assert_equal(
                100,
                (t1.cash_position.PositionRevision.order(:date_created).last.shares - initial_balance_past_year),
                'wrong balance in past years cash_position'
            )
            assert_equal(
                -50,
                (t2.cash_position.PositionRevision.order(:date_created).last.shares - initial_balance_current_year),
                'wrong balance in current cash_position'
            )
            assert_not_equal t1.cash_position, t2.cash_position, 'transaction2 should have created a new cashposition'
        end

        ##
        # Do excessive cash bookings and check balance
        test 'cash position balance correct' do
            smr_seed
            balance = 0
            srand(Time.now.to_i)
            cp = make_position :cash_position=>true

            for i in 1...50 do
                offset = rand(100)
                amount = rand(10000)
                amount *= -1 if offset.odd?
                exectime = Time.now - offset.days

                t = Smr::Transaction.new(cp, User.find(ID_USER), :issue_time=>exectime - 1.days)
                assert t.book(amount, :time=>exectime), t.error
                balance += amount
            end

            # investigate AssetPosition showing the cash position properly
            ap = Smr::AssetPosition.new(cp.id, ID_USER)
            assert_equal balance, ap.invested, 'wrong balance on AssetPosition'
        end

        ##
        # Bookings in past years must create/use cash position of correct (past) year.
        test "make transaction with booking in past year" do
            p = make_position

            t1 = Smr::Transaction.new(p, User.find(ID_USER), :issue_time=>Time.now)
            t2 = Smr::Transaction.new(p, User.find(ID_USER), :issue_time=>Time.now - 1.year)
            assert_not_equal t1.cash_position.id, t2.cash_position.id, 't1 got same cashposition as t2, should have differend ones'

            # execute both transactions, then inspect what happened in the cashpositions
            # - its crucial to first execute both and second look for errors in both
            cpt1_oldbalance = Smr::AssetPosition.new(t1.cash_position.id, ID_USER).shares
            t1.buy 10, 5
            t1.execute 5, Time.now
            cpt1 = Smr::AssetPosition.new(t1.cash_position.id, ID_USER)

            cpt2_oldbalance = Smr::AssetPosition.new(t2.cash_position.id, ID_USER).shares
            t2.buy 20, 5
            t2.execute 5, Time.now-1.year
            cpt2 = Smr::AssetPosition.new(t2.cash_position.id, ID_USER)

            assert t1.error.empty?, t1.to_s
            assert t2.error.empty?, t2.to_s
            assert_equal cpt1_oldbalance - 50, cpt1.shares, 'unexpected cashposition balance in current year'
            assert_equal cpt2_oldbalance - 100, cpt2.shares, 'unexpected cashposition balance in past year'
        end

        ##
        # All charges must be deducted from the cash bookings.
        test 'deduct charges from PURCHASE cash booking' do
            p = make_position
            t = Smr::Transaction.new(p, User.find(ID_USER))
            initial_balance = (t.cash_position.PositionRevision.count > 0 ? t.cash_position.PositionRevision.order(:date_created).last.shares : 0)
            t.set_charges(1, 2, 3)
            t.buy(100, 5)
            assert t.execute(5), t.error
            assert_equal initial_balance-506, t.cash_position.PositionRevision.order(:date_created).last.shares
        end

        ##
        # All charges must be deducted from the cash bookings.
        test 'deduct charges from SALE cash booking' do
            p = make_position
            t = Smr::Transaction.new(p, User.find(ID_USER))
            initial_balance = (t.cash_position.PositionRevision.count > 0 ? t.cash_position.PositionRevision.order(:date_created).last.shares : 0)
            t.set_charges(10, 20, 30)
            t.sell(50, 10)
            assert t.execute(10), t.error
            assert_equal initial_balance+440, t.cash_position.PositionRevision.order(:date_created).last.shares
        end

        ##
        # Transactions can operate only with Order in pending state
        test 'transaction using canceled or executed order' do
            p = make_position
            o = Order.new(
                :date_issued=>Time.now.to_i,
                :date_expire=>(Time.now + 1.month).to_i,
                :id_position=>p.id,
                :expense=>0, :courtage=>0, :provision=>0,
                :shares=>20, :limit=>1, :is_canceled=>true
            )
            o.save!
            t = Smr::Transaction.new(p, User.find(ID_USER), :order=>o)
            assert_not t.buy(100, 1), "purchase accepted on canceled order"
            assert t.error.include?('canceled')

            o.is_canceled = false
            o.quote = 1
            o.save!
            t = Smr::Transaction.new(p, User.find(ID_USER), :order=>o)
            assert_not t.buy(100, 1), "purchase accepted on executed order"
            assert t.error.include?('executed')
        end

        ##
        # test disabled for now, rounding issues exist on multiple levels
        # - involved columns are FLOAT still, thus #settle /must/ run into a remaining blance
        # - http://stackoverflow.com/questions/23120584/why-does-mysql-round-floats-way-more-than-expected
        #
#        test 'rounding-off difference while booking cash' do
#
#            # book amounts that can be roundet at every digit, each into a new year/cash position
#            [ 5.6789, 78.6789, 678.6789, 5678.6789, 456789.6789 ].each do |amount|
#                cp = make_position :cash_position=>true
#                ct =   Smr::Transaction.new(cp, User.find(ID_USER))
#                assert_equal(
#                   2,
#                   ct.book(amount, :comment=>'uneven booking'),
#                   'cash booking failed with: %s' % ct.error
#                )
#                assert_equal(
#                   ct.order.shares,
#                   ct.order.PositionRevision.first.shares,
#                   'spottet rounding-off difference between Order and PositionRevision'
#                )
#            end
#
#            # re-read the cash position: settle should not raise after no rounding-offs were spottet
#            cp = Smr::AssetPosition.new(id_cp, ID_USER)
#p '=settle=> %s' % cp.shares.inspect
#            assert_nil cp.settle
#
#            # re-read at early next year, should be closed with (almost) no balance
#            cp = Smr::AssetPosition.new(id_cp, ID_USER, Time.now.end_of_year + 1.day)
#            assert cp.is_closed?, 'cash position not closed, perhaps #settle failed'
#            assert cp.shares < 0.01, 'some unusual balance remains, perhaps #settle failed'
#p '==> %s' % cp.shares.inspect
#        end

   end

end # module
