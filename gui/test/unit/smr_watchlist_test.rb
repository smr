#
# This file is part of SMR.
#
# SMR is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# SMR is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# SMR.  If not, see <http://www.gnu.org/licenses/>.
#
require 'test_helper'

module Smr  #:nodoc:

    class WatchlistTest < ActiveSupport::TestCase
        ##
        # verify that all securites being held in a position are *not* shown in the watchlist
        test 'only securities not held in positions' do
            date = Time.now
            a = Smr::Asset.new(User.find(1).id, date)
            w = Smr::Watchlist.new(
                    User.find(1), date,
                    :exclude_securities=>a.open_positions.collect{ |p| p.security.id }.uniq
            )
            assert w.is_a?(Smr::Watchlist)
            assert a.is_a?(Smr::Asset)

            w.each do |i|
                found_id=false
                a.open_positions.each{|ap|
                   assert (ap.id_security != i.id), 'watchlist id_security=%i found in Smr::Asset.open_positions' % i.id
                }
            end
        end

        ##
        # when the Security* type model reports #is_expired? == true, the
        # Security should not be shown in the list.
        test 'expired securities not shown' do
            id_matured_bond = 110   # Country A 7.5% 1990(10)
            id_expired_stock = 111  # Company A Common Stock - Old Shares

            date = Time.new(2015,5,30)
            w = Smr::Watchlist.new(User.find(2), date)

            w.each do |wi|
                assert_not_equal id_matured_bond, wi.id, 'found matured bond in watchlist'
                assert_not_equal id_expired_stock, wi.id, 'found expired stock in watchlist'
            end
        end
    end

end # module
